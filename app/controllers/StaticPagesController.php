<?php
class StaticPagesController extends BaseController {

	protected $layout = 'frontend.new.layout';
	
	public function getHomepage()
	{
		$this->layout->content = View::make('frontend.new.homepage');
	}
	public function getForClients()
	{
		$flash = Session::get('forClientsSuccess');
		
		$this->layout->content = View::make('frontend.new.for_clients', array('flashSuccess' => $flash));
	}
	public function postForClients()
	{
		$rules = array(
			'client_name' => array('required'),
			'email_address' => array('required', 'email'),
			'contact_number' => array('required'),
			'comments' => array('required')
		);

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(url('for-clients'))
				->withErrors($validator)
				->withInput(Input::all());
		}
		
		$client_name = Input::get('client_name');
		$email_address = Input::get('email_address');
		$contact_number = Input::get('contact_number');
		$comments = Input::get('comments');
		
		Mail::queue('emails.forms.send_for_client', array('client_name' => $client_name, 'email_address' => $email_address, 'contact_number' => $contact_number, 'comments' => $comments), function($message) use($email_address, $client_name)
		{
			$message
				->to('bookings@lingoing.com', 'Lingoing.com')
				->subject('Message from webpage: For Clients form');
		});
		
		Session::flash('forClientsSuccess', 'Message successfully sent to Lingoing. Thank you.');
		return Redirect::to(url('for-clients'));
	}
	public function getForInterpreters()
	{
		$bodies = array(
			'AIIC' => 'AIIC',
			'CiOL' => 'CiOL',
			'ITI' => 'ITI',
			'NRPSI' => 'NRPSI',
			'ASLI' => 'ASLI',
			'NRCPD' => 'NRCPD',
			'VLP' => 'VLP'
		);
		
		$flash = Session::get('forInterpretersSuccess');
		
		$this->layout->content = View::make('frontend.new.for_interpreters', array('bodies' => $bodies, 'flashSuccess' => $flash));
	}
	public function postForInterpreters()
	{
		$bodies = array(
			'AIIC' => 'AIIC',
			'CiOL' => 'CiOL',
			'ITI' => 'ITI',
			'NRPSI' => 'NRPSI',
			'ASLI' => 'ASLI',
			'NRCPD' => 'NRCPD',
			'VLP' => 'VLP'
		);
		
		$rules = array(
			'interpreter_name' => array('required'),
			'email_address' => array('required', 'email'),
			'contact_number' => array('required'),
			'location_region' => array('required'),
			'language_skills' => array('required'),
			'hourly_rate' => array('required'),
			'expert_level' => array('required')
		);
		
		foreach($bodies as $body)
			$rules['expiration_date_' . $body] = array('date_format:"d/m/Y"', 'date_not_in_past');

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(url('for-interpreters'))
				->withErrors($validator)
				->withInput(Input::all());
		}
		
		$memberships = array();
		foreach($bodies as $body)
		{
			$memberships[$body] = array(
				'reference_number' => Input::get('reference_number_' . $body),
				'expiration_date' => Input::get('expiration_date_' . $body),
				'languages' => Input::get('membership_languages_' . $body)
			);
		}
		
		$interpreter_name = Input::get('interpreter_name');
		$email_address = Input::get('email_address');
		$contact_number = Input::get('contact_number');
		$location_region = Input::get('location_region');
		$language_skills = Input::get('language_skills');
		$hourly_rate = Input::get('hourly_rate');
		$expert_level = Input::get('expert_level');
		$specialisms = Input::get('specialisms');
		$comments = Input::get('comments');
		
		Mail::queue('emails.forms.send_for_interpreters', array('interpreter_name' => $interpreter_name, 'email_address' => $email_address, 'contact_number' => $contact_number, 'location_region' => $location_region, 'language_skills' => $language_skills, 'hourly_rate' => $hourly_rate, 'expert_level' => $expert_level, 'specialisms' => $specialisms, 'comments' => $comments, 'memberships' => $memberships), function($message) use($email_address, $interpreter_name)
		{
			$message
				->to('bookings@lingoing.com', 'Lingoing.com')
				->subject('Message from webpage: For Interpreters form');
		});
		
		Session::flash('forInterpretersSuccess', 'Message successfully sent to Lingoing. Thank you.');
		return Redirect::to(url('for-interpreters'));
	}
	public function getAboutUs()
	{
		$this->layout->content = View::make('frontend.static.about_us');
	}
	public function getFaq()
	{
		$this->layout->content = View::make('frontend.static.faq');
	}
	public function postSendFaqQuestion()
	{
		$rules = array(
			'email_address' => array('required', 'email'),
			'your_question' => array('required')
		);

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			$response = array(
				'status' => 'VALIDATION',
				'errors' => array()
			);
			
			foreach($validator->errors()->getMessages() as $field => $error)
			{
				$response['errors'][] = array(
					'field' => $field,
					'error' => $error
				);
			}
			
			return Response::json($response);
		}
		else
		{
			$email_address = Input::get('email_address');
			$question = Input::get('your_question');
			
			Mail::queue('emails.frontend.send_faq_message', array('email' => $email_address, 'text' => $question), function($message)
			{
				$message
					->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
					->to('info@lingoing.com', 'Lingoing.com')
					->subject('Somebody send you a message via FAQ form');
			});
			
			return Response::json(array('status' => 'OK'));
		}
	}
	public function getFeedback()
	{
		$success = Session::get('feedback_success', null);
		$this->layout->content = View::make('frontend.static.feedback', array(
			'success' => $success
		));
	}
	public function postFeedback()
	{
		$rules = array(
			'name' => array('required'),
			'feedback' => array('required'),
			'email_address' => array('required', 'email')
		);
		
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			$response = array(
				'status' => 'VALIDATION',
				'errors' => array()
			);
			
			foreach($validator->errors()->getMessages() as $field => $error)
			{
				$response['errors'][] = array(
					'field' => $field,
					'error' => $error
				);
			}
			
			return Response::json($response);
		}
		else
		{
			$name = Input::get('name');
			$email = Input::get('email_address');
			$text = Input::get('feedback');
			
			Mail::queue('emails.frontend.send_feedback', array('name' => $name, 'email' => $email, 'text' => $text), function($message)
			{
				$message
					->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
					->to('info@lingoing.com', 'Lingoing.com')
					->subject('Somebody send you a message via "Feedback" form');
			});
			
			return Response::json(array('status' => 'OK'));
		}
	}
	public function getContactUs()
	{
		$this->layout->content = View::make('frontend.static.contact_us');
	}
	public function postSendContactUsMessage()
	{
		$rules = array(
			'name' => array('required'),
			'email_address' => array('required', 'email'),
			'message' => array('required')
		);

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			$response = array(
				'status' => 'VALIDATION',
				'errors' => array()
			);
			
			foreach($validator->errors()->getMessages() as $field => $error)
			{
				$response['errors'][] = array(
					'field' => $field,
					'error' => $error
				);
			}
			
			return Response::json($response);
		}
		else
		{
			$name = Input::get('name');
			$email = Input::get('email_address');
			$text = Input::get('message');
			
			Mail::queue('emails.frontend.send_contact_us_message', array('name' => $name, 'email' => $email, 'text' => $text), function($message)
			{
				$message
					->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
					->to('info@lingoing.com', 'Lingoing.com')
					->subject('Somebody send you a message via "Contact us" form');
			});
			
			return Response::json(array('status' => 'OK'));
		}
	}
	public function getCookies()
	{
		$this->layout->content = View::make('frontend.static.cookies'); 
	}
}