<?php

use \Carbon\Carbon;
use \Illuminate\Support\Collection;

class CronController extends BaseController {
	
	private function expireAtwPackages()
	{
		$packages = AtwSupportPackage::where('status', '!=', 'expired')
			->get();

		foreach($packages as $package)
		{
			$expires = Carbon::createFromFormat('Y-m-d', $package->support_end);
			if(Carbon::today()->gt($expires))
			{
				//send email to person
				$client_name = $package->person->getName();
				$client_email = $package->person->email;
				
				Mail::send('emails.profile.atw_expired', array(), function($message) use ($client_email, $client_name)
				{
					$message
						->to($client_email, $client_name)
						->subject(Lang::get('translations.expire_atw_package_subject'));
				});

				//expire 
				$package->status = 'expired';
				$package->save();
			}
		}
	}
	private function expireMembershipBodies()
	{
		$memberships = PersonMembership::where('expired', '=', '0')
			->get();
		
		foreach($memberships as $membership)
		{
			if(empty($membership->expiry_date))
				continue;
			
			$expires = Carbon::createFromFormat('Y-m-d', $membership->expiry_date);
			if(Carbon::today()->gt($expires))
			{
				//send email to person
				$client_name = $membership->person()->getName();
				$client_email = $membership->person()->email;

				Mail::send('emails.profile.membership_expired', array('expiration_date' => Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y'), 'membership_body' => $membership->membership_body, 'person_name' => $client_name), function($message) use ($client_email, $client_name)
				{
					$message
						->to($client_email, $client_name)
						->subject(Lang::get('translations.expire_membership_body_subject'));
				});

				//expire 
				$membership->expired = true;
				$membership->save();
			}
		}
	}
	private function sendExpirationMailForMembershipBodies()
	{
		$memberships = PersonMembership::where('expired', '=', '0')
			->where('sent_expired_notification', '=', 0)
			->get();
			
		foreach($memberships as $membership)
		{
			$expiration = Carbon::createFromFormat('Y-m-d', $membership->expiry_date);
			if(Carbon::now()->addDays(30)->gt($expiration)) //expired
			{
				$client_name = $membership->person()->getName();
				$client_email = $membership->person()->email;

				Mail::send('emails.profile.membership_will_expired', array('expiration_date' => Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y'), 'membership_body' => $membership->membership_body, 'person_name' => $client_name), function($message) use ($client_email, $client_name)
				{
					$message
						->to($client_email, $client_name)
						->subject(Lang::get('translations.expiration_mail_for_membership_bodies_subject'));
				});

				$membership->sent_expired_notification = true;
				$membership->save();
			}
		}
	}
	
	private function changeStatusToInProgress()
	{
		//awaiting_applications jobs
		$jobs = Job::where('status', '=', 'awaiting_applications')
			->get();

		foreach($jobs as $job)
		{
			$firstDate = $job->firstDate();
			if(empty($firstDate))
				continue;

			$date = Carbon::createFromFormat('Y-m-d H:i:s', $firstDate->date . ' ' . $firstDate->start_time);
			if($date->lte(Carbon::now()))
			{
				if(count($job->taken_positions()) == 0) //nobody was hired - cancel the job!
				{
					$client_email = $job->person->email;
					$client_name = $job->person->getName();
					
					$job_id = $job->id;
					$job_name = $job->getTitle();
					
					Mail::send('emails.job_deleted', array('job_id' => $job_id, 'job_name' => $job_name), function($message) use ($client_name, $client_email)
					{
						$message
							->to($client_email, $client_name)
							->subject(Lang::get('translations.job_status_changed_no_lsp_hired_subject'));
					});

					$job->status = 'no_lsp_hired';
					$job->save();
				}
				else //somebody was hired
				{
					$job->status = 'confirmed';
					$job->save();

					$client = $job->person;
					
					$client_email = $client->email;
					$client_name = $client->getName();
					
					$job_id = $job->id;
					$job_name = $job->getTitle();

					//sent notification to client - if all positions are closed
					Notification::sendNotificationTo(null, $client->id, $job->id, Lang::get('translations.job_status_changed_confirmed_notification'), 'jobs/view/{JOB_ID}', 'View details');

					Mail::send('emails.job_status_changed_confirmed', array('job_id' => $job_id, 'job_name' => $job_name), function($message) use ($client_name, $client_email)
					{
						$message
							->to($client_email, $client_name)
							->subject(Lang::get('translations.job_status_changed_confirmed_subject'));
					});
				}
			}
		}
	}
	private function changeStatusToFinished()
	{
		//finish the jobs
		$jobs = Job::where('status', '=', 'confirmed')
			->get();

		foreach ($jobs as $job)
		{
			$lastDate = $job->lastDate();
			if(empty($lastDate))
				continue;

			$date = Carbon::createFromFormat('Y-m-d H:i:s', $lastDate->date . ' ' . $lastDate->start_time);
			if($date->lte(Carbon::now()))
			{
				$job->status = 'finished';
				$job->save();

				$client = $job->person;
				$client_email = $client->email;
				$client_name = $client->getName();
				
				$job_id = $job->id;
				$job_name = $job->getTitle();
				
				Mail::send('emails.job_status_changed_finished', array('job_id' => $job_id, 'job_name' => $job_name), function($message) use ($client_name, $client_email)
				{
					$message
						->to($client_email, $client_name)
						->subject(Lang::get('translations.job_status_changed_finished_subject'));
				});
			}
		}
	}

	private function sendInvoices()
	{
		$cron = new CronJob();
		$cron->description = 'Sending invoices';
		
		try {
			
			//sent invoices			
			$jobs = Job::where('status', '=', 'finished')
				->where('payment_method', '=', 'paying_card')
				->get();

			foreach ($jobs as $job)
			{
				$firstDate = $job->firstDate();
				foreach($job->taken_positions() as $position)
				{
					$worker = Person::find($position->worker_id);
					$workerAddress = $worker->default_address();

					$billingAddress = $job->person->billing_address();
					if(empty($billingAddress))
						$billingAddress = $job->person->default_address();

					$invoice = new Invoice();
					$invoice->person_id = $job->person_id;
					$invoice->worker_id = $position->worker_id;
					$invoice->job_id = $job->id;
					$invoice->status = 'unpaid';
					$invoice->number = Invoice::nextInvoiceNumber(date('Y', strtotime($firstDate->date)));

					$invoice->client_title = $billingAddress->title;
					$invoice->client_name = $billingAddress->first_name;
					$invoice->client_surname = $billingAddress->last_name;
					$invoice->client_address_1 = $billingAddress->address_1;
					$invoice->client_address_2 = $billingAddress->address_2;
					$invoice->client_city = $billingAddress->city;
					$invoice->client_country = Person::listCountries($billingAddress->country);
					$invoice->client_county = $billingAddress->county;
					$invoice->client_postcode = $billingAddress->postcode;
					$invoice->client_email = $job->person->email;

					$invoice->lsp_name = $workerAddress->getName();
					$invoice->lsp_address_1 = $workerAddress->address_1;
					$invoice->lsp_address_2 = $workerAddress->address_2;
					$invoice->lsp_city = $workerAddress->city;
					$invoice->lsp_country = Person::listCountries($workerAddress->country);
					$invoice->lsp_county = $workerAddress->county;
					$invoice->lsp_postcode = $workerAddress->postcode;
					$invoice->lsp_email = $workerAddress->email;

					$option = null;
					$invoice->lsp_language_type = "";
					if((bool)$position->spoken_language_interpreter)
					{
						$invoice->lsp_language_type = "Spoken language interpreter: ";
						$invoice->lsp_position = 'Spoken language interpreter';

						$option = JobOpenPositionOption::where('job_open_position_id', '=', $position->id)
							->where('option_key', '=', 'sli')
							->first();
					}	
					elseif((bool)$position->sign_language_interpreter)
					{
						$invoice->lsp_language_type = "Sign language interpreter: ";
						$invoice->lsp_position = 'Sign language interpreter';

						$option = JobOpenPositionOption::where('job_open_position_id', '=', $position->id)
							->where('option_key', '=', 'sli2')
							->first();
					}
					elseif((bool)$position->lipspeaker)
					{
						$invoice->lsp_language_type = "Lipspeaker: ";
						$invoice->lsp_position = 'Lipspeaker';

						$option = JobOpenPositionOption::where('job_open_position_id', '=', $position->id)
							->where('option_key', '=', 'lipspeaker')
							->first();
					}
					elseif((bool)$position->speech_to_text_reporter)
					{
						$invoice->lsp_language_type = "Speech to text reporter: ";
						$invoice->lsp_position = 'Speech to text reporter';

						$option = JobOpenPositionOption::where('job_open_position_id', '=', $position->id)
							->where('option_key', '=', 'sttr')
							->first();
					}
					elseif((bool)$position->sign_language_translator)
					{
						$invoice->lsp_language_type = "Sign language translator: ";
						$invoice->lsp_position = "Sign language translator";

						$option = JobOpenPositionOption::where('job_open_position_id', '=', $position->id)
							->where('option_key', '=', 'slt')
							->first();
					}
					elseif((bool)$position->note_taker)
					{
						$invoice->lsp_language_type = "Notetaker: ";
						$invoice->lsp_position = 'Notetaker';

						$option = JobOpenPositionOption::where('job_open_position_id', '=', $position->id)
							->where('option_key', '=', 'notetaker')
							->first();
					}
					if($option != null)
						$invoice->lsp_language_type .= $option->option_value;

					if(!empty($worker->organization_id))
					{
						$org = Organization::find($worker->organization_id);

						$invoice->lsp_organization_name = $org->name;
						$invoice->lsp_organization_address_1 = $org->address_1;
						$invoice->lsp_organization_address_2 = $org->address_2;
						$invoice->lsp_organization_city = $org->city;
						$invoice->lsp_organization_country = Person::listCountries($org->country);
						$invoice->lsp_organization_county = $org->county;
						$invoice->lsp_organization_postcode = $org->postcode;
						$invoice->lsp_organization_registration_number = $org->registration_number;
						$invoice->lsp_organization_vat_registered = $org->vat_registered;
						$invoice->lsp_organization_vat_number = $org->vat_number;
					}

					$application = JobApplication::where('job_id', '=', $job->id)
						->where('person_id', '=', $worker->id)
						->first();

					$overtimes_hours = 0.0;
					$overtimes_costs = 0.0;

					$overtimes = JobOvertime::where('job_id', '=', $job->id)
						->where('person_id', '=', $worker->id)
						->get();

					foreach($overtimes as $overtime)
					{
						$overtime_hours = $overtime->hours + $overtime->minutes;
						$overtime_costs = $overtime_hours * $overtime->hourly_rate;

						$overtimes_hours += $overtime_hours;
						$overtimes_costs += $overtime_costs;
					}

					$invoice->no_of_hours = $job->getDurationInHours();
					$invoice->overtime_hours = $overtimes_hours;
					$invoice->lsp_cost_overtime = $overtimes_costs;

					$combined_commission = null;
					if($job->project_budget == 'fixed')
					{
						$invoice->lsp_cost = (double)$job->budget_lsp_only;
						$combined_commission = ((double)Config::get('pricing.commission') / 100 * ((double)$job->budget + (double)$invoice->lsp_cost_overtime));
					}
					elseif($job->project_budget == 'fixed_hourly_rate')
					{
						$invoice->cost_per_hour = (double)$job->hourly_rate_lsp_only;
						$invoice->lsp_cost = (double)$invoice->no_of_hours * (double)$invoice->cost_per_hour;
						$combined_commission = ((double)Config::get('pricing.commission') / 100 * ((double)$job->budget + (double)$invoice->lsp_cost_overtime));
					}
					else
					{
						$invoice->lsp_cost = (double)$application->budget;
						$combined_commission = ((double)Config::get('pricing.commission') / 100 * ((double)$application->budget + (double)$invoice->lsp_cost_overtime));
					}
					
					$invoice->lingoing_commission_vat = ((double)$combined_commission * (double)Config::get('pricing.commission_vat') / 100);
					$invoice->lingoing_commission = $combined_commission - $invoice->lingoing_commission_vat;
					$invoice->total_cost = (double)$invoice->lsp_cost + (double)$invoice->lsp_cost_overtime + (double)$combined_commission;

					$invoice->payment_due = date('Y-m-d', strtotime("+" . Config::get('pricing.payment_due') . " day", time()));
					$invoice->payment_type = Person::listPaymentMethods($worker->payment_type);
					$invoice->save();

					foreach($job->dates() as $date)
					{
						$invoice_date = new InvoiceJobDate();
						$invoice_date->invoice_id = $invoice->id;
						$invoice_date->date = $date->date;
						$invoice_date->start_time = $date->start_time;
						$invoice_date->end_time = $date->end_time;
						$invoice_date->save();
					}

					Notification::sendNotificationTo(null, $position->worker_id, $job->id, "We just send you an invoice for job posting <strong>{JOB}</strong>.", 'my-invoices', 'My invoices');

					$job_id = $job->id;
					$job_name = $job->getTitle();
					
					$person_name = $worker->getName();
					$person_email = $worker->email;
					
					Mail::send('emails.invoice_sent', array('job_id' => $job_id, 'job_name' => $job_name, 'person_name' => $person_name), function($message) use ($person_name, $person_email)
					{
						$message
							->to($person_email, $person_name)
							->subject('We just send you an invoice on Lingoing.com');
					});
				}

				$job->last_invoice_sent = Carbon::now();
				$job->status = 'invoice_sent';
				$job->save();
			}
			
			$cron->last_status = 'OK';
		} catch (Exception $e) {
			Log::error($e->getMessage());
			
			$cron->last_status = $e->getMessage();
		}
		
		$cron->save();
	}
	private function sendAtWInvoices()
	{
		$invoices = array();
			
		$sql = "SELECT 
			j.*, 
			jd.date, jd.start_time
		FROM
			jobs j
		INNER JOIN
			jobs_dates jd
			ON
				j.id=jd.job_id
		WHERE
			j.status='finished' AND
			j.support_package_id IS NOT NULL
		ORDER BY
			jd.date, jd.start_time";
		
		DB::connection()->setFetchMode(PDO::FETCH_ASSOC);
		$rows = DB::select($sql);
		DB::connection()->setFetchMode(PDO::FETCH_OBJ);

		foreach($rows as $row)
		{
			//load db row in object
			$job = new Job();
			$job->setRawAttributes($row, true);
			$job->exists = true;
			
			$package = $job->supportPackage();
			
			$sendAnInvoice = false;
			$beginAtWDate = null;
			$endAtWDate = null;

			$date = $job->firstDate();
			$firstDate = Carbon::createFromFormat('Y-m-d', $date->date);

			//calculate if we need to send an invoice
			switch($package->receive_invoice)
			{
				case 'end_of_month':
					if(Carbon::today()->firstOfMonth()) //first day of the month
					{
						if(empty($job->last_invoice_sent) || Carbon::createFromFormat('Y-m-d', $job->last_invoice_sent)->ne(Carbon::today()))
						{
							$sendAnInvoice = true;

							$beginAtWDate = Carbon::today()->subMonth()->startOfMonth();
							$endAtWDate = Carbon::today()->subMonth()->endOfMonth();
						}
					}
					break;

				case 'every_two_weeks':
					if(Carbon::now()->firstOfMonth() || Carbon::today()->day == 15) //1st or 15th in month
					{
						if(empty($job->last_invoice_sent) || Carbon::createFromFormat('Y-m-d', $job->last_invoice_sent)->ne(Carbon::today()))
						{
							$sendAnInvoice = true;

							if(Carbon::now()->firstOfMonth()) //previous 14 days
							{
								$beginAtWDate = Carbon::today()->subMonth()->day = 15;
								$endAtWDate = Carbon::today()->subMonth()->endOfMonth();
							}
							else
							{
								$beginAtWDate = Carbon::today()->startOfMonth();
								$endAtWDate = Carbon::today()->subDay(); //14th in month
							}
						}
					}
					break;

				default: //job_by_job
					if($job->status == 'finished')
					{						
						$sendAnInvoice = true;

						$beginAtWDate = Carbon::createFromFormat('Y-m-d H:i:s', $date->date . ' ' . $date->start_time);
						$endAtWDate = Carbon::createFromFormat('Y-m-d H:i:s', $date->date . ' ' . $date->end_time);
					}
					break;
			}

			if(!$sendAnInvoice || ($package->receive_invoice != 'job_by_job' && !$firstDate->between($beginAtWDate, $endAtWDate))) //we don't need to send an invoice
				continue;

			//get a worker for single job and his position
			$worker = null;
			$lspPosition = null;
			foreach($job->taken_positions() as $position)
			{
				$worker = Person::find($position->worker_id);
				if((bool)$position->spoken_language_interpreter)
					$lspPosition = 'Spoken language interpreter';
				elseif((bool)$position->sign_language_interpreter)
					$lspPosition = 'Sign language interpreter';
				elseif((bool)$position->lipspeaker)
					$lspPosition = 'Lipspeaker';
				elseif((bool)$position->speech_to_text_reporter)
					$lspPosition = 'Speech to text reporter';
				elseif((bool)$position->sign_language_translator)
					$lspPosition = "Sign language translator";
				elseif((bool)$position->note_taker)
					$lspPosition = 'Notetaker';
					
				$worker = Person::find($position->worker_id);
			}

			$invoice = null;
			if($package->receive_invoice != 'job_by_job') //if there is a job_by_job method we need to send invoice for every job extra, so no combining
			{
				if(isset($invoices[$job->person_id][$worker->id][$lspPosition][$beginAtWDate->format('Y-m-d') . '-' . $endAtWDate->format('Y-m-d')]))
					$invoice = Invoice::find($invoices[$job->person_id][$worker->id][$lspPosition][$beginAtWDate->format('Y-m-d') . '-' . $endAtWDate->format('Y-m-d')]);
			}

			if($invoice == null)
			{
				$workerAddress = $worker->default_address();

				$billingAddress = $job->person->billing_address();
				if(empty($billingAddress))
					$billingAddress = $job->person->default_address();

				$invoice = new Invoice();
				$invoice->person_id = $job->person_id;
				$invoice->worker_id = $position->worker_id;
				$invoice->atw_package_id =  $package->id;
				$invoice->job_id = $job->id;
				$invoice->status = 'unpaid';
				$invoice->number = Invoice::nextInvoiceNumber(date('Y', strtotime($date->date)));
				$invoice->atw_date_begin = $beginAtWDate;
				$invoice->atw_date_end = $endAtWDate;

				$invoice->client_title = $billingAddress->title;
				$invoice->client_name = $billingAddress->first_name;
				$invoice->client_surname = $billingAddress->last_name;
				$invoice->client_address_1 = $billingAddress->address_1;
				$invoice->client_address_2 = $billingAddress->address_2;
				$invoice->client_city = $billingAddress->city;
				$invoice->client_country = Person::listCountries($billingAddress->country);
				$invoice->client_county = $billingAddress->county;
				$invoice->client_postcode = $billingAddress->postcode;
				$invoice->client_email = $job->person->email;

				$invoice->lsp_name = $workerAddress->getName();
				$invoice->lsp_address_1 = $workerAddress->address_1;
				$invoice->lsp_address_2 = $workerAddress->address_2;
				$invoice->lsp_city = $workerAddress->city;
				$invoice->lsp_country = Person::listCountries($workerAddress->country);
				$invoice->lsp_county = $workerAddress->county;
				$invoice->lsp_postcode = $workerAddress->postcode;
				$invoice->lsp_email = $workerAddress->email;

				$option = null;
				$invoice->lsp_language_type = "";
				if((bool)$position->spoken_language_interpreter)
				{
					$invoice->lsp_language_type = "Spoken language interpreter: ";
					$invoice->lsp_position = 'Spoken language interpreter';

					$option = JobOpenPositionOption::where('job_open_position_id', '=', $position->id)
						->where('option_key', '=', 'sli')
						->first();
				}	
				elseif((bool)$position->sign_language_interpreter)
				{
					$invoice->lsp_language_type = "Sign language interpreter: ";
					$invoice->lsp_position = 'Sign language interpreter';

					$option = JobOpenPositionOption::where('job_open_position_id', '=', $position->id)
						->where('option_key', '=', 'sli2')
						->first();
				}
				elseif((bool)$position->lipspeaker)
				{
					$invoice->lsp_language_type = "Lipspeaker: ";
					$invoice->lsp_position = 'Lipspeaker';

					$option = JobOpenPositionOption::where('job_open_position_id', '=', $position->id)
						->where('option_key', '=', 'lipspeaker')
						->first();
				}
				elseif((bool)$position->speech_to_text_reporter)
				{
					$invoice->lsp_language_type = "Speech to text reporter: ";
					$invoice->lsp_position = 'Speech to text reporter';

					$option = JobOpenPositionOption::where('job_open_position_id', '=', $position->id)
						->where('option_key', '=', 'sttr')
						->first();
				}
				elseif((bool)$position->sign_language_translator)
				{
					$invoice->lsp_language_type = "Sign language translator: ";
					$invoice->lsp_position = "Sign language translator";

					$option = JobOpenPositionOption::where('job_open_position_id', '=', $position->id)
						->where('option_key', '=', 'slt')
						->first();
				}
				elseif((bool)$position->note_taker)
				{
					$invoice->lsp_language_type = "Notetaker: ";
					$invoice->lsp_position = 'Notetaker';

					$option = JobOpenPositionOption::where('job_open_position_id', '=', $position->id)
						->where('option_key', '=', 'notetaker')
						->first();
				}
				if($option != null)
					$invoice->lsp_language_type .= $option->option_value;

				if(!empty($worker->organization_id))
				{
					$org = Organization::find($worker->organization_id);

					$invoice->lsp_organization_name = $org->name;
					$invoice->lsp_organization_address_1 = $org->address_1;
					$invoice->lsp_organization_address_2 = $org->address_2;
					$invoice->lsp_organization_city = $org->city;
					$invoice->lsp_organization_country = Person::listCountries($org->country);
					$invoice->lsp_organization_county = $org->county;
					$invoice->lsp_organization_postcode = $org->postcode;
					$invoice->lsp_organization_registration_number = $org->registration_number;
					$invoice->lsp_organization_vat_registered = $org->vat_registered;
					$invoice->lsp_organization_vat_number = $org->vat_number;
				}

				$application = JobApplication::where('job_id', '=', $job->id)
					->where('person_id', '=', $worker->id)
					->first();

				$overtimes_hours = 0.0;
				$overtimes_costs = 0.0;

				$overtimes = JobOvertime::where('job_id', '=', $job->id)
					->where('person_id', '=', $worker->id)
					->get();

				foreach($overtimes as $overtime)
				{
					$overtime_hours = $overtime->hours + $overtime->minutes;
					$overtime_costs = $overtime_hours * $overtime->hourly_rate;

					$overtimes_hours += $overtime_hours;
					$overtimes_costs += $overtime_costs;
				}

				$invoice->no_of_hours = $job->getDurationInHours();
				$invoice->overtime_hours = $overtimes_hours;
				$invoice->lsp_cost_overtime = $overtimes_costs;

				$combined_commission = null;
				if($job->project_budget == 'fixed')
				{
					$invoice->lsp_cost = (double)$job->budget_lsp_only;
					$combined_commission = ((double)Config::get('pricing.commission') / 100 * ((double)$job->budget + (double)$invoice->lsp_cost_overtime));
				}
				elseif($job->project_budget == 'fixed_hourly_rate')
				{
					$invoice->cost_per_hour = (double)$job->hourly_rate_lsp_only;
					$invoice->lsp_cost = (double)$invoice->no_of_hours * (double)$invoice->cost_per_hour;
					$combined_commission = ((double)Config::get('pricing.commission') / 100 * ((double)$job->budget + (double)$invoice->lsp_cost_overtime));
				}
				else
				{
					$invoice->lsp_cost = (double)$application->budget;
					$combined_commission = ((double)Config::get('pricing.commission') / 100 * ((double)$application->budget + (double)$invoice->lsp_cost_overtime));
				}

				$invoice->lingoing_commission_vat = ((double)$combined_commission * (double)Config::get('pricing.commission_vat') / 100);
				$invoice->lingoing_commission = $combined_commission - $invoice->lingoing_commission_vat;
				$invoice->total_cost = (double)$invoice->lsp_cost + (double)$invoice->lsp_cost_overtime + (double)$combined_commission;

				$invoice->payment_due = date('Y-m-d', strtotime("+" . Config::get('pricing.payment_due') . " day", time()));
				$invoice->payment_type = Person::listPaymentMethods($worker->payment_type);
				$invoice->save();

				foreach($job->dates() as $date)
				{
					$invoice_date = new InvoiceJobDate();
					$invoice_date->invoice_id = $invoice->id;
					$invoice_date->date = $date->date;
					$invoice_date->start_time = $date->start_time;
					$invoice_date->end_time = $date->end_time;
					$invoice_date->save();
				}

				//remember invoice's id
				$invoices[$job->person_id][$worker->id][$lspPosition][$beginAtWDate->format('Y-m-d') . '-' . $endAtWDate->format('Y-m-d')] = $invoice->id;
			}
			else //combining
			{
				//add date
				foreach($job->dates() as $date)
				{
					$invoice_date = new InvoiceJobDate();
					$invoice_date->invoice_id = $invoice->id;
					$invoice_date->date = $date->date;
					$invoice_date->start_time = $date->start_time;
					$invoice_date->end_time = $date->end_time;
					$invoice_date->save();
				}

				//add costs
				$overtimes_hours = 0.0;
				$overtimes_costs = 0.0;

				$overtimes = JobOvertime::where('job_id', '=', $job->id)
					->where('person_id', '=', $worker->id)
					->get();

				foreach($overtimes as $overtime)
				{
					$overtime_hours = $overtime->hours + $overtime->minutes;
					$overtime_costs = $overtime_hours * $overtime->hourly_rate;

					$overtimes_hours += $overtime_hours;
					$overtimes_costs += $overtime_costs;
				}

				$invoice->no_of_hours += $job->getDurationInHours();
				$invoice->overtime_hours += $overtimes_hours;
				$invoice->lsp_cost_overtime += $overtimes_costs;

				$combined_commission = null;
				if($job->project_budget == 'fixed')
				{
					$invoice->lsp_cost += (double)$job->budget_lsp_only;
					$combined_commission = ((double)Config::get('pricing.commission') / 100 * ((double)$job->budget + (double)$invoice->lsp_cost_overtime));
				}
				elseif($job->project_budget == 'fixed_hourly_rate')
				{
					$invoice->cost_per_hour = (double)$job->hourly_rate_lsp_only;
					$invoice->lsp_cost = (double)$invoice->no_of_hours * (double)$invoice->cost_per_hour;
					$combined_commission = ((double)Config::get('pricing.commission') / 100 * ((double)$job->budget + (double)$invoice->lsp_cost_overtime));
				}
				else
				{
					$invoice->lsp_cost += (double)$application->budget;
					$combined_commission = ((double)Config::get('pricing.commission') / 100 * ((double)$application->budget + (double)$invoice->lsp_cost_overtime));
				}

				$commission_vat = ((double)$combined_commission * (double)Config::get('pricing.commission_vat') / 100);

				$invoice->lingoing_commission_vat += $commission_vat;
				$invoice->lingoing_commission += $combined_commission - $commission_vat;					
				$invoice->total_cost = (double)$invoice->lsp_cost + (double)$invoice->lsp_cost_overtime + (double)$invoice->lingoing_commission + (double)$invoice->lingoing_commission_vat;
				
				$invoice->save();
			}

			if($job->status == 'finished')
				$job->status = 'invoice_sent';
			$job->last_invoice_sent = Carbon::today();
			$job->save();
		}
	}
	
	private function sentAtWFormsNotificaitons()
	{
		
	}
	
	public function getExecute()
	{
		$this->expireAtwPackages();
		$this->sendExpirationMailForMembershipBodies();
		$this->expireMembershipBodies();
		
		$this->changeStatusToInProgress();
		$this->changeStatusToFinished();
		
//		$this->sendInvoices();
		$this->sendAtWInvoices();
	}
	public function getNewsletter()
	{
		$cron = new CronJob();
		$cron->description = 'Sending newsletters';
		
		try {
			
			$fields = array(
				'sli',
				'sli2',
				'lipspeaker',
				'sttr',
				'slt',
				'notetaker',
				'specialisms'
			);
			
			$sendMessages = 0;
			
			$persons = Person::where('confirmed', '=', '1')
				->where('account_type', '=', 'translator')
				->get();
				
			foreach($persons as $person)
			{
				$filter = array();
				
				foreach($fields as $field)
				{
					$options = $person->searchOptions($field);
					if(count($options) > 0)
					{
						foreach ($options as $option)
						{
							$title = null;
							
							switch($field)
							{
								case 'sli':
									$title = 'Spoken Language - ' . Person::listSpokenLanguageInterpreter($option->option_value);
									break;
									
								case 'sli2':
									$title = Person::listSignLanguageInterpreter($option->option_value);
									break;
									
								case 'lipspeaker':
									$title = Person::listLipspeaker($option->option_value);
									break;
									
								case 'sttr':
									$title = 'Speech to Text Reporter - ' . Person::listSpokenLanguageInterpreter($option->option_value);
									break;
									
								case 'slt':
									$title = 'Sign Language Translator - ' . Person::listSignLanguageInterpreter($option->option_value);
									break;
									
								case 'notetaker':
									$title = 'Notetaker - ' . Person::listSpokenLanguageInterpreter($option->option_value);
									break;
									
								case 'specialisms':
									$title = $option->option_value . ' Specialist';
									break;
							}
							
							$sql = "SELECT
									j.*
								FROM
									(
										SELECT
											*
										FROM
											(
											SELECT 
												*
											FROM
												jobs_dates
											ORDER BY 
												date, 
												start_time
										) AS dates
										GROUP BY
											dates.job_id
									) AS dt,
									jobs j
								INNER JOIN
									jobs_open_position jop
									ON
										j.id=jop.job_id
								INNER JOIN
									jobs_open_position_option jopo
									ON
										jop.id=jopo.job_open_position_id
								WHERE
									j.id=dt.job_id AND
									j.confirmed=1 AND
									j.status = 'awaiting_applications' AND
									dt.date >= '" . date('Y-m-d', time()) . "' AND
									jopo.option_key='{$field}' AND
									jopo.option_value='{$option->option_value}'
								ORDER BY
									dt.date,
									dt.start_time
								LIMIT 3
							";
							
							DB::connection()->setFetchMode(PDO::FETCH_ASSOC);
							$results = DB::select($sql);
							DB::connection()->setFetchMode(PDO::FETCH_OBJ);
							
							if(count($results) > 0)
							{
								$index = 0;
								$jobs = new Collection();
								foreach($results as $result)
								{									
									$job = new Job();
									$job->setRawAttributes($result, true);
									$job->exists = true;

									$jobs->put($index, $job);

									$index++;
								}
								
								$sendMessages++;
								
								Mail::send('emails.newsletter.jobs', array('jobs' => $jobs, 'title' => $title), function($message) use ($person)
								{
									$message
										->to($person->email, $person->getName())
										->subject('Daily newsletter from Lingoing');
								});
							}
						}
					}
				}
			}
		
			$cron->last_status = 'OK';
			$cron->no_of_sent_mails = $sendMessages;
		} catch (Exception $e) {
			$cron->last_status = $e->getMessage();
		}
		
		$cron->save();
	}	

}