<?php
use \Carbon\Carbon;
use \Aws\S3\Enum\CannedAcl;
use \Illuminate\Support\MessageBag;
use \Illuminate\Support\Collection;

class JobsController extends BaseController {

	protected $layout = 'layouts.frontend';
	
	public function getJobs($page = 1)
	{
		$filter = Session::get('jobs_filter', null);
		
		$count = 0;
		$per_page = 5;
		$max_pages = 0;
		
		$user_filter = array();
		if(Auth::check() && Auth::user()->account_type == 'translator') //only for loged in user
		{
			if(Auth::user()->spoken_language_interpreter)
				$user_filter['sli'] = Auth::user()->memberships('sli');
			
			if(Auth::user()->sign_language_interpreter)
				$user_filter['sli2'] = Auth::user()->memberships('sli2');
			
			if(Auth::user()->lipspeaker)
				$user_filter['lipspeaker'] = Auth::user()->memberships('lipspeaker');
			
			if(Auth::user()->speech_to_text_reporter)
				$user_filter['sttr'] = Auth::user()->memberships('sttr');
							
			if(Auth::user()->sign_language_translator)
				$user_filter['slt'] = Auth::user()->memberships('slt');
			
			if(Auth::user()->note_taker)
				$user_filter['notetaker'] = Auth::user()->memberships('notetaker');
				
			$user_filter['specialisms'] = Auth::user()->specialisms();
		}
			
		$sql = "SELECT
				j.id
			FROM
				(
					SELECT
						*
					FROM
						(
						SELECT 
							*
						FROM
							jobs_dates
						ORDER BY 
							date, 
							start_time
					) AS dates
					GROUP BY
						dates.job_id
				) AS dt,
				jobs j
			INNER JOIN
				jobs_open_position jop
				ON
					j.id=jop.job_id
			INNER JOIN
				jobs_open_position_option jopo
				ON
					jop.id=jopo.job_open_position_id
			WHERE
				j.id=dt.job_id AND
				j.confirmed=1 AND
				j.status = 'awaiting_applications' AND
				dt.date >= '" . date('Y-m-d', time()) . "'";
				
		if(isset($filter['atw_jobs']))
		{
			switch($filter['atw_jobs'])
			{
				case 'only_atw':
					$sql .= " AND j.payment_method='atw_customer'";
					break;
					
				case 'without_atw':
					$sql .= " AND j.payment_method='paying_card'";
					break;
					
				default:
					break;
			}
		}

		if(count($user_filter) > 0)
		{
			$index = 0;
			$query = '';
			
			foreach($user_filter as $option_key => $options_array)
			{
				if($options_array == null || count($options_array) == 0)
					continue;
				
				$counter = 0;
				
				if($index > 0)
					$query .= " || ";

				$query .= "(";
				$query .= "jopo.option_key = '{$option_key}' && ";
				$query .= "(";
				
				if($option_key == 'specialisms')
				{
					foreach ($options_array as $specialism)
					{
						if($counter > 0)
							$query .= " || ";

						$query .= "jopo.option_value = '{$specialism->specialism}'";

						$counter++;
					}
				}
				else
				{
					foreach($options_array as $membership)
					{
						foreach($membership->languages() as $language)
						{
							if($counter > 0)
								$query .= " || ";

							$query .= "jopo.option_value = '{$language->language}'";

							$counter++;
						}
					}
				}
				$query .= ")";
				$query .= ")";

				$index++;
			}
			
			if(!empty($query))
				$sql .= " AND (" . $query . ")";
		}
		
		if((isset($filter['fixed_budget']) && $filter['fixed_budget'] == 'yes') ||
		   (isset($filter['fixed_hourly_rate']) && $filter['fixed_hourly_rate'] == 'yes') ||
		   (isset($filter['open_to_quotes']) && $filter['open_to_quotes'] == 'yes'))
		{
			$query  = "(";
			if(isset($filter['fixed_budget']) && $filter['fixed_budget'] == 'yes')
				$query .= "j.project_budget='fixed'";
				
			if(isset($filter['fixed_hourly_rate']) && $filter['fixed_hourly_rate'] == 'yes')
			{
				if($query != '(')
					$query .= ' OR ';
					
				$query .= "j.project_budget='fixed_hourly_rate'";
			}
			
			if(isset($filter['open_to_quotes']) && $filter['open_to_quotes'] == 'yes')
			{
				if($query != '(')
					$query .= ' OR ';
					
				$query .= "j.project_budget='open_to_quotes'";
			}
			$query .= ")";
			
			$sql .= " AND " . $query;
		}
		$ids = DB::select($sql);
		
		$new_ids = array();
		foreach($ids as $id)
			$new_ids[] = $id->id;
		
		$jobs = array();
		$count = count($new_ids);
		if($count > 0)
		{
			$jobs = Job::whereIn('id', $new_ids)
				->orderBy('type', 'ASC')
				->skip($per_page * ($page - 1))
				->take($per_page)
				->get();
		}
		else
			$jobs = array();
		
		$max_pages = (int)ceil($count / $per_page);
		
		$this->layout->content = View::make('frontend.jobs.find_jobs', array(
			'jobs' => $jobs,
			'page' => $page,
			'max_pages' => $max_pages,
			'filter' => $filter
		));
	}
	public function postJobs()
	{
		$filter = Session::get('jobs_filter', array());
		
		$search_button = Input::get('search');
		if(isset($search_button))
		{
			$filter['atw_jobs'] = Input::get('atw_jobs');
			
			$filter['fixed_budget'] = Input::get('fixed_budget');
			$filter['fixed_hourly_rate'] = Input::get('fixed_hourly_rate');
			$filter['open_to_quotes'] = Input::get('open_to_quotes');
		}
		else
			$filter = array();
		
		Session::put('jobs_filter', $filter);
		
		return Redirect::to('jobs');
	}

	public function getStep1()
	{
		$job = null;
		$job_id = Session::get('job_id');
		if(!empty($job_id))
		{
			$job = Job::find($job_id);
			if($job == null || $job->person_id != Auth::user()->id)
			{
				Session::forget('job_id');
				return Redirect::to(action('JobsController@getStep1'));
			}
		}
		
		$this->layout->content = View::make('frontend.jobs.jobs_layout')->nest('content', 'frontend.jobs.steps.step1', array(
			'job' => $job
		));
	}
	public function postStep1()
	{
		$job = null;
		$job_id = Session::get('job_id');
		if(!empty($job_id))
		{
			$job = Job::find($job_id);
			if($job == null || $job->person_id != Auth::user()->id)
			{
				Session::forget('job_id');
				return Redirect::to(action('JobsController@getStep1'));
			}
		}
		
		$rules = array(
			'type_of_job' => array('required'),
			'payment_method' => array('required')
		);
		if(Input::get('type_of_job') == 'other')
			$rules['type_other'] = array('required');
		if(Input::get('budget_type') == 'fixed')
			$rules['budget'] = array('required', 'numeric');
		if(Input::get('budget_type') == 'fixed_hourly_rate')
			$rules['budget_hr'] = array('required', 'numeric');
		if(Input::get('other_arrangement') == 'yes')
			$rules['other_arrangement_text'] = array('required');
		if(Input::get('payment_method') == 'atw_customer')
			$rules['support_package'] = array('required');
			
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to('/post-a-job')
				->withErrors($validator)
				->withInput();
		}
		
		if($job == null)
		{
			$job = new Job();
			$job->person_id = Auth::user()->id;
		}
		$job->status = 'awaiting_applications';
		$job->confirmed = false;
		$job->description = Input::get('job_description');
		$job->type = Input::get('type_of_job');
		$job->type_other = Input::get('type_other');
		$job->project_budget = Input::get('budget_type');
		if($job->project_budget == 'fixed_hourly_rate')
			$job->hourly_rate = Input::get('budget_hr');
		else
			$job->budget = Input::get('budget');
		$job->travel_expenses = Input::get('travel_expenses') == 'yes';
		$job->co_worker = Input::get('co_worker') == 'yes';
		$job->co_worker_or_double_rate = Input::get('co_worker_or_double_rate') == 'yes';
		$job->unsociable_hours = Input::get('unsociable_hours') == 'yes';
		$job->other_arrangement = Input::get('other_arrangement') == 'yes';
		$job->other_arrangement_text = Input::get('other_arrangement_text');
		$job->payment_method = Input::get('payment_method');
		if($job->payment_method == 'atw_customer')
		{			
			$support_package_id = (int)Input::get('support_package');
			if($support_package_id > 0)
				$job->support_package_id = $support_package_id;
		}
		$job->save();
		
		Session::put('job_id', $job->id);
		return Redirect::to(action('JobsController@getStep2'));
	}
	
	public function getStep2()
	{
		$job_id = Session::get('job_id');
		if($job_id == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		$job = Job::find($job_id);
		if($job == null)
			return Redirect::to(action('JobsController@getStep1'));
		
		if($job->person_id != Auth::user()->id)
		{
			Session::forget('job_id');
			return Redirect::to(action('JobsController@getStep1'));
		}
		
		$positions = $job->positions;
			
		$this->layout->content = View::make('frontend.jobs.jobs_layout')->nest('content', 'frontend.jobs.steps.step2', array(
			'positions' => $positions
		));
	}
	public function postStep2()
	{
		$job_id = Session::get('job_id');
		if($job_id == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		$job = Job::find($job_id);
		if($job == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		if($job->person_id != Auth::user()->id)
		{
			Session::forget('job_id');
			return Redirect::to(action('JobsController@getStep1'));
		}
		
		$count = (int)Input::get('no_of_professionals');
		if($count == 0)
			return Redirect::to(action('JobsController@getStep2'));
			
		$errors = array();
		$rules = array();
		for($i = 1; $i <= $count; $i++)
		{
			if(Input::get('spoken_language_interpreter_' . $i) != 'yes' &&
			   Input::get('sign_language_interpreter_' . $i) != 'yes' &&
			   Input::get('lipspeaker_' . $i) != 'yes' &&
			   Input::get('speech_to_text_reporter_' . $i) != 'yes' &&
			   Input::get('sign_language_translator_' . $i) != 'yes' &&
			   Input::get('note_taker_' . $i) != 'yes')
				$errors['requirements_' . $i] = 'You have to select at least one option!';
			
			if(Input::get('spoken_language_interpreter_' . $i) == 'yes')
				$rules['sli_languages_' . $i] = array('required');

			if(Input::get('sign_language_interpreter_' . $i) == 'yes')
				$rules['sli2_languages_' . $i] = array('required');

			if(Input::get('lipspeaker_' . $i) == 'yes')
				$rules['lipspeaker_languages_' . $i] = array('required');

			if(Input::get('speech_to_text_reporter_' . $i) == 'yes')
				$rules['sttr_languages_' . $i] = array('required');

			if(Input::get('sign_language_translator_' . $i) == 'yes')
				$rules['slt_languages_' . $i] = array('required');

			if(Input::get('note_taker_' . $i) == 'yes')
				$rules['note_taker_languages_' . $i] = array('required');

			if(Input::get('number_or_audience_' . $i) == 'group')
				$rules['no_of_people_' . $i] = array('required', 'numeric');
		}
		
		$messages = array();
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
			$messages = $validator->errors()->getMessages();
			
		if(count($errors) > 0 || count($messages) > 0)
		{
			return Redirect::to(action('JobsController@getStep2'))
				->withErrors(new MessageBag($messages + $errors))
				->withInput();
		}
		
		$job->clearPositions();
		for($i = 1; $i <= $count; $i++)
		{			
			$position = new JobOpenPosition();
			$position->job()->associate($job);
			$position->spoken_language_interpreter = Input::get('spoken_language_interpreter_' . $i) == 'yes';
			$position->sign_language_interpreter = Input::get('sign_language_interpreter_' . $i) == 'yes';
			$position->lipspeaker = Input::get('lipspeaker_' . $i) == 'yes';
			$position->speech_to_text_reporter = Input::get('speech_to_text_reporter_' . $i) == 'yes';
			$position->sign_language_translator = Input::get('sign_language_translator_' . $i) == 'yes';
			$position->note_taker = Input::get('note_taker_' . $i) == 'yes';
			$position->number_of_audience = Input::get('number_or_audience_' . $i);
			if($position->number_of_audience == 'group')
			{
				$position->no_in_groups = Input::get('no_of_people_' . $i);
				$position->coworker = (Input::get('coworker_' . $i) == 'yes');
			}
			$position->save();
			
			if($position->spoken_language_interpreter)
			{
				$languages = Input::get('sli_languages_' . $i);
				foreach($languages as $language)
				{
					$option = new JobOpenPositionOption();
					$option->job_open_position()->associate($position);
					$option->option_key = 'sli';
					$option->option_value = $language;
					$option->save();
				}
			}
			
			if($position->sign_language_interpreter)
			{
				$languages = Input::get('sli2_languages_' . $i);
				foreach($languages as $language)
				{
					$option = new JobOpenPositionOption();
					$option->job_open_position()->associate($position);
					$option->option_key = 'sli2';
					$option->option_value = $language;
					$option->save();
				}
			}
			
			if($position->lipspeaker) //save languages
			{
				$languages = Input::get('lipspeaker_languages_' . $i);
				foreach($languages as $language)
				{
					$option = new JobOpenPositionOption();
					$option->job_open_position()->associate($position);
					$option->option_key = 'lipspeaker';
					$option->option_value = $language;
					$option->save();
				}
			}
			
			if($position->speech_to_text_reporter)
			{
				$languages = Input::get('sttr_languages_' . $i);
				foreach($languages as $language)
				{
					$option = new JobOpenPositionOption();
					$option->job_open_position()->associate($position);
					$option->option_key = 'sttr';
					$option->option_value = $language;
					$option->save();
				}
			}
			
			if($position->sign_language_translator)
			{
				$languages = Input::get('slt_languages_' . $i);
				foreach($languages as $language)
				{
					$option = new JobOpenPositionOption();
					$option->job_open_position()->associate($position);
					$option->option_key = 'slt';
					$option->option_value = $language;
					$option->save();
				}
			}
			
			if($position->note_taker)
			{
				$languages = Input::get('note_taker_languages_' . $i);
				foreach($languages as $language)
				{
					$option = new JobOpenPositionOption();
					$option->job_open_position()->associate($position);
					$option->option_key = 'note_taker';
					$option->option_value = $language;
					$option->save();
				}
			}
			
			$specialisms = Input::get('specialisms_' . $i);
			if(count($specialisms) > 0)
			{
				foreach($specialisms as $specialism)
				{
					$option = new JobOpenPositionOption();
					$option->job_open_position()->associate($position);
					$option->option_key = 'specialism';
					$option->option_value = $specialism;
					$option->save();
				}
			}
		}
		
		return Redirect::to(action('JobsController@getStep3'));
	}
	
	public function getStep3()
	{
		$job_id = Session::get('job_id');
		if($job_id == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		$job = Job::find($job_id);
		if($job == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		if($job->person_id != Auth::user()->id)
		{
			Session::forget('job_id');
			return Redirect::to(action('JobsController@getStep1'));
		}
		
		$this->layout->content = View::make('frontend.jobs.jobs_layout')->nest('content', 'frontend.jobs.steps.step3', array(
			'job' => $job
		));
	}
	public function postStep3()
	{
		$job_id = Session::get('job_id');
		if($job_id == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		$job = Job::find($job_id);
		if($job == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		if($job->person_id != Auth::user()->id)
		{
			Session::forget('job_id');
			return Redirect::to(action('JobsController@getStep1'));
		}
		
		$rules = null;
		if(Input::get('job_address_type') == 'existing')
		{
			$rules = array(
				'existing_address' => array('required')
			);
		}
		else
		{
			$rules = array(
				'address_1' => array('required'),
				'city' => array('required'),
				'country' => array('required'),
				'postcode' => array('required')
			);
		}
		
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(action('JobsController@getStep3'))
				->withErrors($validator)
				->withInput();
		}
		
		if(Input::get('job_address_type') == 'existing')
		{
			$address = PersonAddress::find(Input::get('existing_address'));
			
			$job->address_1 = $address->address_1;
			$job->address_2 = $address->address_2;
			$job->city = $address->city;
			$job->postcode = $address->postcode;
			$job->county = $address->county;
			$job->country = $address->country;
			
			Session::put('existing_address_id', Input::get('existing_address'));
		}
		else
		{
			$job->address_1 = Input::get('address_1');
			$job->address_2 = Input::get('address_2');
			$job->city = Input::get('city');
			$job->postcode = Input::get('postcode');
			$job->county = Input::get('county');
			$job->country = Input::get('country');
			
			Session::forget('existing_address_id');
		}
		$job->save();
		
		return Redirect::to(action('JobsController@getStep4'));
	}
	
	public function getStep4()
	{
		$job_id = Session::get('job_id');
		if($job_id == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		$job = Job::find($job_id);
		if($job == null)
			return Redirect::to(action('JobsController@getStep1'));
		
		if($job->person_id != Auth::user()->id)
		{
			Session::forget('job_id');
			return Redirect::to(action('JobsController@getStep1'));
		}
			
		if(empty($job->date_type))
			$job->date_type = 'one_day';
		
		$dates = null;
		$count = 0;
		if($job->date_type == 'one_day')
		{
			$dates = Session::get('dates');
			if($dates == null)
			{
				$job_dates = $job->dates();
				if(count($job_dates) > 0)
				{
					$dates = array();
					$index = 1;
					foreach($job_dates as $date)
					{
						$dates[$index] = array(
							'date' => date('d/m/Y', strtotime($date->date)),
							'start_time' => date('H:i', strtotime($date->start_time)),
							'end_time' => date('H:i', strtotime($date->end_time))
						);
						
						$index++;
					}
					
					$job->clearDates();
				}
				else
				{
					$dates = array(
						1 => array(
							'date' => '',
							'start_time' => '',
							'end_time' => ''
						)
					);
				}
				
				Session::put('dates', $dates);
			}
		}
		
		$this->layout->content = View::make('frontend.jobs.jobs_layout')->nest('content', 'frontend.jobs.steps.step4', array(
			'job' => $job,
			'dates' => $dates
		));
	}
	public function postStep4()
	{
		$job_id = Session::get('job_id');
		if($job_id == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		$job = Job::find($job_id);
		if($job == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		if($job->person_id != Auth::user()->id)
		{
			Session::forget('job_id');
			return Redirect::to(action('JobsController@getStep1'));
		}
		
		$rules = array();
		if(Input::get('date_type') == 'one_day')
		{
			$dates = Session::get('dates');
			foreach($dates as $index => $value)
			{
				$rules['start_date_' . $index] = array('required', 'date_format:"d/m/Y"', 'date_not_in_past');
				
				$rules['start_time_' . $index] = array('required', 'date_format:"H:i"');
				$rules['end_time_' . $index] = array('required', 'date_format:"H:i"', 'time_start_end:start_time_' . $index);
			}
		}
		else //weekly
		{
			$rules = array(
				'start_date_weekly' => array('required', 'date_format:"d/m/Y"', 'date_not_in_past'),
				'end_date_weekly' => array('required', 'date_format:"d/m/Y"', 'date_not_in_past', 'date_start_before_date_end:start_date_weekly'),
				
				'start_time_mon' => array('required_with:end_time_mon', 'date_format:"H:i"'),
				'end_time_mon' => array('required_with:start_time_mon', 'date_format:"H:i"', 'time_start_end:start_time_mon'),
				
				'start_time_tue' => array('required_with:end_time_tue', 'date_format:"H:i"'),
				'end_time_tue' => array('required_with:start_time_tue', 'date_format:"H:i"', 'time_start_end:start_time_tue'),
				
				'start_time_wed' => array('required_with:end_time_wed', 'date_format:"H:i"'),
				'end_time_wed' => array('required_with:start_time_wed', 'date_format:"H:i"', 'time_start_end:start_time_wed'),
				
				'start_time_thu' => array('required_with:end_time_thu', 'date_format:"H:i"'),
				'end_time_thu' => array('required_with:start_time_thu', 'date_format:"H:i"', 'time_start_end:start_time_thu'),
				
				'start_time_fri' => array('required_with:end_time_fri', 'date_format:"H:i"'),
				'end_time_fri' => array('required_with:start_time_fri', 'date_format:"H:i"', 'time_start_end:start_time_fri'),
				
				'start_time_sat' => array('required_with:end_time_sat', 'date_format:"H:i"'),
				'end_time_sat' => array('required_with:start_time_sat', 'date_format:"H:i"', 'time_start_end:start_time_sat'),
				
				'start_time_sun' => array('required_with:end_time_sun', 'date_format:"H:i"'),
				'end_time_sun' => array('required_with:start_time_sun', 'date_format:"H:i"', 'time_start_end:start_time_sun')
			);
		}
			
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(action('JobsController@getStep4'))
				->withErrors($validator)
				->withInput();
		}
		
		if(Input::get('date_type') == 'one_day')
		{
			//no dates inserted
			$dates = Session::get('dates');
			if(count($dates) == 0)
			{
				return Redirect::to(action('JobsController@getStep4'))
					->withErrors(new MessageBag(array('one_day_error' => Lang::get('translations.one_day_error'))))
					->withInput();
			}
		}
		else //weekly
		{
			//if there are no times
			if(
				empty(Input::get('start_time_mon')) && empty(Input::get('end_time_mon')) && 
				empty(Input::get('start_time_tue')) && empty(Input::get('end_time_tue')) &&
				empty(Input::get('start_time_wed')) && empty(Input::get('end_time_wed')) &&
				empty(Input::get('start_time_thu')) && empty(Input::get('end_time_thu')) &&
				empty(Input::get('start_time_fri')) && empty(Input::get('end_time_fri')) &&
				empty(Input::get('start_time_sat')) && empty(Input::get('end_time_sat')) &&
				empty(Input::get('start_time_sun')) && empty(Input::get('end_time_sun'))
			)
			{
				return Redirect::to(action('JobsController@getStep4'))
					->withErrors(new MessageBag(array('weekly_error' => Lang::get('translations.one_day_error'))))
					->withInput();
			}
		}
		
		$job->clearDates();
		
		$job->date_type = Input::get('date_type');
		if($job->date_type == 'one_day')
		{			
			$dates = Session::get('dates');
			foreach ($dates as $index => $date)
			{
				$job_date = new JobDate();
				$job_date->job_id = $job->id;
				$job_date->date = Carbon::createFromFormat('d/m/Y', Input::get('start_date_' . $index));
				$job_date->start_time = Carbon::createFromFormat('H:i', Input::get('start_time_' . $index));
				$job_date->end_time = Carbon::createFromFormat('H:i', Input::get('end_time_' . $index));
				$job_date->save();
			}
			
			if($job->project_budget == 'fixed_hourly_rate')
				$job->budget = $job->getDurationInHours() * $job->hourly_rate;
		}
		else
		{
			$start_time_mon = Input::get('start_time_mon');
			$end_time_mon = Input::get('end_time_mon');
			
			$start_time_tue = Input::get('start_time_tue');
			$end_time_tue = Input::get('end_time_tue');
			
			$start_time_wed = Input::get('start_time_wed');
			$end_time_wed = Input::get('end_time_wed');
			
			$start_time_thu = Input::get('start_time_thu');
			$end_time_thu = Input::get('end_time_thu');
			
			$start_time_fri = Input::get('start_time_fri');
			$end_time_fri = Input::get('end_time_fri');
			
			$start_time_sat = Input::get('start_time_sat');
			$end_time_sat = Input::get('end_time_sat');
			
			$start_time_sun = Input::get('start_time_sun');
			$end_time_sun = Input::get('end_time_sun');
			
			$start_date = Carbon::createFromFormat('d/m/Y', Input::get('start_date_weekly'));
			$end_date = Carbon::createFromFormat('d/m/Y', Input::get('end_date_weekly'));
			
			$current_date = $start_date;
			
			while($current_date->lte($end_date))
			{
				$current_day = strtolower($current_date->format('D'));
				switch($current_day)
				{
					case 'mon':
						if(!empty($start_time_mon) && !empty($end_time_mon))
						{
							$job_date = new JobDate();
							$job_date->job_id = $job->id;
							$job_date->date = $current_date;
							$job_date->start_time = Carbon::createFromFormat('H:i', $start_time_mon);
							$job_date->end_time = Carbon::createFromFormat('H:i', $end_time_mon);
							$job_date->save();
						}
						break;
					
					case 'tue':
						if(!empty($start_time_tue) && !empty($end_time_tue))
						{
							$job_date = new JobDate();
							$job_date->job_id = $job->id;
							$job_date->date = $current_date;
							$job_date->start_time = Carbon::createFromFormat('H:i', $start_time_tue);
							$job_date->end_time = Carbon::createFromFormat('H:i', $end_time_tue);
							$job_date->save();
						}
						break;
						
					case 'wed':
						if(!empty($start_time_wed) && !empty($end_time_wed))
						{
							$job_date = new JobDate();
							$job_date->job_id = $job->id;
							$job_date->date = $current_date;
							$job_date->start_time = Carbon::createFromFormat('H:i', $start_time_wed);
							$job_date->end_time = Carbon::createFromFormat('H:i', $end_time_wed);
							$job_date->save();
						}
						break;
						
					case 'thu':
						if(!empty($start_time_thu) && !empty($end_time_thu))
						{
							$job_date = new JobDate();
							$job_date->job_id = $job->id;
							$job_date->date = $current_date;
							$job_date->start_time = Carbon::createFromFormat('H:i', $start_time_thu);
							$job_date->end_time = Carbon::createFromFormat('H:i', $end_time_thu);
							$job_date->save();
						}
						break;
						
					case 'fri':
						if(!empty($start_time_fri) && !empty($end_time_fri))
						{
							$job_date = new JobDate();
							$job_date->job_id = $job->id;
							$job_date->date = $current_date;
							$job_date->start_time = Carbon::createFromFormat('H:i', $start_time_fri);
							$job_date->end_time = Carbon::createFromFormat('H:i', $end_time_fri);
							$job_date->save();
						}
						break;
						
					case 'sat':
						if(!empty($start_time_sat) && !empty($end_time_sat))
						{
							$job_date = new JobDate();
							$job_date->job_id = $job->id;
							$job_date->date = $current_date;
							$job_date->start_time = Carbon::createFromFormat('H:i', $start_time_sat);
							$job_date->end_time = Carbon::createFromFormat('H:i', $end_time_sat);
							$job_date->save();
						}
						break;
						
					case 'sun':
						if(!empty($start_time_sun) && !empty($end_time_sun))
						{
							$job_date = new JobDate();
							$job_date->job_id = $job->id;
							$job_date->date = $current_date;
							$job_date->start_time = Carbon::createFromFormat('H:i', $start_time_sun);
							$job_date->end_time = Carbon::createFromFormat('H:i', $end_time_sun);
							$job_date->save();
						}
						break;
				}
				
				$current_date = $current_date->addDay();
			}
			
			Session::put('start_time_mon', $start_time_mon);
			Session::put('end_time_mon', $end_time_mon);
			
			Session::put('start_time_tue', $start_time_tue);
			Session::put('end_time_tue', $end_time_tue);
			
			Session::put('start_time_wed', $start_time_wed);
			Session::put('end_time_wed', $end_time_wed);
			
			Session::put('start_time_thu', $start_time_thu);
			Session::put('end_time_thu', $end_time_thu);
			
			Session::put('start_time_fri', $start_time_fri);
			Session::put('end_time_fri', $end_time_fri);
			
			Session::put('start_time_sat', $start_time_sat);
			Session::put('end_time_sat', $end_time_sat);
			
			Session::put('start_time_sun', $start_time_sun);
			Session::put('end_time_sun', $end_time_sun);
			
			Session::put('start_date_weekly', Input::get('start_date_weekly'));
			Session::put('end_date_weekly', Input::get('end_date_weekly'));
		}
		$job->save();
		
		Session::forget('dates');
		
		return Redirect::to(action('JobsController@getReviewAllDates'));
	}
	
	public function getReviewAllDates()
	{
		$job_id = Session::get('job_id');
		if($job_id == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		$job = Job::find($job_id);
		if($job == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		$dates = $job->dates();
		if($dates->count() == 0)
			return Redirect::to(action('JobsController@getStep4'));
			
		$this->layout->content = View::make('frontend.jobs.jobs_layout')->nest('content', 'frontend.jobs.steps.review_dates', array(
			'job' => $job,
			'dates' => $dates
		));
	}
	public function postReviewAllDates()
	{
		$job_id = Session::get('job_id');
		if($job_id == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		$job = Job::find($job_id);
		if($job == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		if($job->person_id != Auth::user()->id)
		{
			Session::forget('job_id');
			return Redirect::to(action('JobsController@getStep1'));
		}
		
		$rules = array();
		
		$dates = $job->dates();
		foreach($dates as $date)
		{
			$rules['date_' . $date->id] = array('required', 'date_format:"d/m/Y"');
			$rules['start_time_' . $date->id] = array('required', 'date_format:"H:i"');
			$rules['end_time_' . $date->id] = array('required', 'date_format:"H:i"', 'time_start_end:start_time_' . $date->id);
		}

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(action('JobsController@getReviewAllDates'))
				->withErrors($validator)
				->withInput(Input::all());
		}
		
		//load last posted data
		foreach($dates as $date)
		{
			$date->date = Carbon::createFromFormat('d/m/Y', Input::get('date_' . $date->id));
			$date->start_time = Input::get('start_time_' . $date->id);
			$date->end_time = Input::get('end_time_' . $date->id);
			$date->notes = Input::get('notes_' . $date->id);
			$date->save();
		}
		
		$job->clearChildJobs();

		$pos_counter = 0;
		$counter = 0;
		$dates = $job->dates();
		$positions = $job->positions;
		foreach($positions as $position)
		{
			foreach($dates as $date)
			{
				if($counter == 0) //skip first one
				{
					$counter++;
					continue;
				}

				$newJob = $job->createChildJob();				
				$newPosition = $position->createDuplicate($newJob->id);
				$newDate = $date->createDuplicate($newJob->id, $job->id);
				
				if($newJob->project_budget == 'fixed_hourly_rate')
					$newJob->budget = $newJob->getDurationInHours() * $newJob->hourly_rate;
				$newJob->save();
			}
			
			if($pos_counter != 0)
				$position->delete();
			
			$pos_counter++;
		}	
		
		//delete all but first date
		$counter = 0;
		$dates = $job->dates();
		foreach($dates as $date)
		{
			if($counter == 0) //skip first one
			{
				$counter++;
				continue;
			}
			
			$date->delete();
		}
		if($job->project_budget == 'fixed_hourly_rate')
		{
			$job->budget = $job->getDurationInHours() * $job->hourly_rate;
			$job->save();
		}
		
		return Redirect::to(action('JobsController@getStep5'));
	}
	
	public function getStep5()
	{
		$job_id = Session::get('job_id');
		if($job_id == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		$job = Job::find($job_id);
		if($job == null)
			return Redirect::to(action('JobsController@getStep1'));
		
		if($job->person_id != Auth::user()->id)
		{
			Session::forget('job_id');
			return Redirect::to(action('JobsController@getStep1'));
		}
		
		$attachments = $job->attachments();
		$this->layout->content = View::make('frontend.jobs.jobs_layout')->nest('content', 'frontend.jobs.steps.step5', array(
			'job' => $job,
			'attachments' => $attachments
		));
	}
	public function postStep5()
	{
		return Redirect::to(url('jobs/preview-job'));
	}
	
	public function getPartialOneDayBooking()
	{
		$index = Input::get('index');
		if($index == null)
			return;
		
		$dates = Session::get('dates');
		$dates[$index] = array(
			'date' => '',
			'start_time' => '',
			'end_time' => ''
		);
		Session::put('dates', $dates);
	}
	public function postRemoveBookingDate()
	{
		$id = Input::get('id');
		
		$job_date = JobDate::find($id);
		if($job_date == null)
			return;
			
		$job_date->delete();
		return Response::make('');
	}
	public function postRemoveOneDayBooking()
	{
		$index = Input::get('index');
		if($index == null)
			return;
		
		$dates = Session::get('dates');
		if(isset($dates[$index]))
			unset($dates[$index]);

		Session::put('dates', $dates);
	}
	public function postUploadJobAttachment($dataTarget, $dataId, $jobId = null)
	{
		$job = null;
		$attachments = null;
		
		$attachment = new JobAttachment();
		$attachment->filename = Input::file('attachment')->getClientOriginalName();
		if($dataTarget == 'job_date')
		{
			Input::file('attachment')->move(
				public_path() . '/uploads_jobs/' . $jobId . '/dates/' . $dataId . '/', 
				Input::file('attachment')->getClientOriginalName()
			);
			
			$attachment->job_date_id = $dataId;
			$attachment->save();
			
			$jobDate = JobDate::find($dataId);
			$attachments = $jobDate->attachments();
			
			$job = Job::find($jobId);
			
			$html = (string)View::make('frontend.jobs.steps.review_dates_partial_attachments_list', array(
				'job' => $job,
				'attachments' => $attachments,
				'date' => $jobDate,
				'target' => $dataTarget
			));

			return Response::json(array(
				'success' => 'OK', 
				'html' => $html
			));
		}
		else
		{
			Input::file('attachment')->move(
				public_path() . '/uploads_jobs/' . $dataId . '/', 
				Input::file('attachment')->getClientOriginalName()
			);
			
			$attachment->job_id = $dataId;
			$attachment->save();
			
			$job = Job::find($dataId);
			$attachments = $job->attachments();
			
			$html = (string)View::make('frontend.jobs.steps.review_dates_partial_attachments_list', array(
				'job' => $job,
				'attachments' => $attachments,
				'target' => $dataTarget
			));
			
			return Response::json(array(
				'success' => 'OK', 
				'html' => $html
			));
		}
	}
	public function postRemoveAttachment()
	{
		$id = Input::get('data_id');
		$attachment = JobAttachment::find($id);
		if($attachment == null)
			return Response::json(array('success' => 'FAIL'));
			
		$attachment->delete();
		
		return Response::json(array('success' => 'OK'));
	}
	
	public function getPreviewJob()
	{
		$job_id = Session::get('job_id');
		if($job_id == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		$job = Job::find($job_id);
		if($job == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		if($job->person_id != Auth::user()->id)
		{
			Session::forget('job_id');
			return Redirect::to(action('JobsController@getStep1'));
		}
			
		if($job->confirmed)
			return Redirect::to(action('JobsController@getStep1'));
		
		$this->layout->content = View::make('frontend.jobs.steps.preview', array(
			'job' => $job
		));
	}
	public function postPreviewJob()
	{
		$job_id = Session::get('job_id');
		if($job_id == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		$job = Job::find($job_id);
		if($job == null)
			return Redirect::to(action('JobsController@getStep1'));
			
		if($job->person_id != Auth::user()->id)
		{
			Session::forget('job_id');
			return Redirect::to(action('JobsController@getStep1'));
		}
			
		if($job->confirmed)
			return Redirect::to(action('JobsController@getStep1'));
			
		if($job->project_budget != 'open_to_quotes')
		{
			$job->budget_lsp_only = (double)$job->budget * (1 - ((double)Config::get('pricing.commission') / 100)); //netto value for LSPs only
			if(!empty($job->hourly_rate))
				$job->hourly_rate_lsp_only = (double)$job->hourly_rate * (1 - ((double)Config::get('pricing.commission') / 100));
				
			foreach ($job->childJobs() as &$child_job)
			{
				$child_job->budget = $job->budget;
				$child_job->budget_lsp_only = $job->budget_lsp_only;
				if(!empty($child_job->hourly_rate))
					$child_job->hourly_rate_lsp_only = $job->hourly_rate_lsp_only;
					
				$child_job->save();
			}
		}
		
		foreach ($job->childJobs() as $child_job)
		{
			$child_job->confirmed = true;
			$child_job->save();
		}
		
		$job->confirmed = true;
		$job->save();
		
		if(!Session::has('existing_address_id')) //we inserted new address on step3 - save it as my new address
		{
			$address = new PersonAddress();
			$address->person_id = Auth::user()->id;			
			$address->address_1 = $job->address_1;
			$address->address_2 = $job->address_2;
			$address->city = $job->city;
			$address->postcode = $job->postcode;
			$address->county = $job->county;
			$address->country = $job->country;
			$address->save();
		}
		
		//sent notification to client
		Notification::sendNotificationTo(null, Auth::user()->id, $job->id, "You successfully posted a job - <strong>{JOB}</strong>!", 'jobs/view/{JOB_ID}', 'View/Manage job');
		
		//sent email to client
		$person = Auth::user();
		$person_name = $person->getName();
		$person_email = $person->email;
		
		$job_id = $job->id;
		$job_name = $job->getTitle();
		
		Mail::queue('emails.jobs.post_a_job', array('job_id' => $job_id, 'job_name' => $job_name, 'person_name' => $person_name), function($message) use ($person_name, $person_email)
		{
			$message
				->to($person_email, $person_name)
				->subject(Lang::get('translations.job_posting_subject'));
		});
		
		//sent email to admin
		$admins = Person::listAdmins();
		foreach ($admins as $admin)
		{
			$admin_name = $admin->getName();
			$admin_email = $admin->email;
			
			Mail::queue('emails.admin_only.post_a_job', array('job_id' => $job_id, 'job_name' => $job_name, 'person_name' => $person_name, 'admin_name' => $admin_name), function($message) use ($admin_name, $admin_email)
			{
				$message
					->to($admin_email, $admin_name)
					->subject(Lang::get('translations.job_posting_subject'));
			});
		}
		
		//sending to all relevant translators
		
		Session::forget('job_id');
		Session::forget('start_time_mon');
		Session::forget('end_time_mon');
		Session::forget('start_time_tue');
		Session::forget('end_time_tue');
		Session::forget('start_time_wed');
		Session::forget('end_time_wed');
		Session::forget('start_time_thu');
		Session::forget('end_time_thu');
		Session::forget('start_time_fri');
		Session::forget('end_time_fri');
		Session::forget('start_time_sat');
		Session::forget('end_time_sat');
		Session::forget('start_time_sun');
		Session::forget('end_time_sun');
		Session::forget('start_date_weekly');
		Session::forget('end_date_weekly');
		Session::forget('existing_address_id');
		
		Session::flash('success', Lang::get('translations.job_posting_message', array('job_title' => $job->getTitle())));
		return Redirect::to(action('JobsController@getView') . '/' . $job->id);
	}
	
	public function getCancel($id)
	{
		$job = Job::find($id);
		if($job == null)
			return Redirect::to(action('JobsController@getMyJobs'));
			
		if($job->status != 'awaiting_applications')
			return Redirect::to(action('JobsController@getMyJobs'));
			
		if($job->person_id != Auth::user()->id)
			return Redirect::to(action('JobsController@getMyJobs'));
		
		$job->status = 'canceled';
		$job->save();
		
		return Redirect::to(action('JobsController@getMyJobs'));
	}
	
	public function getView($id, $tab = 'details')
	{
		$job = Job::find($id);
		if($job == null)
			return Redirect::to(action('JobsController@getMyJobs'));
			
		if(!$job->confirmed)
			return Redirect::to(action('JobsController@getMyJobs'));
			
		$this->layout->content = View::make('frontend.jobs.view', array(
			'job' => $job,
			'tab' => $tab
		));
	}
	
	public function getMyJobs($type = 'opened')
	{
		$jobs = null;
		if(Auth::user()->account_type == 'client')
		{
			if($type == 'opened')
			{
				$jobs = Auth::user()
					->jobs()
					->where('status', '=', 'awaiting_applications')
					->where('confirmed', '=', '1')
					->get();
			}
			elseif($type == 'canceled')
			{
				$jobs = Auth::user()
					->jobs()
					->where('status', '=', 'canceled')
					->get();
			}
			elseif($type == 'confirmed')
			{
				$jobs = Auth::user()
					->jobs()
					->where('status', '=', 'confirmed')
					->get();
			}
			else
			{
				$jobs = Auth::user()
					->jobs()
					->where('status', '=', 'finished')
					->orWhere('status', '=', 'invoice_sent')
					->get();
			}
		}
		elseif(Auth::user()->account_type == 'translator')
		{
			$ids = null;
			if($type == 'opened')
			{
				$ids = DB::table('jobs')
					->join('jobs_applicants', 'jobs.id', '=', 'jobs_applicants.job_id')
					->where('jobs_applicants.person_id', '=', Auth::user()->id)
					->where('jobs.status', '=', 'awaiting_applications')
					->where('jobs.confirmed', '=', '1')
					->select(DB::raw('DISTINCT(jobs.id) AS id'))
					->lists('id');
			}	
			elseif($type == 'confirmed')
			{
				$ids = DB::table('jobs')
					->join('jobs_open_position', 'jobs.id', '=', 'jobs_open_position.job_id')
					->where('jobs_open_position.worker_id', '=', Auth::user()->id)
					->where('jobs.status', '=', 'confirmed')
					->select('jobs.id')
					->lists('id');
			}
			elseif($type == 'completed')
			{
				$ids = DB::table('jobs')
					->join('jobs_open_position', 'jobs.id', '=', 'jobs_open_position.job_id')
					->where('jobs_open_position.worker_id', '=', Auth::user()->id)
					->where(function($query)
					{
						$query->where('jobs.status', '=', 'finished')
							->orWhere('jobs.status', '=', 'invoice_sent');
					})
					->select('jobs.id')
					->lists('id');
			}
			else
			{
				$ids = DB::table('jobs')
					->join('jobs_open_position', 'jobs.id', '=', 'jobs_open_position.job_id')
					->where('jobs_open_position.worker_id', '=', Auth::user()->id)
					->where('jobs.status', '=', 'canceled')
					->select('jobs.id')
					->lists('id');
			}
			
			if(count($ids) > 0)			
				$jobs = Job::whereIn('id', $ids)->get();
			else
				$jobs = array();
		}
		
		$this->layout->content = View::make('frontend.jobs.my_jobs', array(
			'jobs' => $jobs,
			'type' => $type
		));
	}

	public function postApply($id)
	{
		$job = Job::find($id);
		if($job == null)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
			
		if($job->status != 'awaiting_applications')
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
			
		$budget = null;
		if($job->project_budget == 'open_to_quotes')
		{
			$validator = Validator::make(Input::all(), array(
				'budget' => array('required', 'numeric')
			));
			if($validator->fails())
				return Response::json(array(
					'status' => 'VALIDATION',
					'error' => $validator->errors()->first('budget')
				));
				
			$budget = Input::get('budget');
		}
		
		//user has already applied
		$application = JobApplication::where('job_id', '=', $id)
			->where('person_id', '=', Auth::user()->id)
			->first();
		if($application != null)
			return Response::json(array(
				'status' => 'CLOSE'
			));

		$application = new JobApplication();
		$application->job_id = $id;
		$application->person_id = Auth::user()->id;
		if($job->project_budget == 'open_to_quotes')
		{
			$application->budget = $budget;
			$application->budget_show = $budget * (1 + ((double)Config::get('pricing.commission') / 100)); //gross value
		}
		$application->save();
		
		Notification::sendNotificationTo(Auth::user()->id, $job->person_id, $job->id, Lang::get('translations.job_posting_notification'), 'jobs/view/{JOB_ID}/applicants', 'View applications');
		
		$job_name = $job->getTitle();
		
		//send to client
		$client = Person::find($job->person_id);
		$client_name = $client->getName();
		$client_email = $client->email;
		
		$worker = Auth::user();
		$worker_name = $worker->getName();
		$worker_email = $worker->email;
		
		$profile_image = 'http://' . $_SERVER['HTTP_HOST'] . $worker->getProfilePicture();
		$profile_link = 'http://' . $_SERVER['HTTP_HOST'] . '/profile/' . $worker->id;
		
		Mail::queue('emails.jobs.apply_for_the_job_to_client', array('job_id' => $id, 'job_name' => $job_name, 'worker_name' => $worker_name, 'client_name' => $client_name, 'profile_image' => $profile_image, 'profile_link' => $profile_link), function($message) use ($client_name, $client_email)
		{
			$message
				->to($client_email, $client_name)
				->subject(Lang::get('translations.job_posting_received_subject'));
		});
		
		//send to myself - worker
		Mail::queue('emails.jobs.apply_for_the_job_to_worker', array('job_id' => $id, 'job_name' => $job_name, 'person_name' => $worker_name), function($message) use ($worker_email, $worker_name)
		{
			$message
				->to($worker_email, $worker_name)
				->subject(Lang::get('translations.job_posting_send_subject'));
		});
		
		return Response::json(array(
			'success' => 'OK'
		));
	}
	
	public function postSendMessage($id)
	{
		$job = Job::find($id);
		if($job == null)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
			
		if($job->person_id != Auth::user()->id && !$job->hasUserApplied(Auth::user()->id))
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
			
		$validator = Validator::make(Input::all(), array(
			'message' => array('required')
		));
		if($validator->fails())
			return Response::json(array(
				'status' => 'VALIDATION',
				'error' => $validator->errors()->first('message')
			));
			
		$message = new JobMessage();
		$message->job_id = $id;
		$message->sender_id = Auth::user()->id;
		$message->message = Input::get('message');
		$message->save();
		
		$job_name = $job->getTitle();
		$person_name = $person->getName();

		//send notifications to client
		$person = Auth::user();
		if($person->id != $job->person_id)
		{
			$client = $job->person;
			Notification::sendNotificationTo(Auth::user()->id, $job->person_id, $job->id, Lang::get('translations.send_message_client_notification'), 'jobs/view/{JOB_ID}/messages', 'View messages');
			
			$client_name = $client->getName();
			$client_email = $client->email;
			
			Mail::queue('emails.jobs.job_message', array('job_id' => $id, 'job_name' => $job_name, 'person_name' => $person_name), function($message) use ($client_email, $client_name)
			{
				$message
					->to($client_email, $client_name)
					->subject(Lang::get('translations.send_message_subject'));
			});
		}
		
		//send notifications to all coworkers on job 
		foreach($job->taken_positions() as $position)
		{
			if($position->worker_id == $person->id)
				continue;
			
			Notification::sendNotificationTo(Auth::user()->id, $position->worker_id, $job->id, Lang::get('translations.send_message_coworkers_notification'), 'jobs/view/{JOB_ID}/messages', 'View messages');
			
			$coworker = $position->person;
			$coworker_email = $coworker->email;
			$coworker_name = $coworker->getName();
			
			Mail::queue('emails.jobs.job_message', array('job_id' => $id, 'job_name' => $job_name, 'person_name' => $person_name), function($message) use ($coworker_email, $coworker_name)
			{
				$message
					->to($coworker_email, $coworker_name)
					->subject(Lang::get('translations.send_message_subject'));
			});
		}
		
		return Response::json(array(
			'status' => 'OK'
		));
	}
	
	public function postHireLsp($id)
	{
		$job = Job::find($id);
		if($job == null)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
			
		if($job->person_id != Auth::user()->id)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
			
		$position = $job->positions()->whereNull('worker_id')->orderBy('id', 'ASC')->first();
		if($position == null)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
			
		$position->worker_id = Input::get('person_id');
		$position->save();
		
		//sent notification to worker
		Notification::sendNotificationTo(Auth::user()->id, $position->worker_id, $job->id, Lang::get('translations.you_were_hired_notification'), 'jobs/view/{JOB_ID}', 'View job');
		
		$worker = Person::find($position->worker_id);
		$worker->no_of_bookings += 1;
		$worker->save();
		
		$worker_email = $worker->email;
		$worker_name = $worker->getName();
		
		$job_name = $job->getTitle();
		
		Mail::queue('emails.jobs.hire_lsp', array('job_id' => $id, 'job_name' => $job_name), function($message) use ($worker_email, $worker_name)
		{
			$message
				->to($worker_email, $worker_name)
				->subject(Lang::get('translations.you_were_hired_subject'));
		});
			
		return Response::json(array(
			'status' => 'OK'
		));
	}
	public function postFireLsp($id)
	{
		if(!Auth::check())
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => '/'
			));
		
		$pass = Input::get('pass'); //password
		if(empty($pass))
			return Response::json(array(
				'status' => 'VALIDATION', 
				'validation' => 'Your password is empty!'
			));
		
		if(!Hash::check($pass, Auth::user()->password))
			return Response::json(array(
				'status' => 'VALIDATION', 
				'validation' => 'Your password is incorrect!'
			));
		
		$job = Job::find($id);
		if($job == null)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
			
		if($job->person_id != Auth::user()->id)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
		
		$positionId = Input::get('position_id');
		$position = JobOpenPosition::find($positionId);
		if($position == null)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
			
		$worker_id = $position->worker_id;
		$position->worker_id = null;
		$position->save();
		
		//send notification to fired coworker
		Notification::sendNotificationTo(null, $worker_id, $job->id, Lang::get('translations.you_were_removed_notification'), 'jobs/view/{JOB_ID}', 'View job');
		
		$job_name = $job->getTitle();
		
		$worker = Person::find($worker_id);
		$worker->no_of_bookings -= 1;
		$worker->save();
		
		$worker_email = $worker->email;
		$worker_name = $worker->getName();
		
		Mail::queue('emails.jobs.fire_lsp', array('job_id' => $id, 'job_name' => $job_name), function($message) use ($worker_email, $worker_name)
		{
			$message
				->to($worker_email, $worker_name)
				->subject(Lang::get('translations.you_were_removed_subject'));
		});
			
		return Response::json(array(
			'status' => 'OK'
		));
	}
	public function postDidNotAttend()
	{
		$position_id = Input::get('position_id');
		$text = Input::get('message');
		
		if(empty($position_id))
			return Response::json(array(
				'success' => 'FAILED'
			));
			
		$position = JobOpenPosition::find($position_id);
		if($position == null)
			return Response::json(array(
				'success' => 'FAILED'
			));
			
		$job = $position->job;
		if($job->person_id != Auth::user()->id)
			return Response::json(array(
				'success' => 'FAILED'
			));
			
		if(empty($text))
			return Response::json(array(
				'success' => 'VALIDATOR',
				'message' => 'Your opinion is required'
			));
			
		if(!$position->lspDidAttend())
			return Response::json(array(
				'success' => 'FAILED'
			));
			
		$jobAttending = new PersonJobAttending();
		$jobAttending->client_id = Auth::user()->id;
		$jobAttending->worker_id = $position->worker_id;
		$jobAttending->job_id = $job->id;
		$jobAttending->comment = $text;
		$jobAttending->save();
		
		$person = $position->person;
		$person->no_of_non_attendings += 1;
		$person->save();
		
		$lsp = Person::find($position->worker_id);
		$lsp_name = $lsp->getName();
		$lsp_email = $lsp->email;
		$lsp_id = $lsp->id;
		
		$client_id = Auth::user()->id;
		$client_email = Auth::user()->email;
		$client_name = Auth::user()->getName();
		
		$job_name = $job->getTitle();
		$job_id = $job->id;
		
		//send message to admin
		$admins = Person::listAdmins();
		foreach ($admins as $admin)
		{
			$admin_name = $admin->getName();
			$admin_email = $admin->email;
			
			Mail::queue('emails.admin_only.translator_did_not_attend', array('lsp_id' => $lsp_id, 'client_id' => $client_id, 'job_id' => $job_id, 'job_name' => $job_name, 'text' => $text, 'client_name' => $client_name, 'lsp_name' => $lsp_name), function($message) use ($admin_name, $admin_email)
			{
				$message
					->to($admin_email, $admin_name)
					->subject(Lang::get('translations.translator_did_not_attend_subject'));
			});
		}
		
		return Response::json(array(
			'success' => 'OK'
		));
	}
	
	public function postRequestOvertime($id)
	{
		$job = Job::find($id);
		if($job == null)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
			
		if($job->person_id == Auth::user()->id)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
			
		$rules = array(
			'hours' => array('required_without:minutes', 'numeric'),
			'minutes' => array('required_without:hours', 'numeric')
		);
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			$field = null;
			
			$response = array(
				'status' => 'VALIDATION',
				'error' => '',
				'field' => ''
			);
			
			if($validator->errors()->has('minutes'))
				$field = 'minutes';
			else if($validator->errors()->has('hours'))
				$field = 'hours';
				
			$response['error'] = $validator->errors()->first($field);
			$response['field'] = $field;
			
			return Response::json($response);
		}
		
		Notification::sendNotificationTo(Auth::user()->id, $job->person_id, $job->id, Lang::get('translations.request_overtime_notification'), 'jobs/view/{JOB_ID}/overtime', 'Accept/Reject');
		
		$worker = Auth::user();
		$worker_name = $worker->getName();
		
		$client = Person::find($job->person_id);
		$client_name = $client->getName();
		$client_email = $client->email;
		
		$job_name = $job->getTitle();
		
		Mail::queue('emails.jobs.request_overtime', array('job_id' => $id, 'job_name' => $job_name, 'worker_name' => $worker_name), function($message) use ($client_email, $client_name)
		{
			$message
				->to($client_email, $client_name)
				->subject(Lang::get('translations.request_overtime_subject'));
		});
			
		$overtime = new JobOvertime();
		$overtime->job_id = $id;
		$overtime->person_id = Auth::user()->id;
		$overtime->minutes = Input::get('minutes');
		$overtime->hours = Input::get('hours');
		$overtime->status = 'open';
		if($job->project_budget == 'fixed_hourly_rate')
			$overtime->hourly_rate = $job->budget;
		else
			$overtime->hourly_rate = Auth::user()->hourly_rate;
		$overtime->save();
		
		return Response::json(array(
			'status' => 'OK'
		));
	}
	public function postRejectOvertime($id)
	{
		$job = Job::find($id);
		if($job == null)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
			
		if($job->person_id != Auth::user()->id)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
		
		$overtime_id = Input::get('overtime_id');
		
		$overtime = JobOvertime::find($overtime_id);
		if($overtime == null)
			return Response::json(array(
				'status' => 'FAILED',
				'redirect' => url('jobs/view/') . '/' . $id . '/overtime'
			));
			
		$overtime->status = 'denied';
		$overtime->save();
		
		Notification::sendNotificationTo(Auth::user()->id, $overtime->person_id, $job->id, Lang::get('translations.rejected_overtime_notification'), 'jobs/view/{JOB_ID}/overtime', 'View overtime');
		
		$worker = Person::find($overtime->person_id);
		$worker_name = $worker->getName();
		$worker_email = $worker->email;
		
		$client = Auth::user();
		$client_name = $client->getName();
		
		$job_name = $job->getTitle();
		
		Mail::queue('emails.jobs.reject_overtime', array('job_id' => $id, 'job_name' => $job_name, 'client_name' => $client_name), function($message) use ($worker_email, $worker_name)
		{
			$message
				->to($worker_email, $worker_name)
				->subject(Lang::get('translations.rejected_overtime_subject'));
		});
			
		return Response::json(array(
			'status' => 'OK'
		));
	}
	public function postApproveOvertime($id)
	{
		$job = Job::find($id);
		if($job == null)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
			
		if($job->person_id != Auth::user()->id)
			return Response::json(array(
				'status' => 'FAILED', 
				'redirect' => url('jobs')
			));
		
		$overtime_id = Input::get('overtime_id');
		
		$overtime = JobOvertime::find($overtime_id);
		if($overtime == null)
			return Response::json(array(
				'status' => 'FAILED',
				'redirect' => url('jobs/view/') . '/' . $id . '/overtime'
			));
			
		$overtime->status = 'approved';
		$overtime->save();
		
		Notification::sendNotificationTo(Auth::user()->id, $overtime->person_id, $job->id, Lang::get('translations.approved_overtime_notification'), 'jobs/view/{JOB_ID}/overtime', 'View overtime');
		
		$job_name = $job->getTitle();
		
		$worker = Person::find($overtime->person_id);
		$worker_email = $worker->email;
		$worker_name = $worker->getName();
		
		$client = Auth::user();
		$client_name = $client->getName();
		Mail::queue('emails.jobs.approve_overtime', array('job_id' => $id, 'job_name' => $job_name, 'client_name' => $client_name), function($message) use ($worker_email, $worker_name)
		{
			$message
				->to($worker_email, $worker_name)
				->subject(Lang::get('translations.approved_overtime_subject'));
		});
			
		return Response::json(array(
			'status' => 'OK'
		));
	}
	
	public function postRateLsp($id)
	{
		if(empty(Input::get('stars')) && empty(Input::get('review')))
			return Response::json(array(
				'success' => 'OK'
			));
		
		$job = Job::find($id);
		if($job == null)
			return Response::json(array(
				'success' => 'OK'
			));
			
		if($job->person_id != Auth::user()->id)
			return Response::json(array(
				'success' => 'OK'
			));
			
		$position_id = (int)Input::get('data_position_id');
		if($position_id == 0)
			return Response::json(array(
				'success' => 'OK'
			));
			
		$position = JobOpenPosition::find($position_id);
		if($position == null)
			return Response::json(array(
				'success' => 'OK'
			));
			
		$review = PersonReview::where('job_id', '=', $job->id)
			->where('person_id', '=', $position->worker_id)
			->where('client_id', '=', Auth::user()->id)
			->first();
			
		if($review != null)
			return Response::json(array(
				'success' => 'OK'
			));
			
		$worker = Person::find($position->worker_id);
		if($worker == null)
			return Response::json(array(
				'success' => 'OK'
			));
			
		$review = new PersonReview();
		$review->job_id = $job->id;
		$review->person_id = $position->worker_id;
		$review->client_id = Auth::user()->id;
		$review->stars = Input::get('stars');
		$review->review = Input::get('review');
		$review->save();
		
		$worker->no_of_votes += 1;
		$worker->votes_sum += (int)Input::get('stars');
		$worker->save();
		
		return Response::json(array(
			'success' => 'OK'
		));
	}

	public function postResign($id)
	{
		$job = Job::find($id);
		if($job == null)
			return Response::json(array(
				'success' => 'OK'
			));
			
		$rules = array(
			'reason' => array('required')
		);
		
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Response::json(array(
				'success' => 'VALIDATION',
				'error' => $validator->errors()->first('message')
			));
		}
		
		$client = $job->person;
		$worker = Auth::user();	
		DB::transaction(function() use ($id, $client, $worker, $job)
		{
			//remove user as worker
			$position = JobOpenPosition::where('job_id', '=', $id)
				->where('worker_id', '=', Auth::user()->id)
				->first();
				
			if($position != null)
			{
				$position->worker_id = null;
				$position->save();
			}
		
			//remove his application
			$application = JobApplication::where('job_id', '=', $id)
				->where('person_id', '=', Auth::user()->id)
				->first();
			
			if($application != null)
				$application->delete();
				
			//insert resign
			$resign = new ResignLog();
			$resign->person_id = Auth::user()->id;
			$resign->job_id = $id;
			$resign->reason = Input::get('reason');
			$resign->ip = $_SERVER['REMOTE_ADDR'];
			$resign->save();
			
			//sent notification to client
			Notification::sendNotificationTo($worker->id, $client->id, $job->id, Lang::get('translations.translator_resigned_notification'), 'jobs/view/{JOB_ID}/applicants', 'View applicants');

			//put job on the market
			$job->status = 'awaiting_applications';
			$job->save();
		});
		
		$job_name = $job->getTitle();
		
		$worker_name = $worker->getName();
		
		$client_email = $client->email;
		$client_name = $client->getName();
		
		Mail::queue('emails.jobs.worker_resigned', array('job_id' => $id, 'job_name' => $job_name, 'worker_name' => $worker_name), function($message) use ($client_email, $client_name)
		{
			$message
				->to($client_email, $client_name)
				->subject(Lang::get('translations.translator_resigned_subject'));
		});
		
		return Response::json(array(
			'success' => 'OK'
		));
	}

	public function postSendRecommendation($id)
	{
		$job = Job::find($id);
		if($job == null)
			return Response::json(array(
				'success' => 'OK'
			));
			
		$rules = array(
			'email' => array('required', 'email'),
			'message' => array('required')
		);
		
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Response::json(array(
				'success' => 'VALIDATION',
				'error' => $validator->errors()->first('message')
			));
		}
		
		$job_name = $job->getTitle();
		
		$email = Input::get('email');
		
		$worker = Auth::user();
		$worker_name = $worker->getName();
		
		$text = Input::get('message');
		Mail::queue('emails.jobs.send_recommendation', array('job_id' => $id, 'job_name' => $job_name, 'worker_name' => $worker_name, 'text' => $text), function($message) use ($email)
		{
			$message
				->to($email)
				->subject(Lang::get('translations.job_recommendation'));
		});
		
		return Response::json(array(
			'success' => 'OK'
		));
	}

	public function postAtwFormSent()
	{
		$id = Input::get('id');
		if(empty($id))
			return Response::json(array('success' => 'FAIL'));
			
		$invoice = Invoice::find($id);
		if(empty($invoice))
			return Response::json(array('success' => 'FAIL'));
			
		$invoice->atw_form_sent = true;
		$invoice->atw_form_sent_at = Carbon::now();
		$invoice->save();
		
		$client_id = Auth::user()->id;
		$client_name = Auth::user()->getName();
		
		$job = Job::find($invoice->job_id);
		$job_id = $job->id;
		$job_name = $job->getTitle();
		
		//send a mail to admin
		$admins = Person::listAdmins();
		foreach ($admins as $admin)
		{
			$admin_name = $admin->getName();
			$admin_email = $admin->email;
			
			Mail::queue('emails.admin_only.client_atw_form_sent', array('client_id' => $client_id, 'job_id' => $job_id, 'job_name' => $job_name, 'client_name' => $client_name), function($message) use ($admin_name, $admin_email)
			{
				$message
					->to($admin_email, $admin_name)
					->subject(Lang::get('translations.client_atw_form_sent'));
			});
		}
			
		return Response::json(array('success' => 'OK', 'html' => 'Thank you!'));
	}
}