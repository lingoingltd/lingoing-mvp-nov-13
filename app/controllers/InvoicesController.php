<?php
use \Carbon\Carbon;

class InvoicesController extends BaseController {

	protected $layout = 'layouts.frontend';
	
	private function initGoCardless()
	{
		if(App::environment() == 'production')
			GoCardless::$environment = "production";
		else
			GoCardless::$environment = "sandbox";

		$credentials = array(
			'app_id' => Config::get('pricing.gocardless_app_id'),
			'app_secret' => Config::get('pricing.gocardless_app_secret'),
			'merchant_id' => Config::get('pricing.gocardless_merchant_id'),
			'access_token' => Config::get('pricing.gocardless_access_token')
		);
		
		GoCardless::set_account_details($credentials);
	}
	
	public function getMyInvoices($type = 'unpaid')
	{
		$invoices = null;
		
		if(Auth::user()->account_type == 'client')
		{
			if($type == 'unpaid')
			{
				$invoices = Auth::user()
					->unpaid_invoices();
			}
			elseif($type == 'paid')
			{
				$invoices = Auth::user()
					->paid_invoices();
			}
			elseif($type == 'pending')
			{
				$invoices = Auth::user()
					->pending_invoices();
			}
		}
		else
		{
			if($type == 'unpaid')
			{
				$invoices = Invoice::where('worker_id', '=', Auth::user()->id)
					->where('status_worker', '=', 'unpaid')
					->get();
			}
			else
			{
				$invoices = Invoice::where('worker_id', '=', Auth::user()->id)
					->where('status_worker', '=', 'paid')
					->get();
			}
		}
		
		$this->layout->content = View::make('frontend.invoices.my_invoices', array(
			'type' => $type,
			'invoices' => $invoices
		));
	}
	
	public function getDownload($invoice_id)
	{
		$invoice = Invoice::find($invoice_id);
		if($invoice == null)
			return Redirect::to(url('my-invoices'));
			
		if($invoice->person_id != Auth::user()->id && $invoice->worker_id != Auth::user()->id && !(bool)Auth::user()->admin)
			return Redirect::to(url('my-invoices'));
			
		$package = $invoice->atwSupportPackage();
		if(empty($package))
		{
			if(Auth::user()->account_type == 'client' || (bool)Auth::user()->admin)
				$invoice->renderPDF()->Output('invoice-' . $invoice->number . '.pdf', 'D');
			else
				$invoice->renderPDFForLSP()->Output('invoice-' . $invoice->number . '.pdf', 'D');
		}
		else
			$invoice->renderPDFWithAtwClaimForm()->Output('invoice-' . $invoice->number . '.pdf', 'D');
	}

	public function getPay($invoice_id, $card_type)
	{
		$invoice = Invoice::find($invoice_id);
		if($invoice == null)
			return Redirect::to(url('my-invoices'));
			
		if($invoice->person_id != Auth::user()->id)
			return Redirect::to(url('my-invoices'));
			
		if($invoice->status == 'paid')
			return Redirect::to(url('my-invoices'));
		
		if($card_type == 'debit')
		{
			//only for next request
			Session::flash('debit_invoice_id', $invoice_id);
			
			$this->initGoCardless();
			
			$state = md5(rand(1, 9999999999) . microtime(true));
			$payment_details = array(
				'amount' => number_format($invoice->total_cost, 2, '.', ""),
				'name' => "Lingoing.com's invoice " . $invoice->number,
				'redirect_uri' => url('invoices/debit-confirmed'),
				'cancel_uri' => url('invoices/debit-canceled'),
				'state' => $state
			);
			Session::put("gocardless_state", $state);

			$bill_url = GoCardless::new_bill_url($payment_details);
			
			$this->layout->content = View::make('frontend.invoices.debit.pay_debit', array(
				'card_type' => $card_type,
				'invoice' => $invoice,
				'bill_url' => $bill_url
			));
		}
		elseif($card_type == 'credit')
		{
			$this->layout->content = View::make('frontend.invoices.credit.pay_credit', array(
				'card_type' => $card_type,
				'invoice' => $invoice
			));
		}
		else
			return Redirect::to(url('my-invoices'));
	}
	public function postPay($invoice_id, $card_type)
	{
		$invoice = Invoice::find($invoice_id);
		if($invoice == null)
			return Redirect::to(url('my-invoices'));
			
		if($invoice->person_id != Auth::user()->id)
			return Redirect::to(url('my-invoices'));
			
		if($invoice->status != 'unpaid')
			return Redirect::to(url('my-invoices'));
		
		if($card_type == 'credit')
		{
			Stripe::setApiKey(Config::get('pricing.stripe_secret_api_key'));
			
			$token = Input::get('stripeToken');
			
			try
			{
				$charge = Stripe_Charge::create(array(
				  "amount" => round((float)$invoice->total_cost * 100, 0), // amount in cents, again
				  "currency" => "gbp",
				  "card" => $token,
				  "description" => "Lingoing.com's invoice " . $invoice->number)
				);
				
				$invoice->status = 'paid';
				$invoice->paid_date = Carbon::now();
				$invoice->save();
				
				Notification::sendNotificationTo(null, Auth::user()->id, $invoice->job_id, "You just <strong>paid</strong> an invoice number <strong>" . $invoice->number . "</strong>.", 'my-invoices', 'My invoices');
				
				//send mail to client
				$worker = Auth::user();
				$worker_email = $worker->email;
				$worker_name = $worker->getName();
				
				Mail::queue('emails.invoice.invoice_paid', array('invoice' => $invoice), function($message) use ($worker_email, $worker_name)
				{
					$message
						->to($worker_email, $worker_name)
						->subject('Invoice paid at Lingoing.com');
				});
				
				//send info to financial officers
				$financials = Person::listFinancialTeam();
				foreach($financials as $email => $name)
				{
					Mail::queue('emails.admin_only.invoice_paid_stripe', array('invoice' => $invoice), function($message) use($email, $name)
					{
						$message
							->to($email, $name)
							->subject('Lingoing.com: Invoice was just paid via Stripe');
					});
				}
			} 
			catch(Stripe_CardError $e)
			{
				//log the error
				Log::error($e->getMessage());
				
				//send error to every developer
				$developers = Person::listDevelopmentTeam();
				foreach($developers as $email => $name)
				{
					$error = $e->getMessage();
					
					Mail::queue('emails.common.text', array('text' => $error), function($message) use ($email, $name)
					{
						$message
							->to($email, $name)
							->subject('Lingoing.com: Error during paying with credit card via Stripe');
					});
				}
				
			  	$this->layout->content = View::make('frontend.invoices.credit_card_error', array(
					'card_type' => $card_type,
					'invoice' => $invoice,
					'e' => $e
				));
				
				return;
			}
			
			$this->layout->content = View::make('frontend.invoices.invoice_successfully_paid', array(
				'card_type' => $card_type,
				'invoice' => $invoice
			));
		}
		else
			return Redirect::to(url('my-invoices'));
	}
	
	public function getDebitConfirmed()
	{
		$this->initGoCardless();
			
		//state check
		if(Input::get('state') != Session::get('gocardless_state'))
		{
			Session::forget('gocardless_state');
			return Redirect::to(url('my-invoices'));
		}
		
		// Required confirm variables
		$confirm_params = array(
		  'resource_id'    => Input::get('resource_id'),
		  'resource_type'  => Input::get('resource_type'),
		  'resource_uri'   => Input::get('resource_uri'),
		  'signature'      => Input::get('signature')
		);
		
		// State is optional
		if (Input::has('state'))
		  $confirm_params['state'] = Input::get('state');
		
		$confirmed_resource = GoCardless::confirm_resource($confirm_params);
		
		$invoice_id = (int)Session::get('debit_invoice_id');
		if($invoice_id == 0)
			return Redirect::to(url('my-invoices'));
			
		$invoice = Invoice::find($invoice_id);
		if($invoice == null)
			return Redirect::to(url('my-invoices'));
			
		if($invoice->person_id != Auth::user()->id)
			return Redirect::to(url('my-invoices'));
		
		$invoice->gocardless_id = $confirmed_resource->id;
		$invoice->save();
		
		$this->layout->content = View::make('frontend.invoices.invoice_successfully_paid', array(
			'invoice' => $invoice
		));
	}
	public function getDebitCancel()
	{
		//state check
		if(Input::get('state') != Session::get('gocardless_state'))
		{
			Session::forget('gocardless_state');
			return Redirect::to(url('my-invoices'));
		}
		
		$invoice_id = (int)Session::get('debit_invoice_id');
		if($invoice_id == 0)
			return Redirect::to(url('my-invoices'));
			
		$invoice = Invoice::find($invoice_id);
		if($invoice == null)
			return Redirect::to(url('my-invoices'));
			
		if($invoice->person_id != Auth::user()->id)
			return Redirect::to(url('my-invoices'));
		
		$this->layout->content = View::make('frontend.invoices.debit.cancel_payment', array(
			'invoice' => $invoice
		));
	}
	
	public function anyDebitWebhooks()
	{
		try {
			$this->initGoCardless();

			$webhook = file_get_contents('php://input');
			$webhook_array = json_decode($webhook, true);
			$webhook_valid = GoCardless::validate_webhook($webhook_array['payload']);

			if ($webhook_valid == TRUE)
			{
				$data = $webhook_array['payload'];

				if($data['resource_type'] == 'bill')
				{
					$action = $data['action'];
					$bill_status = $action;
					
					if($action == 'created')
						$bill_status = 'pending';
					
					foreach($data['bills'] as $bill)
					{
						$bill_id = $bill['id'];

						$invoice = Invoice::where('gocardless_id', '=', $bill_id)->first();
						
						$client = Person::find($invoice->person_id);
						$client_name = $client->getName();
						$client_email = $client->email;
						
						if($bill_status == 'paid' || $bill_status == 'pending' || $bill_status == 'failed' || $bill_status == 'cancelled' || $bill_status == 'chargedback')
						{
							$invoice->status = $bill_status;
							if($bill_status == 'paid')
								$invoice->paid_date = Carbon::now();
							else
								$invoice->paid_date = null;
							$invoice->save();			
						}

						//send notifications
						switch($bill_status)
						{
							case "paid":
								Mail::queue('emails.invoice.invoice_paid', array('invoice' => $invoice), function($message) use ($client_email, $client_name)
								{
									$message
										->to($client_email, $client_name)
										->subject('Lingoing.com: You successfully paid an invoice');
								});
								
								$financials = Person::listFinancialTeam();
								foreach($financials as $email => $name)
								{
									Mail::queue('emails.admin_only.invoice_paid', array('invoice' => $invoice), function($message) use ($email, $name)
									{
										$message
											->to($email, $name)
											->subject('Lingoing.com: Invoice has been paid via GoCardless');
									});
								}
								break;

							case "pending":
								Mail::queue('emails.invoice.invoice_pending', array('invoice' => $invoice), function($message) use ($client_email, $client_name)
								{
									$message
										->to($client_email, $client_name)
										->subject('Lingoing.com: Your payment is pending');
								});
								
								$financials = Person::listFinancialTeam();
								foreach($financials as $email => $name)
								{
									Mail::queue('emails.admin_only.invoice_pending', array('invoice' => $invoice), function($message) use ($email, $name)
									{
										$message
											->to($email, $name)
											->subject('Lingoing.com: Invoice payment is pending via GoCardless');
									});
								}
								break;

							case "failed":
								Mail::queue('emails.invoice_failed', array('invoice' => $invoice), function($message) use ($client_email, $client_name)
								{
									$message
										->to($client_email, $client_name)
										->subject('Lingoing.com: Your payment failed');
								});
								
								$financials = Person::listFinancialTeam();
								foreach($financials as $email => $name)
								{
									Mail::queue('emails.admin_only.invoice_failed', array('invoice' => $invoice), function($message) use ($email, $name)
									{
										$message
											->to($email, $name)
											->subject('Lingoing.com: Invoice payment failed via GoCardless');
									});
								}
								break;

							case "cancelled":
								Mail::queue('emails.invoice_canceled', array('invoice' => $invoice), function($message) use ($client_email, $client_name)
								{
									$message
										->to($client_email, $client_name)
										->subject('Lingoing.com: You canceled the payment');
								});
								
								$financials = Person::listFinancialTeam();
								foreach($financials as $email => $name)
								{
									Mail::queue('emails.admin_only.invoice_canceled', array('invoice' => $invoice), function($message) use ($email, $name)
									{
										$message
											->to($email, $name)
											->subject('Lingoing.com: Invoice payment has been canceled via GoCardless');
									});
								}
								break;

							case "chargedback":
								Mail::queue('emails.invoice_chargedback', array('invoice' => $invoice), function($message) use ($client_email, $client_name)
								{
									$message
										->to($client_email, $client_name)
										->subject('Lingoing.com: Your payment has been chargedback');
								});
								
								$financials = Person::listFinancialTeam();
								foreach($financials as $email => $name)
								{
									Mail::queue('emails.admin_only.invoice_chargedback', array('invoice' => $invoice), function($message) use ($email, $name)
									{
										$message
											->to($email, $name)
											->subject('Lingoing.com: Invoice payment has been chargedback via GoCardless');
									});
								}
								break;

							default:
								$financials = Person::listFinancialTeam();
								foreach($financials as $email => $name)
								{
									Mail::queue('emails.admin_only.invoice_unknown_status_gocardless', array('invoice' => $invoice, 'status' => $bill_status), function($message) use ($email, $name)
									{
										$message
											->to($email, $name)
											->subject('Lingoing.com: Payment via GoCardless respond with unknown status');
									});
								}
								break;
						}
					}
				}

				header('HTTP/1.1 200 OK');
				die();
			}
			else
			{
				header('HTTP/1.1 403 Invalid signature');
				die();
			}
		} 
		catch (Exception $e) 
		{
			//log the error
			Log::error($e->getMessage());
			
			$error = $e->getMessage();
			
			//send error to every developer
			$developers = Person::listDevelopmentTeam();
			foreach($developers as $email => $name)
			{
				Mail::queue('emails.common.text', array('text' => $error), function($message) use ($email, $name)
				{
					$message
						->to($email, $name)
						->subject('Lingoing.com: Error during processing GoCardless webhook');
				});
			}
		}
	}
}