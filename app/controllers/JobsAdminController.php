<?php
use \Carbon\Carbon;

class JobsAdminController extends BaseController {

	protected $layout = 'layouts.admin';
	
	public function getJobs()
	{
		$jobs = Job::all();
		
		$this->layout->content = View::make('admin.jobs.list', array(
			'jobs' => $jobs
		));
	}
	public function getChange($id)
	{
		$job = Job::find($id);
		if($job == null)
			return Redirect::to(url('admin/jobs/jobs'));
			
		$this->layout->content = View::make('admin.jobs.change', array(
			'job' => $job,
			'job_id' => $id
		));
	}
	public function postChange($id)
	{
		$job = Job::find($id);
		if($job == null)
			return Redirect::to(url('admin/jobs/jobs'));
		
		$new_payment_method = Input::get('payment_method');	
		if($new_payment_method != $job->payment_method)
		{
			//send mail to job's owner
		}
		
		$job->payment_method = $new_payment_method;
		$job->save();
			
		return Redirect::to(url('admin/jobs/change') . '/' . $id);
	}
	public function postChangeConfirmed()
	{
		$id = Input::get('id');
		if(empty($id))
			return Response::json(array('success' => 'FAIL'));
			
		$status = Input::get('status');
		
		$job = Job::find($id);
		if($job == null)
			return Response::json(array('success' => 'FAIL'));
			
		$job->confirmed = $status;
		$job->save();
		return Response::json(array('success' => 'OK'));
	}
	public function postSendAtwForm()
	{
		$id = Input::get('id');
		if(empty($id))
			return Response::json(array('success' => 'FAIL'));
			
		$invoice = Invoice::find($id);
		if($invoice == null)
			return Response::json(array('success' => 'FAIL'));
			
		$invoice->lingoing_atw_form_sent = true;
		$invoice->lingoing_atw_form_sent_at = Carbon::now();
		$invoice->save();
		
		return Response::json(array('success' => 'OK', 'html' => 'Yes (' . Carbon::now()->format('d/m/Y H:i') . ')'));
	}
	
}