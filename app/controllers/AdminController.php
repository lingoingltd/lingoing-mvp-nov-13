<?php
use \Illuminate\Support\MessageBag;

class AdminController extends BaseController {

	protected $layout = 'layouts.admin';
	
	public function getLogin()
	{
		$this->layout->content = View::make('admin.login');
	}
	public function postLogin()
	{
		$rules = array(
			'email' => array('required', 'email'),
			'password' => array('required', 'min:4', 'max:40')
		);
		
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(action('AdminController@getLogin'))
				->withErrors($validator)
				->withInput(Input::except('password'));
		}
		
		if(!Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password'), 'enabled' => true, 'admin' => true, 'confirmed' => true)))
		{
			return Redirect::to(action('AdminController@getLogin'))
				->withErrors(new MessageBag(array('email' => 'Username or password is invalid.')))
				->withInput(Input::except('password'));
		}		
		return Redirect::to(action('AdminController@getDashboard'));
	}
	public function getDashboard()
	{
		$this->layout->content = View::make('admin.dashboard');
	}
	
}