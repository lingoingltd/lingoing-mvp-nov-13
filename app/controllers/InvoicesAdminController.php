<?php
use \Illuminate\Support\MessageBag;

class InvoicesAdminController extends BaseController {
	
	protected $layout = 'layouts.admin';

	public function getLspInvoices($status = 'unpaid')
	{
		$invoices = null;
		if($status == 'unpaid')
		{
			$invoices = Invoice::where('status_worker', '=', 'unpaid')
				->orWhere('status_worker', '=', 'pending')
				->orderBy('status_worker', 'ASC')
				->orderBy('payment_due', 'ASC')
				->get();
		}
		else
		{
			$invoices = Invoice::where('status_worker', '=', 'paid')
				->orderBy('payment_due', 'ASC')
				->get();
		}
		
		$this->layout->content = View::make('admin.invoices.list_lsp', array(
			'invoices' => $invoices
		));
	}

	public function getInvoices($status = 'unpaid')
	{
		$invoices = null;
		if($status == 'unpaid')
		{
			$invoices = Invoice::where('status', '=', 'unpaid')
				->orWhere('status', '=', 'pending')
				->orderBy('status', 'ASC')
				->orderBy('payment_due', 'ASC')
				->get();
		}
		else
		{
			$invoices = Invoice::where('status', '=', 'paid')
				->orderBy('payment_due', 'ASC')
				->get();
		}
		
		$this->layout->content = View::make('admin.invoices.list_client', array(
			'invoices' => $invoices
		));
	}
	
	public function postMarkAsPaid()
	{
		$id = Input::get('invoice_id');
		$invoice = Invoice::find($id);
		if($invoice == null)
			return "";
			
		if(Input::get('field') == 'status')
			$invoice->status = 'paid';
		elseif(Input::get('field') == 'status_worker')
			$invoice->status_worker = 'paid';
			
		$invoice->save();
		return "";
	}
}