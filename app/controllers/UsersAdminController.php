<?php
use \Illuminate\Support\MessageBag;

class UsersAdminController extends BaseController {

	protected $layout = 'layouts.admin';

	public function getClients()
	{
		$users = Person::where('account_type', '=', 'client')->get();
		
		$this->layout->content = View::make('admin.users.list_clients', array(
			'users' => $users
		));
	}
	public function getTranslators()
	{
		$users = Person::where('account_type', '=', 'translator')->get();
		
		$this->layout->content = View::make('admin.users.list_translators', array(
			'users' => $users
		));
	}
	public function getSalesRep()
	{
		$users = Person::whereRaw('referer_code IS NOT NULL')->get();
		
		$this->layout->content = View::make('admin.users.list_sales_rep', array(
			'users' => $users
		));
	}
	
	public function getLoginAsUser($id)
	{
		if(!Auth::check() || !(bool)Auth::user()->admin)
			return Redirect::to(url('/'));
		
		$person = Person::find($id);
		if($person == null)
			return Redirect::to(url($_SERVER['HTTP_REFERER']));
			
		Auth::loginUsingId($id);
		return Redirect::to(url('dashboard'));
	}
	
	public function postEnableSalesRep()
	{
		$personId = Input::get('person_id');
		if(empty($personId))
			return Response::json(array('status' => 'FAIL'));
		
		$person = Person::find($personId);
		if(empty($person))
			return Response::json(array('status' => 'FAIL'));
			
		if(empty($person->referer_code))
		{
			$default_address = $person->default_address();
			$person->referer_code = strtoupper(substr($default_address->first_name, 0, 1) . substr($default_address->last_name, 0, 1) . rand(1000, 9999));

			//make sure it is unique
			while(true)
			{
				$oldPerson = Person::where('referer_code', '=', $person->referer_code)
					->first();
					
				if(empty($oldPerson))
					break;
					
				$person->referer_code = strtoupper(substr($default_address->first_name, 0, 1) . substr($default_address->last_name, 0, 1) . rand(1000, 9999));
			}
		}
		$person->sales_rep_enabled = true;
		$person->save();
			
		return Response::json(array('status' => 'OK', 'html' => 'Yes (<a href="'. url('register') . '/' . $person->referer_code . '">View register URL</a>)'));
	}
	public function postChangeSalesRepStatus()
	{
		$personId = Input::get('personId');
		$status = Input::get('status');
		
		$person = Person::find($personId);
		if(empty($person))
			return Response::json(array('status' => 'FAIL'));
			
		$person->sales_rep_enabled = ($status == 'Y');
		$person->save();
		
		return Response::json(array('status' => 'OK'));
	}
	
	public function getProfile($id)
	{
		$person = Person::find($id);
		if($person == null)
			return Redirect::to(url('admin/admin/dashboard'));
			
		$insurance_documents = $person->documents('insurance');
		$crb_documents = $person->documents('crb');
		
		$defaultAddress = $person->default_address();
		$billingAddress = $person->billing_address();
			
		$this->layout->content = View::make('admin.users.profile_' . $person->account_type, array(
			'person' => $person,
			'insurance_documents' => $insurance_documents,
			'crb_documents' => $crb_documents,
			'defaultAddress' => $defaultAddress,
			'billingAddress' => $billingAddress
		));
	}
	
	public function postConfirmAccount()
	{
		$id = Input::get('account_id');
		
		$person = Person::find($id);
		if($person == null)
			return Response::json(array(
				'response' => 'FAIL'
			));
		
		$person->confirmed = true;
		$person->save();
		
		$body = Input::get('message');
		$body = str_replace('{NAME}', $person->getName(), $body);
		
		$person_name = $person->getName();
		$person_email = $person->email;
		
		Mail::queue('emails.common.text', array('text' => $body), function($message) use ($person_name, $person_email)
		{
			$message
				->to($person_email, $person_name)
				->subject('Your account on Lingoing.com has been confirmed!');
		});
		
		return Response::json(array(
			'response' => 'OK'
		));
	}
	public function postSendMessage()
	{
		$id = Input::get('account_id');
		
		$person = Person::find($id);
		if($person == null)
			return Response::json(array(
				'response' => 'FAIL'
			));
		
		$person_name = $person->getName();
		$person_email = $person->email;
		
		Mail::queue('emails.common.text', array('text' => Input::get('message')), function($message) use ($person_name, $person_email)
		{
			$message
				->to($person_email, $person_name)
				->subject('Message from Lingoing.com');
		});
		
		return Response::json(array(
			'response' => 'OK'
		));
	}
	
	public function getAtwPackages($id)
	{
		$person = Person::find($id);
		if($person == null)
			return Redirect::to('/admin/users/clients');
			
		$packages = $person->atw_packages();
			
		$this->layout->content = View::make('admin.users.atw_packages', array(
			'person' => $person,
			'packages' => $packages
		));
	}
	public function getAtwPackage($id)
	{
		$package = AtwSupportPackage::find($id);
		if($package == null)
			return Redirect::to('/admin/users/clients');
			
		$person = Person::find($package->person_id);
		if($person == null)
			return Redirect::to('/admin/users/clients');
			
		$address = PersonAddress::find($package->address_id);
			
		$this->layout->content = View::make('admin.users.atw_package', array(
			'person' => $person,
			'package' => $package,
			'address' => $address
		));
	}
	public function postAtwPackage($id)
	{
		$package = AtwSupportPackage::find($id);
		if($package == null)
			return Redirect::to('/admin/users/clients');
			
		$person = Person::find($package->person_id);
		if($person == null)
			return Redirect::to('/admin/users/clients');
			
		$person_name = $person->getName();
		$person_email = $person->email;
			
		$postData = Input::all();
		if(isset($postData['verify_package'])) //hack because Input::has returns false is variable is empty
		{
			$package->status = 'verified';
			$package->save();

			//send email to customer
			Mail::queue('emails.profile.atw_approved', array(), function($message) use ($person_name, $person_email)
			{
				$message
					->to($person_email, $person_name)
					->subject('Your AtW support package has been verified');
			});
		}
		elseif(isset($postData['cancel_package']))
		{
			$package->status = 'canceled';
			$package->save();

			//send email to customer
			Mail::queue('emails.profile.atw_canceled', array(), function($message) use ($person_name, $person_email)
			{
				$message
					->to($person_email, $person_name)
					->subject('Your AtW support package has been canceled');
			});
		}
		elseif(isset($postData['in_review']))
		{
			$package->status = 'in_review';
			$package->save();

			//send email to customer
			Mail::queue('emails.profile.atw_in_review', array(), function($message) use ($person_name, $person_email)
			{
				$message
					->to($person_email, $person_name)
					->subject('Your AtW support package has been "In review"');
			});
		}
		
		return Redirect::to(url('admin/users/atw-package', array('id' => $package->id)));
	}
	public function getPackageDownloadAttachment($id)
	{
		$package = AtwSupportPackage::find($id);
		if($package == null)
			return Redirect::to('/admin/users/clients');
			
		$person = Person::find($package->person_id);
		if($person == null)
			return Redirect::to('/admin/users/clients');
			
		$pathToFile = public_path() . '/uploads/' . $person->id . '/atw/' . $package->id . '/' . $package->filename;
		$headers = array(
			'Content-Length: ' . filesize($pathToFile),
			'Content-Type: ' . $package->filename_mime_type,
			'Content-Disposition: attachment; filename="' . $package->filename . '"'
		);
		
		return Response::download($pathToFile, $package->filename, $headers);
	}
}