<?php
class ClientController extends BaseController {

	protected $layout = 'layouts.frontend';
	
	public function getMyClients($type = null)
	{
		$this->layout->content = View::make('frontend.clients.my_clients', array(
			'clients' => Auth::user()->clients(),
			'type' => $type
		));
	}
	public function getMyProfessionals($type = null)
	{
		$professionals = null;
		if($type == 'preferred')
			$professionals = Auth::user()->preferred_list();
		elseif($type == 'blocked')
			$professionals = Auth::user()->blocked_list();
		else
			$professionals = Auth::user()->professionals();
		
		$this->layout->content = View::make('frontend.professionals.my_professionals', array(
			'professionals' => $professionals,
			'type' => $type,
			'blocked_list' => Auth::user()->blocked_list()->lists('client_lsp_id'),
			'preferred_list' => Auth::user()->preferred_list()->lists('client_lsp_id')
		));
	}
}