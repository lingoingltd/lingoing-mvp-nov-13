<?php
use \Carbon\Carbon;
use \WideImage\WideImage;
use \Aws\S3\Enum\CannedAcl;
use \Illuminate\Support\Collection;
use \Illuminate\Support\MessageBag;

class FrontPageController extends BaseController {

	protected $layout = 'layouts.frontend';
	
	public function getAgreeToCookies()
	{
		$cookie = Cookie::forever('lingoing_cookies', 'agreed');
		
		$response = Response::json(array('status' => 'OK'));
		return $response->withCookie($cookie);
	}
		
	public function getAccountNotApproved()
	{
		$this->layout->content = View::make('frontend.user.account_not_confirmed');
	}
	public function getProfileNotCompleted()
	{
		$this->layout->content = View::make('frontend.user.profile_not_completed');
	}

	public function getRegister($code = null)
	{
		$data = array();
		
		$referer = null;
		if(!empty($code))
		{
			$invitation = PersonSendInvitation::where('token', '=', $code)
				->first();
				
			if($invitation != null && !(bool)$invitation->registered)
			{
				$person = Person::find($invitation->person_id);
				
				$data = array(
					'first_name' => $invitation->first_name,
					'last_name' => $invitation->last_name,
					'ref_code' => $person->referer_code,
					'email' => $invitation->email
				);
				
				Session::put('register_referer', $code);
			}
		}
		
		$this->layout->content = View::make('frontend.user.register.register', $data);
	}
	public function postRegister()
	{
		$rules = array(
			'first_name' => array('required'),
			'last_name' => array('required'),
			'email' => array('required', 'email', 'unique:persons,email'),
			'password' => array('required', 'min:4', 'max:40'),
			'confirm_password' => array('required', 'min:4', 'max:40', 'same:password'),
			'terms_agreed' => array('accepted')
		);
		
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(url('register'))
				->withErrors($validator)
				->withInput(Input::flashExcept('password', 'confirm_password'));
		}
		
		$person = Person::where('email', '=', Input::get('email'))->first();
		if($person != null)
		{
			if(!empty($person->facebook_id)) //user logedin via facebook
			{
				return Redirect::to(url('register'))
					->withErrors(new MessageBag(array('email' => Lang::get('translations.login_via_facebook'))))
					->withInput(Input::flashExcept('password', 'confirm_password'));
			}
			elseif(!empty($person->linkedin_id))
			{
				return Redirect::to(url('register'))
					->withErrors(new MessageBag(array('email' => Lang::get('translations.login_via_linkedin'))))
					->withInput(Input::flashExcept('password', 'confirm_password'));
			}
			else
			{
				return Redirect::to(url('register'))
					->withErrors(new MessageBag(array('email' => Lang::get('translations.email_already_registered'))));
			}
		}
		
		$ref_code = Input::get('ref_code');
		$token = Session::get('register_referer');
		$invitation = PersonSendInvitation::where('token', '=', $token)
			->first();
	
		$person = new Person();
		$person->email = Input::get('email');
		$person->password = Hash::make(Input::get('password'));
		$person->account_type = Input::get('account_type');
		$person->newsletter = true;
		$person->terms_agreed = (Input::get('terms_agreed') == 'yes');
		$person->enabled = true;
		$person->confirmed = false;
		if(!empty($invitation)) //user register via register url
		{
			$person->referer_id = $invitation->person_id;
			
			$invitation->registered = true;
			$invitation->save();
		}
		elseif(!empty($ref_code)) //user inserted ref code
		{
			$salesRep = Person::where('referer_code', '=', $ref_code)
				->first();
			
			if($salesRep != null)
				$person->referer_id = $salesRep->id;
		}
		$person->save();
		
		$address = new PersonAddress();
		$address->address_type = 'default';
		$address->person_id = $person->id;
		$address->first_name = Input::get('first_name');
		$address->last_name = Input::get('last_name');
		$address->organization_name = Input::get('business_name');
		$address->save();
		
		$backup = new PersonalDataBackup();
		$backup->person_id = $person->id;
		$backup->first_name = Input::get('first_name');
		$backup->last_name = Input::get('last_name');
		$backup->email = Input::get('email');
		$backup->save();
		
		$person_name = $address->getName();
		$person_email = $person->email;
		Mail::queue('emails.frontend.welcome', array('person_name' => $person_name), function($message) use ($person_email, $person_name)
		{
			$message
				->to($person_email, $person_name)
				->subject();
		});
		
		Notification::sendNotificationTo(null, $person->id, null, Lang::get('translations.welcome_notification_message'));

		Auth::login($person, true);
		Session::forget('register_referer');
		
		return Redirect::to(action('FrontPageController@getProfileStep1'));
	}
	
	public function getLogin()
	{
		$this->layout->content = View::make('frontend.user.login');
	}
	public function postLogin()
	{
		$rules = array(
			'email' => array('required'),
			'password' => array('required', 'min:4', 'max:40')
		);
		
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to('/login')
				->withErrors($validator)
				->withInput(Input::flashExcept('password'));
		}
		
		if(!Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password'), 'enabled' => true), true))
		{
			return Redirect::to('/login')
				->withErrors(new MessageBag(array('email' => Lang::get('translations.username_password_invalid'))))
				->withInput(Input::flashExcept('password'));
		}
		
		if(Auth::user()->profile_completed)
		{
			if((bool)Auth::user()->confirmed)
				return Redirect::to(url('dashboard'));
			else
				return Redirect::to(url('my-profile'));
		}
		else
			return Redirect::to(url('my-profile'));
	}
	
	public function getLogout()
	{
		Session::flush();
		Auth::logout();
		return Redirect::to('/');
	}
	
	public function getForgotPassword()
	{
		$this->layout->content = View::make('frontend.user.forgot_password');
	}
	public function postForgotPassword()
	{
		$rules = array(
			'email' => array('required', 'email', 'max:150')
		);
		
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(action('FrontPageController@getForgotPassword'))
				->withErrors($validator)
				->withInput();
		}
		
		$person = Person::where('email', '=', Input::get('email'))
			->first();
		
		if($person == null)
		{
			return Redirect::to(action('FrontPageController@getForgotPassword'))
				->withErrors(new MessageBag(array('email' => Lang::get('translations.email_not_registered'))))
				->withInput();
		}
		
		$credentials = array(
			'email' => Input::get('email')
		);

		Input::flash();
	    Password::remind($credentials, function($message, $user)
		{
		    $message->subject(Lang::get('translations.password_remind_subject'));
		
			$this->layout->content = View::make('frontend.user.forgot_password_confirm');
		});
	}
	
	public function getPasswordReset($token)
	{
		$reminder = PasswordReminder::where('token', '=', $token)
			->first();
			
		if($reminder == null)
			$this->layout->content = View::make('frontend.user.password_reset_expired');
		else
		{
			$expiring = Carbon::createFromFormat('Y-m-d H:i:s', $reminder->created_at)->addDays(1);
			if($expiring->lt(Carbon::now()))
				$this->layout->content = View::make('frontend.user.password_reset_expired');
			else
			{
				$this->layout->content = View::make('frontend.user.password_reset', array(
					'token' => $token
				));
			}
		}
	}
	public function postPasswordReset($token)
	{
		$rules = array(
			'email' => array('required', 'email', 'max:150'),
			'password' => array('required', 'min:4', 'max:40'),
			'password_confirmation' => array('required', 'same:password', 'min:4', 'max:40')
		);
		
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to('password-reset/' . $token)
				->withErrors($validator)
				->withInput(Input::flashExcept('password', 'password_confirmation'));
		}
		
		$credentials = array(
	        'email' => Input::get('email'),
	        'password' => Input::get('password'),
	        'password_confirmation' => Input::get('password_confirmation')
	    );

	    $this->layout->content = Password::reset($credentials, function($person, $password)
	    {
	        $person->password = Hash::make($password);
	        $person->save();

	        return View::make('frontend.user.password_reset_confirm', array(
	        	'person_name' => $person->getName()
	        ));
	    });
	}
	
	public function getProfileStep1()
	{
		$default_address = Auth::user()->default_address();
		if($default_address == null)
		{
			$default_address = new PersonAddress();
			$default_address->person_id = Auth::user()->id;
			$default_address->address_type = 'default';
			$default_address->save();
		}
		
		$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.profile_step1', array(
			'default_address' => $default_address
		));
	}
	public function postProfileStep1()
	{
		$rules = array(
			'title' => array('required'),
			'first_name' => array('required'),
			'last_name' => array('required'),
			'mobile_number' => array('required'),
			'address_1' => array('required'),
			'city' => array('required'),
			'country' => array('required'),
			'postcode' => array('required')
		);
		
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(action('FrontPageController@getProfileStep1'))
				->withErrors($validator)
				->withInput(Input::flashExcept('password'));
		}
		
		$person = Auth::user();
		
		if(!empty(Input::get('password')))
			$person->password = Hash::make(Input::get('password'));
			
		$person->job_title = Input::get('job_title');
		$person->profile_step1 = true;
		$person->save();
		
		$default_address = Auth::user()->default_address();
		if($default_address == null)
		{
			$default_address = new PersonAddress();
			$default_address->person_id = Auth::user()->id;
			$default_address->address_type = 'default';
		}
		$default_address->title = Input::get('title');
		$default_address->first_name = Input::get('first_name');
		$default_address->last_name = Input::get('last_name');
		$default_address->mobile_number = Input::get('mobile_number');
		$default_address->address_1 = Input::get('address_1');
		$default_address->address_2 = Input::get('address_2');
		$default_address->city = Input::get('city');
		$default_address->country = Input::get('country');
		$default_address->county = Input::get('county');
		$default_address->postcode = Input::get('postcode');
		
		$default_address->save();
		
		if(Auth::user()->account_type == 'client')
		{
			if((bool)$person->profile_completed)
				return Redirect::to(action('FrontPageController@getProfileStep1'));
			else
			{
				if(Input::get('different_billing_address') == 'yes')
				{
					Session::put('wizard', 'true');
					Session::put('different_billing_address', 'yes');
					
					$billing_address = Auth::user()->billing_address();
					if($billing_address == null)
					{
						Session::put('address_type', 'billing');
						return Redirect::to(action('FrontPageController@getChangeAddress'));
					}
					else
						return Redirect::to(action('FrontPageController@getChangeAddress') . '/' . $billing_address->id);
				}
				else
				{
					Session::forget('different_billing_address');
					return Redirect::to(action('FrontPageController@getProfileStep3'));
				}
			}
		}
		else
		{
			if((bool)$person->profile_completed)
				return Redirect::to(action('FrontPageController@getProfileStep1'));
			else
				return Redirect::to(action('FrontPageController@getProfileStep2'));
		}
	}

	public function getProfileStep2()
	{
		$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.profile_step2_' . Auth::user()->account_type);
	}
	public function postProfileStep2()
	{
		$person = Auth::user();
		$complete_profile = $person->profile_completed;
		if($person->account_type == 'client')
		{			
			//else validate and save
			$rules = array(
				'title' => array('required'),
				'first_name' => array('required'),
				'last_name' => array('required'),
				'mobile_number' => array('required'),
				'address_1' => array('required'),
				'city' => array('required'),
				'postcode' => array('required'),
				'country' => array('required')
			);
			
			$validator = Validator::make(Input::all(), $rules);
			if($validator->fails())
			{
				return Redirect::to(action('FrontPageController@getProfileStep2'))
					->withErrors($validator)
					->withInput();
			}
			
			$person->billing_title = Input::get('title');
			$person->billing_first_name = Input::get('first_name');
			$person->billing_last_name = Input::get('last_name');
			$person->billing_job_title = Input::get('job_title');
			$person->billing_mobile_number = Input::get('mobile_number');
			$person->billing_mobile_sms_only = Input::get('mobile_sms_only') == 'yes';
			$person->billing_phone_number = Input::get('phone_number');
			$person->billing_fax_number = Input::get('fax_number');
			$person->billing_organization_name = Input::get('billing_organization_name');
			$person->billing_reference = Input::get('billing_reference');
			$person->billing_address_1 = Input::get('address_1');
			$person->billing_address_2 = Input::get('address_2');
			$person->billing_city = Input::get('city');
			$person->billing_postcode = Input::get('postcode');
			$person->billing_county = Input::get('county');
			$person->billing_country = Input::get('country');
			$person->profile_step2 = true;
			$person->profile_completed = true;
			$person->save();
			
			if(!$complete_profile)
			{				
				//send all admins
				$admins = Person::listAdmins();
				foreach ($admins as $admin)
				{
					$admin_name = $admin->getName();
					$admin_email = $admin->email;
					
					$person_name = Auth::user()->getName();
					
					Mail::queue('emails.admin_only.user_completed_profile', array('person_name' => $person_name), function($message) use ($admin_name, $admin_email)
					{
						$message
							->to($admin_email, $admin_name)
							->subject(Lang::get('translations.profile_completed_subject'));
					});
				}
				
				Session::flash('success', Lang::get('translations.profile_completed_message'));
				return Redirect::to(url('account-not-approved'));
			}
			else
				return Redirect::to(action('FrontPageController@getProfileStep2'));
		}
		elseif($person->account_type == 'translator')
		{
			$rules = array();
			
			$membership_bodies = array(
				'sli' => Person::listSpokenLanguageMembershipBodies(),
				'sli2' => Person::listSignLanguageMembershipBodies(),
				'lipspeaker' => Person::listSpokenLanguageMembershipBodies(),
				'sttr' => Person::listSpokenLanguageMembershipBodies(),
				'slt' => Person::listSignLanguageMembershipBodies(),
				'notetaker' => Person::listSpokenLanguageMembershipBodies()
			);
			
			$sli = (Input::get('sli') == 'yes');
			if($sli)
			{
				$bodies = $membership_bodies['sli'];
				foreach($bodies as $body)
				{
					$body = strtolower($body);
					if(!empty(Input::get('sli_'.$body.'_reference_number')) || !empty(Input::get('sli_'.$body.'_expiry_date')) || count(Input::get('sli_'.$body.'_languages')) > 0)
					{
						$rules['sli_'.$body.'_reference_number'] = array('required');
						$rules['sli_'.$body.'_expiry_date'] = array('required', 'date_format:"d/m/Y"', 'date_not_in_past');
						$rules['sli_'.$body.'_languages'] = array('required');
					}
				}
			}
			
			$sli2 = (Input::get('sli2') == 'yes');
			if($sli2)
			{
				$bodies = $membership_bodies['sli2'];
				foreach($bodies as $body)
				{
					$body = strtolower($body);
					if(!empty(Input::get('sli2_'.$body.'_reference_number')) || !empty(Input::get('sli2_'.$body.'_expiry_date')) || count(Input::get('sli2_'.$body.'_languages')) > 0)
					{
						$rules['sli2_'.$body.'_reference_number'] = array('required');
						$rules['sli2_'.$body.'_expiry_date'] = array('required', 'date_format:"d/m/Y"', 'date_not_in_past');
						$rules['sli2_'.$body.'_languages'] = array('required');
					}
				}
			}
			
			$lipspeaker = (Input::get('lipspeaker') == 'yes');
			if($lipspeaker)
			{
				$bodies = $membership_bodies['lipspeaker'];
				foreach($bodies as $body)
				{
					$body = strtolower($body);
					if(!empty(Input::get('lipspeaker_'.$body.'_reference_number')) || !empty(Input::get('lipspeaker_'.$body.'_expiry_date')) || count(Input::get('lipspeaker_'.$body.'_languages')) > 0)
					{
						$rules['lipspeaker_'.$body.'_reference_number'] = array('required');
						$rules['lipspeaker_'.$body.'_expiry_date'] = array('required', 'date_format:"d/m/Y"', 'date_not_in_past');
						$rules['lipspeaker_'.$body.'_languages'] = array('required');
					}
				}
			}
			
			$sttr = (Input::get('sttr') == 'yes');
			if($sttr)
			{
				$bodies = $membership_bodies['sttr'];
				foreach($bodies as $body)
				{
					$body = strtolower($body);
					if(!empty(Input::get('sttr_'.$body.'_reference_number')) || !empty(Input::get('sttr_'.$body.'_expiry_date')) || count(Input::get('sttr_'.$body.'_languages')) > 0)
					{
						$rules['sttr_'.$body.'_reference_number'] = array('required');
						$rules['sttr_'.$body.'_expiry_date'] = array('required', 'date_format:"d/m/Y"', 'date_not_in_past');
						$rules['sttr_'.$body.'_languages'] = array('required');
					}
				}
			}
			
			$slt = (Input::get('slt') == 'yes');
			if($slt)
			{
				$bodies = $membership_bodies['slt'];
				foreach($bodies as $body)
				{
					$body = strtolower($body);
					if(!empty(Input::get('slt_'.$body.'_reference_number')) || !empty(Input::get('slt_'.$body.'_expiry_date')) || count(Input::get('slt_'.$body.'_languages')) > 0)
					{
						$rules['slt_'.$body.'_reference_number'] = array('required');
						$rules['slt_'.$body.'_expiry_date'] = array('required', 'date_format:"d/m/Y"', 'date_not_in_past');
						$rules['slt_'.$body.'_languages'] = array('required');
					}
				}
				
			}
			
			$notetaker = (Input::get('notetaker') == 'yes');
			if($notetaker)
			{
				$bodies = $membership_bodies['notetaker'];
				foreach($bodies as $body)
				{
					$body = strtolower($body);
					if(!empty(Input::get('notetaker_'.$body.'_reference_number')) || !empty(Input::get('notetaker_'.$body.'_expiry_date')) || count(Input::get('notetaker_'.$body.'_languages')) > 0)
					{
						$rules['notetaker_'.$body.'_reference_number'] = array('required');
						$rules['notetaker_'.$body.'_expiry_date'] = array('required', 'date_format:"d/m/Y"', 'date_not_in_past');
						$rules['notetaker_'.$body.'_languages'] = array('required');
					}
				}
			}
			
			//validation
			$validator = Validator::make(Input::all(), $rules);
			if($validator->fails())
			{
				return Redirect::to(action('FrontPageController@getProfileStep2'))
					->withErrors($validator)
					->withInput();
			}
			
			$validTypes = array();
			
			if($sli) $validTypes[] = 'sli';
			if($sli2) $validTypes[] = 'sli2';
			if($lipspeaker) $validTypes[] = 'lipspeaker';
			if($sttr) $validTypes[] = 'sttr';
			if($slt) $validTypes[] = 'slt';
			if($notetaker) $validTypes[] = 'notetaker';
				
			foreach($validTypes as $type)
			{
				$bodies = $membership_bodies[$type];
				foreach($bodies as $body)
				{					
					$membership_field = $type . '_'.strtolower($body).'_id';
					$ref_number_field = $type . '_'.strtolower($body).'_reference_number';
					$expiry_date_field = $type . '_'.strtolower($body).'_expiry_date';
					$languages_field = $type . '_'.strtolower($body).'_languages';
					
					if(!empty(Input::get($ref_number_field)) || !empty(Input::get($expiry_date_field)) || count(Input::get($languages_field)) > 0)
					{
						$id = (int)Input::get($membership_field);
						
						$membership = null;
						if($id > 0)
						{
							$membership = PersonMembership::find($id);
							if((bool)$membership->expired) //if it's expired, we need to create a new one
								$membership = null;
						}
						
						if($membership == null)
						{
							$membership = new PersonMembership();
							$membership->person_id = Auth::user()->id;
							$membership->type = $type;
						}
						$membership->membership_body = $body;
						$membership->reference_number = Input::get($ref_number_field);
						$membership->expiry_date = Carbon::createFromFormat('d/m/Y', Input::get($expiry_date_field));
						$membership->save();
						
						$membership->clearLanguages();
						
						$languages = Input::get($languages_field);
						foreach($languages as $language)
						{
							$membershipLanguage = new PersonMembershipLanguage();
							$membershipLanguage->membership_id = $membership->id;
							$membershipLanguage->language = $language;
							$membershipLanguage->save();
						}
					}
				}
			}
						
			
			$person->clearSpecialisms();
			$specialisms = Input::get('specialisms', array());
			foreach($specialisms as $specialism)
			{
				$newSpecialism = new PersonSpecialism();
				$newSpecialism->person_id = $person->id;
				$newSpecialism->specialism = $specialism;
				$newSpecialism->save();
			}
			
			$person->spoken_language_interpreter = $sli;
			$person->sign_language_interpreter = $sli2;
			$person->lipspeaker = $lipspeaker;
			$person->speech_to_text_reporter = $sttr;
			$person->sign_language_translator = $slt;
			$person->note_taker = $notetaker;
			$person->profile_step3 = true;
			$person->save();

			if(!(bool)$person->profile_completed || $person->account_type == 'translator')
				return Redirect::to(action('FrontPageController@getProfileStep3')); //we should redirect user for uploading a insurance documents
			else
				return Redirect::to(action('FrontPageController@getProfileStep2'));
		}
	}

	public function getProfileStep3()
	{
		$data = array();
		if(Auth::user()->account_type == 'translator')
		{
			$data['insurance_documents'] = Auth::user()->documents('insurance');
			$data['crb_documents'] = Auth::user()->documents('crb');
			$data['memberships'] = Auth::user()->memberships();
		}
		
		$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.profile_step3_' . Auth::user()->account_type, $data);
	}
	public function postProfileStep3()
	{
		$person = Auth::user();
		
		if(Auth::user()->account_type == 'client')
		{
			$rules = array(
				'how_to_pay_bills' => array('required')
			);

			$validator = Validator::make(Input::all(), $rules);
			if($validator->fails())
			{
				return Redirect::to(url('profile_step3'))
					->withErrors($validator)
					->withInput(Input::all());
			}
			
			$profileCompleted = $person->profile_completed;
			
			$person->how_to_pay_bills = Input::get('how_to_pay_bills');
			if($person->how_to_pay_bills == 'paying_card')
				$person->profile_completed = true;
			$person->save();
			
			if($person->how_to_pay_bills == 'atw_customer')
				return Redirect::to(action('FrontPageController@getChangeAtw'));
			else
			{
				if(!$profileCompleted && $person->profile_completed)
				{
					Session::flash('success', Lang::get('translations.profile_completed_message'));
					return Redirect::to(action('FrontPageController@getProfileStep1'));
				}
				else
					return Redirect::to(action('FrontPageController@getProfileStep3'));
			}
		}
	}
	
	public function getProfileStep4()
	{
		$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.profile_step4_' . Auth::user()->account_type);
	}
	public function postProfileStep4()
	{
		if(Auth::user()->account_type == 'translator')
		{
			$person = Auth::user();
			$person->experience = Input::get('experience');
			$person->accreditations = Input::get('accreditations');		
			$person->profile_step4 = true;
			$person->save();
			
			if((bool)$person->profile_completed)
				return Redirect::to(action('FrontPageController@getProfileStep4'));
			else //if profile is not completed, we move through wizard
				return Redirect::to(action('FrontPageController@getProfileStep5'));
		}
	}
	
	public function getProfileStep5()
	{
		$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.profile_step5');
	}
	public function postProfileStep5()
	{
		$rules = array(
			'hourly_rate' => array('required', 'numeric'),
			'callout_time' => array('required'),
			'payment_method' => array('required'),
			'payment_period' => array('required')
		);
		
		$payment_method = Input::get("payment_method");
		if($payment_method == "paypal")
			$rules["paypal_email"] = array("required");
		elseif($payment_method == "bacs")
		{
			$rules["bacs_account_number"] = array("required");
			$rules["bacs_sort_code"] = array("required");
		}
			
		
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(action('FrontPageController@getProfileStep5'))
				->withErrors($validator)
				->withInput();
		}
		
		$person = Auth::user();
		$complete_profile = $person->profile_completed;
		
		$person->hourly_rate = Input::get('hourly_rate');
		$person->min_callout = Input::get('callout_time');
		$person->payment_type = Input::get('payment_method');
		$person->payment_period = Input::get('payment_period');
		$person->bacs_account_number = Input::get('bacs_account_number');
		$person->bacs_sort_code = Input::get('bacs_sort_code');
		$person->paypal_email = Input::get('paypal_email');
		$person->use_lingoing_address = (bool)Input::get('lingoing_address');
		$person->profile_completed = true;
		$person->profile_step5 = true;
		$person->save();
		
		if(!$complete_profile)
		{
			//send all admins
			$admins = Person::listAdmins();
			foreach ($admins as $admin)
			{
				$person_name = $person->getName();
				
				$admin_name = $admin->getName();
				$admin_email = $admin->email;
				
				Mail::queue('emails.admin_only.user_completed_profile', array('person_name' => $person_name), function($message) use ($admin_email, $admin_name)
				{
					$message
						->to($admin_email, $admin_name)
						->subject(Lang::get('translations.profile_completed_subject'));
				});
			}
			
			Session::flash('success', Lang::get('translations.profile_completed_message'));
			return Redirect::to(url('account-not-approved'));
		}
		else
			return Redirect::to(action('FrontPageController@getProfileStep5'));
	}

	public function getProfileStep6()
	{
		$organization = Auth::user()->organization;
		
		if($organization == null || $organization->person_id == Auth::user()->id) //if there is no organization or logedin person is admin
		{
			$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.profile_step6_edit', array(
				'organization' => $organization
			));
		}
		else
		{
			$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.profile_step6_show', array(
				'organization' => $organization
			));
		}
	}
	public function postProfileStep6()
	{
		$action = Input::get('action');
		if($action == 'add_new')
		{
			$rules = array(
				'name' => array('required'),
				'address_1' => array('required'),
				'city' => array('required'),
				'postcode' => array('required'),
				'country' => array('required'),
				'phone' => array('required')
			);
			
			if(Input::get('vat_registered') == 'yes')
				$rules['vat_number'] = array('required');
				
			$validator = Validator::make(Input::all(), $rules);
			if($validator->fails())
			{
				return Redirect::to(action('FrontPageController@getProfileStep6'))
					->withErrors($validator)
					->withInput();
			}
			
			$organization = new Organization();
			$organization->name = Input::get('name');
			$organization->address_1 = Input::get('address_1');
			$organization->address_2 = Input::get('address_2');
			$organization->city = Input::get('city');
			$organization->postcode = Input::get('postcode');
			$organization->county = Input::get('county');
			$organization->country = Input::get('country');
			$organization->phone = Input::get('phone');
			$organization->fax = Input::get('fax');
			$organization->organization_type = Input::get('organization_type');
			$organization->registration_number = Input::get('registration_number');
			$organization->vat_registered = Input::get('vat_registered') == 'yes';
			$organization->vat_number = Input::get('vat_number');
			$organization->person_id = Auth::user()->id;
			$organization->save();
			
			Auth::user()->organization_id = $organization->id;
			Auth::user()->save();
			
			return Redirect::to(action('FrontPageController@getProfileStep6'));
		}
		elseif($action == 'add_existing')
		{
			$rules = array(
				'organization' => array('required')
			);
			
			$validator = Validator::make(Input::all(), $rules);
			if($validator->fails())
			{
				return Redirect::to(action('FrontPageController@getProfileStep6'))
					->withErrors($validator)
					->withInput();
			}
			
			Auth::user()->organization_id = Input::get('organization');
			Auth::user()->save();
			
			return Redirect::to(action('FrontPageController@getProfileStep6'));
		}
		elseif($action == 'remove_organization')
		{
			Auth::user()->organization_id = null;
			Auth::user()->save();
			
			return Redirect::to(action('FrontPageController@getProfileStep6'));
		}
		elseif($action == 'update_remove')
		{
			if(!empty(Input::get('update'))) //update button
			{
				
				$organization = Auth::user()->organization;
				$organization->name = Input::get('name');
				$organization->address_1 = Input::get('address_1');
				$organization->address_2 = Input::get('address_2');
				$organization->city = Input::get('city');
				$organization->postcode = Input::get('postcode');
				$organization->county = Input::get('county');
				$organization->country = Input::get('country');
				$organization->phone = Input::get('phone');
				$organization->fax = Input::get('fax');
				$organization->organization_type = Input::get('organization_type');
				$organization->registration_number = Input::get('registration_number');
				$organization->vat_registered = Input::get('vat_registered') == 'yes';
				$organization->vat_number = Input::get('vat_number');
				$organization->person_id = Auth::user()->id;
				$organization->save();
				
				return Redirect::to(action('FrontPageController@getProfileStep6'));
			}
			elseif(!empty(Input::get('remove'))) //remove button
			{
				Auth::user()->organization_id = null;
				Auth::user()->save();

				return Redirect::to(action('FrontPageController@getProfileStep6'));
			}
		}
	}

	public function getMyAddressBook()
	{
		$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.my_address_book');
	}
	public function getChangeAddress($id = null)
	{
		$address = null;
		if($id != null)
		{
			$address = PersonAddress::where('id', '=', $id)
				->where('person_id', '=', Auth::user()->id)
				->first();
		}
			
		$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.change_address', array(
			'address_id' => $id,
			'address' => $address
		));
	}
	public function postChangeAddress($id = null)
	{
		$address = null;
		if($id != null)
		{
			$address = PersonAddress::where('id', '=', $id)
				->where('person_id', '=', Auth::user()->id)
				->first();
		}
		else
		{
			$address = new PersonAddress();
			$address->person_id = Auth::user()->id;
			$address->save();
		}
			
		$rules = array(
			'title' => array('required'),
			'first_name' => array('required_with:last_name'),
			'last_name' => array('required_with:first_name'),
			'organization_name' => array('required_without:first_name,last_name'),
			'mobile_number' => array('required'),
			'address_1' => array('required'),
			'city' => array('required'),
			'country' => array('required'),
			'postcode' => array('required')
		);

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(url('change-address', array('id' => $id)))
				->withErrors($validator)
				->withInput(Input::all());
		}
		
		$address->title = Input::get('title');
		$address->first_name = Input::get('first_name');
		$address->last_name = Input::get('last_name');
		$address->organization_name = Input::get('organization_name');
		$address->address_1 = Input::get('address_1');
		$address->address_2 = Input::get('address_2');
		$address->city = Input::get('city');
		$address->country = Input::get('country');
		$address->county = Input::get('county');
		$address->postcode = Input::get('postcode');
		$address->mobile_number = Input::get('mobile_number');
		$address->phone_number = Input::get('phone_number');
		$address->fax_number = Input::get('fax_number');
		$address->billing_reference = Input::get('billing_reference');
		if($address->address_type != 'default')
		{
			if(Input::get('billing_address') == 'yes' || Session::get('address_type') == 'billing')
				$address->address_type = 'billing';
		}
		$address->save();
		
		Session::forget('address_type');
		if(Session::get('wizard') == 'true')
			return Redirect::to(action('FrontPageController@getProfileStep3'));
		else
			return Redirect::to(url('my-address-book'));
	}
	public function getRemoveAddress($id)
	{
		$address = PersonAddress::where('id', '=', $id)
			->where('person_id', '=', Auth::user()->id)
			->first();
			
		if($address == null)
			return Redirect::to(url('my-address-book'));
			
		if($address->address_type == 'default')
			return Redirect::to(url('my-address-book'));
			
		$address->delete();
		return Redirect::to(url('my-address-book'));
	}

	public function getAtwPackages()
	{
		$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.atw_support_packages');
	}
	public function getChangeAtw($id = null)
	{
		$package = null;
		if($id != null)
		{
			$package = AtwSupportPackage::where('id', '=', $id)
				->where('person_id', '=', Auth::user()->id)
				->first();
		}
		
		$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.change_atw', array(
			'package_id' => $id,
			'package' => $package
		));
	}
	public function postChangeAtw($id = null)
	{
		$package = null;
		if($id != null)
		{
			$package = AtwSupportPackage::where('id', '=', $id)
				->where('person_id', '=', Auth::user()->id)
				->first();
		}
		
		$rules = array(
			'atw_support_type' => array('required'),
			'national_insurance' => array('required'),
			'atw_email_address' => array('required', 'email'),
			'your_budget_in' => array('required'),
			'your_budget' => array('required', 'numeric'),
			'type_of_language_professional' => array('required'),
			'receive_invoice' => array('required'),
			'support_ends' => array('required', 'date_format:"d/m/Y"', 'date_not_in_past'),
			'atw_address' => array('required'),
			'hours_per' => array('required'),
			'hours_per_value' => array('required')
		);
		if($package == null)
			$rules['atw_confirmation_letter'] = array('required');
		
		if(Input::get('atw_address_type') == 'existing')
			$rules['atw_address'] = array('required');
		else
		{
			$rules['atw_first_name'] = array('required');
			$rules['atw_last_name'] = array('required');
			$rules['atw_company'] = array('required');
			$rules['atw_address_1'] = array('required');
			$rules['atw_city'] = array('required');
			$rules['atw_country'] = array('required');
			$rules['atw_postcode'] = array('required');
		}
		if(Input::get('type_of_language_professional') == 'other')
			$rules['type_of_language_professional_other'] = array('required');

		$file_field = 'atw_confirmation_letter';

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(url('change-atw', array('id' => $id)))
				->withErrors($validator)
				->withInput(Input::flashExcept($file_field));
		}
		
		if($package == null)
		{
			$package = new AtwSupportPackage();
			$package->person_id = Auth::user()->id;
		}
		
		$package->given_to = Input::get('atw_support_type');
		if(Input::get('atw_address_type') == 'existing')
			$package->address_id = Input::get('atw_address');
		else
		{
			$address = new PersonAddress();
			$address->person_id = Auth::user()->id;
			$address->organization_name = Input::get('atw_company');
			$address->first_name = Input::get('atw_first_name');
			$address->last_name = Input::get('atw_last_name');
			$address->address_1 = Input::get('atw_address_1');
			$address->address_2 = Input::get('atw_address_2');
			$address->city = Input::get('atw_city');
			$address->country = Input::get('atw_country');
			$address->county = Input::get('atw_county');
			$address->postcode = Input::get('atw_postcode');
			$address->save();
			
			$package->address_id = $address->id;
		}

		$package->national_insurance = Input::get('national_insurance');
		$package->email_address = Input::get('atw_email_address');
		$package->your_budget_in = Input::get('your_budget_in');
		$package->budget = Input::get('your_budget');
		$package->type_of_lsp = Input::get('type_of_language_professional');
		if($package->type_of_lsp == 'other')
			$package->type_of_lsp_other = Input::get('type_of_language_professional_other');
		$package->receive_invoice = Input::get('receive_invoice');
		$package->additional_information = Input::get('additional_info');
		$package->support_end = Carbon::createFromFormat('d/m/Y', Input::get('support_ends'));
		$package->claim_address = Input::get('atw_address_claim');
		$package->hours_per = Input::get('hours_per');
		$package->hours_per_sum = Input::get('hours_per_value');
		if(Input::hasFile($file_field))
		{
			$package->filename = Input::file($file_field)->getClientOriginalName();
			$package->filename_mime_type = Input::file($file_field)->getMimeType();
		}
		$package->status = 'not_verified';
		if($package->exists)
		{
			$fields = $package->getDirty();
			if(count($fields) > 0) //no of changed fields
			{
				//send admin about updating support package
				$admins = Person::listAdmins();
				foreach ($admins as $admin)
				{
					$person_id = Auth::user()->id;
					$person_name = Auth::user()->getName();
					
					$admin_name = $admin->getName();
					$admin_email = $admin->email;
					
					Mail::queue('emails.admin_only.atw_updated', array('person_name' => $person_name, 'person_id' => $person_id), function($message) use ($admin_name, $admin_email)
					{
						$message
							->to($admin_email, $admin_name)
							->subject(Lang::get('translations.atw_package_updated_subject'));
					});
				}
			}
		}
		else
		{
			//send admin about new support package
			$admins = Person::listAdmins();
			foreach ($admins as $admin)
			{
				$person_id = Auth::user()->id;
				$person_name = Auth::user()->getName();
				
				$admin_name = $admin->getName();
				$admin_email = $admin->email;
				
				Mail::queue('emails.admin_only.atw_added', array('person_name' => $person_name, 'person_id' => $person_id), function($message) use ($admin_name, $admin_email)
				{
					$message
						->to($admin_email, $admin_name)
						->subject(Lang::get('translations.atw_package_added_subject'));
				});
			}
		}
		$package->save();
		
		if(Input::hasFile($file_field))
		{
			$infos = pathinfo(Input::file($file_field)->getClientOriginalName());
			$file = 'atw-' . $package->id . '-' . $infos['filename'] . '-' . date('YmdHis', time()) . '.' . $infos['extension'];
			
			Input::file($file_field)->move(
				public_path() . '/tmp_uploads/', 
				$file
			);
			
			$s3 = App::make('aws')->get('s3');
			try {
				$s3->putObject(array(
				    'Bucket'     => Config::get('amazon.bucket_prefix') . 'atw',
				    'Key'        => $file,
				    'SourceFile' => public_path() . '/tmp_uploads/' . $file,
					'ACL'		 => CannedAcl::AUTHENTICATED_READ
				));

				$package->filename = $s3->getObjectUrl(
					Config::get('amazon.bucket_prefix') . 'atw',
					$file
				);
				$package->save();
				
				@unlink(public_path() . '/tmp_uploads/' . $file);
				
				Session::flash('atw-success', Lang::get('translations.atw_package_save'));
			} catch (Exception $e) {
				Log::error($e->getMessage());
				
				Session::flash('atw-error', Lang::get('translations.atw_package_upload_failed'));
				
				return Redirect::to(url('change-atw', array('id' => $id)))
					->withErrors($validator)
					->withInput(Input::flashExcept($file_field));
			}
		}
		
		if(Session::get('wizard') == 'true')
		{
			Auth::user()->profile_completed = true;
			Auth::user()->save();
			
			Session::forget('wizard');
			Session::forget('different_billing_address');
			
			return Redirect::to(url('my-profile'));
		}
		else
			return Redirect::to(url('atw-packages'));
	}
	public function getRemoveAtw($id)
	{
		$package = AtwSupportPackage::where('id', '=', $id)
			->where('person_id', '=', Auth::user()->id)
			->first();
			
		if($package == null)
			return Redirect::url('atw-packages');
			
		$package->removed = true;
		$package->save();
		
		Session::flash('atw-success', Lang::get('translations.atw_package_removed'));
		return Redirect::to(url('atw-packages'));
	}

	public function anyProfileAttachments($id = null)
	{
		if(!Auth::check())
		{
			return Response::json(array(
				'success' => 'FAIL'
			)); 
		}

		$membership = null;
		if($id != null)
		{
			$membership = PersonMembership::find($id);
			if($membership == null || $membership->person_id != Auth::user()->id)
			{
				return Response::json(array(
					'success' => 'FAIL'
				));
			}
		}
		
		$type = Input::get('attachment_type');
		
		$file_field = null;
		if(Input::hasFile('insurance_documents'))
			$file_field = 'insurance_documents';
		elseif(Input::hasFile('crb_documents'))
			$file_field = 'crb_documents';
			
		if($file_field != null)
		{
			$infos = pathinfo(Input::file($file_field)->getClientOriginalName());	
			$filename = $type . '-' . (empty($id) ? '' : $id . '-') . $infos['filename'] . '-' . date('YmdHis', time()) . '.' . $infos['extension'];
			
			$s3 = App::make('aws')->get('s3');
			try {
				$s3->putObject(array(
				  	'Bucket'     => Config::get('amazon.bucket_prefix') . 'docs',
				    'Key'        => $filename,
				    'SourceFile' => Input::file($file_field)->getRealPath(),
					'ACL'		 => CannedAcl::AUTHENTICATED_READ
				));
			} catch (Exception $e) {
				Log::error($e->getMessage());
				
				return Response::json(array(
					'success' => 'FAIL'
				));
			}
			
			$document = new PersonDocument();
			if($id != null)
				$document->membership_id = $id;
			else
				$document->person_id = Auth::user()->id;
			$document->type = $type;
			$document->filename = $filename;
			$document->filesize = Input::file($file_field)->getSize();
			$document->save();
			
			@unlink(Input::file($file_field)->getRealPath());
		}
		
		$documents = null;
		if($membership != null)
			$documents = $membership->documents();
		else
			$documents = Auth::user()->documents($type);
		
		return Response::json(array(
			'success' => 'OK', 
			'view' => (string)View::make('frontend.user.profile.profile_step3_partial_list', array(
				'documents' => $documents,
				'type' => $type
			))
		));
	}
	public function postSetProfilePicture()
	{
		if(!Auth::check())
			return Response::json(array(
				'success' => 'FAILED',
				'message' => ''
			));

		$field = 'profile-picture';
		if(!Input::hasFile($field))
			return Response::json(array(
				'success' => 'FAILED',
				'message' => ''
			));
		
		$image = null;
		try {
			$image = WideImage::load(Input::file($field)->getRealPath());
		} catch (Exception $e) {
			Log::error($e->getMessage());
			
			return Response::json(array(
				'success' => 'FAILED',
				'message' => Lang::get('translations.profile_picture_invalid_format')
			));
		}
		if($image->getHeight() < 150 || $image->getWidth() < 150)
			return Response::json(array(
				'success' => 'FAILED',
				'message' => Lang::get('translations.profile_picture_invalid_size')
			));
			
		//generate temp_img
		$image->saveToFile(public_path() . '/tmp_uploads/' . Auth::user()->id . '_temp.jpg', 80);
		
		return Response::json(array(
			'success' => 'OK',
			'url' => '/tmp_uploads/' . Auth::user()->id . '_temp.jpg'
		));
	}
	public function getDeleteProfilePicture()
	{
		$s3 = App::make('aws')->get('s3');
		try {
			$s3->deleteObject(array(
			    'Bucket'     => Config::get('amazon.bucket_prefix') . 'avatars',
			    'Key'        => Auth::user()->id . '.jpg'
			));
		} catch (Exception $e) {
			Log::error($e->getMessage());
			
			return Response::json(array(
				'success' => 'FAILED',
				'message' => $e->getMessage()
			));
		}
		
		Auth::user()->profile_picture_url = null;
		Auth::user()->save();
		
		return Response::json(array(
			'success' => 'OK',
			'url' => '/img/placeholder.gif'
		));
	}
	public function postSetProfilePicturePosition()
	{
		if(!Auth::check())
			return Response::json(array(
				'success' => 'FAILED',
				'message' => ''
			));
			
		$temp_file = public_path() . '/tmp_uploads/' . Auth::user()->id . '_temp.jpg';
		if(!@file_exists($temp_file))
			return Response::json(array(
				'success' => 'FAILED',
				'message' => ''
			));
			
		$file = public_path() . '/tmp_uploads/' . Auth::user()->id . '.jpg';
		
		$thumbnail = Input::get('thumbnail');
		$position_x = Input::get('position_x');
		$position_y = Input::get('position_y');
		
		$image = WideImage::load($temp_file);
		if($thumbnail == 'yes')
		{
			$image->resize(150, 150)
				->saveToFile($file, 80);
		}
		else
		{
			$image->crop($position_x, $position_y, 150, 150)
				->saveToFile($file, 80);
		}
		
		$s3 = App::make('aws')->get('s3');
		try {
			$s3->putObject(array(
			    'Bucket'     => Config::get('amazon.bucket_prefix') . 'avatars',
			    'Key'        => Auth::user()->id . '.jpg',
			    'SourceFile' => $file,
				'ACL'		 => CannedAcl::PUBLIC_READ
			));

			Auth::user()->profile_picture_url = $s3->getObjectUrl(
				Config::get('amazon.bucket_prefix') . 'avatars',
			    Auth::user()->id . '.jpg'
			);
		} catch (Exception $e) {
			Log::error($e->getMessage());
			
			return Response::json(array(
				'success' => 'FAILED',
				'message' => $e->getMessage()
			));
		}
		Auth::user()->save();
		
		@unlink($temp_file);
		@unlink($file);
		
		return Response::json(array(
			'success' => 'OK',
			'url' => Auth::user()->getProfilePicture()
		));
	}

	public function getFacebook($code = null)
	{
		$referer = null;
		if(!empty($code))
			$referer = Person::where('referer_code', '=', $code)->first();
		
		$error_reason = Input::get('error_reason');
		if($error_reason == 'user_denied')
			$this->layout->content = View::make('frontend.user.social.facebook_user_denied');
		else
		{
			$code = Input::get('code');
			if(empty($code))
				$this->layout->content = View::make('frontend.user.social.facebook_failed');
			else
			{
				$token_url = "https://graph.facebook.com/oauth/access_token?"
					. "client_id=" . Config::get('social.facebook.app_id') . "&redirect_uri=" . urlencode('http://' . $_SERVER['HTTP_HOST'] . '/facebook')
					. "&client_secret=" . Config::get('social.facebook.secret_id') . "&code=" . $code . "&scope=" . Config::get('social.facebook.scope');

				$response = @file_get_contents($token_url);
				if($response)
				{
					$params = null;
					parse_str($response, $params);

					if(!isset($params['access_token']) || empty($params['access_token']))
						$this->layout->content = View::make('frontend.user.social.facebook_failed');
					else
					{
						$graph_url = "https://graph.facebook.com/me?access_token=" . $params['access_token'];

						$response = @file_get_contents($graph_url);
						if($response)
						{
							$user = json_decode($response);

							$person = Person::where('email', '=', $user->email)->first();
							if($person == null)
							{
								$person = new Person();
								$person->email = $user->email;
								$person->enabled = true;
								$person->confirmed = false;
								$person->profile_completed = false;
								$person->facebook_id = $params['access_token'];
								$person->facebook_profile = $user->link;
								if($referer != null)	
									$person->referer_id = $referer->id;

								$address = new PersonAddress();
								$address->address_type = 'default';
								$address->first_name = $user->first_name;
								$address->last_name = $user->last_name;

								Session::flash('facebook_object', $person);
								Session::flash('facebook_object2', $address);

								header('Location: ' . action('FrontPageController@getRegisterFacebook'));
								exit();
							}
							else
							{
								$person->facebook_id = $params['access_token'];
								$person->facebook_profile = $user->link;
								$person->save();

								Auth::login($person, true);

								$url = null;
								if($person->profile_completed)
									$url = action('FrontPageController@getDashboard');
								else
									$url = url('my-profile');

								header('Location: ' . $url);
								exit();
							}
						}
						else
						{
							$error = error_get_last();
							Log::error(print_r($error, true));
							$this->layout->content = View::make('frontend.user.social.facebook_failed');
						}
					}
				}
				else
				{
					$error = error_get_last();
					Log::error(print_r($error, true));
					$this->layout->content = View::make('frontend.user.social.facebook_failed');
				}
			}
		}
	}
	public function getRegisterFacebook()
	{
		$person = Session::get('facebook_object');
		$address = Session::get('facebook_object2');
		
		Session::flash('facebook_object', $person);
		Session::flash('facebook_object2', $address);
		
		$this->layout->content = View::make('frontend.user.register.register_facebook', array(
			'person' => $person,
			'address' => $address
		));
	}
	public function postRegisterFacebook()
	{
		$rules = array(
			'terms_agreed' => array('accepted')
		);
		
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			Session::flash('facebook_object', Session::get('facebook_object'));
			return Redirect::to(action('FrontPageController@getRegisterFacebook'))
				->withErrors($validator)
				->withInput();
		}
		
		$person = Session::get('facebook_object');
		$person->title = Input::get('title');
		$person->account_type = Input::get('account_type');
		$person->newsletter = true;
		$person->terms_agreed = (Input::get('terms_agreed') == 'yes');
		$person->confirmed = false;
		$person->save();
		
		$address = Session::get('facebook_object2');
		$address->person_id = $person->id;
		$address->save();
		
		$person_name = $address->getName();
		$person_email = $person->email;
		Mail::queue('emails.frontend.welcome', array('person_name' => $person_name), function($message) use ($person_email, $person_name)
		{
			$message
				->to($person_email, $person_name)
				->subject(Lang::get('translations.welcome_mail_subject'));
		});
		
		Notification::sendNotificationTo(null, $person->id, null, Lang::get('translations.welcome_notification_message'));
		
		Auth::login($person, true);		
		return Redirect::to(url('my-profile'));
	}
	
	public function getLinkedin($code = null)
	{
		$referer = null;
		if(!empty($code))
			$referer = Person::where('referer_code', '=', $code)->first();
		
		$config = array(
			'appKey' => Config::get('social.linkedin.api_key'),
			'appSecret' => Config::get('social.linkedin.secret_key'),
			'callbackUrl' => 'http://' . $_SERVER['HTTP_HOST'] . '/linkedin'
		);
		
		$client = new LinkedIn($config);
		$oauth_token = Input::get('oauth_token');
		if(empty($oauth_token))
		{
			$response = $client->retrieveTokenRequest();
			Session::put('linkedin', $response['linkedin']);
	        if($response['success'] === true)
				return Redirect::to(LinkedIn::_URL_AUTH . $response['linkedin']['oauth_token']);
		}
		else
		{
			$linkedIn = Session::get('linkedin');
			
			$response = $client->retrieveTokenAccess($linkedIn['oauth_token'], $linkedIn['oauth_token_secret'], Input::get('oauth_verifier'));
			if($response['success'] === true)
			{
				Session::put('linkedin_access', $response['linkedin']);
				
				$response = $client->profile('~:(' . Config::get('social.linkedin.scope') . ')');
				if($response['success'] === true)
				{
					Session::put('linkedin_object', $response['linkedin']);
					
					$xml = simplexml_load_string($response['linkedin']);
					
					$person = Person::where('email', '=', (string)$xml->{'email-address'})->first();
					if($person == null)
					{
						$person = new Person();
						$person->email = (string)$xml->{'email-address'};
						$person->enabled = true;
						$person->confirmed = false;
						$person->profile_completed = false;
						if($referer != null)
							$person->referer_id = $referer->id;
						$person->linkedin_id = serialize(Session::get('linkedin_access'));
						$person->linkedin_profile = (string)$xml->{'public-profile-url'};
						
						$address = new PersonAddress();
						$address->address_type = 'default';
						$address->first_name = (string)$xml->{'first-name'};
						$address->last_name = (string)$xml->{'last-name'};
						
						Session::flash('linkedin_object', $person);
						Session::flash('linkedin_object2', $address);
						
						//we are not allowed to redirect with laravel's redirection
						header('Location: ' . action('FrontPageController@getRegisterLinkedin'));
						exit();
					}
					else
					{
						$person->linkedin_id = serialize(Session::get('linkedin_access'));
						$person->linkedin_profile = (string)$xml->{'public-profile-url'};
						$person->save();
						
						Auth::login($person, true);
						
						$url = null;
						if($person->profile_completed)
							$url = action('FrontPageController@getDashboard');
						else
							$url = url('my-profile');
							
						//we are not allowed to redirect with laravel's redirection
						header('Location: ' . $url);
						exit();
					}
				}
	        }
		}
	}
	public function getRegisterLinkedin()
	{
		$person = Session::get('linkedin_object');
		$address = Session::get('linkedin_object2');
		
		Session::flash('linkedin_object', $person);
		Session::flash('linkedin_object2', $address);
		
		$this->layout->content = View::make('frontend.user.register.register_linkedin', array(
			'person' => $person,
			'address' => $address
		));
	}
	public function postRegisterLinkedin()
	{
		$rules = array(
			'terms_agreed' => array('accepted')
		);
		
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			Session::flash('linkedin_object', Session::get('linkedin_object'));
			return Redirect::to(action('FrontPageController@getRegisterLinkedin'))
				->withErrors($validator)
				->withInput();
		}
		
		$person = Session::get('linkedin_object');
		$person->account_type = Input::get('account_type');
		$person->newsletter = true;
		$person->terms_agreed = (Input::get('terms_agreed') == 'yes');
		$person->confirmed = false;
		$person->save();
		
		$address = Session::get('linkedin_object2');
		$address->person_id = $person->id;
		$address->save();
		
		$person_name = $address->getName();
		$person_email = $person->email;
		Mail::queue('emails.frontend.welcome', array('person_name' => $person_name), function($message) use ($person_email, $person_name)
		{
			$message
				->to($person_email, $person_name)
				->subject(Lang::get('translations.welcome_mail_subject'));
		});
		
		Notification::sendNotificationTo(null, $person->id, null, Lang::get('translations.welcome_notification_message'));
		
		Auth::login($person, true);		
		return Redirect::to(url('my-profile'));
	}
	
	public function getDashboard()
	{
		$jobs = Auth::user()
			->jobs()
			->orderBy('created_at', 'DESC')
			->take(3)
			->get();
			
		$notifications = Auth::user()
			->notifications()
			->orderBy('created_at', 'DESC')
			->take(10)
			->get();
		
		$this->layout->content = View::make('frontend.user.dashboard', array(
			'jobs' => $jobs,
			'notifications' => $notifications
		));
	}

	public function getNotifications($page = 1)
	{
		$per_page = 10;
		$max_pages = 0;
		$count = Auth::user()
			->notifications()
			->count();
			
		$notifications = Auth::user()
			->notifications()
			->orderBy('created_at', 'DESC')
			->skip($per_page * ($page - 1))
			->take($per_page)
			->get();
			
		$max_pages = (int)ceil($count / $per_page);
		
		//mark all notifications as read
		Auth::user()->notifications()->update(array('read' => '1'));
		
		$this->layout->content = View::make('frontend.user.notifications', array(
			'notifications' => $notifications,
			'per_page' => $per_page,
			'page' => $page,
			'max_pages' => $max_pages
		));
	}

	public function getProfessionals($page = 1)
	{
		$page = (int)$page;
		if($page == 0)
			$page = 1;
		
		$filter = Session::get('professionals_filter', null);
		
		$count = 0;
		$per_page = 5;
		$max_pages = 0;
		
		$professionals = array();
		if($filter == null)
		{
			$count = Person::translators()
				->where('profile_completed', '=', '1')
				->where('confirmed', '=', '1')
				->count();
				
			$sql = "SELECT
				p.*
			FROM 
				persons p
			INNER JOIN
				persons_addresses pa
				ON
					p.id = pa.person_id
			WHERE
				p.account_type = 'translator' AND
				p.profile_completed = 1 AND
				p.confirmed = 1 AND
				pa.address_type = 'default'
			ORDER BY
				pa.first_name ASC
			LIMIT 
			$per_page OFFSET " . ($per_page * ($page - 1));
			
			DB::connection()->setFetchMode(PDO::FETCH_ASSOC);
			$results = DB::select($sql);
			DB::connection()->setFetchMode(PDO::FETCH_OBJ);
			
			$professionals = new Collection();
			foreach($results as $index => $result)
			{
				$person = new Person();
				$person->setRawAttributes($result, true);
				$person->exists = true;
				
				$professionals->put($index, $person);
			}
		}
		else
		{
			$sql = "SELECT
				DISTINCT(p.id) AS id
			FROM 
				persons p
			INNER JOIN
				persons_addresses pa
				ON
					p.id = pa.person_id
			INNER JOIN
				persons_memberships pm
				ON 
					p.id = pm.person_id
			INNER JOIN
				persons_memberships_languages pml
				ON
					pm.id = pml.membership_id ";
			if(!empty($filter['specialism']))
			{
				$sql .= "INNER JOIN
					persons_specialisms ps
				ON
					p.id=ps.person_id ";
			}
			$sql .= "WHERE
				p.account_type = 'translator' AND
				p.profile_completed = 1 AND
				p.confirmed = 1 AND 
				pa.address_type = 'default'
			";
				
			if(!empty($filter['search_string']))
			{
				$search_string = explode(' ', $filter['search_string']);
				if(count($search_string) > 0)
				{
					$query = "";
					foreach($search_string as $token)
					{
						$token = trim($token);
						if(empty($token))
							continue;
							
						if($query != "")
							$query .= " OR ";
						
						$query .= "IFNULL(pa.organization_name, CONCAT_WS(' ', pa.first_name, pa.last_name)) LIKE '%$token%' ";
					}
					
					$sql .= " AND (" . $query . ")";
				}
			}
			
			$filter_query = array();
			
			if($filter['sli'] == 'yes') $filter_query['sli'] = 'sli_languages';
			if($filter['sli2'] == 'yes') $filter_query['sli2'] = 'sli2_languages';
			if($filter['lipspeaker'] == 'yes') $filter_query['lipspeaker'] = 'lipspeaker_languages';
			if($filter['sttr'] == 'yes') $filter_query['sttr'] = 'sttr_languages';
			if($filter['slt'] == 'yes')  $filter_query['slt'] = 'slt_languages';
			if($filter['notetaker'] == 'yes') $filter_query['notetaker'] = 'notetaker_languages';
			
			
			$query = "";
			foreach($filter_query as $option_key => $index)
			{
				$languages = $filter[$index];
				if(!is_array($languages))
					$languages = array($languages);
					
				if(count($languages) > 0)
				{
					$counter = 0;
					$query  = "pm.type='$option_key' AND (";
					foreach($languages as $language)
					{
						if($counter > 0)
							$query .= " AND ";
							
						$query .= "pml.language='$language'";;
						
						$counter++;
					}
					$query .= ")";
				}
			}
			
			$counter = 0;
			if(!empty($filter['specialism']))
			{
				foreach($filter['specialism'] as $specialism)
				{
					if($counter > 0 || !empty($query))
						$query .= " AND ";

					$query .= "ps.specialism='$specialism'";
					$counter++;
				}
			}
			
			if(!empty($query))
				$sql .= " AND (" . $query . ")";
				
/*			echo $sql;
			die();*/
				
			$ids = DB::select($sql);
			$new_ids = array();
			foreach($ids as $id)
				$new_ids[] = $id->id;
			
			$professionals = new Collection();
			if(count($new_ids) > 0)
			{
				$sql = "SELECT 
					p.*
				FROM
					persons p
				INNER JOIN
					persons_addresses pa
					ON
						p.id=pa.person_id
				WHERE
					p.id IN (" . implode($new_ids, ',') . ")
				ORDER BY 
					pa.first_name ASC, 
					pa.last_name ASC
				LIMIT 
				$per_page OFFSET " . ($per_page * ($page - 1));

				DB::connection()->setFetchMode(PDO::FETCH_ASSOC);
				$results = DB::select($sql);
				DB::connection()->setFetchMode(PDO::FETCH_OBJ);

				foreach($results as $index => $result)
				{
					$person = new Person();
					$person->setRawAttributes($result, true);
					$person->exists = true;

					$professionals->put($index, $person);
				}
			}
		}
		
		$max_pages = (int)ceil($count / $per_page);
		
		$this->layout->content = View::make('frontend.professionals.find_professionals', array(
			'professionals' => $professionals,
			'page' => $page,
			'max_pages' => $max_pages,
			'filter' => $filter,
			'blocked_list' => Auth::check() ? Auth::user()->blocked_list()->lists('client_lsp_id') : array(),
			'preferred_list' => Auth::check() ? Auth::user()->preferred_list()->lists('client_lsp_id') : array()
		));
	}
	public function postProfessionals()
	{
		$filter = Session::get('professionals_filter', array());
		
		$search_button = Input::get('search');
		if(isset($search_button))
		{
			$filter['search_string'] = Input::get('search_string');

			$filter['sli'] = Input::get('spoken_language_interpreter');		
			$filter['sli_languages'] = Input::get('sli_language');

			$filter['sli2'] = Input::get('sign_language_interpreter');
			$filter['sli2_languages'] = Input::get('sli2_language');

			$filter['lipspeaker'] = Input::get('lipspeaker');
			$filter['lipspeaker_languages'] = Input::get('lipspeaker_language');

			$filter['sttr'] = Input::get('speech_to_text_reporter');
			$filter['sttr_languages'] = Input::get('sttr_language');

			$filter['slt'] = Input::get('sign_language_translator');
			$filter['slt_languages'] = Input::get('slt_language');

			$filter['notetaker'] = Input::get('notetaker');
			$filter['notetaker_languages'] = Input::get('notetaker_language');

			$filter['specialism'] = Input::get('specialism', array());

			$filter['location'] = Input::get('location');
		}
		else
			$filter = array();
		
		Session::put('professionals_filter', $filter);
		
		return Redirect::to('professionals');
	}

	public function getProfile($id)
	{
		$person = Person::find($id);
		if($person == null)
			return Redirect::to(url('professionals'));
			
		$insurance_documents = $person->documents('insurance');
		$crb_documentation = $person->documents('crb');
			
		$this->layout->content = View::make('frontend.user.public_profile.' . $person->account_type, array(
			'person' => $person,
			'insurance_documents' => $insurance_documents,
			'crb_documentation' => $crb_documentation
		));
	}
	public function getReviews($id)
	{
		$person = Person::find($id);
		if($person == null)
			return Redirect::to(url('professionals'));
			
		if(!(bool)$person->profile_completed)
			return Redirect::to(url('professionals'));
			
		$reviews = $person->reviews()
			->orderBy('created_at', 'DESC')
			->get();
			
		$this->layout->content = View::make('frontend.user.reviews', array(
			'person' => $person,
			'reviews' => $reviews
		));
	}

	public function postAddToPreferredList()
	{
		$person_id = (int)Input::get('person_id');
		if(empty($person_id))
			return;
		
		$list = PersonList::where('person_id', '=', Auth::user()->id)
			->where('client_lsp_id', '=', $person_id)
			->first();
			
		if($list == null)
		{
			$list = new PersonList();
			$list->person_id = Auth::user()->id;
			$list->client_lsp_id = $person_id;
		}

		$list->type = 'preferred';
		$list->save();
		
		return Response::json(array(
			'status' => 'OK'
		));
	}
	public function postRemoveFromPreferredList()
	{
		$person_id = (int)Input::get('person_id');
		if(empty($person_id))
			return;
			
		$list = PersonList::where('person_id', '=', Auth::user()->id)
			->where('client_lsp_id', '=', $person_id)
			->first();
			
		if($list != null)
			$list->delete();
			
		return Response::json(array(
			'status' => 'OK'
		));
	}
	public function postAddToBlockList()
	{
		$person_id = (int)Input::get('person_id');
		if(empty($person_id))
			return;
			
		$list = PersonList::where('person_id', '=', Auth::user()->id)
			->where('client_lsp_id', '=', $person_id)
			->first();
			
		if($list == null)
		{
			$list = new PersonList();
			$list->person_id = Auth::user()->id;
			$list->client_lsp_id = $person_id;
		}

		$list->type = 'blocked';
		$list->save();
		
		return Response::json(array(
			'status' => 'OK'
		));
	}
	public function postRemoveFromBlockList()
	{
		$person_id = (int)Input::get('person_id');
		if(empty($person_id))
			return;
			
		$list = PersonList::where('person_id', '=', Auth::user()->id)
			->where('client_lsp_id', '=', $person_id)
			->first();
			
		if($list != null)
			$list->delete();
			
		return Response::json(array(
			'status' => 'OK'
		));
	}		

	public function getRecruitments($page = 1)
	{
		$filter = Session::get('recruitment_filter', array());
		
		$recruitments = array();
		if(isset($filter['job_type']) && $filter['job_type'] != 'all')
			$recruitments = Recruitment::where('job_type', '=', $filter['job_type'])->get();
		else
			$recruitments = Recruitment::all();
		
		$this->layout->content = View::make('frontend.static.recruitments.list', array(
			'recruitments' => $recruitments,
			'filter' => $filter
		));
	}
	public function postRecruitments()
	{
		$filter = Session::get('recruitment_filter', array());
		if(!is_array($filter))
			$filter = array();
			
		$search_button = Input::get('search');
		if(isset($search_button))
			$filter['job_type'] = Input::get('job_type');
		else
			$filter['job_type'] = 'all';
		
		Session::set('recruitment_filter', $filter);
		return Redirect::to(url('recruitments'));
	}
	public function getRecruitment($id)
	{
		$recruitment = Recruitment::find($id);
		if($recruitment == null)
			return Redirect::to(url('recruitments'));
			
		$this->layout->content = View::make('frontend.static.recruitments.details', array(
			'recruitment' => $recruitment
		));
	}
	public function postRecruitment($id)
	{
		$recruitment = Recruitment::find($id);
		if($recruitment == null)
			return Redirect::to(url('recruitments'));
		
		$rules = array(
			'name' => array('required'),
			'email' => array('required', 'email'),
			'address' => array('required'),
			'phone' => array('required'),
			'answer' => array('required'),
			'cv' => array('required')
		);

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(url('recruitment') . '/' . $id)
				->withErrors($validator)
				->withInput(Input::except('cv'));
		}
		
		$infos = pathinfo(Input::file('cv')->getClientOriginalName());	
		$filename = 'recruitment-' . $id . '-' . $infos['filename'] . '-' . date('YmdHis', time()) . '.' . $infos['extension'];
		
		$s3 = App::make('aws')->get('s3');
		try {
			$s3->putObject(array(
			  	'Bucket'     => Config::get('amazon.bucket_prefix') . 'recruitments',
			    'Key'        => $filename,
			    'SourceFile' => Input::file('cv')->getRealPath(),
				'ACL'		 => \Aws\S3\Enum\CannedAcl::AUTHENTICATED_READ
			));
		} catch (Exception $e) {
			Log::error($e->getMessage());
			
			Session::flash('recruitment_error', Lang::get('translations.recruitment_application_upload_failed'));
			return Redirect::to(url('recruitment') . '/' . $id);
		}
		
		$application = new RecruitmentApplication();
		$application->recruitment_id = $id;
		$application->name = Input::get('name');
		$application->email = Input::get('email');
		$application->address = Input::get('address');
		$application->phone = Input::get('phone');
		$application->answer = Input::get('answer');
		$application->cv = true;
		$application->attachment = $filename;
		$application->save();
		
		$application_id = $application->id;
		$data = array(
			'recruitment_title' => $recruitment->title,
			'recruitment_question' => $recruitment->question,
			'application_name' => $application->name,
			'application_email' => $application->email,
			'application_address' => $application->address,
			'application_phone' => $application->phone,
			'application_answer' => $application->answer,
			'attachment_url' => $s3->getObjectUrl(Config::get('amazon.bucket_prefix') . 'recruitments', $filename, '+1 day'),
			'filename' => $filename
		);
		Mail::queue('emails.frontend.recruitment', $data, function($message) use ($id, $application_id)
		{
			$message
				->to('jobs@lingoing.com')
				->subject(Lang::get('translations.recruitment_application_subject'));
		});
		
		Session::flash('recruitment_success', Lang::get('translations.recruitment_application_message'));
		return Redirect::to(url('recruitment') . '/' . $id);
	}

	public function getChangePassword()
	{
		$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.change_password');
	}
	public function postChangePassword()
	{
		$rules = array(
			'new_password' => array('required'),
			'new_password_repeat' => array('required', 'min:4', 'max:40', 'same:new_password')
		);

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(url('change-password'))
				->withErrors($validator);
		}

		Auth::user()->password = Hash::make(Input::get('new_password'));
		Auth::user()->save();

		Session::flash('success', Lang::get('translations.change_password_message'));
		return Redirect::to(url('my-profile'));
	}

	public function getChangeEmailAddress()
	{
		$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.change_email_address');
	}
	public function postChangeEmailAddress()
	{
		$rules = array(
			'new_email_address' => array('required', 'email', 'unique:persons,email'),
			'new_email_address_repeat' => array('required', 'email', 'same:new_email_address_repeat')
		);

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(url('change-email-address'))
				->withErrors($validator);
		}

		Auth::user()->unconfirmed_email = Input::get('new_email_address');
		Auth::user()->new_email_confirmation = sha1(Hash::make(Input::get('new_email_address')));
		Auth::user()->save();

		$person_email = Auth::user()->email;
		$person_name = Auth::user()->getName();

		$data = array(
			'person_name' => $person_name,
			'unconfirmed_email' => Auth::user()->unconfirmed_email,
			'new_email_confirmation' => Auth::user()->new_email_confirmation
		);

		Mail::queue('emails.profile.change_email_address', $data, function($message) use ($person_email, $person_name)
		{
			$message
				->to($person_email, $person_name)
				->subject(Lang::get('translations.change_email_subject'));
		});

		Session::flash('success', Lang::get('translations.change_email_message', array('old_email' => Auth::user()->email)));
		return Redirect::to(url('my-profile'));
	}
	public function getConfirmNewEmail($token)
	{
		$person = Person::where('new_email_confirmation', '=', $token)
			->first();

		if($person == null)
			return Redirect::to(url('/'));

		$person->email = $person->unconfirmed_email;
		$person->new_email_confirmation = null;
		$person->unconfirmed_email = null;
		$person->save();
		
		$address = $person->default_address();
		
		$backup = new PersonalDataBackup();
		$backup->person_id = $person->id;
		$backup->first_name = $address->first_name;
		$backup->last_name = $address->last_name;
		$backup->email = $person->email;
		$backup->save();

		if(Auth::check())
		{
			Session::flash('success', Lang::get('translations.change_email_confirmation_message', array('email' => $person->email)));
			return Redirect::to(url('my-profile'));
		}
		else
			return Redirect::to(url('/'));
	}
	
	public function getCustomerInvite()
	{
		$this->layout->content = View::make('frontend.user.profile.profile_layout')->nest('profile', 'frontend.user.profile.customer_invite');
	}
	public function postCustomerInvite()
	{
		$rules = array(
			'first_name' => array('required'),
			'last_name' => array('required'),
			'email_address' => array('required', 'email')
		);

		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails())
		{
			return Redirect::to(url('customer-invite'))
				->withErrors($validator)
				->withInput(Input::all());
		}
		
		$token = str_shuffle(sha1(Input::get('first_name').Input::get('last_name').Input::get('email_address').microtime(true)));
		$token = hash_hmac('sha1', $token, Config::get('app.key'));
		
		$invitation = new PersonSendInvitation();
		$invitation->person_id = Auth::user()->id;
		$invitation->first_name = Input::get('first_name');
		$invitation->last_name = Input::get('last_name');
		$invitation->email = Input::get('email_address');
		$invitation->message = Input::get('message');
		$invitation->token = $token;
		$invitation->save();
		
		$client_name = Input::get('first_name') . ' ' . Input::get('last_name');
		$client_email = Input::get('email_address');
		
		$data = array(
			'client_name' => $client_name,
			'person_name' => Auth::user()->getName(),
			'text' => Input::get('message'),
			'token' => $token
		);
		
		Mail::queue('emails.frontend.customer_invitation', $data, function($message) use($client_email, $client_name)
		{
			$message
				->to($client_email, $client_name)
				->subject(Lang::get('translations.customer_invite_subject'));
		});
		
		Session::flash('success', Lang::get('translations.customer_invite_message', array('client_name' => $client_name . ' - ' . $client_email)));
		return Redirect::to(url('customer-invite'));
	}
}