<?php
use \Carbon\Carbon;
use \Illuminate\Support\MessageBag;

class StatsAdminController extends BaseController {
	
	protected $layout = 'layouts.admin';
	
	
	public function getSalesRep()
	{
		$sql = "SELECT
			DISTINCT p.referer_id,
			p.created_at
		FROM
			persons p
		WHERE
			p.referer_id IS NOT NULL";
		
		$raw = DB::select($sql);
		
		$ids = array();
		$salesStats = array();
		foreach($raw as $row)
		{
			$month = Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->format('01/m/Y');
			
			if(!isset($salesStats[$month]))
				$salesStats[$month] = array();
				
			if(!isset($salesStats[$month][$row->referer_id]))
				$salesStats[$month][$row->referer_id] = array('count' => 0, 'commission' => 0.0);
				
			$salesStats[$month][$row->referer_id]['count'] += 1;
			$ids[] = $row->referer_id;
		}
		
		if(count($ids) > 0)
		{
			$sql = "SELECT
				p.referer_id,
				i.paid_date,
				i.lingoing_commission AS lingoing_commission
			FROM
				invoices i
			INNER JOIN
				persons p
				ON
					i.person_id=p.id
			WHERE
				i.status = 'paid' AND
				p.referer_id IN (" . implode($ids, ',') . ")";

			$raw = DB::select($sql);
			
			foreach($raw as $row)
			{
				$month = Carbon::createFromFormat('Y-m-d H:i:s', $row->paid_date)->format('01/m/Y');
				
				$salesStats[$month][$row->referer_id]['commission'] += (double)$row->lingoing_commission;
			}
		}
		
		$this->layout->content = View::make('admin.stats.sales', array(
			'salesStats' => $salesStats
		));
	}
}