<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//redirection from /admin to /admin/login
Route::get('admin', function(){
	return Redirect::action('AdminController@getLogin');
});
Route::get('admin/login', array('before' => 'guest.admin', 'uses' => 'AdminController@getLogin'));
Route::post('admin/login', array('before' => 'csrf', 'uses' => 'AdminController@postLogin'));

//Admin controller
Route::group(array('prefix' => 'admin', 'before' => 'auth.admin'), function()
{
	Route::controller('admin', 'AdminController');
	Route::controller('users', 'UsersAdminController');
	Route::controller('invoices', 'InvoicesAdminController');
	Route::controller('jobs', 'JobsAdminController');
	Route::controller('stats', 'StatsAdminController');
});

//static pages
Route::get('/', array('uses' => 'StaticPagesController@getHomepage'));

Route::get('/about-us', array('uses' => 'StaticPagesController@getAboutUs'));
Route::get('/faq', array('uses' => 'StaticPagesController@getFaq'));
Route::post('/send-faq-question', array('uses' => 'StaticPagesController@postSendFaqQuestion'));

Route::get('/feedback', array('uses' => 'StaticPagesController@getFeedback'));
Route::post('/send-feedback', array('uses' => 'StaticPagesController@postFeedback'));

Route::get('/contact-us', array('uses' => 'StaticPagesController@getContactUs'));
Route::post('/send-contact-us-message', array('uses' => 'StaticPagesController@postSendContactUsMessage'));

Route::get('/terms-conditions', array('uses' => 'StaticPageController@getTermsConditions'));

Route::get('/for-interpreters', array('uses' => 'StaticPagesController@getForInterpreters'));
Route::post('/for-interpreters', array('uses' => 'StaticPagesController@postForInterpreters'));

Route::get('/for-clients', array('uses' => 'StaticPagesController@getForClients'));
Route::post('/for-clients', array('uses' => 'StaticPagesController@postForClients'));

Route::get('/cookies', array('uses' => 'StaticPagesController@getCookies'));

//Recruitments
Route::get('recruitments/{page?}', array('uses' => 'FrontPageController@getRecruitments'))->where('page', '[0-9]+');
Route::post('recruitments', array('uses' => 'FrontPageController@postRecruitments'));

//Find Jobs
Route::get('jobs/{page?}', array('uses' => 'JobsController@getJobs'))->where('page', '[0-9]+');
Route::post('jobs', array('uses' => 'JobsController@postJobs'));

//Find Language Professionals
Route::get('professionals/{page?}', array('uses' => 'FrontPageController@getProfessionals'))->where('page', '[0-9]+');
Route::post('professionals', array('uses' => 'FrontPageController@postProfessionals'));

//Login
Route::get('login', array('uses' => 'FrontPageController@getLogin'));
Route::post('login', array('before' => 'csrf', 'uses' => 'FrontPageController@postLogin'));

//Register
Route::get('register/{referer?}', array('uses' => 'FrontPageController@getRegister'));
Route::post('register/{referer?}', array('before' => 'csrf', 'uses' => 'FrontPageController@postRegister'));

Route::any('/invoices/debit-webhooks', array('uses' => 'InvoicesController@anyDebitWebhooks'));

//profile both VERBS - get and post
Route::group(array('before' => 'auth'), function()
{
	Route::get('account-not-approved', array('uses' => 'FrontPageController@getAccountNotApproved'));
	
	Route::get('my-profile', array('uses' => 'FrontPageController@getProfileStep1'));
	Route::post('my-profile', array('before' => 'csrf', 'uses' => 'FrontPageController@postProfileStep1'));

	Route::get('change-password', array('uses' => 'FrontPageController@getChangePassword'));
	Route::post('change-password', array('before' => 'csrf', 'uses' => 'FrontPageController@postChangePassword'));

	Route::get('change-email-address', array('uses' => 'FrontPageController@getChangeEmailAddress'));
	Route::post('change-email-address', array('before' => 'csrf', 'uses' => 'FrontPageController@postChangeEmailAddress'));

	Route::get('my-address-book', array('uses' => 'FrontPageController@getMyAddressBook'));
	
	Route::get('profile_step2', array('uses' => 'FrontPageController@getProfileStep2'));
	Route::post('profile_step2', array('before' => 'csrf', 'uses' => 'FrontPageController@postProfileStep2'));
	
	Route::get('profile_step3', array('uses' => 'FrontPageController@getProfileStep3'));
	Route::post('profile_step3', array('before' => 'csrf', 'uses' => 'FrontPageController@postProfileStep3'));
	
	Route::get('profile_step4', array('uses' => 'FrontPageController@getProfileStep4'));
	Route::post('profile_step4', array('before' => 'csrf', 'uses' => 'FrontPageController@postProfileStep4'));
	
	Route::get('profile_step5', array('uses' => 'FrontPageController@getProfileStep5'));
	Route::post('profile_step5', array('before' => 'csrf', 'uses' => 'FrontPageController@postProfileStep5'));
	
	Route::get('profile_step6', array('uses' => 'FrontPageController@getProfileStep6'));
	Route::post('profile_step6', array('before' => 'csrf', 'uses' => 'FrontPageController@postProfileStep6'));
	
	Route::get('atw-packages', array('uses' => 'FrontPageController@getAtwPackages'));
	Route::post('atw-packages', array('before' => 'csrf', 'uses' => 'FrontPageController@postAtwPackages'));
	
	//profile file upload
	Route::any('profile_attachments/{id?}', array('uses' => 'FrontPageController@anyProfileAttachments'))->where('id', '[0-9]+');
	Route::any('profile_attachments_delete', array('uses' => 'FrontPageController@anyProfileAttachmentsDelete'));
	
	//dashboard
	Route::get('dashboard', array('uses' => 'FrontPageController@getDashboard'));
	Route::post('dashboard', array('before' => 'csrf', 'uses' => 'FrontPageController@getDashboard'));
	
	//notifications
	Route::get('notifications/{page?}', array('uses' => 'FrontPageController@getNotifications'))->where('page', '[0-9]+');

	Route::get('my-jobs/{type?}', array('uses' => 'JobsController@getMyJobs'));
	Route::controller('/jobs', 'JobsController');

	Route::get('my-invoices/{type?}', array('uses' => 'InvoicesController@getMyInvoices'));
	Route::controller('/invoices', 'InvoicesController');

	Route::get('my-clients/{type?}', array('uses' => 'ClientController@getMyClients'));
	Route::get('my-professionals/{type?}', array('uses' => 'ClientController@getMyProfessionals'));
	Route::controller('/clients', 'ClientController');
	
	Route::group(array('before' => 'auth.enabled_and_profile_completed'), function(){
		Route::get('post-a-job', array('uses' => 'JobsController@getStep1'));
		Route::post('post-a-job', array('uses' => 'JobsController@postStep1'));
	});
});

Route::controller('/cron', 'CronController');
Route::controller('/', 'FrontPageController');