<?php

use \Carbon\Carbon;

class DateTimeValidator extends Illuminate\Validation\Validator {

	public function validateDateStartBeforeDateEnd($attribute, $value, $parameters)
	{
		$start_time = Carbon::createFromFormat('d/m/Y', Input::get($parameters[0]));
		$end_time = Carbon::createFromFormat('d/m/Y', $value);
		
		return $start_time->lt($end_time);
	}
    public function validateTimeStartEnd($attribute, $value, $parameters)
    {
		$start_time = strtotime(Input::get($parameters[0]));
		$end_time = strtotime($value);
		
		return ($start_time < $end_time);
    }
	public function validateDateNotInPast($attribute, $value, $parameters)
	{
		$time = Carbon::createFromFormat('d/m/Y', $value)->timestamp;
		$today = strtotime('today');
		
		return ($time >= $today);
	}

}