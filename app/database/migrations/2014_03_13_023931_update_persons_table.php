<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePersonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('persons', function(Blueprint $table)
		{
			$table->string('bacs_account_number', 255)->nullable()->default(null);
			$table->string('bacs_sort_code', 255)->nullable()->default(null);
			
			$table->string('paypal_email', 255)->nullable()->default(null);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('persons', function(Blueprint $table)
		{
			$table->dropColumn('bacs_account_number');
			$table->dropColumn('bacs_sort_code');
			
			$table->dropColumn('paypal_email');
		});
	}

}