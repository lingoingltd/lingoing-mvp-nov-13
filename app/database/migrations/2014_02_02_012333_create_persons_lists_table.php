<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsListsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persons_list', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->integer('person_id')->unsigned();
			$table->integer('client_lsp_id')->unsigned();
			$table->enum('type', array('preferred', 'blocked'));
			
			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
			$table->foreign('client_lsp_id')->references('id')->on('persons')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persons_list');
	}

}