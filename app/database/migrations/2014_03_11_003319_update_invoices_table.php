<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			$table->integer('worker_id')->unsigned()->after('person_id');
			
			$table->decimal('no_of_hours', 10, 2)->nullable()->default(null)->after('list_expiry');
			$table->decimal('cost_per_hour', 10, 2)->nullable()->default(null)->after('no_of_hours');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invoices', function(Blueprint $table)
		{			
			$table->dropColumn('no_of_hours');
			$table->dropColumn('cost_per_hour');
		});
	}

}