<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvoicesTable6 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			
			$table->string('gocardless_id')->nullable();
			
			$table->index('gocardless_id');
			
		});
		
		DB::statement("ALTER TABLE `invoices` CHANGE `status` `status` ENUM('paid', 'unpaid', 'pending', 'failed', 'chargedback', 'cancelled') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unpaid';");
		DB::statement("ALTER TABLE `invoices` CHANGE `status_worker` `status_worker` ENUM('paid', 'unpaid', 'pending', 'failed', 'chargedback', 'cancelled') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unpaid';");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			
			$table->dropColumn('gocardless_id');
			
		});
		
		DB::statement("ALTER TABLE `invoices` CHANGE `status` `status` ENUM('paid', 'unpaid', 'pending') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unpaid';");
		DB::statement("ALTER TABLE `invoices` CHANGE `status_worker` `status_worker` ENUM('paid', 'unpaid', 'pending') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unpaid';");
	}

}