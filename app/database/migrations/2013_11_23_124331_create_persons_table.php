<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persons', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			
			$table->timestamps();
			
			$table->string('title', 20)->nullable();
			$table->string('job_title', 255)->nullable();
			$table->string('first_name', 255)->nullable();
			$table->string('last_name', 255)->nullable();
			$table->string('email', 255);
			$table->string('password', 100);
			$table->enum('account_type', array('client', 'translator', 'agent'));
			$table->boolean('newsletter')->default(true);
			$table->boolean('terms_agreed')->default(false);
			$table->boolean('enabled')->default(true);
			$table->boolean('confirmed')->default(false);
			$table->boolean('admin')->default(false);
			$table->boolean('picture_uploaded')->default(false);
			$table->boolean('profile_step1')->default(false);
			$table->boolean('profile_step2')->default(false);
			$table->boolean('profile_step3')->default(false);
			$table->boolean('profile_step4')->default(false);
			$table->boolean('profile_step5')->default(false);
			$table->boolean('profile_step6')->default(false);
			$table->boolean('profile_completed')->default(false);
			
			$table->string('facebook_id', 255)->nullable();
			$table->string('facebook_profile', 255)->nullable();
			$table->string('linkedin_id', 255)->nullable();
			$table->string('linkedin_profile', 255)->nullable();
			
			//your details
			$table->string('mobile_number', 50);
			$table->string('address_1', 255);
			$table->string('address_2', 255);
			$table->string('city', 255);
			$table->string('country', 255);
			$table->string('county', 255);
			$table->string('postcode', 255);
			
			//organization
			$table->integer('organization_id')->unsigned();
			
			//language skills
			$table->boolean('spoken_language_interpreter')->default(false);
			$table->boolean('sign_language_interpreter')->default(false);
			$table->boolean('lipspeaker')->default(false);
			
			$table->boolean('speech_to_text_reporter')->default(false);
			$table->boolean('sign_language_translator')->default(false);
			$table->boolean('note_taker')->default(false);
			
			//experience
			$table->text('experience')->nullable();
			$table->text('accreditations')->nullable();
			
			//payment details
			$table->double('hourly_rate', 5, 2)->nullable();
			$table->integer('min_callout')->nullable();
			$table->enum('payment_type', array('bacs', 'paypal', 'stripe'))->nullable();
			$table->enum('payment_period', array('monthly', 'job_by_job'))->nullable();
			
			$table->integer('no_of_votes')->default(0);
			$table->integer('votes_sum')->default(0);
			
			//billing details
			$table->boolean('different_billing_address')->default(false);
			$table->string('billing_title', 20);
			$table->string('billing_first_name', 255);
			$table->string('billing_last_name', 255);
			$table->string('billing_job_title', 255);
			$table->string('billing_mobile_number', 50);
			$table->boolean('billing_mobile_sms_only')->default(false);
			$table->string('billing_phone_number', 50);
			$table->string('billing_fax_number', 50);
			$table->string('billing_organization_name', 255);
			$table->string('billing_reference', 255);
			$table->string('billing_address_1', 255);
			$table->string('billing_address_2', 255);
			$table->string('billing_city', 255);
			$table->string('billing_postcode', 50);
			$table->string('billing_county', 255);
			$table->string('billing_country', 255);
			
			$table->index('facebook_id');
			$table->index('linkedin_id');
			$table->index('organization_id');
			$table->index('picture_uploaded');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persons');
	}
}