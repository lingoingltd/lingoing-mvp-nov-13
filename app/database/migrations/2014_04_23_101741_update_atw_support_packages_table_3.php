<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAtwSupportPackagesTable3 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('atw_support_packages', function(Blueprint $table){
			
			$table->boolean('removed')->default(false);
			
			$table->index('removed');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('atw_support_packages', function(Blueprint $table){
			
			$table->dropColumn('removed');
			
		});
	}

}