<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResignsLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resigns_log', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			
			$table->timestamps();
			
			$table->integer('person_id')->unsigned();
			$table->integer('job_id')->unsigned();
			$table->text('reason')->nullable()->default(null);
			$table->string('ip', 15)->nullable()->default(null);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('resigns_log');
	}

}