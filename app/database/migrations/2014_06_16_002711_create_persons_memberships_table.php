<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsMembershipsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persons_memberships', function(Blueprint $table){

			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->timestamps();
			$table->integer('person_id')->unsigned()->nullable();
			$table->string('type', 50); //sli, sli2, slt, notetaker, ...
			$table->string('membership_body', 255);
			$table->string('reference_number', 255)->nullable();
			$table->date('expiry_date')->nullable();
			$table->boolean('expired')->default(false);
			$table->boolean('sent_expired_notification')->default(false);

			$table->index('membership_body');
			$table->index('expired');
			$table->index('sent_expired_notification');

			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');

		});
		
		Schema::create('persons_memberships_languages', function(Blueprint $table){

			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->timestamps();
			$table->integer('membership_id')->unsigned()->nullable();
			$table->string('language', 255);

			$table->foreign('membership_id')->references('id')->on('persons_memberships')->onDelete('cascade');

		});
		
		/*
		
		$array = array('sli', 'sli2', 'slt');

		$persons = Person::all();
		foreach($persons as $person)
		{
			foreach($array as $type)
			{
				$options = $person->searchOptions($type);
				if(count($options) > 0)
				{
					$memberships = $person->searchOptions($type . '_membership_bodies');
					if(count($memberships) > 0)
					{
						foreach($memberships as $membership)
						{
							$newMembership = new PersonMembership();
							$newMembership->membership_body = $membership->option_value;
							$newMembership->person_id = $membership->person_id;
							$newMembership->type = $type;
							$newMembership->save();

							foreach($options as $option)
							{
								$language = new PersonMembershipLanguage();
								$language->membership_id = $newMembership->id;
								$language->language = $option->option_value;
								$language->save();
							}
						}
					}

					$reference_number = $person->searchOptions($type . '_membership_bodies_reference_number');
					if(count($reference_number) > 0)
					{
						$reference_number = $reference_number[0];

						$ref = explode(',', $reference_number->option_value);
						foreach($ref as &$r)
							$r = trim($r);

						$memberships = PersonMembership::where('person_id', '=', $reference_number->person_id)->where('type', '=', $type)->get();

						$i = 0;
						while($i < count($ref))
						{
							$member = $memberships[$i];
							$member->reference_number = $ref[$i];
							$member->save();

							$i++;
						}
					}
				}
			}
		}
		
		*/
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}