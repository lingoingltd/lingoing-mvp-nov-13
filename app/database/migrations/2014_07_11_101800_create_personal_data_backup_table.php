<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalDataBackupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('personal_data_backup', function(Blueprint $table){
			
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->timestamps();
			$table->integer('person_id')->unsigned();
			$table->string('first_name', 255)->nullable();
			$table->string('last_name', 255)->nullable();
			$table->string('email', 255);
			
			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
			
		});
		
		$persons = Person::all();
		foreach($persons as $person)
		{
			$address = $person->default_address();
			
			$backup = new PersonalDataBackup();
			$backup->person_id = $person->id;
			$backup->first_name = $address->first_name;
			$backup->last_name = $address->last_name;
			$backup->email = $person->email;
			$backup->save();
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('personal_data_backup');
	}

}