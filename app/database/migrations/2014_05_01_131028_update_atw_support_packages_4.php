<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAtwSupportPackages4 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('atw_support_packages', function(Blueprint $table){
			
			$table->dropColumn('verified');
			
			$table->enum('status', array('not_verified', 'verified', 'in_review', 'canceled', 'expired'))->default('not_verified');
			$table->index('status');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('atw_support_packages', function(Blueprint $table){
			
			$table->dropColumn('status');
			
			$table->boolean('verified')->default(false);
			$table->index('verified');
			
		});
	}

}