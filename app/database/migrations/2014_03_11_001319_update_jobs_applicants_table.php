<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateJobsApplicantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('jobs_applicants', function(Blueprint $table)
		{
			$table->decimal('budget_show', 10, 2)->nullable()->default(null)->after('budget');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('jobs_applicants', function(Blueprint $table)
		{
			$table->dropColumn('budget_show');
		});
	}

}