<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesJobsDatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices_jobs_dates', function(Blueprint $table)
		{
			
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->integer('invoice_id')->unsigned();
			$table->date('date');
			$table->time('start_time');
			$table->time('end_time');
			
			$table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices_jobs_dates');
	}

}