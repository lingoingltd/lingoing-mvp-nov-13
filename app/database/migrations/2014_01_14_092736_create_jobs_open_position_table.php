<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsOpenPositionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs_open_position', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->integer('job_id')->unsigned();
			$table->integer('worker_id')->unsigned()->nullable()->default(null);
			$table->boolean('spoken_language_interpreter')->default(false);
			$table->boolean('sign_language_interpreter')->default(false);
			$table->boolean('lipspeaker')->default(false);
			$table->boolean('speech_to_text_reporter')->default(false);
			$table->boolean('sign_language_translator')->default(false);
			$table->boolean('note_taker')->default(false);
			$table->boolean('no_of_people')->default(false);
			
			$table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
			$table->foreign('worker_id')->references('id')->on('persons')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs_open_position');
	}

}