<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvoicesTable5 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			
			$table->dropColumn('begin');
			$table->dropColumn('end');
			$table->dropColumn('list_expiry');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			
			$table->dateTime('begin')->nullable()->default(null);
			$table->dateTime('end')->nullable()->default(null);
			$table->date('list_expiry')->nullable()->default(null);
			
		});
	}

}