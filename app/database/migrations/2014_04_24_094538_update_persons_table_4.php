<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePersonsTable4 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('persons', function(Blueprint $table)
		{
			
			$table->string('referer_code', 255)->nullable();
			$table->boolean('sales_rep_enabled')->default(false);
			$table->integer('referer_id')->unsigned()->nullable();
			
			$table->index('referer_code');
			$table->index('sales_rep_enabled');
			$table->foreign('referer_id')->references('id')->on('persons')->onDelete('cascade');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE `persons` DROP FOREIGN KEY `persons_referer_id_foreign` ;');
		
		Schema::table('persons', function(Blueprint $table)
		{			
			
			$table->dropColumn('sales_rep_enabled');
			$table->dropColumn('referer_code');
			$table->dropColumn('referer_id');
			
		});
	}

}