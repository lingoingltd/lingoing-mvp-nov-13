<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateJobsOpenPositionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('jobs_open_position', function(Blueprint $table){
		
			$table->dropColumn('no_of_people');
		
			$table->string('number_of_audience', 10)->nullable();
			$table->integer('no_in_groups')->nullable();
			$table->boolean('coworker')->default(false);
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('jobs_open_position', function(Blueprint $table){
		
			$table->boolean('no_of_people')->default(false);
			
			$table->dropColumn('number_of_audience');
			$table->dropColumn('no_in_groups');
			$table->dropColumn('coworker');
			
		});
	}

}