<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cron_jobs', function(Blueprint $table){
			
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			
			$table->timestamps();
			
			$table->string('description', 255);
			$table->text('last_status');
			$table->integer('no_of_sent_mails')->nullable();
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cron_jobs');
	}

}