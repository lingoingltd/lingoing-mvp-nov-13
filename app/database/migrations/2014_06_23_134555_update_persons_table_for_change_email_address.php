<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePersonsTableForChangeEmailAddress extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('persons', function(Blueprint $table){

			$table->string('unconfirmed_email', 255)->nullable();
			$table->string('new_email_confirmation', 40)->nullable();

			$table->index('new_email_confirmation');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('persons', function(Blueprint $table){

			$table->dropColumn('unconfirmed_email');
			$table->dropColumn('new_email_confirmation');

		});
	}

}