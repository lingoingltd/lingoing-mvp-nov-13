<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRecruitmentsApplicationsTableForS3Purpose extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recruitments_applications', function(Blueprint $table){
			
			$table->string('attachment', 255)->nullable();
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recruitments_applications', function(Blueprint $table){
			
			$table->dropColumn('attachment');
			
		});
	}

}