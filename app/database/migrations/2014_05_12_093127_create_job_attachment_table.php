<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobAttachmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs_attachments', function(Blueprint $table){
			
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			
			$table->integer('job_id')->unsigned()->nullable();
			$table->integer('job_date_id')->unsigned()->nullable();
			$table->string('filename', 255);
			$table->string('description', 255);
			
			$table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
			$table->foreign('job_date_id')->references('id')->on('jobs_dates')->onDelete('cascade');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs_attachments');
	}

}