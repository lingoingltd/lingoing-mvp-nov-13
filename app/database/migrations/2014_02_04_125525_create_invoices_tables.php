<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->timestamps();
			$table->integer('person_id')->unsigned();
			$table->integer('job_id')->unsigned();
			$table->enum('status', array('paid', 'unpaid', 'pending'))->default('unpaid');
			
			$table->string('number', 20);
			
			$table->string('client_name', 255);
			$table->string('client_address_1', 255);
			$table->string('client_address_2', 255)->nullable();
			$table->string('client_city', 255);
			$table->string('client_country', 255);
			$table->string('client_county', 255)->nullable();
			$table->string('client_postcode', 255);
			
			$table->string('lsp_name', 255);
			$table->string('lsp_address_1', 255);
			$table->string('lsp_address_2', 255)->nullable();
			$table->string('lsp_city', 255);
			$table->string('lsp_country', 255);
			$table->string('lsp_county', 255)->nullable();
			$table->string('lsp_postcode', 255);
			
			$table->string('lsp_organization_name', 255);
			$table->string('lsp_organization_address_1', 255);
			$table->string('lsp_organization_address_2', 255)->nullable();
			$table->string('lsp_organization_city', 255);
			$table->string('lsp_organization_country', 255);
			$table->string('lsp_organization_county', 255)->nullable();
			$table->string('lsp_organization_postcode', 255);
			$table->string('lsp_organization_registration_number', 20)->nullable();
			$table->boolean('lsp_organization_vat_registered')->default(false);
			$table->string('lsp_organization_vat_number', 15);
			
			$table->dateTime('begin')->nullable()->default(null);
			$table->dateTime('end')->nullable()->default(null);
			$table->date('list_expiry')->nullable()->default(null);
			
			$table->decimal('lsp_cost', 10, 2)->nullable()->default(null);
			$table->decimal('overtime_hours', 10, 2)->nullable()->default(null);
			$table->decimal('cost_per_overtime_hour', 10, 2)->nullable()->default(null);
			$table->decimal('lsp_cost_overtime', 10, 2)->nullable()->default(null);
			$table->decimal('lingoing_commission', 10, 2)->nullable()->default(null);
			$table->decimal('lingoing_commission_vat', 10, 2)->nullable()->default(null);
			$table->decimal('total_cost', 10, 2)->nullable()->default(null);
			
			$table->date('payment_due')->nullable()->default(null);
			$table->enum('payment_type', array('bacs', 'paypal', 'stripe'))->nullable();
			
			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
			$table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices');
	}

}