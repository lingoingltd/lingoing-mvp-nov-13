<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsDatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs_dates', function(Blueprint $table){
		
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->integer('job_id')->unsigned();
			$table->date('date');
			$table->time('start_time');
			$table->time('end_time');
			
			$table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
		
		});
		
		Schema::table('jobs', function(Blueprint $table){
			
			$table->integer('parent_job_id')->nullable();
			$table->string('date_type', 20)->default('one_day');
			
			$table->index('parent_job_id');
			$table->index('date_type');
			
			$table->dropColumn('begin');
			$table->dropColumn('end');
			$table->dropColumn('list_expiry');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs_dates');
		
		Schema::table('jobs', function(Blueprint $table){
		
			$table->dateTime('begin')->nullable()->default(null);
			$table->dateTime('end')->nullable()->default(null);
			$table->date('list_expiry')->nullable()->default(null);
			
			$table->dropColumn('parent_job_id');
			$table->dropColumn('date_type');
					
		});
	}

}