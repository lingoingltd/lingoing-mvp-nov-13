<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsJobsAttendingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persons_jobs_attending', function(Blueprint $table){
			
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->timestamps();
			$table->integer('client_id')->unsigned();
			$table->integer('worker_id')->unsigned();
			$table->integer('job_id')->unsigned()->nullable();
			$table->text('comment')->nullable();
			
			$table->foreign('client_id')->references('id')->on('persons')->onDelete('cascade');
			$table->foreign('worker_id')->references('id')->on('persons')->onDelete('cascade');
			
		});
		
		Schema::table('persons', function(Blueprint $table){
			
			$table->integer('no_of_bookings')->default(0);
			$table->integer('no_of_non_attendings')->default(0);
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persons_jobs_attending');
		
		Schema::table('persons', function(Blueprint $table){
			
			$table->dropColumn('no_of_bookings');
			$table->dropColumn('no_of_non_attendings');
			
		});
	}

}