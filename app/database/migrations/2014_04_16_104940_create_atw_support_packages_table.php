<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtwSupportPackagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('atw_support_packages', function(Blueprint $table){
			
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->integer('person_id')->unsigned();
			$table->integer('address_id')->unsigned();
			$table->boolean('verified')->default(false);
			
			$table->enum('given_to', array('freelancer', 'company'))->nullable();
			$table->string('national_insurance', 255)->nullable();
			$table->string('email_address', 255)->nullable();
			$table->enum('your_budget_in', array('total', 'per_hour'))->nullable();
			$table->decimal('budget', 10, 2)->nullable();
			$table->enum('type_of_lsp', array('british_sign_language', 'communication_support_worker', 'note_taker', 'lip_speaker', 'other'))->nullable();
			$table->string('type_of_lsp_other', 255)->nullable();			
			$table->enum('receive_invoice', array('end_of_month', 'every_two_weeks', 'job_by_job'))->nullable();
			$table->string('additional_information', 255)->nullable();
			$table->date('support_end');
			$table->enum('hours_per', array('week', 'month', 'year'));
			$table->decimal('hours_per_sum', 10, 2);
			
			
			$table->index('verified');
			$table->index('given_to');
			$table->index('receive_invoice');
			$table->index('support_end');
			$table->index('hours_per');
			
			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
			$table->foreign('address_id')->references('id')->on('persons_addresses')->onDelete('cascade');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('atw_support_packages');
	}

}