<?php
use \Aws\S3\Enum\CannedAcl;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persons_documents', function(Blueprint $table){

			$table->engine = 'InnoDB';

			$table->increments('id');
			$table->timestamps();
			$table->integer('person_id')->unsigned()->nullable();
			$table->integer('membership_id')->unsigned()->nullable();
			$table->string('type', 50)->nullable(); //crb, insurance
			$table->string('filename', 255)->nullable();
			$table->text('filename_url')->nullable();
			$table->integer('filesize')->default(0);

			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
			$table->foreign('membership_id')->references('id')->on('persons_memberships')->onDelete('cascade');

			$table->index('type');
		});
		
/*		$s3 = App::make('aws')->get('s3');
		
		$options = PersonOption::all();
		foreach($options as $option)
		{
			if($option->option_key == 'insurance' || $option->option_key == 'crb')
			{
				$filename = public_path() . '/uploads/' . $option->person_id . '/' . $option->option_value;
				$infos = pathinfo($filename);
				
				$newFilename = $option->option_key . '-' . $option->person_id . '-' . $infos['filename'] . '-' . date('YmdHis', time()) . '.' . $infos['extension'];
				
				$s3->putObject(array(
				    'Bucket'     => Config::get('amazon.bucket_prefix') . 'docs',
				    'Key'        => $newFilename,
				    'SourceFile' => $filename,
					'ACL'		 => CannedAcl::AUTHENTICATED_READ
				));
				
				$document = new PersonDocument();
				$document->person_id = $option->person_id;
				$document->type = $option->option_key;
				$document->filename = $newFilename;
				$document->filesize = filesize($filename);
				$document->save();
				
//				$option->delete();
				
//				@unlink($filename);
			}
		}*/
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}