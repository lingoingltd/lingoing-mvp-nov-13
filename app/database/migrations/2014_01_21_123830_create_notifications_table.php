<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->timestamps();
			$table->integer('sender_id')->unsigned()->nullable();
			$table->integer('receiver_id')->unsigned()->nullable();
			$table->integer('job_id')->unsigned()->nullable();
			$table->string('notification', 255);
			$table->string('link', 255)->nullable();
			$table->string('link_title', 255)->nullable();
			
			$table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
			$table->foreign('sender_id')->references('id')->on('persons')->onDelete('cascade');
			$table->foreign('receiver_id')->references('id')->on('persons')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
	}

}