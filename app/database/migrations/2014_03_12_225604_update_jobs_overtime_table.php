<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateJobsOvertimeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('jobs_overtimes', function(Blueprint $table)
		{
			$table->decimal('hourly_rate', 10, 2)->nullable()->default(null);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('jobs_overtimes', function(Blueprint $table)
		{
			$table->dropColumn('hourly_rate');
		});
	}

}