<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePersonTable3 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('persons', function(Blueprint $table)
		{
			
			$table->enum('how_to_pay_bills', array('paying_card', 'atw_customer'))->nullable();
			
			$table->index('how_to_pay_bills');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('persons', function(Blueprint $table)
		{
			
			$table->dropColumn('how_to_pay_bills');
			
		});
	}

}