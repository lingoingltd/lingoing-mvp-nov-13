<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateJobsTable4 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('jobs', function(Blueprint $table){
			
			$table->enum('payment_method', array('paying_card', 'atw_customer'))->default('paying_card');
			$table->integer('support_package_id')->unsigned()->nullable();
			$table->enum('lsp_payment', array('lsp_directly', 'client_will_pay'))->default('lsp_directly')->nullable();
			
			$table->index('payment_method');
			$table->index('lsp_payment');
			
			$table->foreign('support_package_id')->references('id')->on('atw_support_packages')->onDelete('cascade');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('jobs', function(Blueprint $table){
			
			$table->dropColumn('payment_method');
			$table->dropColumn('support_package_id');
			$table->dropColumn('lsp_payment');
			
		});
	}

}