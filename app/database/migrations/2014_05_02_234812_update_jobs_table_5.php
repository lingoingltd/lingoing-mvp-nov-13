<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateJobsTable5 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('jobs', function(Blueprint $table){
			
			$table->dropColumn('lsp_payment');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('jobs', function(Blueprint $table){
			
			$table->enum('lsp_payment', array('lsp_directly', 'client_will_pay'))->default('lsp_directly')->nullable();
			
			$table->index('lsp_payment');
			
		});
	}

}