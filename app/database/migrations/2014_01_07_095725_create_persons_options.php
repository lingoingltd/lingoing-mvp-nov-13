<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsOptions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persons_options', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->integer('person_id')->unsigned();
			$table->string('option_key', 255);
			$table->string('option_value', 255);
			
			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
			
			$table->index('option_key');
			$table->index('option_value');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persons_options');
	}

}