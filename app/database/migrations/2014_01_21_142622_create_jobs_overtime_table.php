<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsOvertimeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs_overtimes', function(Blueprint $table)
		{
			$table->engine = "InnoDB";
			
			$table->increments('id');
			$table->timestamps();
			$table->integer('job_id')->unsigned();
			$table->integer('person_id')->unsigned();
			$table->decimal('minutes', 10, 2)->nullable();
			$table->integer('hours')->nullable();
			$table->enum('status', array('open', 'approved', 'denied'))->default('open');
			
			$table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs_overtimes');
	}

}