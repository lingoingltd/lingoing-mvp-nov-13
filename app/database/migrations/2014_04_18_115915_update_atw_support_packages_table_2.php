<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAtwSupportPackagesTable2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('atw_support_packages', function(Blueprint $table){
			
			$table->timestamps();
			
			$table->string('filename', 255);
			$table->string('filename_mime_type', 255);
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('atw_support_packages', function(Blueprint $table){
			
			$table->dropColumn('created_at');
			$table->dropColumn('updated_at');
			$table->dropColumn('filename');
			$table->dropColumn('filename_mime_type');
			
		});
	}

}