<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('organizations', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->timestamps();
			$table->integer('person_id')->unsigned(); //organizations admin person
			
			//organization details
			$table->string('name', 255);
			$table->string('address_1', 255);
			$table->string('address_2', 255)->nullable()->default(null);
			$table->string('city', 255);
			$table->string('postcode', 50);
			$table->string('county', 255)->nullable()->default(null);
			$table->string('country', 2);
			$table->string('phone', 255);
			$table->string('fax', 255)->nullable()->default(null);
			$table->enum('organization_type', array('soletrader', 'partnership', 'limitedcompany'))->nullable()->default(null);
			$table->string('registration_number', 20)->nullable()->default(null);
			$table->boolean('vat_registered')->default(false);
			$table->string('vat_number', 15)->nullable()->default(null);
			
			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('organizations');
	}

}