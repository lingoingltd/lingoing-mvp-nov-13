<?php
use \Aws\S3\Enum\CannedAcl;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePersonsTableForS3Purpose extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('persons', function(Blueprint $table){

			$table->string('profile_picture_url', 255)->nullable();

		});

		$s3 = App::make('aws')->get('s3');

		//copy every profile picture to S3
		$persons = Person::all();
		foreach($persons as $person)
		{
			$sourceTempFile = public_path() . '/uploads/avatars/' . $person->id . '_temp.jpg';
			$sourceFile = public_path() . '/uploads/avatars/' . $person->id . '.jpg';
			if(@file_exists($sourceFile))
			{
				$s3->putObject(array(
				    'Bucket'     => Config::get('amazon.bucket_prefix') . 'avatars',
				    'Key'        => $person->id . '.jpg',
				    'SourceFile' => $sourceFile,
					'ACL'		 => CannedAcl::PUBLIC_READ
				));

				$person->profile_picture_url = $s3->getObjectUrl(
					Config::get('amazon.bucket_prefix') . 'avatars', 
					$person->id . '.jpg'
				);
				$person->save();

//				@unlink($sourceFile);
			}

			if(@file_exists($sourceTempFile))
				@unlink($sourceTempFile);
		}

		Schema::table('persons', function(Blueprint $table){

			$table->dropColumn('picture_uploaded');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('persons', function(Blueprint $table){

			$table->boolean('picture_uploaded')->default(false);

			$table->index('picture_uploaded');

		});

		//copy everything from S3 to local disc
		$s3 = App::make('aws')->get('s3');
		$model = $s3->listObjects(array(
			'Bucket'	=> Config::get('amazon.bucket_prefix') . 'avatars'
		));

		$objects = $model['Contents'];
		foreach($objects as $object)
		{
			$id = (int)str_replace('.jpg', '', $object['Key']);
			$person = Person::find($id);

			$s3->getObject(array(
			    'Bucket' => Config::get('amazon.bucket_prefix') . 'avatars',
			    'Key'    => $object['Key'],
			    'SaveAs' => public_path() . '/uploads/avatars/' . $object['Key']
			));

			$s3->deleteObject(array(
				'Bucket'	=> Config::get('amazon.bucket_prefix') . 'avatars',
				'Key'		=> $object['Key']
			));

			$person->picture_uploaded = true;
			$person->save();
		}

		Schema::table('persons', function(Blueprint $table){

			$table->dropColumn('profile_picture_url');

		});
	}

}