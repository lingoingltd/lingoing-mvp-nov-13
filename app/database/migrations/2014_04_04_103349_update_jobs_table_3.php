<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateJobsTable3 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('jobs', function(Blueprint $table){
		
			$table->decimal('hourly_rate', 10, 2)->nullable()->after('budget_lsp_only');
			$table->decimal('hourly_rate_lsp_only', 10, 2)->nullable()->after('hourly_rate');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('jobs', function(Blueprint $table){
			
			$table->dropColumn('hourly_rate');
			$table->dropColumn('hourly_rate_lsp_only');
			
		});
	}

}