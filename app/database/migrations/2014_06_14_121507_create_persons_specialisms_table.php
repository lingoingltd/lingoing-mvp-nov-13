<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsSpecialismsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persons_specialisms', function(Blueprint $table){
		
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->timestamps();
			$table->integer('person_id')->unsigned()->nullable();
			$table->string('specialism', 255);
			
			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
			
		});
		
/*		$options = PersonOption::all();
		foreach($options as $option)
		{
			if($option->option_key == 'specialisms')
			{
				$specialism = new PersonSpecialism();
				$specialism->person_id = $option->person_id;
				$specialism->specialism = $option->option_value;
				$specialism->save();
				
//				$option->delete();
			}
		}*/
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}