<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvoicesTable8 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoices', function(Blueprint $table){
			
			$table->date('paid_date')->nullable()->after('status_worker');
			
			$table->boolean('atw_form_sent')->default(false);
			$table->datetime('atw_form_sent_at')->nullable();
			$table->boolean('atw_form_notification_sent')->default(false);
			
			$table->boolean('lingoing_atw_form_sent')->default(false);
			$table->datetime('lingoing_atw_form_sent_at')->nullable();
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('invoices', function(Blueprint $table){
			
			$table->dropColumn('paid_date');
			
			$table->dropColumn('atw_form_sent');
			$table->dropColumn('atw_form_sent_at');
			$table->dropColumn('atw_form_notification_sent');
			
			$table->dropColumn('lingoing_atw_form_sent');
			$table->dropColumn('lingoing_atw_form_sent_at');
			
		});
	}

}