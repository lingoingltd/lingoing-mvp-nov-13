<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsAddressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persons_addresses', function(Blueprint $table){
			
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->integer('person_id')->unsigned();
			$table->enum('address_type', array('default', 'billing'))->nullable();
			$table->string('title', 20)->nullable();
			$table->string('first_name', 255)->nullable();
			$table->string('last_name', 255)->nullable();
			$table->string('organization_name', 255)->nullable();
			$table->string('address_1', 255);
			$table->string('address_2', 255);
			$table->string('city', 255);
			$table->string('country', 255);
			$table->string('county', 255);
			$table->string('postcode', 255);
			$table->string('mobile_number', 50);
			$table->string('phone_number', 50);
			$table->string('fax_number', 50);
			$table->string('billing_reference', 255);
			
			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
			
			$table->index('address_type');
			
		});
		
		$persons = Person::all();
		foreach($persons as $person)
		{
			$address = new PersonAddress();
			$address->person_id = $person->id;
			$address->address_type = 'default';
			$address->first_name = $person->first_name;
			$address->last_name = $person->last_name;
			$address->address_1 = $person->address_1;
			$address->address_2 = $person->address_2;
			$address->city = $person->city;
			$address->country = $person->country;
			$address->county = $person->county;
			$address->postcode = $person->postcode;
			$address->title = $person->title;
			$address->mobile_number = $person->mobile_number;
			$address->save();
		}
		
		Schema::table('persons', function(Blueprint $table){
			
			$table->dropColumn('first_name');
			$table->dropColumn('last_name');
			$table->dropColumn('address_1');
			$table->dropColumn('address_2');
			$table->dropColumn('city');
			$table->dropColumn('country');
			$table->dropColumn('county');
			$table->dropColumn('postcode');
			$table->dropColumn('title');
			$table->dropColumn('mobile_number');
			
			$table->dropColumn('different_billing_address');
			$table->dropColumn('billing_title');
			$table->dropColumn('billing_first_name');
			$table->dropColumn('billing_last_name');
			$table->dropColumn('billing_organization_name');
			$table->dropColumn('billing_job_title');
			$table->dropColumn('billing_mobile_number');
			$table->dropColumn('billing_mobile_sms_only');
			$table->dropColumn('billing_phone_number');
			$table->dropColumn('billing_fax_number');
			$table->dropColumn('billing_reference');
			$table->dropColumn('billing_address_1');
			$table->dropColumn('billing_address_2');
			$table->dropColumn('billing_city');
			$table->dropColumn('billing_postcode');
			$table->dropColumn('billing_county');
			$table->dropColumn('billing_country');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('persons', function(Blueprint $table){
		
			$table->string('first_name', 255)->nullable()->after('updated_at');
			$table->string('last_name', 255)->nullable()->after('first_name');
			$table->string('address_1', 255)->after('last_name');
			$table->string('address_2', 255)->after('address_1');
			$table->string('city', 255)->after('address_2');
			$table->string('country', 255)->after('city');
			$table->string('county', 255)->after('country');
			$table->string('postcode', 255)->after('county');
			$table->string('title', 20)->nullable()->after('postcode');
			$table->string('mobile_number', 50)->after('title');
			
			$table->boolean('different_billing_address')->default(false);
			$table->string('billing_title', 20);
			$table->string('billing_first_name', 255);
			$table->string('billing_last_name', 255);
			$table->string('billing_job_title', 255);
			$table->string('billing_mobile_number', 50);
			$table->boolean('billing_mobile_sms_only')->default(false);
			$table->string('billing_phone_number', 50);
			$table->string('billing_fax_number', 50);
			$table->string('billing_organization_name', 255);
			$table->string('billing_reference', 255);
			$table->string('billing_address_1', 255);
			$table->string('billing_address_2', 255);
			$table->string('billing_city', 255);
			$table->string('billing_postcode', 50);
			$table->string('billing_county', 255);
			$table->string('billing_country', 255);
			
		});
		
		$persons = Person::all();
		foreach($persons as $person)
		{
			$address = PersonAddress::where('person_id', '=', $person->id)
				->where('address_type', '=', 'default')
				->first();
				
			if($address == null)
				continue;
				
			$person->first_name = $address->first_name;
			$person->last_name = $address->last_name;
			$person->address_1 = $address->address_1;
			$person->address_2 = $address->address_2;
			$person->city = $address->city;
			$person->country = $address->country;
			$person->county = $address->county;
			$person->postcode = $address->postcode;
			$person->title = $address->title;
			$person->mobile_number = $address->mobile_number;
			$person->save();		
		}
		
		Schema::drop('persons_addresses');
	}

}