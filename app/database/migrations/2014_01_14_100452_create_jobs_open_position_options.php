<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsOpenPositionOptions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs_open_position_option', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->integer('job_open_position_id')->unsigned();
			$table->string('option_key', 255);
			$table->string('option_value', 255);
			
			$table->foreign('job_open_position_id')->references('id')->on('jobs_open_position')->onDelete('cascade');
			
			$table->index('option_key');
			$table->index('option_value');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs_open_position_option');
	}

}