<?php

use Illuminate\Database\Migrations\Migration;

class UpdateJobsTable2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE `jobs` CHANGE `project_budget` `project_budget` ENUM( 'open_to_quotes', 'fixed', 'fixed_hourly_rate' ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement("ALTER TABLE `jobs` CHANGE `project_budget` `project_budget` ENUM( 'open_to_quotes', 'fixed' ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;");
	}

}