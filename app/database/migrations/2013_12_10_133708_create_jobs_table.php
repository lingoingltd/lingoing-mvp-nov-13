<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->timestamps();
			$table->integer('person_id')->unsigned(); //organizations admin person
			$table->boolean('confirmed')->default(false);
			$table->enum('status', array('awaiting_applications', 'confirmed', 'finished', 'canceled', 'invoice_sent'));
			$table->string('title', 255);
			$table->text('description');
			$table->string('type', 100)->nullable()->default(null);
			$table->integer('related_bookings')->unsigned()->nullable()->default(null);
			$table->enum('project_budget', array('open_to_quotes', 'fixed'));
			$table->decimal('budget', 10, 2)->nullable()->default(null);
			$table->decimal('budget_lsp_only', 10, 2)->nullable()->default(null);
			$table->boolean('travel_expenses')->default(false);
			$table->boolean('co_worker')->default(false);
			$table->boolean('co_worker_or_double_rate')->default(false);
			$table->boolean('unsociable_hours')->default(false);
			$table->boolean('other_arrangement')->default(false);
			$table->string('other_arrangement_text')->nullable()->default(null);
			
			$table->string('address_1', 255);
			$table->string('address_2', 255)->nullable()->default(null);
			$table->string('city', 255);
			$table->string('postcode', 50);
			$table->string('county', 255)->nullable()->default(null);
			$table->string('country', 2);
			
			$table->dateTime('begin')->nullable()->default(null);
			$table->dateTime('end')->nullable()->default(null);
			$table->date('list_expiry')->nullable()->default(null);
			
			$table->index('confirmed');
			$table->index('status');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs');
	}

}