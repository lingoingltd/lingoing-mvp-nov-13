<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persons_reviews', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->timestamps();
			$table->integer('person_id')->unsigned();
			$table->integer('client_id')->unsigned();
			$table->integer('job_id')->unsigned();
			
			$table->integer('stars');
			$table->text('review');
			
			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
			$table->foreign('client_id')->references('id')->on('persons')->onDelete('cascade');
			$table->foreign('job_id')->references('id')->on('jobs')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persons_reviews');
	}

}