<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvoicesTable7 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('invoices', function(Blueprint $table)
		{
			
			$table->string('client_surname')->after('client_name');
			$table->string('client_title')->after('number');
			$table->string('client_email')->after('client_postcode');
			$table->string('lsp_position')->after('lsp_email');
			$table->integer('atw_package_id')->unsigned()->nullable()->default(null)->after('job_id');
			
			$table->date('atw_date_begin')->nullable();
			$table->date('atw_date_end')->nullable();
			
			$table->foreign('atw_package_id')->references('id')->on('atw_support_packages')->onDelete('cascade');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE `invoices` DROP FOREIGN KEY `invoices_atw_package_id_foreign`;');
		
		Schema::table('invoices', function(Blueprint $table)
		{
			
			$table->dropColumn('client_email');
			$table->dropColumn('client_surname');
			$table->dropColumn('client_title');
			$table->dropColumn('lsp_position');
			$table->dropColumn('atw_package_id');
			$table->dropColumn('atw_date_begin');
			$table->dropColumn('atw_end_begin');
			
		});
	}

}