<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsSendInvitationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persons_send_invitations', function(Blueprint $table){
			
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->timestamps();
			$table->integer('person_id')->unsigned();
			$table->string('first_name', 255);
			$table->string('last_name', 255);
			$table->string('email', 255);
			$table->text('message')->nullable();
			$table->string('token', 255);
			$table->boolean('registered')->default(false);
			
			$table->index('token');
			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persons_send_invitations');
	}

}