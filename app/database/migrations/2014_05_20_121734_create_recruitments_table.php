<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecruitmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recruitments', function(Blueprint $table){
			
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->timestamps();
			$table->string('title', 255)->nullable();
			$table->text('short_description')->nullable();
			$table->string('job_type', 50);
			$table->string('salary', 255)->nullable();
			$table->string('location')->nullable();
			$table->text('description');
			$table->string('question', 255)->nullable();
			
			$table->index('job_type');
			
		});
		
		Schema::create('recruitments_applications', function(Blueprint $table){
		
			$table->engine = 'InnoDB';
			
			$table->increments('id');
			$table->integer('recruitment_id')->unsigned()->nullable();
			$table->string('name', 255);
			$table->string('email', 255);
			$table->string('address', 255);
			$table->string('phone', 30);
			$table->string('answer', 255);
			$table->boolean('cv')->default(false);
			$table->integer('person_id')->unsigned()->nullable();
			
			$table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
			$table->foreign('recruitment_id')->references('id')->on('recruitments')->onDelete('cascade');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recruitments');
		Schema::drop('recruitments_applications');
	}

}