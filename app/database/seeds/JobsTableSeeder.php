<?php
class JobsTableSeeder extends Seeder {

    public function run()
    {
		$job = Job::create(array(
			'person_id' => 1,
			'confirmed' => 1,
			'status' => 'awaiting_applications',
			'type' => 'office',
			'project_budget' => 'fixed',
			'budget' => 100.00,
			'budget_lsp_only' => 80.00,
			'hourly_rate' => 100.00,
			'hourly_rate_lsp_only' => 80.00,
			'address_1' => 'Cvetna ulica 2a',
			'city' => 'Šentjur',
			'postcode' => '3230',
			'country' => 'UK',
			'date_type' => 'one_day',
			'payment_method' => 'atw_customer',
			'support_package_id' => 1
		));
		
		$jobDate = JobDate::create(array(
			'job_id' => $job->id,
			'date' => '2014-05-10',
			'start_time' => '10:00:00',
			'end_time' => '11:00:00'
		));
		
		$jobOpenPosition = JobOpenPosition::create(array(
			'job_id' => $job->id,
			'spoken_language_interpreter' => 1,
			'number_of_audience' => 'one',
			'coworker' => 0,
			'worker_id' => 2
		));
		
		$jobOpenPositionOption = JobOpenPositionOption::create(array(
			'job_open_position_id' => $jobOpenPosition->id,
			'option_key' => 'sli',
			'option_value' => 'Arabic'
		));
		
		
		$job = Job::create(array(
			'person_id' => 1,
			'confirmed' => 1,
			'status' => 'awaiting_applications',
			'type' => 'office',
			'project_budget' => 'fixed',
			'budget' => 100.00,
			'budget_lsp_only' => 80.00,
			'hourly_rate' => 100.00,
			'hourly_rate_lsp_only' => 80.00,
			'address_1' => 'Cvetna ulica 2a',
			'city' => 'Šentjur',
			'postcode' => '3230',
			'country' => 'UK',
			'date_type' => 'one_day',
			'payment_method' => 'atw_customer',
			'support_package_id' => 1
		));
		
		$jobDate = JobDate::create(array(
			'job_id' => $job->id,
			'date' => '2014-05-14',
			'start_time' => '11:00:00',
			'end_time' => '12:00:00'
		));
		
		$jobOpenPosition = JobOpenPosition::create(array(
			'job_id' => $job->id,
			'sign_language_interpreter' => 1,
			'number_of_audience' => 'one',
			'coworker' => 0,
			'worker_id' => 2
		));
		
		$jobOpenPositionOption = JobOpenPositionOption::create(array(
			'job_open_position_id' => $jobOpenPosition->id,
			'option_key' => 'sli2',
			'option_value' => 'Registered Sign Language Translator (Deaf Relay)'
		));
		
		
		$job = Job::create(array(
			'person_id' => 1,
			'confirmed' => 1,
			'status' => 'awaiting_applications',
			'type' => 'office',
			'project_budget' => 'fixed',
			'budget' => 100.00,
			'budget_lsp_only' => 80.00,
			'hourly_rate' => 100.00,
			'hourly_rate_lsp_only' => 80.00,
			'address_1' => 'Cvetna ulica 2a',
			'city' => 'Šentjur',
			'postcode' => '3230',
			'country' => 'UK',
			'date_type' => 'one_day',
			'payment_method' => 'atw_customer',
			'support_package_id' => 1
		));
		
		$jobDate = JobDate::create(array(
			'job_id' => $job->id,
			'date' => '2014-05-16',
			'start_time' => '12:00:00',
			'end_time' => '13:00:00'
		));
		
		$jobOpenPosition = JobOpenPosition::create(array(
			'job_id' => $job->id,
			'sign_language_interpreter' => 1,
			'number_of_audience' => 'one',
			'coworker' => 0,
			'worker_id' => 2
		));
		
		$jobOpenPositionOption = JobOpenPositionOption::create(array(
			'job_open_position_id' => $jobOpenPosition->id,
			'option_key' => 'sli2',
			'option_value' => 'Registered Sign Language Translator (Deaf Relay)'
		));
		
		
		$job = Job::create(array(
			'person_id' => 1,
			'confirmed' => 1,
			'status' => 'awaiting_applications',
			'type' => 'office',
			'project_budget' => 'fixed',
			'budget' => 100.00,
			'budget_lsp_only' => 80.00,
			'hourly_rate' => 100.00,
			'hourly_rate_lsp_only' => 80.00,
			'address_1' => 'Cvetna ulica 2a',
			'city' => 'Šentjur',
			'postcode' => '3230',
			'country' => 'UK',
			'date_type' => 'one_day',
			'payment_method' => 'atw_customer',
			'support_package_id' => 1
		));
		
		$jobDate = JobDate::create(array(
			'job_id' => $job->id,
			'date' => '2014-05-13',
			'start_time' => '15:00:00',
			'end_time' => '16:00:00'
		));
		
		$jobOpenPosition = JobOpenPosition::create(array(
			'job_id' => $job->id,
			'spoken_language_interpreter' => 1,
			'number_of_audience' => 'one',
			'coworker' => 0,
			'worker_id' => 2
		));
		
		$jobOpenPositionOption = JobOpenPositionOption::create(array(
			'job_open_position_id' => $jobOpenPosition->id,
			'option_key' => 'sli',
			'option_value' => 'Slovene'
		));
		
		
		
		//saduf
		$job = Job::create(array(
			'person_id' => 4,
			'confirmed' => 1,
			'status' => 'awaiting_applications',
			'type' => 'office',
			'project_budget' => 'fixed',
			'budget' => 100.00,
			'budget_lsp_only' => 80.00,
			'hourly_rate' => 100.00,
			'hourly_rate_lsp_only' => 80.00,
			'address_1' => 'Cvetna ulica 2a',
			'city' => 'Šentjur',
			'postcode' => '3230',
			'country' => 'UK',
			'date_type' => 'one_day',
			'payment_method' => 'atw_customer',
			'support_package_id' => 1
		));
		
		$jobDate = JobDate::create(array(
			'job_id' => $job->id,
			'date' => '2014-05-10',
			'start_time' => '07:00:00',
			'end_time' => '08:00:00'
		));
		
		$jobOpenPosition = JobOpenPosition::create(array(
			'job_id' => $job->id,
			'spoken_language_interpreter' => 1,
			'number_of_audience' => 'one',
			'coworker' => 0,
			'worker_id' => 2
		));
		
		$jobOpenPositionOption = JobOpenPositionOption::create(array(
			'job_open_position_id' => $jobOpenPosition->id,
			'option_key' => 'sli',
			'option_value' => 'English'
		));
		
		
		$job = Job::create(array(
			'person_id' => 4,
			'confirmed' => 1,
			'status' => 'awaiting_applications',
			'type' => 'office',
			'project_budget' => 'fixed',
			'budget' => 100.00,
			'budget_lsp_only' => 80.00,
			'hourly_rate' => 100.00,
			'hourly_rate_lsp_only' => 80.00,
			'address_1' => 'Cvetna ulica 2a',
			'city' => 'Šentjur',
			'postcode' => '3230',
			'country' => 'UK',
			'date_type' => 'one_day',
			'payment_method' => 'atw_customer',
			'support_package_id' => 1
		));
		
		$jobDate = JobDate::create(array(
			'job_id' => $job->id,
			'date' => '2014-05-13',
			'start_time' => '08:00:00',
			'end_time' => '09:00:00'
		));
		
		$jobOpenPosition = JobOpenPosition::create(array(
			'job_id' => $job->id,
			'spoken_language_interpreter' => 1,
			'number_of_audience' => 'one',
			'coworker' => 0,
			'worker_id' => 2
		));
		
		$jobOpenPositionOption = JobOpenPositionOption::create(array(
			'job_open_position_id' => $jobOpenPosition->id,
			'option_key' => 'sli',
			'option_value' => 'English'
		));
		
		
		$job = Job::create(array(
			'person_id' => 4,
			'confirmed' => 1,
			'status' => 'awaiting_applications',
			'type' => 'office',
			'project_budget' => 'fixed',
			'budget' => 100.00,
			'budget_lsp_only' => 80.00,
			'hourly_rate' => 100.00,
			'hourly_rate_lsp_only' => 80.00,
			'address_1' => 'Cvetna ulica 2a',
			'city' => 'Šentjur',
			'postcode' => '3230',
			'country' => 'UK',
			'date_type' => 'one_day',
			'payment_method' => 'atw_customer',
			'support_package_id' => 1
		));
		
		$jobDate = JobDate::create(array(
			'job_id' => $job->id,
			'date' => '2014-05-14',
			'start_time' => '09:00:00',
			'end_time' => '10:00:00'
		));
		
		$jobOpenPosition = JobOpenPosition::create(array(
			'job_id' => $job->id,
			'sign_language_interpreter' => 1,
			'number_of_audience' => 'one',
			'coworker' => 0,
			'worker_id' => 2
		));
		
		$jobOpenPositionOption = JobOpenPositionOption::create(array(
			'job_open_position_id' => $jobOpenPosition->id,
			'option_key' => 'sli2',
			'option_value' => 'Registered Sign Language Translator (Deaf Relay)'
		));
		
		
		$job = Job::create(array(
			'person_id' => 4,
			'confirmed' => 1,
			'status' => 'awaiting_applications',
			'type' => 'office',
			'project_budget' => 'fixed',
			'budget' => 100.00,
			'budget_lsp_only' => 80.00,
			'hourly_rate' => 100.00,
			'hourly_rate_lsp_only' => 80.00,
			'address_1' => 'Cvetna ulica 2a',
			'city' => 'Šentjur',
			'postcode' => '3230',
			'country' => 'UK',
			'date_type' => 'one_day',
			'payment_method' => 'atw_customer',
			'support_package_id' => 1
		));
		
		$jobDate = JobDate::create(array(
			'job_id' => $job->id,
			'date' => '2014-05-16',
			'start_time' => '10:00:00',
			'end_time' => '11:00:00'
		));
		
		$jobOpenPosition = JobOpenPosition::create(array(
			'job_id' => $job->id,
			'sign_language_interpreter' => 1,
			'number_of_audience' => 'one',
			'coworker' => 0,
			'worker_id' => 2
		));
		
		$jobOpenPositionOption = JobOpenPositionOption::create(array(
			'job_open_position_id' => $jobOpenPosition->id,
			'option_key' => 'sli2',
			'option_value' => 'Registered Sign Language Translator (Deaf Relay)'
		));
    }

}