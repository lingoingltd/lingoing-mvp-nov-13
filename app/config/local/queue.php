<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Default Queue Driver
	|--------------------------------------------------------------------------
	|
	| The Laravel queue API supports a variety of back-ends via an unified
	| API, giving you convenient access to each back-end using the same
	| syntax for each one. Here you may set the default queue driver.
	|
	| Supported: "sync", "beanstalkd", "sqs", "iron"
	|
	*/

	'default' => 'sqs',

	/*
	|--------------------------------------------------------------------------
	| Queue Connections
	|--------------------------------------------------------------------------
	|
	| Here you may configure the connection information for each server that
	| is used by your application. A default configuration has been added
	| for each back-end shipped with Laravel. You are free to add more.
	|
	*/

	'connections' => array(
		'sqs' => array(
			'driver' => 'sqs',
			'key'    => 'AKIAIOQKFSZ6SLL2FN2Q',
			'secret' => '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',
			'queue'  => 'https://sqs.eu-west-1.amazonaws.com/426245278519/LingoingQueue',
			'region' => 'eu-west-1',
		),
	),

);
