<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Default Queue Driver
	|--------------------------------------------------------------------------
	|
	| The Laravel queue API supports a variety of back-ends via an unified
	| API, giving you convenient access to each back-end using the same
	| syntax for each one. Here you may set the default queue driver.
	|
	| Supported: "sync", "beanstalkd", "sqs", "iron"
	|
	*/

	'default' => 'sqs',

	/*
	|--------------------------------------------------------------------------
	| Queue Connections
	|--------------------------------------------------------------------------
	|
	| Here you may configure the connection information for each server that
	| is used by your application. A default configuration has been added
	| for each back-end shipped with Laravel. You are free to add more.
	|
	*/

	'connections' => array(
		'sqs' => array(
			'driver' => 'sqs',
			'key'    => 'AKIAJYY6BJNSCEWMKJHA',
			'secret' => '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',
			'queue'  => 'https://sqs.eu-west-1.amazonaws.com/673718332952/LingoingTesting',
			'region' => 'eu-west-1',
		),
	),

);
