<?php
class Notification extends Eloquent{

	protected $table = 'notifications';
	
	protected $with = array('receiver', 'sender');
	
	public function receiver()
	{
		return $this->belongsTo('Person');
	}
	public function sender()
	{
		return $this->belongsTo('Person');
	}
	public function job()
	{
		return $this->belongsTo('Job');
	}
	
	private function exportIntervalToString($interval)
	{
		$output = '';
		if($interval->d == 1)
			$output = '1 day ago';
		elseif($interval->d > 1)
			$output = $interval->d . ' days ago';
		else
		{
			if($interval->h == 1)
				$output = '1 hour ago';
			elseif($interval->h > 1)
				$output = $interval->h . ' hours ago';
			else
			{
				if($interval->i == 1)
					$output = '1 minute ago';
				elseif($interval->i > 1)
					$output = $interval->i . ' minutes ago';
				else
					$output = 'now';
			}
		}
		return $output;
	}
	public function showTimePassedCreated()
	{
		$now = new DateTime("now");
		$created_at = date_create($this->created_at);
		$interval = date_diff($now, $created_at);
		
		return $this->exportIntervalToString($interval);
	}
	public function showTimePassedUpdated()
	{
		$now = new DateTime("now");
		$updated_at = date_create($this->updated_at);
		$interval = date_diff($now, $updated_at);

		return $this->exportIntervalToString($interval);
	}
	
	public function renderText()
	{
		$output = $this->notification;
		
		if(!empty($this->sender_id))
			$output = str_replace('{SENDER}', $this->sender->getName(), $output);
		if(!empty($this->receiver_id))
			$output = str_replace('{RECEIVER}', $this->receiver->getName(), $output);
		if(!empty($this->job_id))
			$output = str_replace('{JOB}', $this->job->title, $output);
		
		return $output;
	}
	public function renderLink()
	{
		$output = $this->link;
		
		if(!empty($this->job_id))
			$output = str_replace('{JOB_ID}', $this->job_id, $output);
		
		return $output;
	}
	
	public static function sendNotificationTo($sender_id, $receiver_id, $job_id, $text, $link = null, $link_title = null)
	{
		$notification = new Notification();
		$notification->sender_id = $sender_id;
		$notification->receiver_id = $receiver_id;
		$notification->job_id = $job_id;
		$notification->notification = $text;
		$notification->link = $link;
		$notification->link_title = $link_title;
		$notification->save();
		
		return $notification;
	}
}