<?php
class PersonJobAttending extends Eloquent{

	protected $table = 'persons_jobs_attending';
	
	public function person()
	{
		return Person::find($this->person_id);
	}
	public function job()
	{
		return Job::find($this->job_id);
	}
}