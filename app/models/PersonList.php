<?php
class PersonList extends Eloquent{

	protected $table = 'persons_list';
	
	public $timestamps = false;
	
	protected $with = array('person', 'client_lsp');
	
	public function person()
	{
		return $this->belongsTo('Person');
	}
	public function client_lsp()
	{
		return $this->belongsTo('Person', 'client_lsp_id');
	}
}