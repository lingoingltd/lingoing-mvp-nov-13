<?php
class Job extends Eloquent{

	protected $table = 'jobs';
	
	protected $with = array('positions');
	
	public function person()
	{
		return $this->belongsTo('Person');
	}
	
	public function positions()
	{
		return $this->hasMany('JobOpenPosition');
	}
	public function clearPositions()
	{
		foreach($this->positions as $position)
			$position->delete();
	}
	public function open_positions()
	{
		return $this->positions()
			->whereNull('worker_id')
			->get();
	}
	public function taken_positions()
	{
		return $this->positions()
			->whereNotNull('worker_id')
			->get();
	}
	
	public function messages()
	{
		return $this->hasMany('JobMessage');
	}
	
	public function applicants()
	{
		return $this->hasMany('JobApplication');
	}
	
	public function overtimes()
	{
		return $this->hasMany('JobOvertime');
	}
	
	public function dates()
	{
		return JobDate::where('job_id', '=', $this->id)
			->orderBy('date', 'ASC')
			->orderBy('start_time', 'ASC')
			->get();
	}
	public function firstDate()
	{
		return JobDate::where('job_id', '=', $this->id)
			->orderBy('date', 'ASC')
			->orderBy('start_time', 'ASC')
			->first();
	}
	public function lastDate()
	{
		return JobDate::where('job_id', '=', $this->id)
			->orderBy('date', 'DESC')
			->orderBy('start_time', 'DESC')
			->first();
	}
	public function clearDates()
	{
		foreach($this->dates() as $date)
			$date->delete();
	}
	
	public function attachments()
	{
		return JobAttachment::where('job_id', '=', $this->id)
			->get();
	}
	
	public function getLocation()
	{
		if(empty($this->address_1))
			return null;
			
		$address = $this->address_1;
		if(!empty($this->address_2))
			$address .= ', ' . $this->address_2;
		if(!empty($this->county))
			$address .= ', ' . Person::listCounties($this->county);
		$address .= ', ' . $this->city;
		$address .= ', ' . $this->postcode;
		$address .= ', ' . Person::listCountries($this->country);
		
		return $address;
	}
	public function getShortLocation()
	{
		if(empty($this->address_1))
			return null;
			
		$address  = $this->city;
		$address .= ', ' . substr($this->postcode, 0, 3);
		$address .= ', ' . Person::listCountries($this->country);
		return $address;
	}
	public function getDuration()
	{
		$b = new DateTime($this->begin);
		if(!$b)
			return null;
			
		$e = new DateTime($this->end);
		if(!$e)
			return null;
		
		$diff = $e->diff($b, true);
		if(!$diff)
			return null;
			
		if($diff->days > 0)
		{
			if($diff->days == 1)
				$output = '1 day';
			else
				$output = $diff->days . ' days';
			
			if($diff->h == 1)
				$output .= ' 1 hour';
			elseif($diff->h > 1)
				$output .= ' ' . $diff->h . ' hours';
			
			return $output;
		}
		elseif($diff->h > 0)
		{
			if($diff->h == 1)
				return '1 hour';
			else
				return $diff->h . ' hours';
		}
		return null;
	}
	public function getDurationInHours()
	{
		$hours = 0;
		
		$dates = $this->dates();
		foreach($dates as $date)
		{
			$b = new DateTime(date('H:i', strtotime($date->start_time)));
			if(!$b)
				return null;

			$e = new DateTime(date('H:i', strtotime($date->end_time)));
			if(!$e)
				return null;

			$diff = $e->diff($b, true);
			if(!$diff)
				return null;
			
			$hours += $diff->h;
			$hours += ($diff->i / 60);
		}
		return round($hours, 2);
	}
	public function getStatusAsString($before = null, $end = null)
	{
		$output = '';
		if($before != null)
			$output .= $before;
			
		$output .= self::listStatuses($this->status);
		
		if($end != null)
			$output .= $end;

		return $output;
	}
	
	public function createChildJob()
	{
		$job = new Job();
		$job->person_id = $this->person_id;
		$job->status = 'awaiting_applications';
		$job->description = $this->description;
		$job->type = $this->type;
		$job->type_other = $this->type_other;
		$job->project_budget = $this->project_budget;
		$job->hourly_rate = $this->hourly_rate;
		$job->hourly_rate_lsp_only = $this->hourly_rate_lsp_only;
		$job->address_1 = $this->address_1;
		$job->address_2 = $this->address_2;
		$job->city = $this->city;
		$job->postcode = $this->postcode;
		$job->county = $this->county;
		$job->country = $this->country;
		$job->date_type = $this->date_type;
		$job->parent_job_id = $this->id;
		$job->payment_method = $this->payment_method;
		$job->support_package_id = $this->support_package_id;
		$job->save();
		
		//we copy attachments
		foreach($this->attachments() as $attachment)
		{
			$newAttachment = new JobAttachment();
			$newAttachment->job_id = $job->id;
			$newAttachment->filename = $attachment->filename;
			$newAttachment->description = $attachment->description;
			$newAttachment->save();
		}
		
		return $job;
	}
	public function childJobs()
	{
		return Job::where('parent_job_id', '=', $this->id)
			->get();
	}
	public function clearChildJobs()
	{
		DB::table('jobs')
			->where('parent_job_id', '=', $this->id)
			->delete();
	}
	public function countChildJobs()
	{
		return (int)DB::table('jobs')
			->where('parent_job_id', '=', $this->id)
			->count();
	}
	
	public function supportPackage()
	{
		if(empty($this->support_package_id))
			return null;
		
		return AtwSupportPackage::where('id', '=', $this->support_package_id)
			->first();
	}
	
	public function getTitle()
	{
		if($this->type == 'other')
			return $this->type_other;
		
		return Job::listTypes($this->type);
	}
	public function getBudget()
	{
		$value = null;
		if(Auth::check() && Auth::user()->id == $this->person_id)
			$value = $this->budget; 
		else
			$value = $this->budget_lsp_only;
		
		return $value;
	}
		
	public function hasUserApplied($person_id)
	{
		$count = $this->applicants()->where('person_id', '=', $person_id)->count();
		return ($count == 1);
	}
	public function isUserWorker($person_id)
	{
		$count = $this->positions()->where('worker_id', '=', $person_id)->count();
		return ($count == 1);
	}
		
	public static function listTypes($type = null)
	{
		$types = array(
			'community' => 'Community',
			'conference' => 'Conference',
			'court' => 'Court',
			'education' => 'Education',
			'employment' => 'Employment',
			'medical' => 'Medical',
			'mental health' => 'Mental health',
			'office' => 'Office',
			'police' => 'Police',
			'political' => 'Political',
			'religion' => 'Religion',
			'social services' => 'Social services',
			'solicitor' => 'Solicitor',
			'religious' => 'Religious',
			'television/media' => 'Television/Media',
			'theatre' => 'Theatre',
			'training courses' => 'Training courses',
			'other' => 'Other'
		);
		
		if($type == null)
			return $types;
			
		return $types[$type];
	}
	public static function listStatuses($status = null)
	{
		$statuses = array(
			'awaiting_applications' => 'Awaiting applications',
			'confirmed' => 'Confirmed',
			'finished' => 'Finished',
			'canceled' => 'Canceled',
			'invoice_sent' => 'Invoice sent',
			'no_lsp_hired' => 'No LSP hired'
		);
		
		if($status == null)
			return $statuses;
			
		return $statuses[$status];
	}
	public static function listPaymentMethods($method = null)
	{
		$data = array(
			'paying_card' => 'Credit/Debit card',
			'atw_customer' => 'I am an Access to Work customer'
		);
		
		if($method == null)
			return $data;
			
		return $data[$method];
	}
	public static function listProjectBudget($budget = null)
	{
		$data = array(
			'open_to_quotes' => 'Open to quotes',
			'fixed' => 'Fixed budget',
			'fixed_hourly_rate' => 'Fixed hourly rate'
		);
		
		if($budget == null)
			return $data;
			
		return $data[$budget];
	}
	
	/* Override functions */
	public function delete()
	{
		//job date delete
		foreach($this->dates() as $date)
			$date->delete();
		
		//job attachments delete
		foreach ($this->attachments() as $attachment)
			$attachment->delete();
		
		parent::delete();
	}
}