<?php

use \Illuminate\Support\Collection;

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Person extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'persons';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	public function getAuthIdentifier()
	{
		return $this->getKey();
	}
	public function getAuthPassword()
	{
		return $this->password;
	}
	public function getReminderEmail()
	{
		return $this->email;
	}
	public function getRememberToken()
	{
	    return $this->remember_token;
	}
	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}
	public function getRememberTokenName()
	{
	    return 'remember_token';
	}
	
	public function getName()
	{
		$default_address = $this->default_address();
		
		if(empty($default_address))
			return $this->email;
			
		if(empty($default_address->organization_name))
			return $default_address->first_name . ' ' . $default_address->last_name;
		else
			return $default_address->organization_name;
	}
	public function getBillingName()
	{
		if(empty($this->billing_first_name))
			return $this->email;
			
		return $this->billing_first_name . ' ' . $this->billing_last_name;
	}
	public function getRating()
	{
		if($this->no_of_votes == 0)
			return 0;
			
		return round($this->votes_sum / $this->no_of_votes, 2);
	}
	
	public function clients()
	{
		$professionals = new Collection();
		
		$sql = "SELECT
			DISTINCT(j.person_id) AS id
		FROM
			jobs j
		INNER JOIN
			jobs_open_position jop
			ON
				jop.job_id=j.id
		WHERE
			jop.worker_id=" . $this->id . "
		";
		
		$ids = DB::select($sql);
		if(count($ids) == 0)
			return $professionals;
			
		$new_ids = array();
		foreach($ids as $id)
			$new_ids[] = $id->id;
			
		$sql = "SELECT
			p.*
		FROM
			persons p
		INNER JOIN
			persons_addresses pa
			ON
				p.id=pa.person_id
		WHERE
			p.id IN (" . implode($new_ids, ',') . ")
		ORDER BY
			pa.first_name ASC,
			pa.last_name ASC";
		
		DB::connection()->setFetchMode(PDO::FETCH_ASSOC);
		$results = DB::select($sql);
		DB::connection()->setFetchMode(PDO::FETCH_OBJ);

		foreach($results as $index => $result)
		{
			$person = new Person();
			$person->setRawAttributes($result, true);
			$person->exists = true;
			
			$professionals->put($index, $person);
		}
		return $professionals;
		
		$ids = DB::table('jobs')
			->join('jobs_open_position', 'jobs.id', '=', 'jobs_open_position.job_id')
			->where('jobs_open_position.worker_id', '=', $this->id)
			->select('jobs.person_id AS id')
			->distinct()
			->lists('id');
			
		if(count($ids) == 0)
			return array();
			
		return Person::whereIn('id', $ids)
			->orderBy('first_name', 'ASC')
			->get();
	}
	public function professionals()
	{
		$professionals = new Collection();
		
		$sql = "SELECT
			DISTINCT(jop.worker_id) AS id
		FROM
			jobs j
		INNER JOIN
			jobs_open_position jop
			ON
				jop.job_id=j.id
		WHERE
			j.person_id=" . $this->id . " AND
			jop.worker_id IS NOT NULL
		";
		
		$ids = DB::select($sql);
		if(count($ids) == 0)
			return $professionals;
			
		$new_ids = array();
		foreach($ids as $id)
			$new_ids[] = $id->id;
			
		$sql = "SELECT
			p.*
		FROM
			persons p
		INNER JOIN
			persons_addresses pa
			ON
				p.id=pa.person_id
		WHERE
			p.id IN (" . implode($new_ids, ',') . ")
		ORDER BY
			pa.first_name ASC,
			pa.last_name ASC";
		
		DB::connection()->setFetchMode(PDO::FETCH_ASSOC);
		$results = DB::select($sql);
		DB::connection()->setFetchMode(PDO::FETCH_OBJ);

		foreach($results as $index => $result)
		{
			$person = new Person();
			$person->setRawAttributes($result, true);
			$person->exists = true;
			
			$professionals->put($index, $person);
		}
		return $professionals;
	}
	
	public function addresses()
	{
		return $this->hasMany('PersonAddress');
	}
	public function addressesAsArray()
	{
		$data = array();
		foreach($this->addresses()->get() as $address)
		{
			$title = '';
			if(empty($address->organization_name))
				$title = $address->first_name . ' ' . $address->last_name;
			else
				$title = $address->organization_name;
				
			$title .= ', ' . $address->address_1;
			$title .= ', ' . $address->postcode . ' ' . $address->city;
			$title .= ', ' . Person::listCountries($address->country);
			
			$data[$address->id] = $title;
		}
		return $data;
	}
	public function default_address()
	{
		return PersonAddress::where('person_id', '=', $this->id)
			->where('address_type', '=', 'default')
			->first();
	}
	public function billing_address()
	{
		return PersonAddress::where('person_id', '=', $this->id)
			->where('address_type', '=', 'billing')
			->first();
	}
	
	/* jobs */
	public function jobs()
	{
		return $this->hasMany('Job');
	}
	public function jobsAsArray()
	{
		$jobs = $this->jobs;
		
		$data = array();
		foreach($jobs as $job)
			$data[$job->id] = $job->getTitle();
			
		return $data;
	}
	public function jobsConfirmedAsArray()
	{
		$jobs = $this->jobs()
			->where('confirmed', '=', '1')
			->get();
		
		$data = array();
		foreach($jobs as $job)
			$data[$job->id] = $job->getTitle();
			
		return $data;
	}
	public function countJobs()
	{
		return (int)DB::table('jobs')
			->join('jobs_open_position', 'jobs.id', '=', 'jobs_open_position.job_id')
			->where('jobs_open_position.worker_id', '=', $this->id)
			->count();
	}
	
	public function isPersonRated($job_id)
	{
		$count = (int)DB::table('persons_reviews')
			->where('person_id', '=', $this->id)
			->where('job_id', '=', $job_id)
			->count();
			
		return ($count == 1);
	}
	
	public function accountTypeAsString()
	{
		if($this->account_type == 'translator')
			return 'Language professional/Translator';
		elseif($this->account_type == 'agent')
			return 'Agent';
		else
			return 'Client';
	}
	
	public function getLocation()
	{
		if(empty($this->address_1))
			return null;
			
		$address = $this->address_1;
		if(!empty($this->address_2))
			$address .= ', ' . $this->address_2;
		if(!empty($this->county))
			$address .= ', ' . Person::listCounties($this->county);
		$address .= ', ' . $this->city;
		$address .= ', ' . $this->postcode;
		$address .= ', ' . Person::listCountries($this->country);
		
		return $address;
	}
	public function getShortLocation()
	{
		if(empty($this->city) || empty($this->country))
			return '';
		
		return $this->city . ', ' . Person::listCountries($this->country);
	}
	
	/**
	 * Functions returns person's memberships
	 * 
	 * @param string $type Default null. Possible values:sli, sli2, notetaker, lipspeaker, sttr, slt
	 * @param bool $onlyNonExpired Do we want expired memberships returned also?
	 * 
	 * @returns Illuminate\Support\Collection 
	 */
	public function memberships($type = null, $onlyNonExpired = true)
	{
		$sql = PersonMembership::where('person_id', '=', $this->id);
		if(!empty($type))
			$sql = $sql->where('type', '=', $type);
			
		if($onlyNonExpired)
			$sql = $sql->where('expired', '=', 0);
			
		return $sql->get();
	}
	/**
	 * Functions returns person's memberships languages.
	 * 
	 * @param string $type Default null. Possible values:sli, sli2, notetaker, lipspeaker, sttr, slt
	 * 
	 * @returns Illuminate\Support\Collection 
	 */
	public function membershipsLanguages($type = null)
	{
		$membershipIds = null;
		if(empty($type))
		{
			$membershipIds = PersonMembership::where('person_id', '=', $this->id)
				->get()
				->lists('id');
		}
		else
		{
			$membershipIds = PersonMembership::where('person_id', '=', $this->id)
				->where('type', '=', $type)
				->get()
				->lists('id');
		}
		
		if(count($membershipIds) == 0)
			return array();
			
		$tmp = array();
		
		$data = array();
		$result = PersonMembershipLanguage::whereIn('membership_id', $membershipIds)
			->get();
			
		foreach($result as $r)
		{
			if(!in_array($r->language, $tmp))
			{
				$data[] = $r;
				$tmp[] = $r->language;
			}
		}
		
		return $data;
	}
	/**
	 * Functions returns person's memberships for specific type and membership body.
	 * 
	 * @param string $type Default null. Possible values:sli, sli2, notetaker, lipspeaker, sttr, slt
	 * @param string $membership_body Which membership body we want - NRCPD, AIIC, ...
	 * 
	 * @returns PersonMembership
	 */
	public function searchForMembership($type, $membership_body, $onlyUnexpired = true)
	{
		$query = PersonMembership::where('person_id', '=', $this->id)
			->where('type', '=', $type)
			->where('membership_body', '=', $membership_body);
			
		if($onlyUnexpired)
			$query = $query->where('expired', '=', 0);
		
		return $query->first();
	}
	
	/**
	 * Functions returns person's documents
	 * 
	 * @param string $type Default null. Possible values:crb, insurance.
	 * 
	 * @returns Illuminate\Support\Collection 
	 */
	public function documents($type = null)
	{
		if(empty($type))
		{
			return PersonDocument::where('person_id', '=', $this->id)
				->whereRaw(DB::raw('membership_id IS NULL'))
				->get();
		}
		else
		{
			return PersonDocument::where('person_id', '=', $this->id)
				->whereRaw(DB::raw('membership_id IS NULL'))
				->where('type', '=', $type)
				->get();
		}
	}
	/**
	 * Functions returns person's specialisms
	 * 
	 * @returns Illuminate\Support\Collection 
	 */
	public function specialisms()
	{
		return PersonSpecialism::where('person_id', '=', $this->id)
			->get();
	}
	public function clearSpecialisms()
	{
		return PersonSpecialism::where('person_id', '=', $this->id)
			->delete();
	}
	
	public function organization()
	{
		return $this->belongsTo('Organization');
	}
	
	public static function countUnconfirmed($accountType)
	{
		return (int)DB::table('persons')
			->where('account_type', '=', $accountType)
			->where('confirmed', '=', 0)
			->count();
	}
	
	public function getProfilePicture()
	{
		if(!empty($this->profile_picture_url))
			return $this->profile_picture_url;
		else
			return '/img/placeholder.gif';
	}
	
	public function scopeTranslators($query)
	{
		return $query->where('account_type', '=', 'translator');
	}
	
	public function notifications()
	{
		return Notification::where('receiver_id', '=', $this->id);
	}
	
	public function preferred_list()
	{
		return PersonList::where('person_id', '=', $this->id)
			->where('type', '=', 'preferred')
			->get();
	}
	public function blocked_list()
	{
		return PersonList::where('person_id', '=', $this->id)
			->where('type', '=', 'blocked')
			->get();
	}
	
	public function all_invoices()
	{
		return Invoice::where('person_id', '=', $this->id)
			->get();
	}
	public function unpaid_invoices()
	{
		return Invoice::where('person_id', '=', $this->id)
			->where('status', '=', 'unpaid')
			->orderBy('id', 'ASC')
			->get();
	}
	public function paid_invoices()
	{
		return Invoice::where('person_id', '=', $this->id)
			->where('status', '=', 'paid')
			->orderBy('id', 'ASC')
			->get();
	}
	public function pending_invoices()
	{
		return Invoice::where('person_id', '=', $this->id)
			->where('status', '=', 'pending')
			->orderBy('id', 'ASC')
			->get();
	}
	
	public function atw_packages()
	{
		return AtwSupportPackage::where('person_id', '=', $this->id)
			->where('removed', '=', '0')
			->orderBy('id', 'ASC')
			->get();
	}
	public function atw_verified_packages()
	{
		return AtwSupportPackage::where('person_id', '=', $this->id)
			->where('status', '=', 'verified')
			->where('removed', '=', '0')
			->get();
	}
	public function atw_unverified_packages()
	{
		return AtwSupportPackage::where('person_id', '=', $this->id)
			->where('status', '!=', 'verified')
			->where('removed', '=', '0')
			->get();
	}
	public function atwVerifiedPackagesAsArray()
	{
		$data = array();
		
		$packages = $this->atw_verified_packages();
		foreach($packages as $package)
		{
			$title = '';
			if($package->given_to == 'freelancer')
				$title = 'Freelancer - ';
			else
				$title = 'Company - ';
				
			if(!empty($package->organization_name))
				$title .= $package->organization_name . ' - ';
			else
				$title .= $package->first_name . ' ' . $package->last_name . ' - ';
			
			$title .= 'Expiring: ' . date('d/m/Y', strtotime($package->support_end));
			
			$data[$package->id] = $title;
		}
		
		return $data;
	}
	
	public function reviews()
	{
		return PersonReview::where('person_id', '=', $this->id);
	}
	
	//////////////////////////////////////////////////////// STATIC FUNCTIONS & HELPERS ////////////////////////////////////////////
	public static function listAdmins()
	{
		return self::where('admin', '=', 1)->get();
	}
	
	public static function renderValue($value)
	{
		return empty($value) ? '/' : $value;
	}

	public static function listCountries($country = null)
	{
		$countries = array(
		  'GB' => 'United Kingdom',
		  'US' => 'United States',
		  'AF' => 'Afghanistan',
		  'AX' => 'Åland Islands',
		  'AL' => 'Albania',
		  'DZ' => 'Algeria',
		  'AS' => 'American Samoa',
		  'AD' => 'Andorra',
		  'AO' => 'Angola',
		  'AI' => 'Anguilla',
		  'AQ' => 'Antarctica',
		  'AG' => 'Antigua and Barbuda',
		  'AR' => 'Argentina',
		  'AM' => 'Armenia',
		  'AW' => 'Aruba',
		  'AU' => 'Australia',
		  'AT' => 'Austria',
		  'AZ' => 'Azerbaijan',
		  'BS' => 'Bahamas',
		  'BH' => 'Bahrain',
		  'BD' => 'Bangladesh',
		  'BB' => 'Barbados',
		  'BY' => 'Belarus',
		  'BE' => 'Belgium',
		  'BZ' => 'Belize',
		  'BJ' => 'Benin',
		  'BM' => 'Bermuda',
		  'BT' => 'Bhutan',
		  'BO' => 'Bolivia',
		  'BA' => 'Bosnia and Herzegovina',
		  'BW' => 'Botswana',
		  'BV' => 'Bouvet Island',
		  'BR' => 'Brazil',
		  'IO' => 'British Indian Ocean Territory',
		  'BN' => 'Brunei Darussalam',
		  'BG' => 'Bulgaria',
		  'BF' => 'Burkina Faso',
		  'BI' => 'Burundi',
		  'KH' => 'Cambodia',
		  'CM' => 'Cameroon',
		  'CA' => 'Canada',
		  'CV' => 'Cape Verde',
		  'KY' => 'Cayman Islands',
		  'CF' => 'Central African Republic',
		  'TD' => 'Chad',
		  'CL' => 'Chile',
		  'CN' => 'China',
		  'CX' => 'Christmas Island',
		  'CC' => 'Cocos (Keeling) Islands',
		  'CO' => 'Colombia',
		  'KM' => 'Comoros',
		  'CG' => 'Congo',
		  'CD' => 'Congo, The Democratic Republic of The',
		  'CK' => 'Cook Islands',
		  'CR' => 'Costa Rica',
		  'CI' => 'Cote D\'ivoire',
		  'HR' => 'Croatia',
		  'CU' => 'Cuba',
		  'CY' => 'Cyprus',
		  'CZ' => 'Czech Republic',
		  'DK' => 'Denmark',
		  'DJ' => 'Djibouti',
		  'DM' => 'Dominica',
		  'DO' => 'Dominican Republic',
		  'EC' => 'Ecuador',
		  'EG' => 'Egypt',
		  'SV' => 'El Salvador',
		  'GQ' => 'Equatorial Guinea',
		  'ER' => 'Eritrea',
		  'EE' => 'Estonia',
		  'ET' => 'Ethiopia',
		  'FK' => 'Falkland Islands (Malvinas)',
		  'FO' => 'Faroe Islands',
		  'FJ' => 'Fiji',
		  'FI' => 'Finland',
		  'FR' => 'France',
		  'GF' => 'French Guiana',
		  'PF' => 'French Polynesia',
		  'TF' => 'French Southern Territories',
		  'GA' => 'Gabon',
		  'GM' => 'Gambia',
		  'GE' => 'Georgia',
		  'DE' => 'Germany',
		  'GH' => 'Ghana',
		  'GI' => 'Gibraltar',
		  'GR' => 'Greece',
		  'GL' => 'Greenland',
		  'GD' => 'Grenada',
		  'GP' => 'Guadeloupe',
		  'GU' => 'Guam',
		  'GT' => 'Guatemala',
		  'GG' => 'Guernsey',
		  'GN' => 'Guinea',
		  'GW' => 'Guinea-bissau',
		  'GY' => 'Guyana',
		  'HT' => 'Haiti',
		  'HM' => 'Heard Island and Mcdonald Islands',
		  'VA' => 'Holy See (Vatican City State)',
		  'HN' => 'Honduras',
		  'HK' => 'Hong Kong',
		  'HU' => 'Hungary',
		  'IS' => 'Iceland',
		  'IN' => 'India',
		  'ID' => 'Indonesia',
		  'IR' => 'Iran, Islamic Republic of',
		  'IQ' => 'Iraq',
		  'IE' => 'Ireland',
		  'IM' => 'Isle of Man',
		  'IL' => 'Israel',
		  'IT' => 'Italy',
		  'JM' => 'Jamaica',
		  'JP' => 'Japan',
		  'JE' => 'Jersey',
		  'JO' => 'Jordan',
		  'KZ' => 'Kazakhstan',
		  'KE' => 'Kenya',
		  'KI' => 'Kiribati',
		  'KP' => 'Korea, Democratic People\'s Republic of',
		  'KR' => 'Korea, Republic of',
		  'KW' => 'Kuwait',
		  'KG' => 'Kyrgyzstan',
		  'LA' => 'Lao People\'s Democratic Republic',
		  'LV' => 'Latvia',
		  'LB' => 'Lebanon',
		  'LS' => 'Lesotho',
		  'LR' => 'Liberia',
		  'LY' => 'Libyan Arab Jamahiriya',
		  'LI' => 'Liechtenstein',
		  'LT' => 'Lithuania',
		  'LU' => 'Luxembourg',
		  'MO' => 'Macao',
		  'MK' => 'Macedonia, The Former Yugoslav Republic of',
		  'MG' => 'Madagascar',
		  'MW' => 'Malawi',
		  'MY' => 'Malaysia',
		  'MV' => 'Maldives',
		  'ML' => 'Mali',
		  'MT' => 'Malta',
		  'MH' => 'Marshall Islands',
		  'MQ' => 'Martinique',
		  'MR' => 'Mauritania',
		  'MU' => 'Mauritius',
		  'YT' => 'Mayotte',
		  'MX' => 'Mexico',
		  'FM' => 'Micronesia, Federated States of',
		  'MD' => 'Moldova, Republic of',
		  'MC' => 'Monaco',
		  'MN' => 'Mongolia',
		  'ME' => 'Montenegro',
		  'MS' => 'Montserrat',
		  'MA' => 'Morocco',
		  'MZ' => 'Mozambique',
		  'MM' => 'Myanmar',
		  'NA' => 'Namibia',
		  'NR' => 'Nauru',
		  'NP' => 'Nepal',
		  'NL' => 'Netherlands',
		  'AN' => 'Netherlands Antilles',
		  'NC' => 'New Caledonia',
		  'NZ' => 'New Zealand',
		  'NI' => 'Nicaragua',
		  'NE' => 'Niger',
		  'NG' => 'Nigeria',
		  'NU' => 'Niue',
		  'NF' => 'Norfolk Island',
		  'MP' => 'Northern Mariana Islands',
		  'NO' => 'Norway',
		  'OM' => 'Oman',
		  'PK' => 'Pakistan',
		  'PW' => 'Palau',
		  'PS' => 'Palestinian Territory, Occupied',
		  'PA' => 'Panama',
		  'PG' => 'Papua New Guinea',
		  'PY' => 'Paraguay',
		  'PE' => 'Peru',
		  'PH' => 'Philippines',
		  'PN' => 'Pitcairn',
		  'PL' => 'Poland',
		  'PT' => 'Portugal',
		  'PR' => 'Puerto Rico',
		  'QA' => 'Qatar',
		  'RE' => 'Reunion',
		  'RO' => 'Romania',
		  'RU' => 'Russian Federation',
		  'RW' => 'Rwanda',
		  'SH' => 'Saint Helena',
		  'KN' => 'Saint Kitts and Nevis',
		  'LC' => 'Saint Lucia',
		  'PM' => 'Saint Pierre and Miquelon',
		  'VC' => 'Saint Vincent and The Grenadines',
		  'WS' => 'Samoa',
		  'SM' => 'San Marino',
		  'ST' => 'Sao Tome and Principe',
		  'SA' => 'Saudi Arabia',
		  'SN' => 'Senegal',
		  'RS' => 'Serbia',
		  'SC' => 'Seychelles',
		  'SL' => 'Sierra Leone',
		  'SG' => 'Singapore',
		  'SK' => 'Slovakia',
		  'SI' => 'Slovenia',
		  'SB' => 'Solomon Islands',
		  'SO' => 'Somalia',
		  'ZA' => 'South Africa',
		  'GS' => 'South Georgia and The South Sandwich Islands',
		  'ES' => 'Spain',
		  'LK' => 'Sri Lanka',
		  'SD' => 'Sudan',
		  'SR' => 'Suriname',
		  'SJ' => 'Svalbard and Jan Mayen',
		  'SZ' => 'Swaziland',
		  'SE' => 'Sweden',
		  'CH' => 'Switzerland',
		  'SY' => 'Syrian Arab Republic',
		  'TW' => 'Taiwan, Province of China',
		  'TJ' => 'Tajikistan',
		  'TZ' => 'Tanzania, United Republic of',
		  'TH' => 'Thailand',
		  'TL' => 'Timor-leste',
		  'TG' => 'Togo',
		  'TK' => 'Tokelau',
		  'TO' => 'Tonga',
		  'TT' => 'Trinidad and Tobago',
		  'TN' => 'Tunisia',
		  'TR' => 'Turkey',
		  'TM' => 'Turkmenistan',
		  'TC' => 'Turks and Caicos Islands',
		  'TV' => 'Tuvalu',
		  'UG' => 'Uganda',
		  'UA' => 'Ukraine',
		  'AE' => 'United Arab Emirates',
		  'UM' => 'United States Minor Outlying Islands',
		  'UY' => 'Uruguay',
		  'UZ' => 'Uzbekistan',
		  'VU' => 'Vanuatu',
		  'VE' => 'Venezuela',
		  'VN' => 'Viet Nam',
		  'VG' => 'Virgin Islands, British',
		  'VI' => 'Virgin Islands, U.S.',
		  'WF' => 'Wallis and Futuna',
		  'EH' => 'Western Sahara',
		  'YE' => 'Yemen',
		  'ZM' => 'Zambia',
		  'ZW' => 'Zimbabwe',
		);
		
		if($country == null)
			return $countries;
			
		return $countries[$country];
	}
	public static function listCounties($county = null)
	{
		$data = array(
		  'England' => array(
		    'Avon' => 'Avon',
		    'Bedfordshire' => 'Bedfordshire',
		    'Berkshire' => 'Berkshire',
		    'Buckinghamshire' => 'Buckinghamshire',
		    'Cambridgeshire' => 'Cambridgeshire',
		    'Cheshire' => 'Cheshire',
		    'Cleveland' => 'Cleveland',
		    'Cornwall' => 'Cornwall',
		    'Cumbria' => 'Cumbria',
		    'Derbyshire' => 'Derbyshire',
		    'Devon' => 'Devon',
		    'Dorset' => 'Dorset',
		    'Durham' => 'Durham',
		    'East Sussex' => 'East Sussex',
		    'Essex' => 'Essex',
		    'Gloucestershire' => 'Gloucestershire',
		    'Hampshire' => 'Hampshire',
		    'Herefordshire' => 'Herefordshire',
		    'Hertfordshire' => 'Hertfordshire',
		    'Isle of Wight' => 'Isle of Wight',
		    'Kent' => 'Kent',
		    'Lancashire' => 'Lancashire',
		    'Leicestershire' => 'Leicestershire',
		    'Lincolnshire' => 'Lincolnshire',
		    'London' => 'London',
		    'Merseyside' => 'Merseyside',
		    'Middlesex' => 'Middlesex',
		    'Norfolk' => 'Norfolk',
		    'Northamptonshire' => 'Northamptonshire',
		    'Northumberland' => 'Northumberland',
		    'North Humberside' => 'North Humberside',
		    'North Yorkshire' => 'North Yorkshire',
		    'Nottinghamshire' => 'Nottinghamshire',
		    'Oxfordshire' => 'Oxfordshire',
		    'Rutland' => 'Rutland',
		    'Shropshire' => 'Shropshire',
		    'Somerset' => 'Somerset',
		    'South Humberside' => 'South Humberside',
		    'South Yorkshire' => 'South Yorkshire',
		    'Staffordshire' => 'Staffordshire',
		    'Suffolk' => 'Suffolk',
		    'Surrey' => 'Surrey',
		    'Tyne and Wear' => 'Tyne and Wear',
		    'Warwickshire' => 'Warwickshire',
		    'West Midlands' => 'West Midlands',
		    'West Sussex' => 'West Sussex',
		    'West Yorkshire' => 'West Yorkshire',
		    'Wiltshire' => 'Wiltshire',
		    'Worcestershire' => 'Worcestershire',
		  ),
		  'Scotland' => array(
		    'Aberdeenshire' => 'Aberdeenshire',
		    'Angus' => 'Angus',
		    'Argyll' => 'Argyll',
		    'Ayrshire' => 'Ayrshire',
		    'Banffshire' => 'Banffshire',
		    'Berwickshire' => 'Berwickshire',
		    'Bute' => 'Bute',
		    'Caithness' => 'Caithness',
		    'Clackmannanshire' => 'Clackmannanshire',
		    'Dumfriesshire' => 'Dumfriesshire',
		    'Dunbartonshire' => 'Dunbartonshire',
		    'East Lothian' => 'East Lothian',
		    'Fife' => 'Fife',
		    'Inverness-shire' => 'Inverness-shire',
		    'Kincardineshire' => 'Kincardineshire',
		    'Kinross-shire' => 'Kinross-shire',
		    'Kirkcudbrightshire' => 'Kirkcudbrightshire',
		    'Lanarkshire' => 'Lanarkshire',
		    'Midlothian' => 'Midlothian',
		    'Moray' => 'Moray',
		    'Nairnshire' => 'Nairnshire',
		    'Orkney' => 'Orkney',
		    'Peeblesshire' => 'Peeblesshire',
		    'Perthshire' => 'Perthshire',
		    'Renfrewshire' => 'Renfrewshire',
		    'Ross-shire' => 'Ross-shire',
		    'Roxburghshire' => 'Roxburghshire',
		    'Selkirkshire' => 'Selkirkshire',
		    'Shetland' => 'Shetland',
		    'Stirlingshire' => 'Stirlingshire',
		    'Sutherland' => 'Sutherland',
		    'West Lothian' => 'West Lothian',
		    'Wigtownshire' => 'Wigtownshire',
		  ),
		  'Wales' => array(
		    'Clwyd' => 'Clwyd',
		    'Dyfed' => 'Dyfed',
		    'Gwent' => 'Gwent',
		    'Gwynedd' => 'Gwynedd',
		    'Mid Glamorgan' => 'Mid Glamorgan',
		    'Powys' => 'Powys',
		    'South Glamorgan' => 'South Glamorgan',
		    'West Glamorgan' => 'West Glamorgan',
		  ),
		  'Northern Ireland' => array(
		    'Antrim' => 'Antrim',
		    'Armagh' => 'Armagh',
		    'Down' => 'Down',
		    'Fermanagh' => 'Fermanagh',
		    'Londonderry' => 'Londonderry',
		    'Tyrone' => 'Tyrone',
		  ),
		);
		
		if(!empty($county))
		{
			foreach($data as $k => $v)
			{
				foreach($v as $k1 => $v1)
				{
					if($k1 == $county)
						return $v1;
				}
			}
		}
		
		return $data;
	}
	public static function listTitles()
	{
		return array(				
			'Mr' => 'Mr',
			'Mrs' => 'Mrs',
			'Miss' => 'Miss',
			'Ms' => 'Ms',
			'Dr (male)' => 'Dr (male)',
			'Dr (female)' => 'Dr (female)',
			'Professor (male)' => 'Professor (male)',
			'Professor (female)' => 'Professor (female)'
		);
	}
	public static function listCalloutTimes()
	{
		return array(
			'0' => '0',
			'0.5' => '0.5',
			'1' => '1',
			'1.5' => '1.5',
			'2' => '2',
			'3' => '3',
			'4' => '4'
		);
	}
	public static function listPaymentMethods($method = null)
	{
		$data = array(
			'bacs' => 'BACS',
			'paypal' => 'PayPal'
		);
		
		if(!empty($method))
			return $data[$method];
		
		return $data;
	}
	public static function listPaymentPeriod($period = null)
	{
		$data = array(
//			'monthly' => 'Monthly',
			'job_by_job' => 'Job by Job'
		);
		
		if($period != null)
			return $data[$period];
		
		return $data;
	}
	public static function listSpokenLanguageInterpreter($language = null)
	{
		$data = array(
			'Albanian' => 'Albanian',
			'Algerian' => 'Algerian',
			'Arabic' => 'Arabic',
			'Belarussian' => 'Belarussian',
			'Bengali' => 'Bengali',
			'Bengali-Sylheti' => 'Bengali-Sylheti',
			'Bosnian' => 'Bosnian',
			'Bulgarian' => 'Bulgarian',
			'Catalan' => 'Catalan',
			'Chinese(Cantonese)' => 'Chinese(Cantonese)',
			'Chinese(Mandarin)' => 'Chinese(Mandarin)',
			'Croatian' => 'Croatian',
			'Czech' => 'Czech',
			'Danish' => 'Danish',
			'Dari' => 'Dari',
			'Dogari' => 'Dogari',
			'Dutch' => 'Dutch',
			'Dutch(Flemish)' => 'Dutch(Flemish)',
			'English' => 'English',
			'Farsi' => 'Farsi',
			'Finnish' => 'Finnish',
			'French' => 'French',
			'German' => 'German',
			'Greek' => 'Greek',
			'Gujarati' => 'Gujarati',
			'Hebrew' => 'Hebrew',
			'Hindhko' => 'Hindhko',
			'Hindi' => 'Hindi',
			'Hungarian' => 'Hungarian',
			'IIocano' => 'IIocano',
			'Indonesian' => 'Indonesian',
			'Italian' => 'Italian',
			'Japanese' => 'Japanese',
			'Kikongo' => 'Kikongo',
			'Korean' => 'Korean',
			'Latvian' => 'Latvian',
			'Lingala' => 'Lingala',
			'Lithuanian' => 'Lithuanian',
			'Maltese' => 'Maltese',
			'Mauritian Creole' =>'Mauritian Creole',
			'Mirpuri' => 'Mirpuri',
			'Moldovan' => 'Moldovan',
			'Nepalese' => 'Nepalese',
			'Norwegian' => 'Norwegian',
			'Pahari' => 'Pahari',
			'Pangasinan' => 'Pangasinan',
			'Pashto' => 'Pashto',
			'Polish' => 'Polish',
			'Portuguese' => 'Portuguese',
			'Potohari' => 'Potohari',
			'Punjabi' => 'Punjabi',
			'Romanian' => 'Romanian',
			'Russian' => 'Russian',
			'Serbian' => 'Serbian',
			'Slovak' => 'Serbian',
			'Slovenian' => 'Slovenian',
			'Spanish' => 'Spanish',
			'Swahili' => 'Swahili',
			'Swedish' => 'Swedish',
			'Tagalog' => 'Tagalog',
			'Turkish' => 'Turkish',
			'Ukrainian' => 'Ukrainian',
			'Urdu' => 'Urdu'
		);
		
		if($language == null)
			return $data;
			
		return $data[$language];
	}
	public static function listSignLanguageInterpreter($language = null)
	{
		$data = array(
			'British Sign Language' => 'British Sign Language',
			'American Sign Language' => 'American Sign Language',
			'International Sign' => 'International Sign',
			'Irish Sign Language' => 'Irish Sign Language',
			'Registered Sign Language Translator (Deaf Relay)' => 'Registered Sign Language Translator (Deaf Relay)',
			'Welsh Sign Language' => 'Welsh Sign Language',
			'Tactile Sign Language' => 'Tactile Sign Language',
			'Makaton' => 'Makaton',
			'Visual Frame' => 'Visual Frame',
			'Deaf/Blind Manual' => 'Deaf/Blind Manual',
		);
		
		if($language == null)
			return $data;
			
		return $data[$language];
	}
	public static function listLipspeaker($language = null)
	{
		$data = array(
			'British Sign Language' => 'British Sign Language',
			'American Sign Language' => 'American Sign Language',
			'International Sign' => 'International Sign',
			'Irish Sign Language' => 'Irish Sign Language',
			'Registered Sign Language Translator (Deaf Relay)' => 'Registered Sign Language Translator (Deaf Relay)',
			'Welsh Sign Language' => 'Welsh Sign Language',
			'Deaf/Blind Manual' => 'Deaf/Blind Manual',
		);
		
		if($language == null)
			return $data;
			
		return $data[$language];
	}
	public static function listWorkAreas()
	{
		return array(
			"Central & East Anglia",
			"Devon & Cornwall",
			"East Midlands",
			"London & South East",
			"Mid South Coast",
			"North East",
			"North Wales",
			"Northern Ireland",
			"Scottish Interpreters Network",
			"South Wales",
			"South West",
			"West Midlands"
		);
	}
	public static function listSpecialisms($specialism = null)
	{
		$data = array(
			"Any" => "Any",
			"Court" => "Court",
			"Police" => "Police",
			"Solicitors" => "Solicitors",
			"Community" => "Community",
			"Conference" => "Conference",
			"Education" => "Education",
			"Employment" => "Employment",
			"Health/Medical" => "Health/Medical",
			"Mental Health" => "Mental Health",
			"Police" => "Police",
			"Religion" => "Religion",
			"Political" => "Political",
			"Social Services" => "Social Services",
			"Medical Conferences" => "Medical Conferences",
			"Business Negotiations" => "Business Negotiations",
			"Telephone Interpreting" => "Telephone Interpreting",
			"Computers & IT" => "Computers & IT",
			"Military & defence" => "Military & defence",
			"Speeches & on-stage" => "Speeches & on-stage",
			"Television / Media" => "Television / Media",
			"Theatre" => "Theatre",
			"Training Courses" => "Training Courses",
			"Voiceovers" => "Voiceovers"
		);
		ksort($data);
		
		if($specialism == null)
			return $data;
			
		return $data[$specialism];
	}

	public static function listSignLanguageMembershipBodies($membership_body = null)
	{
		$data = array(
			'AIIC' => 'AIIC',
			'ASLI' => 'ASLI',
			'NRCPD' => 'NRCPD',
			'VLP' => 'VLP'
		);
		
		if($membership_body != null)
			return $data[$membership_body];
		
		return $data;
	}
	public static function listSpokenLanguageMembershipBodies($membership_body = null)
	{
		$data = array(
			'AIIC' => 'AIIC',
			'CiOL' => 'CiOL',
			'ITI' => 'ITI',
			'NRPSI' => 'NRPSI'
		);
		
		if($membership_body != null)
			return $data[$membership_body];
		
		return $data;
	}
	public static function listNotetakerMembershipBodies($membership_body = null)
	{
		$data = array(
			'NRCPD' => 'NRCPD',
			'NRPSI' => 'NRPSI'
		);
		
		if($membership_body != null)
			return $data[$membership_body];
		
		return $data;
	}
	
	public static function listFinancialTeam()
	{
		return array(
			'info@lingoing.com' => 'Saduf Naqvi'
		);
	}
	public static function listDevelopmentTeam()
	{
		return array(
			'vojko@sona.si' => 'Vojko Voga'
		);
	}
	public static function listLanguageSkills()
	{
		return array(
			'Spoken language interpreter' => 'Spoken language interpreter',
			'Sign language interpreter' => 'Sign language interpreter',
			'Lipspeaker' => 'Lipspeaker',
			'Speech to text reporter' => 'Speech to text reporter',
			'Sign language translator' => 'Sign language translator',
			'Note taker' => 'Note taker'
		);
	}
}