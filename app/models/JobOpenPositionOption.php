<?php
class JobOpenPositionOption extends Eloquent{

	protected $table = 'jobs_open_position_option';
	
	public $timestamps = false;
	
	public function job_open_position()
	{
		return $this->belongsTo('JobOpenPosition');
	}
	
	public function createDuplicate($positionId)
	{
		$option = new JobOpenPositionOption();
		$option->job_open_position_id = $positionId;
		$option->option_key = $this->option_key;
		$option->option_value = $this->option_value;
		$option->save();
	}
}