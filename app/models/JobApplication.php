<?php
class JobApplication extends Eloquent{

	protected $table = 'jobs_applicants';
	
	public function job()
	{
		return $this->belongsTo('Job');
	}
	public function person()
	{
		return $this->belongsTo('Person');
	}
}