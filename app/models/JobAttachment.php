<?php
class JobAttachment extends Eloquent{

	protected $table = 'jobs_attachments';
	
	public $timestamps = false;
	
	public function job()
	{
		if(empty($this->job_id))
			return null;
			
		return Job::find($this->job_id);
	}
	public function job_date()
	{
		if(empty($this->job_date_id))
			return null;
			
		return JobDate::find($this->job_date_id);
	}
	
	public function person()
	{
		return $this->belongsTo('Person');
	}
	
	/* override */
	public function delete()
	{
		if(empty($this->job_id))
		{
			$jobDate = JobDate::find($this->job_date_id);
			
			File::delete($_SERVER['DOCUMENT_ROOT'] . '/uploads_jobs/' . $jobDate->job_id . '/dates/' . $jobDate->id . '/' . $this->filename);
		}
		else //
			File::delete($_SERVER['DOCUMENT_ROOT'] . '/uploads_jobs/' . $this->job_id . '/' . $this->filename);
		
		parent::delete();
	}
}