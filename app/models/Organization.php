<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Organization extends Eloquent{

	protected $table = 'organizations';
	
	public function persons()
	{
		return $this->hasMany('Person');
	}
	
	public static function listAll()
	{
		$organizations = Organization::all();
		
		$data = array();
		foreach($organizations as $organization)
			$data[$organization->id] = $organization->name . ', ' . $organization->address_1 . ', ' . $organization->city . ' ' . $organization->postcode;
		
		return $data;
	}
	public static function listTypes($type = null)
	{
		$types = array(
			'soletrader' => 'Sole Trader',
			'partnership' => 'Partnership',
			'limitedcompany' => 'Limited Company'
		);
		
		if($type == null)
			return $types;
			
		return $types[$type];
	}
}