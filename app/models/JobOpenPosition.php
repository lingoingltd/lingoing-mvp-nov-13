<?php
class JobOpenPosition extends Eloquent{

	protected $table = 'jobs_open_position';
	
	public $timestamps = false;
	
	protected $with = array('person');
	
	public function job()
	{
		return $this->belongsTo('Job');
	}
	
	public function options()
	{
		return $this->hasMany('JobOpenPositionOption');
	}
	public function searchOptions($optionKey)
	{
		return JobOpenPositionOption::where('job_open_position_id', '=', $this->id)
			->where('option_key', '=', $optionKey)
			->get();
	}
	public function searchOptionsAsArray($optionKey)
	{
		$collections = $this->searchOptions($optionKey);
		
		$data = array();
		foreach($collections as $item)
			$data[$item->option_value] = $item->option_value;
			
		return $data;
	}
	
	public function person()
	{
		return $this->belongsTo('Person', 'worker_id');
	}
	
	public function review()
	{
		if(empty($this->person))
			return null;
			
		return PersonReview::where('job_id', '=', $this->job_id)
			->where('person_id', '=', $this->person->id)
			->first();
	}
	
	public function lspDidAttend()
	{
		$data = PersonJobAttending::where('job_id', '=', $this->job_id)
			->where('worker_id', '=', $this->worker_id)
			->get();
			
		return (count($data) == 0);
	}
	
	public function createDuplicate($newJobId)
	{
		$position = new JobOpenPosition();
		$position->spoken_language_interpreter = $this->spoken_language_interpreter;
		$position->sign_language_interpreter = $this->sign_language_interpreter;
		$position->lipspeaker = $this->lipspeaker;
		$position->speech_to_text_reporter = $this->speech_to_text_reporter;
		$position->sign_language_translator = $this->sign_language_translator;
		$position->note_taker = $this->note_taker;
		$position->number_of_audience = $this->number_of_audience;
		$position->no_in_groups = $this->no_in_groups;
		$position->coworker = $this->coworker;
		$position->job_id = $newJobId;
		$position->save();
		
		foreach($this->options as $option)
			$newOption = $option->createDuplicate($position->id);
		
		return $position;
	}
}