<?php
class PersonSendInvitation extends Eloquent{

	protected $table = 'persons_send_invitations';

	public function person()
	{
		return Person::find($this->person_id);
	}
}