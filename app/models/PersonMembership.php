<?php
class PersonMembership extends Eloquent{

	protected $table = 'persons_memberships';
	
	public function person()
	{
		return Person::find($this->person_id);
	}
	
	public function languages()
	{
		return PersonMembershipLanguage::where('membership_id', '=', $this->id)
			->orderBy('language', 'ASC')
			->get();
	}
	public function clearLanguages()
	{
		PersonMembershipLanguage::where('membership_id', '=', $this->id)
			->delete();
	}
	
	public function documents()
	{
		return PersonDocument::where('membership_id', '=', $this->id)
			->orderBy('created_at', 'DESC')
			->get();
	}
}