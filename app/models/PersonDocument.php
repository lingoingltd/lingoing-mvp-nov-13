<?php
class PersonDocument extends Eloquent{

	protected $table = 'persons_documents';
	
	public function person()
	{
		return Person::find($this->person_id);
	}
	
	public function humanReadableFilesize()
	{
		if($this->filesize >= 1<<30)
			return number_format($this->filesize / (1<<30), 2) . "GB";
			
		if($this->filesize >= 1<<20)
			return number_format($this->filesize / (1<<20), 2) . "MB";
			
		if($this->filesize >= 1<<10)
			return number_format($this->filesize / (1<<10), 2) . "KB";
			
		return number_format($this->filesize) . " bytes";
	}
}