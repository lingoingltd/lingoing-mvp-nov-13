<?php
class PersonReview extends Eloquent{

	protected $table = 'persons_reviews';
	
	public function person()
	{
		return $this->belongsTo('Person');
	}
	public function client()
	{
		return $this->belongsTo('Person', 'client_id');
	}
}