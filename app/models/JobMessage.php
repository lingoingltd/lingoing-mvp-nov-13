<?php
class JobMessage extends Eloquent{

	protected $table = 'jobs_messages';
	
	public function job()
	{
		return $this->belongsTo('Job');
	}
	public function person()
	{
		return $this->belongsTo('Person', 'sender_id');
	}
}