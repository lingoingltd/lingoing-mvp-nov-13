<?php
class JobOvertime extends Eloquent{

	protected $table = 'jobs_overtimes';
	
	protected $with = array("person");
	
	public function job()
	{
		return $this->belongsTo('Job');
	}
	public function person()
	{
		return $this->belongsTo('Person');
	}
	
	private function exportIntervalToString($interval)
	{
		$output = '';
		if($interval->d == 1)
			$output = '1 day ago';
		elseif($interval->d > 1)
			$output = $interval->d . ' days ago';
		else
		{
			if($interval->h == 1)
				$output = '1 hour ago';
			elseif($interval->h > 1)
				$output = $interval->h . ' hours ago';
			else
			{
				if($interval->i == 1)
					$output = '1 minute ago';
				elseif($interval->i > 1)
					$output = $interval->i . ' minutes ago';
				else
					$output = 'now';
			}
		}
		return $output;
	}
	
	public function showTimePassedCreated()
	{
		$now = new DateTime("now");
		$created_at = date_create($this->created_at);
		$interval = date_diff($now, $created_at);
		
		return $this->exportIntervalToString($interval);
	}
	public function showTimePassedUpdated()
	{
		$now = new DateTime("now");
		$updated_at = date_create($this->updated_at);
		$interval = date_diff($now, $updated_at);

		return $this->exportIntervalToString($interval);
	}
}