<?php
class PersonMembershipLanguage extends Eloquent{

	protected $table = 'persons_memberships_languages';
	
	public function membership()
	{
		return PersonMembership::find($this->membership_id);
	}
}