<?php
class RecruitmentApplication extends Eloquent{

	protected $table = 'recruitments_applications';
	
	public $timestamps = false;
	
	public function person()
	{
		if(empty($this->person_id))
			return null;
			
		return Person::find($this->person_id);
	}
}