<?php
use \Carbon\Carbon;

class Invoice extends Eloquent{

	protected $table = 'invoices';
	
	protected $with = array("person");
	
	public function job()
	{
		return $this->belongsTo('Job');
	}
	public function person()
	{
		return $this->belongsTo('Person');
	}
	public function dates()
	{
		return InvoiceJobDate::where('invoice_id', '=', $this->id)
			->get();
	}
	public function firstDate()
	{
		return InvoiceJobDate::where('invoice_id', '=', $this->id)
			->orderBy('date', 'ASC')
			->orderBy('start_time', 'ASC')
			->first();
	}
	public function lastDate()
	{
		return InvoiceJobDate::where('invoice_id', '=', $this->id)
			->orderBy('date', 'DESC')
			->orderBy('start_time', 'DESC')
			->first();
	}
	
	public function atwSupportPackage()
	{
		if(empty($this->atw_package_id))
			return null;
			
		return AtwSupportPackage::where('id', '=', $this->atw_package_id)
			->first();
	}
	
	public function renderDueDate()
	{
		$b = new DateTime(date('Y-m-d', time()));
		if(!$b)
			return null;
			
		$e = new DateTime($this->payment_due);
		if(!$e)
			return null;
		
		$diff = $e->diff($b);
		if(!$diff)
			return null;
		
		$output = 'Due ';
		if($e < $b)
			$output = 'Overdue';
		else
		{
			if($diff->days > 0)
			{
				if($diff->days == 1)
					$output .= 'in 1 day';
				else
					$output .= 'in ' . $diff->days . ' days';
			}
			else
				$output .= 'today';
		}
		return $output;
	}
	public function isOverdue()
	{
		if($this->status == 'paid')
			return false;
		
		$b = new DateTime(date('Y-m-d', time()));
		if(!$b)
			return false;
			
		$e = new DateTime($this->payment_due);
		if(!$e)
			return false;
		
		$diff = $e->diff($b);
		if(!$diff)
			return false;
		
		return($e < $b);
	}
	public function getDurationInHours()
	{
		$hours = 0;
		
		$dates = $this->dates();
		foreach($dates as $date)
		{
			$b = new DateTime(date('H:i', strtotime($date->start_time)));
			if(!$b)
				return null;

			$e = new DateTime(date('H:i', strtotime($date->end_time)));
			if(!$e)
				return null;

			$diff = $e->diff($b, true);
			if(!$diff)
				return null;
			
			$hours += $diff->h;
			$hours += ($diff->i / 60);
		}
		return $hours;
	}
	public function getDurationInHoursAsString()
	{
		$duration = $this->getDurationInHours();
		
		$hours = floor($duration);
		$minutes = $duration - $hours;
		
		$string = '';
		if($hours == 1)
			$string .= "1 hour";
		elseif($hours > 1)
			$string .= $hours . ' hours';
			
		if($minutes > 0)
		{
			if($string != '')
				$string .= ' ';
				
			$string .= ($minutes * 60) . ' minutes';
		}
		
		return $string;
	}
	
	public function renderPDF()
	{
		$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		
		$pdf->SetMargins(20, 20, 20);
		$pdf->AddPage();
		
		$pdf->SetFont("freesans", "", "10");
		$pdf->SetTextColor(200, 200, 200);
		$pdf->Cell(0, 0, "Summary Invoice");
		$pdf->SetTextColor(0, 0, 0);
		
		$pdf->Ln(18);
		$pdf->Cell(90, 5, "");
		$pdf->Cell(30, 5, "");
		$pdf->SetFont("freesans", "B", "10");
		$pdf->Cell(50, 5, "Lingoing Limited", 0, 0, 'R');
		$pdf->SetFont("freesans", "", "10");
		$pdf->Ln();
		$pdf->Cell(90, 5, "");
		$pdf->Cell(30, 5, "");
		$pdf->Cell(50, 5, "info@lingoing.com", 0, 0, 'R');
		$pdf->Ln();
		$pdf->Cell(90, 5, "");
		$pdf->Cell(30, 5, "");
		$pdf->Cell(50, 5, "www.lingoing.com", 0, 0, 'R');
		$pdf->Ln();
		$pdf->Cell(100, 5, '');
		$pdf->Cell(30, 5, '');
		$pdf->Ln();
		$pdf->SetFont("freesans", "B", "12");
		$pdf->Cell(0, 5, "CLIENT:");
		$pdf->Ln();
		$pdf->SetFont("freesans", "", "12");
		$pdf->Cell(100, 5, $this->client_name);
		$pdf->Ln();
		$pdf->Cell(100, 5, $this->client_address_1);
		$pdf->writeHTML('<span style="font-weight:bold;">Date</span>: ' . date('d. M Y', strtotime($this->created_at)), true, false, true, true, 'R');
		$pdf->Cell(100, 5, $this->client_postcode . ' ' . $this->client_city);	
		$pdf->writeHTML('<span style="font-weight:bold;">Payment due date</span>: ' . date('d. M Y', strtotime($this->payment_due)), true, false, true, true, 'R');
		$pdf->Cell(100, 5, $this->client_country);	
		$pdf->Ln();
		$pdf->Ln();
		
		$pdf->SetFont("freesans", "B", "12");
		$pdf->Cell(0, 5, "LANGUAGE PROFESSIONAL:");
		$pdf->Ln();
		$pdf->SetFont("freesans", "", "12");
		$pdf->Cell(100, 5, $this->lsp_name);
		$pdf->Ln(10);
		$pdf->SetFont("freesans", "B", "12");
		$pdf->Cell(0, 5, 'Dates booked');
		$pdf->Ln(10);
		$pdf->SetFont("freesans", "B", "9");
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(95, 8, 'Date', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(40, 8, 'Start time', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(40, 8, 'End time', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Ln();
		
		$pdf->SetFont("freesans", "", "9");
		$dates = $this->dates();
		foreach($dates as $date)
		{
			$pdf->Cell(95, 8, date('l, d/m/Y', strtotime($date->date)), array('B' => array('width' => .2)), 0, '', 0);
			$pdf->Cell(40, 8, date('H:i', strtotime($date->start_time)), array('B' => array('width' => .2)), 0, '', 0);
			$pdf->Cell(40, 8, date('H:i', strtotime($date->end_time)), array('B' => array('width' => .2)), 0, '', 0);
			$pdf->Ln();
		}
		
		$pdf->Ln(10);
		$pdf->SetFont("freesans", "B", "16");
		$pdf->Cell(175, 5, "INVOICE: " . $this->number);
		$pdf->SetFont("freesans", "B", "9");
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(135, 8, 'Service', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(40, 8, 'Cost', array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Ln();
		
		//lsp service
		$pdf->SetFont("freesans", "", "9");
		$pdf->Cell(135, 8, 'Language professional service', array('B' => array('width' => .2)), 0, '', 0);
		$pdf->Cell(40, 8, '£' . number_format((float)$this->lsp_cost + (float)$this->lsp_cost_overtime, 2), array('B' => array('width' => .2)), 0, 'R', 0);
		$pdf->Ln();
		
		//lingoing cost
		$pdf->SetFont("freesans", "", "9");
		$pdf->Cell(135, 8, 'Commission of Lingoing', array('B' => array('width' => .2)), 0, '', 0);
		$pdf->Cell(40, 8, '£' . number_format((float)$this->lingoing_commission + (double)$this->lingoing_commission_vat, 2), array('B' => array('width' => .2)), 0, 'R', 0);
		$pdf->Ln();
		
		//sum		
		$pdf->SetFont("freesans", "B", "10");
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(150, 8, 'Total cost', array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Cell(25, 8, '£' . number_format($this->total_cost, 2), array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Ln(30);
		
		$pdf->SetFont("freesans", "", "10");
		$pdf->Cell(0, 0, 'BACS Payment to');
		$pdf->Ln();
		$pdf->Cell(0, 0, 'Account With: HSBC');
		$pdf->Ln();
		$pdf->Cell(0, 0, 'Account Number:  42004763');
		$pdf->Ln();
		$pdf->Cell(0, 0, 'Sort Code: 40-02-33');
		$pdf->Ln(15);
		
		$pdf->SetFont("freesans", "", "9");
		$pdf->Cell(0, 0, 'Payment on receipt of invoice.  If it is necessary to send a reminder, a surcharge will apply which will be the greater of £40 (in ');
		$pdf->Ln();
		$pdf->Cell(0, 0, 'accordance with the Late Payment of Commercial Interest Act 1998, as amended (to incorporate the European Directive');
		$pdf->Ln();
		$pdf->Cell(0, 0, '2000/35/EC) by the Late Payment of Commercial Debts Regulations 2002), or 15% of the total.');
		
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/img/logo-pdf.jpg';
		$pdf->Image($filename, 162, 20, 0, 0, 'JPEG', '', '', true);
		
		//second page
		
		$pdf->SetMargins(20, 20, 20);
		$pdf->AddPage();
		
		$pdf->SetTextColor(200, 200, 200);
		$pdf->SetFont("freesans", "", "10");
		$pdf->Cell(0, 0, "Lingoing Charge");
		$pdf->SetTextColor(0, 0, 0);
		
		$pdf->Ln(18);
		$pdf->Cell(90, 5, "");
		$pdf->Cell(30, 5, "");
		$pdf->SetFont("freesans", "B", "10");
		$pdf->Cell(50, 5, "Lingoing Limited", 0, 0, 'R');
		$pdf->SetFont("freesans", "", "10");
		$pdf->Ln();
		$pdf->Cell(90, 5, "");
		$pdf->Cell(30, 5, "");
		$pdf->Cell(50, 5, "info@lingoing.com", 0, 0, 'R');
		$pdf->Ln();
		$pdf->Cell(90, 5, "");
		$pdf->Cell(30, 5, "");
		$pdf->Cell(50, 5, "www.lingoing.com", 0, 0, 'R');
		$pdf->Ln();
		$pdf->Cell(100, 5, '');
		$pdf->Cell(30, 5, '');
		$pdf->Ln(28);
		$pdf->SetFont("freesans", "B", "12");
		$pdf->Cell(0, 5, "CLIENT:");
		$pdf->Ln();
		$pdf->SetFont("freesans", "", "12");
		$pdf->Cell(100, 5, $this->client_name);
		$pdf->Ln();
		$pdf->Cell(100, 5, $this->client_address_1);
		$pdf->writeHTML('<span style="font-weight:bold;">Date</span>: ' . date('d. M Y', strtotime($this->created_at)), true, false, true, true, 'R');
		$pdf->Cell(100, 5, $this->client_postcode . ' ' . $this->client_city);	
		$pdf->writeHTML('<span style="font-weight:bold;">Payment due date</span>: ' . date('d. M Y', strtotime($this->payment_due)), true, false, true, true, 'R');
		$pdf->Cell(100, 5, $this->client_country);	
		$pdf->writeHTML('<span style="font-weight:bold;">VAT number</span>: ' . Config::get('pricing.lingoing_vat_number'), true, false, true, true, 'R');
		$pdf->Ln();
		$pdf->Ln();
		
		$pdf->SetFont("freesans", "B", "12");
		$pdf->Cell(0, 5, "LANGUAGE PROFESSIONAL:");
		$pdf->Ln();
		$pdf->SetFont("freesans", "", "12");
		$pdf->Cell(100, 5, $this->lsp_name);
		$pdf->Ln(10);
		$pdf->SetFont("freesans", "B", "12");
		$pdf->Cell(0, 5, 'Dates booked');
		$pdf->Ln(10);
		$pdf->SetFont("freesans", "B", "9");
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(95, 8, 'Date', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(40, 8, 'Start time', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(40, 8, 'End time', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Ln();
		
		$pdf->SetFont("freesans", "", "9");
		$dates = $this->dates();
		foreach($dates as $date)
		{
			$pdf->Cell(95, 8, date('l, d/m/Y', strtotime($date->date)), array('B' => array('width' => .2)), 0, '', 0);
			$pdf->Cell(40, 8, date('H:i', strtotime($date->start_time)), array('B' => array('width' => .2)), 0, '', 0);
			$pdf->Cell(40, 8, date('H:i', strtotime($date->end_time)), array('B' => array('width' => .2)), 0, '', 0);
			$pdf->Ln();
		}
		
		$pdf->Ln(10);
		$pdf->SetFont("freesans", "B", "16");
		$pdf->Cell(175, 5, "INVOICE: " . $this->number);
		$pdf->SetFont("freesans", "B", "9");
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(125, 8, 'Service', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(25, 8, 'Hours', array('TB' => array('width' => .5)), 0, 'C', 1);
		$pdf->Cell(25, 8, 'Cost', array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Ln();
		
		//lingoing cost
		$pdf->SetFont("freesans", "", "9");
		$pdf->Cell(125, 8, 'Commission of Lingoing', array('B' => array('width' => .2)), 0, '', 0);
		$pdf->Cell(25, 8, $this->no_of_hours + $this->overtime_hours, array('B' => array('width' => .2)), 0, 'C', 0);
		$pdf->Cell(25, 8, '£' . number_format((float)$this->lingoing_commission, 2), array('B' => array('width' => .2)), 0, 'R', 0);
		$pdf->Ln();
		
		$pdf->SetFont("freesans", "", "9");
		$pdf->Cell(125, 8, 'VAT on commission', array('B' => array('width' => .2)), 0, '', 0);
		$pdf->Cell(25, 8, number_format((float)Config::get('pricing.commission_vat'), 2) . '%', array('B' => array('width' => .2)), 0, 'C', 0);
		$pdf->Cell(25, 8, '£' . number_format((float)$this->lingoing_commission_vat, 2), array('B' => array('width' => .2)), 0, 'R', 0);
		$pdf->Ln();
		
		//sum		
		$pdf->SetFont("freesans", "B", "10");
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(150, 8, 'Total cost', array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Cell(25, 8, '£' . number_format((float)$this->lingoing_commission + (float)$this->lingoing_commission_vat, 2), array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Ln(15);
		
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/img/logo-pdf.jpg';
		$pdf->Image($filename, 162, 20, 0, 0, 'JPEG', '', '', true);
		
		//third page
		$pdf->SetMargins(20, 20, 20);
		$pdf->AddPage();
		
		$pdf->SetFont("freesans", "", "10");
		$pdf->SetTextColor(200, 200, 200);
		$pdf->Cell(0, 0, "LSP Charge");
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(60);
		
		$pdf->SetFont("freesans", "B", "12");
		$pdf->Cell(0, 5, "LANGUAGE PROFESSIONAL:");
		$pdf->Ln();
		$pdf->SetFont("freesans", "", "12");
		$pdf->Cell(100, 5, $this->lsp_name);
		$pdf->Ln();
		$pdf->Cell(100, 5, $this->lsp_address_1);
		$pdf->Ln();
		$pdf->Cell(100, 5, $this->lsp_postcode . ' ' . $this->lsp_city);
		$pdf->Ln();
		$pdf->Cell(100, 5, $this->lsp_email);
		$pdf->Ln();
		$pdf->Cell(100, 5, 'Language type: ' . $this->lsp_language_type);
		$pdf->Ln(10);
		$pdf->SetFont("freesans", "B", "12");
		$pdf->Cell(0, 5, 'Dates booked');
		$pdf->Ln(10);
		$pdf->SetFont("freesans", "B", "9");
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(95, 8, 'Date', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(40, 8, 'Start time', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(40, 8, 'End time', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Ln();
		
		$pdf->SetFont("freesans", "", "9");
		$dates = $this->dates();
		foreach($dates as $date)
		{
			$pdf->Cell(95, 8, date('l, d/m/Y', strtotime($date->date)), array('B' => array('width' => .2)), 0, '', 0);
			$pdf->Cell(40, 8, date('H:i', strtotime($date->start_time)), array('B' => array('width' => .2)), 0, '', 0);
			$pdf->Cell(40, 8, date('H:i', strtotime($date->end_time)), array('B' => array('width' => .2)), 0, '', 0);
			$pdf->Ln();
		}
		
		$pdf->Ln(10);
		
		$pdf->SetFont("freesans", "B", "9");
		$pdf->Ln();
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(125, 8, 'Service', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(25, 8, 'Hours', array('TB' => array('width' => .5)), 0, 'C', 1);
		$pdf->Cell(25, 8, 'Cost', array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Ln();
		
		//lsp service
		$pdf->SetFont("freesans", "", "9");
		$pdf->Cell(125, 8, 'Language professional service', array('B' => array('width' => .2)), 0, '', 0);
		$pdf->Cell(25, 8, $this->no_of_hours + $this->overtime_hours, array('B' => array('width' => .2)), 0, 'C', 0);
		$pdf->Cell(25, 8, '£' . number_format((float)$this->lsp_cost + (float)$this->lsp_cost_overtime, 2), array('B' => array('width' => .2)), 0, 'R', 0);
		$pdf->Ln();
		
		//sum		
		$pdf->SetFont("freesans", "B", "10");
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(150, 8, 'Total cost', array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Cell(25, 8, '£' . number_format((float)$this->lsp_cost + (float)$this->lsp_cost_overtime, 2), array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Ln();
		
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/uploads/avatars/'.$this->worker_id.'.jpg';
		if(@file_exists($filename))
			$pdf->Image($filename, 162, 20, 30, 30, 'JPEG', '', '', true);
		
		return $pdf;
	}
	public function renderPDFForLSP()
	{
		$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		//third page
		$pdf->SetMargins(20, 20, 20);
		$pdf->AddPage();
		
		$pdf->SetFont("freesans", "", "10");
		$pdf->SetTextColor(200, 200, 200);
		$pdf->Cell(0, 0, "LSP Charge");
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(60);
		
		$pdf->SetFont("freesans", "B", "12");
		$pdf->Cell(0, 5, "LANGUAGE PROFESSIONAL:");
		$pdf->Ln();
		$pdf->SetFont("freesans", "", "12");
		$pdf->Cell(100, 5, $this->lsp_name);
		$pdf->Ln();
		$pdf->Cell(100, 5, $this->lsp_address_1);
		$pdf->Ln();
		$pdf->Cell(100, 5, $this->lsp_postcode . ' ' . $this->lsp_city);
		$pdf->Ln();
		$pdf->Cell(100, 5, $this->lsp_email);
		$pdf->Ln();
		$pdf->Cell(100, 5, 'Language type: ' . $this->lsp_language_type);
		$pdf->Ln(10);
		$pdf->SetFont("freesans", "B", "12");
		$pdf->Cell(0, 5, 'Dates booked');
		$pdf->Ln(10);
		$pdf->SetFont("freesans", "B", "9");
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(95, 8, 'Date', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(40, 8, 'Start time', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(40, 8, 'End time', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Ln();
		
		$pdf->SetFont("freesans", "", "9");
		$dates = $this->dates();
		foreach($dates as $date)
		{
			$pdf->Cell(95, 8, date('l, d/m/Y', strtotime($date->date)), array('B' => array('width' => .2)), 0, '', 0);
			$pdf->Cell(40, 8, date('H:i', strtotime($date->start_time)), array('B' => array('width' => .2)), 0, '', 0);
			$pdf->Cell(40, 8, date('H:i', strtotime($date->end_time)), array('B' => array('width' => .2)), 0, '', 0);
			$pdf->Ln();
		}
		
		$pdf->Ln(10);
		
		$pdf->SetFont("freesans", "B", "9");
		$pdf->Ln();
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(125, 8, 'Service', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(25, 8, 'Hours', array('TB' => array('width' => .5)), 0, 'C', 1);
		$pdf->Cell(25, 8, 'Cost', array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Ln();
		
		//lsp service
		$pdf->SetFont("freesans", "", "9");
		$pdf->Cell(125, 8, 'Language professional service', array('B' => array('width' => .2)), 0, '', 0);
		$pdf->Cell(25, 8, $this->no_of_hours + $this->overtime_hours, array('B' => array('width' => .2)), 0, 'C', 0);
		$pdf->Cell(25, 8, '£' . number_format((float)$this->lsp_cost + (float)$this->lsp_cost_overtime, 2), array('B' => array('width' => .2)), 0, 'R', 0);
		$pdf->Ln();
		
		//sum		
		$pdf->SetFont("freesans", "B", "10");
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(150, 8, 'Total cost', array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Cell(25, 8, '£' . number_format((float)$this->lsp_cost + (float)$this->lsp_cost_overtime, 2), array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Ln();
		
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/uploads/avatars/'.$this->worker_id.'.jpg';
		if(@file_exists($filename))
			$pdf->Image($filename, 162, 20, 30, 30, 'JPEG', '', '', true);
		
		return $pdf;
	}
	public function renderPDFWithAtwClaimForm()
	{
		$package = $this->atwSupportPackage();
		
		//hack to load TCPDF before FPDI
		$pdf = new TCPDF();
		$pdf = null;
		
		require_once app_path() . '/library/fpdi/fpdi.php';
		$pdf = new FPDI("P", "mm", "A4", true, 'UTF-8', false);
		$pdf->SetCreator('Lingoing Ltd. (lingoing.com)');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->SetProtection(array('modify', 'copy', 'annot-forms', 'fill-forms', 'extract', 'assemble'), '', null, 0, null);
		
		//Importing AtW claim form
		$pdf->setSourceFile($_SERVER['DOCUMENT_ROOT'] . '/files/AtwClaim.pdf');
		
		$pdf->SetMargins(10, 10, 10);
		$pdf->AddPage();
		$tplIdx = $pdf->importPage(1);
		$pdf->useTemplate($tplIdx);
		
		$pdf->SetMargins(10, 10, 10);
		$pdf->AddPage();
		$tplIdx = $pdf->importPage(2);
		$pdf->useTemplate($tplIdx);
		
		//Fill the form
		$pdf->setPage(1);
		
		$pdf->SetFontSize(11);
		$pdf->setXY(120, 10);
		$pdf->Write(0, '- pay as direct');
		$pdf->setXY(120, 15);
		$pdf->Write(0, '- ' . $this->lsp_position . ' claim form');
		
		$pdf->SetFontSize(16);
		
		if($this->client_title == 'Mr')
		{
			$pdf->setXY(50, 67);
			$pdf->Write(0, 'X');
		}
		elseif($this->client_title == 'Mrs')
		{
			$pdf->setXY(70, 67);
			$pdf->Write(0, 'X');
		}
		elseif($this->client_title == 'Miss')
		{
			$pdf->setXY(89.5, 67);
			$pdf->Write(0, 'X');
		}
		elseif($this->client_title == 'Ms')
		{
			$pdf->setXY(110, 67);
			$pdf->Write(0, 'X');
		}
		else
		{
			$pdf->SetFontSize(11);
			$pdf->setXY(133, 67.5);
			$pdf->Write(0, $this->client_title);
		}
		
		$pdf->SetFontSize(11);
		$pdf->setXY(42, 76.5);
		$pdf->Write(0, $this->client_surname);
		
		$pdf->setXY(136, 76.5);
		$pdf->Write(0, $this->client_name);
		
		if(!empty($package))
		{
			$pdf->setXY(42, 85.5);
			$pdf->Write(0, $package->national_insurance);
		}
		
		$pdf->setXY(135, 85.5);
		$pdf->Write(0, $this->client_email);
		
		$pdf->setXY(49, 110.5);
		$pdf->Write(0, date('d', strtotime($this->atw_date_begin)));
		
		$pdf->setXY(70, 110.5);
		$pdf->Write(0, date('m', strtotime($this->atw_date_begin)));
		
		$pdf->setXY(88, 110.5);
		$pdf->Write(0, date('Y', strtotime($this->atw_date_begin)));
		
		$pdf->setXY(143, 110.5);
		$pdf->Write(0, date('d', strtotime($this->atw_date_end)));
		
		$pdf->setXY(162, 110.5);
		$pdf->Write(0, date('m', strtotime($this->atw_date_end)));
		
		$pdf->setXY(180, 110.5);
		$pdf->Write(0, date('Y', strtotime($this->atw_date_end)));
		
		$datesPositions = array(
			1 => array('date' => array('x' => 15, 'y' => 128), 'hours' => array('x' => 38.5, 'y' => 125.3)),
			2 => array('date' => array('x' => 15, 'y' => 135), 'hours' => array('x' => 38.5, 'y' => 132.3)),
			3 => array('date' => array('x' => 15, 'y' => 142), 'hours' => array('x' => 38.5, 'y' => 139.3)),
			4 => array('date' => array('x' => 15, 'y' => 149), 'hours' => array('x' => 38.5, 'y' => 146.3)),
			5 => array('date' => array('x' => 15, 'y' => 156), 'hours' => array('x' => 38.5, 'y' => 153.3)),
			6 => array('date' => array('x' => 15, 'y' => 163), 'hours' => array('x' => 38.5, 'y' => 160.3)),
			7 => array('date' => array('x' => 15, 'y' => 170), 'hours' => array('x' => 38.5, 'y' => 167.3)),
			8 => array('date' => array('x' => 15, 'y' => 177), 'hours' => array('x' => 38.5, 'y' => 174.3)),
			9 => array('date' => array('x' => 15, 'y' => 184), 'hours' => array('x' => 38.5, 'y' => 181.3)),
			10 => array('date' => array('x' => 15, 'y' => 191), 'hours' => array('x' => 38.5, 'y' => 188.3)),
			
			11 => array('date' => array('x' => 82, 'y' => 128), 'hours' => array('x' => 105.5, 'y' => 125.3)),
			12 => array('date' => array('x' => 82, 'y' => 135), 'hours' => array('x' => 105.5, 'y' => 132.3)),
			13 => array('date' => array('x' => 82, 'y' => 142), 'hours' => array('x' => 105.5, 'y' => 139.3)),
			14 => array('date' => array('x' => 82, 'y' => 149), 'hours' => array('x' => 105.5, 'y' => 146.3)),
			15 => array('date' => array('x' => 82, 'y' => 156), 'hours' => array('x' => 105.5, 'y' => 153.3)),
			16 => array('date' => array('x' => 82, 'y' => 163), 'hours' => array('x' => 105.5, 'y' => 160.3)),
			17 => array('date' => array('x' => 82, 'y' => 170), 'hours' => array('x' => 105.5, 'y' => 167.3)),
			18 => array('date' => array('x' => 82, 'y' => 177), 'hours' => array('x' => 105.5, 'y' => 174.3)),
			19 => array('date' => array('x' => 82, 'y' => 184), 'hours' => array('x' => 105.5, 'y' => 181.3)),
			20 => array('date' => array('x' => 82, 'y' => 191), 'hours' => array('x' => 105.5, 'y' => 188.3)),
			
			21 => array('date' => array('x' => 149, 'y' => 128), 'hours' => array('x' => 172.5, 'y' => 125.3)),
			22 => array('date' => array('x' => 149, 'y' => 135), 'hours' => array('x' => 172.5, 'y' => 132.3)),
			23 => array('date' => array('x' => 149, 'y' => 142), 'hours' => array('x' => 172.5, 'y' => 139.3)),
			24 => array('date' => array('x' => 149, 'y' => 149), 'hours' => array('x' => 172.5, 'y' => 146.3)),
			25 => array('date' => array('x' => 149, 'y' => 156), 'hours' => array('x' => 172.5, 'y' => 153.3)),
			26 => array('date' => array('x' => 149, 'y' => 163), 'hours' => array('x' => 172.5, 'y' => 160.3)),
			27 => array('date' => array('x' => 149, 'y' => 170), 'hours' => array('x' => 172.5, 'y' => 167.3)),
			28 => array('date' => array('x' => 149, 'y' => 177), 'hours' => array('x' => 172.5, 'y' => 174.3)),
			29 => array('date' => array('x' => 149, 'y' => 184), 'hours' => array('x' => 172.5, 'y' => 181.3)),
			30 => array('date' => array('x' => 149, 'y' => 191), 'hours' => array('x' => 172.5, 'y' => 188.3)),
			31 => array('date' => array('x' => 149, 'y' => 198), 'hours' => array('x' => 172.5, 'y' => 195.3))
		);
		
		$hoursSum = 0.0;
		$counter = 1;
		foreach($this->dates() as $date)
		{
			if($counter > 31)
				break;
				
			$position = $datesPositions[$counter];
			
			$pdf->setXY($position['date']['x'], $position['date']['y']);
			$pdf->Write(0, date('d/m/Y', strtotime($date->date)));
			
			$fromDate = Carbon::createFromFormat('Y-m-d H:i:s', $date->date . ' ' . $date->start_time);
			$toDate = Carbon::createFromFormat('Y-m-d H:i:s', $date->date . ' ' . $date->end_time);
			
			$pdf->setXY($position['hours']['x'], $position['hours']['y']);
			$pdf->Cell(25, 10, $toDate->diffInHours($fromDate), array(), 0, 'C', 0);
			
			$hoursSum += $toDate->diffInHours($fromDate);
			
			$counter++;
		}
		
		$pdf->setXY(135, 204);
		$pdf->Cell(100, 10, $hoursSum, array(), 0, 'C', 0);
		
		$pdf->setXY(31, 216.2);
		$pdf->Cell(40, 10, number_format($this->total_cost, 2), array(), 0, 'R', 0);
		
		$pdf->setXY(31, 259);
		$pdf->Cell(40, 10, number_format($this->total_cost, 2), array(), 0, 'R', 0);
		
		$pdf->setPage(2);
		
		$pdf->SetFontSize(11);
		
		$pdf->setXY(42, 84.5);
//		$pdf->Write(0, $this->lsp_name);
		
		$pdf->setXY(49, 93);
		$pdf->Write(0, date('d', strtotime($this->atw_date_end)));
		
		$pdf->setXY(70, 93);
		$pdf->Write(0, date('m', strtotime($this->atw_date_end)));
		
		$pdf->setXY(88, 93);
		$pdf->Write(0, date('Y', strtotime($this->atw_date_end)));
		
		$pdf->setXY(136, 64.5);
//		$pdf->Write(0, $this->lsp_position);
		
		$pdf->setXY(136, 76);
		$pdf->Write(0, 'Lingoing Ltd');
		$pdf->setXY(136, 81);
		$pdf->Write(0, '3 Humber Road');
		$pdf->setXY(136, 86);
		$pdf->Write(0, 'London');
		$pdf->setXY(136, 91);
		$pdf->Write(0, 'NW2 6DW');
		
		$pdf->setXY(74, 141);
		$pdf->Write(0, 'Lingoing Ltd');
		
		$pdf->setXY(42, 218);
		$pdf->Write(0, $this->client_name . ' ' . $this->client_surname);
		
		$pdf->setXY(49, 227);
		$pdf->Write(0, date('d', strtotime($this->atw_date_end)));
		
		$pdf->setXY(70, 227);
		$pdf->Write(0, date('m', strtotime($this->atw_date_end)));
		
		$pdf->setXY(88, 227);
		$pdf->Write(0, date('Y', strtotime($this->atw_date_end)));
		
		if(!empty($package))
		{
			if($package->claim_address == 'atw_london')
			{
				$pdf->setXY(42, 246);
				$pdf->Write(0, 'Access to Work');
				
				$pdf->setXY(42, 251);
				$pdf->Write(0, 'Harrow Jobcentreplus');
				
				$pdf->setXY(42, 256);
				$pdf->Write(0, 'Nine Elms Lane');
				
				$pdf->setXY(42, 261);
				$pdf->Write(0, 'Harrow');
				
				$pdf->setXY(42, 266);
				$pdf->Write(0, 'London, SW95 9BH');
			}
			elseif($package->claim_address == 'atw_glasgow')
			{
				$pdf->setXY(42, 246);
				$pdf->Write(0, 'Access to Work');
				
				$pdf->setXY(42, 251);
				$pdf->Write(0, 'Jobcentre Plus');
				
				$pdf->setXY(42, 256);
				$pdf->Write(0, 'Anniesland JCP');
				
				$pdf->setXY(42, 261);
				$pdf->Write(0, 'Baird Street');
				
				$pdf->setXY(42, 266);
				$pdf->Write(0, 'Glasgow, G90 8AN');
			}
			elseif($package->claim_address == 'atw_cardiff')
			{
				$pdf->setXY(42, 246);
				$pdf->Write(0, 'Access to Work OSU');
				
				$pdf->setXY(42, 251);
				$pdf->Write(0, 'Jobcentre Plus, 1st Floor');
				
				$pdf->setXY(42, 256);
				$pdf->Write(0, 'Alexandra House');
				
				$pdf->setXY(42, 261);
				$pdf->Write(0, '377 Cowbridge Road East');
				
				$pdf->setXY(42, 266);
				$pdf->Write(0, 'Canton');
				
				$pdf->setXY(42, 271);
				$pdf->Write(0, 'Cardiff, CF5 1WU');
			}
			elseif($package->claim_address == 'atw_wolverhampton')
			{
				$pdf->setXY(42, 246);
				$pdf->Write(0, 'Access to Work');
				
				$pdf->setXY(42, 251);
				$pdf->Write(0, 'Operational Support Unit');
				
				$pdf->setXY(42, 256);
				$pdf->Write(0, 'Harrow Jobcentre Plus');
				
				$pdf->setXY(42, 261);
				$pdf->Write(0, 'Mail Handling Site A');
				
				$pdf->setXY(42, 266);
				$pdf->Write(0, 'Wolverhampton, WV98 1JE');
			}
		}
		
		//summary
		$pdf->SetMargins(20, 20, 20);
		$pdf->AddPage();
		
		$pdf->SetFont("freesans", "", "10");
		$pdf->SetTextColor(200, 200, 200);
		$pdf->Cell(0, 0, "Summary Invoice");
		$pdf->SetTextColor(0, 0, 0);
		
		$pdf->Ln(18);
		$pdf->Cell(90, 5, "");
		$pdf->Cell(30, 5, "");
		$pdf->SetFont("freesans", "B", "10");
		$pdf->Cell(50, 5, "Lingoing Limited", 0, 0, 'R');
		$pdf->SetFont("freesans", "", "10");
		$pdf->Ln();
		$pdf->Cell(90, 5, "");
		$pdf->Cell(30, 5, "");
		$pdf->Cell(50, 5, "info@lingoing.com", 0, 0, 'R');
		$pdf->Ln();
		$pdf->Cell(90, 5, "");
		$pdf->Cell(30, 5, "");
		$pdf->Cell(50, 5, "www.lingoing.com", 0, 0, 'R');
		$pdf->Ln();
		$pdf->Cell(100, 5, '');
		$pdf->Cell(30, 5, '');
		$pdf->Ln();
		$pdf->SetFont("freesans", "B", "12");
		$pdf->Cell(0, 5, "CLIENT:");
		$pdf->Ln();
		$pdf->SetFont("freesans", "", "12");
		$pdf->Cell(100, 5, $this->client_name . ' ' . $this->client_surname);
		$pdf->Ln();
		$pdf->Cell(100, 5, $this->client_address_1);
		$pdf->writeHTML('<span style="font-weight:bold;">Date</span>: ' . date('d. M Y', strtotime($this->created_at)), true, false, true, true, 'R');
		$pdf->Cell(100, 5, $this->client_postcode . ' ' . $this->client_city);	
		$pdf->writeHTML('<span style="font-weight:bold;">Payment due date</span>: ' . date('d. M Y', strtotime($this->payment_due)), true, false, true, true, 'R');
		$pdf->Cell(100, 5, $this->client_country);	
		$pdf->Ln();
		$pdf->Ln();
		
		$pdf->SetFont("freesans", "B", "12");
		$pdf->Cell(0, 5, "LANGUAGE PROFESSIONAL:");
		$pdf->Ln();
		$pdf->SetFont("freesans", "", "12");
		$pdf->Cell(100, 5, $this->lsp_name . ' - ' . $this->lsp_position);
		$pdf->Ln(10);
		$pdf->SetFont("freesans", "B", "12");
		$pdf->Cell(0, 5, 'Dates booked');
		$pdf->Ln(10);
		$pdf->SetFont("freesans", "B", "9");
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(135, 8, 'Date', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(40, 8, 'No. of hours', array('TB' => array('width' => .5)), 0, 'C', 1);
		$pdf->Ln();
		
		$pdf->SetFont("freesans", "", "9");
		$dates = $this->dates();
		foreach($dates as $date)
		{
			$fromDate = Carbon::createFromFormat('Y-m-d H:i:s', $date->date . ' ' . $date->start_time);
			$toDate = Carbon::createFromFormat('Y-m-d H:i:s', $date->date . ' ' . $date->end_time);
			
			$pdf->Cell(135, 8, date('l, d/m/Y', strtotime($date->date)), array('B' => array('width' => .2)), 0, '', 0);
			$pdf->Cell(40, 8, $toDate->diffInHours($fromDate), array('B' => array('width' => .2)), 0, 'C', 0);
			$pdf->Ln();
		}
		
		$pdf->Ln(10);
		$pdf->SetFont("freesans", "B", "16");
		$pdf->Cell(175, 5, "INVOICE: " . $this->number);
		$pdf->SetFont("freesans", "B", "9");
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(135, 8, 'Service', array('TB' => array('width' => .5)), 0, '', 1);
		$pdf->Cell(40, 8, 'Cost', array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Ln();
		
		//lsp service
		$pdf->SetFont("freesans", "", "9");
		$pdf->Cell(135, 8, 'Language Service Professional Fee', array('B' => array('width' => .2)), 0, '', 0);
		$pdf->Cell(40, 8, '£' . number_format((float)$this->lsp_cost + (float)$this->lsp_cost_overtime, 2), array('B' => array('width' => .2)), 0, 'R', 0);
		$pdf->Ln();
		
		//lingoing cost
		$pdf->SetFont("freesans", "", "9");
		$pdf->Cell(135, 8, 'Lingoing Fee', array('B' => array('width' => .2)), 0, '', 0);
		$pdf->Cell(40, 8, '£' . number_format((float)$this->lingoing_commission + (double)$this->lingoing_commission_vat, 2), array('B' => array('width' => .2)), 0, 'R', 0);
		$pdf->Ln();
		
		//sum		
		$pdf->SetFont("freesans", "B", "10");
		$pdf->SetFillColor(217, 217, 217);
		$pdf->Cell(150, 8, 'Total cost', array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Cell(25, 8, '£' . number_format($this->total_cost, 2), array('TB' => array('width' => .5)), 0, 'R', 1);
		$pdf->Ln(25);
		
		$pdf->SetFont("freesans", "", "10");
		$pdf->Cell(0, 0, 'This is a summary invoice, details available upon request. Payment on receipt...');
		$pdf->Ln(10);
		$pdf->Cell(0, 0, 'BACS Payment to');
		$pdf->Ln();
		$pdf->Cell(0, 0, 'Account With: HSBC');
		$pdf->Ln();
		$pdf->Cell(0, 0, 'Account Number:  42004763');
		$pdf->Ln();
		$pdf->Cell(0, 0, 'Sort Code: 40-02-33');
		$pdf->Ln(15);
		
		$pdf->SetFont("freesans", "", "9");
		$pdf->Cell(0, 0, 'Payment on receipt of invoice.  If it is necessary to send a reminder, a surcharge will apply which will be the greater of £40 (in ');
		$pdf->Ln();
		$pdf->Cell(0, 0, 'accordance with the Late Payment of Commercial Interest Act 1998, as amended (to incorporate the European Directive');
		$pdf->Ln();
		$pdf->Cell(0, 0, '2000/35/EC) by the Late Payment of Commercial Debts Regulations 2002), or 15% of the total.');
		
		$pdf->setPage(3);
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/img/logo-pdf.jpg';
		$pdf->Image($filename, 162, 20, 0, 0, 'JPEG', '', '', true);
		$pdf->setPage(1);
		
		return $pdf;
	}
	
	public static function nextInvoiceNumber($year)
	{
		$id = DB::table('invoices')
			->select(DB::raw('IFNULL(MAX(CAST(SUBSTR(`number`, 1, 5) AS UNSIGNED)), 0) AS Number'))
			->first();
		
		$id = (int)$id->Number + 1;
		return sprintf("%05d", $id) . '-' . $year;
	}
	public static function listBillPayingOptions()
	{
		return array(
			'paying_card' => 'Credit/Debit card',
			'atw_customer' => 'I am an Access to Work customer'
		);
	}
}