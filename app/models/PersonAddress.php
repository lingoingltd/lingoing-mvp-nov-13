<?php
class PersonAddress extends Eloquent{

	protected $table = 'persons_addresses';
	
	protected $with = array('person');
	
	public $timestamps = false;
	
	public function person()
	{
		return $this->belongsTo('Person');
	}
	
	public function getAddressTypeAsString()
	{
		$address_type = '';
		
		switch($this->address_type)
		{
			case 'billing':
				$address_type = 'Billing';
				break;
				
			case 'default':
				$address_type = 'Default';
				break;
				
			default:
				$address_type = '';
				break;
		}
		
		return $address_type;
	}
	public function getName()
	{
		return empty($this->organization_name) ? $this->first_name . ' ' . $this->last_name : $this->organization_name;
	}
	public function getShortLocation()
	{
		if(empty($this->city) || empty($this->country))
			return '';
		
		return $this->city . ', ' . Person::listCountries($this->country);
	}
}