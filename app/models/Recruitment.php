<?php
class Recruitment extends Eloquent{

	protected $table = 'recruitments';
	
	public static function listJobTypes($jobType = null)
	{
		$data = array(
			'part_time' => 'Part time',
			'full_time' => 'Full time',
			'volunteer' => 'Volunteer'
		);
		
		if($jobType != null)
			return $data[$jobType];
		
		return $data;
	}
}