<?php
class PersonSpecialism extends Eloquent{

	protected $table = 'persons_specialisms';
	
	public function person()
	{
		return $this->belongsTo('Person');
	}
}