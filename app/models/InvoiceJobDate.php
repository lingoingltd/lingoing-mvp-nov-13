<?php
class InvoiceJobDate extends Eloquent{

	protected $table = 'invoices_jobs_dates';
	
	public $timestamps = false;
	
	protected $with = array('invoice');

	public function invoice()
	{
		return $this->belongsTo('Invoice');
	}
}