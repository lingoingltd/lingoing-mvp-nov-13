<?php
class JobDate extends Eloquent{

	protected $table = 'jobs_dates';
	
	public $timestamps = false;
	
	protected $with = array('job');

	public function job()
	{
		return $this->belongsTo('Job');
	}
	public function attachments()
	{
		return JobAttachment::where('job_date_id', '=', $this->id)
			->get();
	}
	
	public function createDuplicate($newJobId)
	{
		$date = new JobDate();
		$date->job_id = $newJobId;
		$date->date = $this->date;
		$date->start_time = $this->start_time;
		$date->end_time = $this->end_time;
		$date->notes = $this->notes;
		$date->save();
		
		//we copy attachments
		foreach($this->attachments() as $attachment)
		{
			$newAttachment = new JobAttachment();
			$newAttachment->job_date_id = $date->id;
			$newAttachment->filename = $attachment->filename;
			$newAttachment->description = $attachment->description;
			$newAttachment->save();
		}
		
		return $date;
	}
	
	/* Override functions */
	public function delete()
	{
		foreach($this->attachments() as $attachment)
			$attachment->delete();
		
		File::deleteDirectory($_SERVER['DOCUMENT_ROOT'] . '/uploads_jobs/' . $this->job_id . '/dates/' . $this->id);
		
		parent::delete();
	}
}