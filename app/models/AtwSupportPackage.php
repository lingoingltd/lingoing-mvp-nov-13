<?php
use \Carbon\Carbon;
 
class AtwSupportPackage extends Eloquent{

	protected $table = 'atw_support_packages';
	
	public function person()
	{
		return $this->belongsTo('Person');
	}
	public function address()
	{
		return $this->hasOne('PersonAddress');
	}
	
	public function isExpired()
	{
		return ($this->status == 'expired');
	}
	
	public static function listGivenTo($givenTo = null)
	{
		$data = array(
			'freelancer' => 'Freelance person',
			'company' => 'Company'
		);
		
		if($givenTo == null)
			return $data;
		
		if(isset($data[$givenTo]))
			return $data[$givenTo];
		
		return $givenTo;
	}
	public static function listClaimAddresses($address = null)
	{
		$data = array(
			'atw_london' => 'Access to Work, Harrow Jobcentreplus, Nine Elms Lane, Harrow, London, SW95 9BH',
			'atw_glasgow' => 'Access to Work, Jobcentre Plus, Anniesland JCP, Baird Street, Glasgow, G90 8AN',
			'atw_cardiff' => 'Access to Work OSU, Jobcentre Plus, 1st Floor, Alexandra House, 377 Cowbridge Road East, Canton, Cardiff, CF5 1WU',
			'atw_wolverhampton' => 'Access to Work, Operational Support Unit, Harrow Jobcentre Plus, Mail Handling Site A, Wolverhampton, WV98 1JE'
		);
		
		if(isset($data[$address]))
			return $data[$address];
		
		return $data;
	}
	public static function listHowManyHoursPer($how_many = null)
	{
		$data = array(
			'week' => 'Week', 
			'month' => 'Month',
			'year' => 'Year'
		);
		
		if(isset($data[$how_many]))
			return $data[$how_many];
			
		return $data;
	}
	public static function listTypeOfLanguageProfessional($lsp = null)
	{
		$data = array(
			'british_sign_language' => 'British Sign Language', 
			'communication_support_worker' => 'Communication Support Worker',
			'note_taker' => 'Note Taker',
			'lip_speaker' => 'Lip Speaker',
			'other' => 'Other'
		);
		
		if(isset($data[$lsp]))
			return $data[$lsp];
		
		return $data;
	}
	public static function listReceiveInvoice($receive = null)
	{
		$data = array(
			'end_of_month' => 'End of month', 
			'every_two_weeks' => 'Every two weeks',
			'job_by_job' => 'Job by Job'
		);
		
		if(isset($data[$receive]))
			return $data[$receive];
			
		return $data;
	}
	public static function listYourBudgetIn($your_budget_in = null)
	{
		$data = array(
			'total' => 'Total', 
			'per_hour' => 'Per hour'
		);
		
		if(isset($data[$your_budget_in]))
			return $data[$your_budget_in];
		
		return $data;
	}
}