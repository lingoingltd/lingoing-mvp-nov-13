<html xmlns:fb="http://ogp.me/ns/fb#">
<head>
<meta charset="UTF-8">
<title>Lingoing</title>
	
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo HTML::style('css/bootstrap.min.css'); ?>
	<?php echo HTML::style('css/select2.css'); ?>
	<?php echo HTML::style('css/select2-bootstrap.css'); ?>
	<?php echo HTML::style('css/flick/jquery-ui-1.10.4.custom.min.css'); ?>
	<?php echo HTML::style('css/jquery.ui.timepicker.css'); ?>
	<?php echo HTML::style('css/font-awesome.min.css'); ?>
	<?php echo HTML::style('css/imgareaselect-animated.css'); ?>
	<?php echo HTML::style('css/lingoing.css'); ?>
	<?php echo HTML::style('css/homepage.css'); ?>
    
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
	<?php echo HTML::script('js/jquery-1.10.2.min.js'); ?>
	<?php echo HTML::script('js/jquery-ui-1.10.3.custom.min.js'); ?>
	<?php echo HTML::script('js/jquery.ui.timepicker.js'); ?>
	<?php echo HTML::script('js/bootstrap.min.js'); ?>
	<?php echo HTML::script('js/select2.min.js'); ?>
	<?php echo HTML::script('js/jquery.ui.widget.js'); ?>
	<?php echo HTML::script('js/jquery.iframe-transport.js'); ?>
	<?php echo HTML::script('js/jquery.fileupload.js'); ?>
	<?php echo HTML::script('js/jquery.cycle2.min.js'); ?>
	
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

	<?php echo HTML::script('js/lingoing.js'); ?>

</head>

<body>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=169746843217608";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script> 

	<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom:0;">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
	      <a class="logo" href="/"> <img class="logo" src="http://lingoing.com/img/logo.png"> </a> </div>
	    <div class="head_social_con">
	      <div class="head_social">
	        <div class="martop5"> 
	        <div class="top_links">
	            <ul>
	                <li><a href="<?php echo url('about-us') ?>">About us</a></li>
	               <li>|</li>
	               <li><a href="<?php echo url('faq') ?>">FAQ</a></li>
	               <li>|</li>
	               <li><a href="<?php echo url('contact-us') ?>">Contact us</a></li>
	            </ul>
	        </div>
	        <style>#twitter-widget-0 {width:85px !important; }</style>
	        <a href="https://twitter.com/share" class="twitter-share-button" data-via="lingoingltd">Tweet</a> 
	          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	          <fb:like href="https://www.facebook.com/lingoingltd" width="200" layout="button_count" action="like" show_faces="false" share="false"></fb:like>
	        </div>
	      </div>
	    </div>
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="/">For interpreters / translators</a></li>
	        <li><a href="/">For clients</a></li>
	        <li><a href="<?php echo url('jobs') ?>">Find jobs</a></li>
	        <li><a href="<?php echo url('professionals') ?>">Find language professionals</a></li>
	        <li class="menu_btn"><a href="<?php echo url('login') ?>">Login</a></li>
	      </ul>
	    </div>
	  </div>
	</nav>

	   <div class="container padbot30 marbot30">

	   <div class="row martop50 marbot30 padbot30">
	   		<div class="col-md-12">
	        <div class="font38 light blue_txt marbot30">404 error</div>
	        <div class="font14 lightgrey_txt">Sorry but the page you are looking for is not avalible.<br />
	        <a href="/" class="blue_txt">Click here to return to our home page</a>
	        </div>
	       </div>
	   </div>

	   </div>   




	<div class="clearfix">
		<div class="col-md-12 footer">
			© 2014 by Lingoing Ltd. | FAQs | Feedback | Contact us | Terms & Conditions
	        <span class="rp_footer">Designed by the team at RoosterPunk.com</span>
		</div>
	</div>
</body>
</html>
