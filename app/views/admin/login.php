<h1>Login</h1>
<hr>

<?php echo Form::open(array('action' => 'AdminController@postLogin', 'method' => 'post')); ?>
	<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center">
		<div class="line">
			<?php echo Form::text('email', Input::old('email'), array('class' => 'form-control green-border', 'placeholder' => 'Email address', 'id' => 'login-email')); ?>
			
			<?php foreach ($errors->get('email') as $message): ?>
				<div class="text-left text-danger"><?php echo $message; ?></div>
			<?php endforeach; ?>
		</div>
		
		<div class="line">
			<?php echo Form::password('password', array('class' => 'form-control green-border', 'placeholder' => 'Password', 'id' => 'login-password')); ?>
			
			<?php foreach ($errors->get('password') as $message): ?>
				<div class="text-left text-danger"><?php echo $message; ?></div>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="clearfix"></div>
	
	<div class="text-center">
		<input class="btn btn-primary" role="button" type="submit" value="Login">
	</div>
	
<?php echo Form::close(); ?>