<h3>Sales rep. stats</h3>
<hr>
<?php if(count($salesStats) > 0): ?>
	<?php foreach($salesStats as $month => $stat): ?>
		<div class="stats clearfix line ">
			<h4 class="" style="border-bottom:1px solid black;padding-bottom:5px;"><?php echo \Carbon\Carbon::createFromFormat('d/m/Y', $month)->format('F Y'); ?></h4>
			
			<table class="table">
				<thead>
					<tr>
						<th>Sales rep. name</th>
						<th class="text-center">No of new registrations</th>
						<th class="text-right">Lingoing's commission<br>(without VAT)</th>
						<th class="text-right">Sales rep. comission</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($stat as $person_id => $data): ?>
						<?php $person = Person::find($person_id); ?>
						
						<tr>
							<td>
								<a href="/admin/users/profile/<?php echo $person_id ?>"><?php echo $person->getName(); ?></a><br>
								<a href="#" class="show-registered-users">Show registered users</a>
							</td>
							<td class="text-center"><?php echo $data['count'] ?></td>
							<td class="text-right">£ <?php echo number_format((double)$data['commission'], 2, '.', ','); ?></td>
							<td class="text-right"><strong><?php
								$percent = 0;
								if($data['count'] >= 1 && $data['count'] <= 5)
									$percent = 10;
								elseif($data['count'] >= 6 && $data['count'] <= 10)
									$percent = 15;
								elseif($data['count'] >= 11)
									$percent = 20;
									
								echo '£ ' . number_format((double)$data['commission'] * $percent / 100, 2, '.', ',');
							?></strong></td>
						</tr>
						<tr class="hidden">
							<td colspan="4">
								<table class="table">
									<thead>
										<th>Name</th>
										<th>Email address</th>
										<th>Account type</th>
										<th class="text-center">Confirmed</th>
										<th class="text-center">Profile completed</th>
									</thead>
									<tbody>
										<?php
											$beginMonth = \Carbon\Carbon::createFromFormat('d/m/Y', $month)->firstOfMonth()->format('Y-m-d');
											$endMonth = \Carbon\Carbon::createFromFormat('d/m/Y', $month)->lastOfMonth()->format('Y-m-d');
											
											$persons = Person::whereRaw("created_at BETWEEN '$beginMonth' AND '$endMonth'")
												->where('referer_id', '=', $person_id)
												->get();
										?>
										<?php foreach ($persons as $person): ?>
											<?php $address = $person->default_address(); ?>
											<tr>
												<td><a href="/admin/users/profile/<?php echo $person->id ?>"><?php echo $address->first_name ?> <?php echo $address->last_name ?></a></td>
												<td><a href="mailto:<?php echo $person->email ?>"><?php echo $person->email ?></a></td>
												
												<?php if($person->account_type == 'client'): ?>
													<td>Client</td>
												<?php elseif($person->account_type == 'translator'): ?>
													<td>Translator</td>
												<?php endif; ?>
												
												<?php if((bool)$person->confirmed): ?>
													<td class="success text-center">Yes</td>
												<?php else: ?>
													<td class="danger text-danger text-center">No</td>
												<?php endif; ?>
												
												<?php if((bool)$person->profile_completed): ?>
													<td class="success text-center">Yes</td>
												<?php else: ?>
													<td class="danger text-danger text-center">No</td>
												<?php endif; ?>
											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</td>
						</tr>

					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	<?php endforeach; ?>
<?php else: ?>
	<p class="text-center">No sales stats</p>
<?php endif; ?>