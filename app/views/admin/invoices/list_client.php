<h3>Clients invoices</h3>
<hr>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th>Client</th>
			<th>Number</th>
			<th>Total cost</th>
			<th>Payment due</th>
			<th>Status (Client)</th>
            <th>Status (LSP)</th>
			<th>AtW send <br>(by Lingoing)</th>
			<th>AtW send <br>(by Client)</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($invoices as $invoice): ?>
			<tr>
				<td><?php echo $invoice->person->getName(); ?></td>
				<td><a href="<?php echo url('invoices/download'); ?>/<?php echo $invoice->id ?>" title="Download as PDF"><?php echo $invoice->number; ?></a></td>
				<td>£<?php echo number_format($invoice->total_cost, 2) ?></td>
				<td><?php echo date("d/m/Y", strtotime($invoice->payment_due)); ?></td>
                
                <?php if($invoice->status == 'paid'): ?>
                	<td class="success">Paid</td>
                <?php elseif($invoice->status == 'unpaid'): ?>
                	<?php if($invoice->isOverdue()): ?>
		                <td class="danger">Unpaid (Overdue)</td>
                    <?php else: ?>
	                    <td class="active">Unpaid</td>
                    <?php endif; ?>
                <?php elseif($invoice->status == 'pending'): ?>
                	<td class="info">Pending</td>
                <?php endif; ?>
                
                <?php if($invoice->status_worker == 'paid'): ?>
                	<td class="success">Paid</td>
                <?php elseif($invoice->status_worker == 'unpaid'): ?>
	                <?php if($invoice->isOverdue()): ?>
		                <td class="danger">Unpaid (Overdue)</td>
                    <?php else: ?>
	                    <td class="active">Unpaid</td>
                    <?php endif; ?>
                <?php elseif($invoice->status_worker == 'pending'): ?>
                	<td class="info">Pending</td>
                <?php endif; ?>

				<?php if((bool)$invoice->lingoing_atw_form_sent): ?>
					<td class="sucess text-success">Yes (<?php echo \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $invoice->lingoing_atw_form_sent_at)->format('d/m/Y H:i') ?>)</td>
				<?php else: ?>
					<?php if(empty($invoice->atw_package_id)): ?>
						<td>Not AtW invoice</td>
					<?php else: ?>
						<td class="danger text-danger"><a href="#" class="send-atw-form" data-id="<?php echo $invoice->id ?>">No</a></td>
					<?php endif; ?>
				<?php endif; ?>
				
				<?php if((bool)$invoice->atw_form_sent): ?>
					<td class="sucess text-success">Yes (<?php echo \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $invoice->atw_form_sent_at)->format('d/m/Y H:i') ?>)</td>
				<?php else: ?>
					<?php if(empty($invoice->atw_package_id)): ?>
						<td>Not AtW invoice</td>
					<?php else: ?>
						<td class="danger text-danger">No</td>
					<?php endif; ?>
				<?php endif; ?>
				
				<td class="text-right" style="font-size:16px;">
                	<?php if($invoice->status != 'paid'): ?>
						<!--a href="#" class="mark-as-paid" title="Mark as paid" data-invoice-id="<?php echo $invoice->id ?>" data-field="status"><span class="glyphicon glyphicon-gbp"></span></a-->
                    <?php endif; ?>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>