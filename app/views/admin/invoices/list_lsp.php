<h3>LSPs invoices</h3>
<hr>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th>LSP</th>
			<th>Number</th>
			<th>LSP cost</th>
			<th>Payment due</th>
            <th>Status (Client)</th>
			<th>Status (LSP)</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($invoices as $invoice): ?>
			<tr>
				<td><?php $worker = Person::find($invoice->worker_id); echo $worker->getName(); ?></td>
				<td><a href="<?php echo url('invoices/download'); ?>/<?php echo $invoice->id ?>" title="Download as PDF"><?php echo $invoice->number; ?></a></td>
				<td>£<?php echo number_format($invoice->lsp_cost + $invoice->lsp_cost_overtime, 2) ?></td>
				<td><?php echo date("d/m/Y", strtotime($invoice->payment_due)); ?></td>
                
                <?php if($invoice->status == 'paid'): ?>
                	<td class="success">Paid</td>
                <?php elseif($invoice->status == 'unpaid'): ?>
                	<?php if($invoice->isOverdue()): ?>
		                <td class="danger">Unpaid (Overdue)</td>
                    <?php else: ?>
	                    <td class="active">Unpaid</td>
                    <?php endif; ?>
                <?php elseif($invoice->status == 'pending'): ?>
                	<td class="info">Pending</td>
                <?php endif; ?>
                
                <?php if($invoice->status_worker == 'paid'): ?>
                	<td class="success">Paid</td>
                <?php elseif($invoice->status_worker == 'unpaid'): ?>
	                <?php if($invoice->isOverdue()): ?>
		                <td class="danger">Unpaid (Overdue)</td>
                    <?php else: ?>
	                    <td class="active">Unpaid</td>
                    <?php endif; ?>
                <?php elseif($invoice->status_worker == 'pending'): ?>
                	<td class="info">Pending</td>
                <?php endif; ?>
				
				<td class="text-right" style="font-size:16px;">
                	<?php if($invoice->status_worker != 'paid'): ?>
						<a href="#" class="mark-as-paid" title="Mark as paid" data-invoice-number="<?php echo $invoice->number; ?>" data-invoice-id="<?php echo $invoice->id ?>" data-field="status_worker"><span class="glyphicon glyphicon-gbp"></span></a>
                    <?php endif; ?>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>