<h3>Sales representative</h3>
<hr>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Referer code</th>
			<th>Enabled as sales rep.</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($users as $user): ?>
			<tr>
				<td><?php echo $user->getName() ?></td>
				<td><a href="mailto:<?php echo $user->email ?>"><?php echo $user->email; ?></a></td>
				<td><?php echo $user->referer_code ?></td>
					
				<?php if((bool)$user->sales_rep_enabled): ?>
					<td class="bg-success text-success">Yes</td>
				<?php else: ?>
					<td class="bg-danger text-danger">No</td>
				<?php endif; ?>
				
				<td class="text-right" style="font-size:16px;">
					<a href="<?php echo url('admin/users/login-as-user') ?>/<?php echo $user->id ?>" title="Login as user"><span class="glyphicon glyphicon-random"></span></a>
					<a href="#" data-person-id="<?php echo $user->id ?>" data-person-name="<?php echo $user->getName(); ?>" data-status="N" class="disable_enable_sales_rep <?php if(!(bool)$user->sales_rep_enabled): ?>hidden<?php endif; ?>" title="Disable sales rep"><span class="glyphicon glyphicon-remove"></span></a>
					<a href="#" data-person-id="<?php echo $user->id ?>" data-person-name="<?php echo $user->getName(); ?>" data-status="Y" class="disable_enable_sales_rep <?php if((bool)$user->sales_rep_enabled): ?>hidden<?php endif; ?>" title="Enable sales rep"><span class="glyphicon glyphicon-ok"></span></a>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>

<div class="modal fade" id="modalConfirmed" tabindex="-1" role="dialog" aria-labelledby="modalConfirmedLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalConfirmedLabel">Confirm account “<span class="user_name"></span>”</h4>
			</div>

			<div class="modal-body">
				<?php echo Form::textarea('message', '', array('class' => 'form-control', 'placeholder' => '', 'id' => 'message')); ?>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary confirm-account" data-person-id="" data-url="<?php echo url('admin/users/confirm-account') ?>">Confirm</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="modalMessageLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalMessageLabel">Send message to “<span class="user_name"></span>”</h4>
			</div>

			<div class="modal-body">
				<?php echo Form::textarea('message-text', '', array('class' => 'form-control', 'placeholder' => '', 'id' => 'message-text')); ?>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary send-message" data-person-id="" data-url="<?php echo url('admin/users/send-message') ?>">Send message</button>
			</div>
		</div>
	</div>
</div>