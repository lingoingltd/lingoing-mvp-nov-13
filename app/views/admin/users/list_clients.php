<h3>Clients</h3>
<hr>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Creation date</th>
			<th class="text-center">Profile completed</th>
			<th class="text-center">Account confirmed</th>
			<th class="text-center">AtW customer</th>
			<th class="text-center">Sales rep.</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($users as $user): ?>
			<tr>
				<td><?php echo $user->getName() ?></td>
				<td><a href="mailto:<?php echo $user->email ?>"><?php echo $user->email; ?></a></td>
				<td><?php echo $user->created_at->format('d/m/Y H:i') ?></td>
					
				<?php if((bool)$user->profile_completed): ?>
					<td class="success text-center">Yes</td>
				<?php else: ?>
					<td class="danger text-danger text-center">No</td>
				<?php endif; ?>
				
				<?php if((bool)$user->confirmed): ?>
					<td class="success text-center">Yes</td>
				<?php else: ?>
					<td class="danger text-danger text-center">No</td>
				<?php endif; ?>
				
				<?php if($user->how_to_pay_bills == 'atw_customer'): ?>
					<?php $packages = $user->atw_packages(); ?>
					<?php if(count($packages) == 0): ?>
						<td class="danger text-danger text-center">Yes (<strong>No packages!</strong>)</td>
					<?php else: ?>
						<?php $unverified_packages = $user->atw_unverified_packages(); ?>
						<?php if(count($unverified_packages) > 0): ?>
							<td class="danger text-danger text-center">Yes (<a href="<?php echo url('admin/users/atw-packages') ?>/<?php echo $user->id ?>"><?php echo count($unverified_packages) ?> - unverified AtW</a>)</td>
						<?php else: ?>
							<td class="success text-success text-center">Yes (<a href="<?php echo url('admin/users/atw-packages') ?>/<?php echo $user->id ?>">all AtW verified</a>)</td>
						<?php endif; ?>
					<?php endif; ?>
				<?php else: ?>
					<td class="text-center">No</td>
				<?php endif; ?>
				
				<?php if((bool)$user->sales_rep_enabled): ?>
					<td class="bg-success text-success">Yes</td>
				<?php else: ?>
					<td class="text-center">No</td>
				<?php endif; ?>
				
				<td class="text-right" style="font-size:16px;">
					<a href="<?php echo url('admin/users/login-as-user') ?>/<?php echo $user->id ?>" title="Login as user"><span class="glyphicon glyphicon-random"></span></a> 
					<a href="<?php echo url('admin/users/profile') ?>/<?php echo $user->id ?>" title="View profile"><span class="glyphicon glyphicon-user"></span></a>
					<a href="#" class="send-message" title="Send message" data-person-id="<?php echo $user->id ?>" data-person-name="<?php echo $user->getName() ?>"><span class="glyphicon glyphicon-send"></span></a>
					<?php if(!$user->confirmed): ?>
						<a href="#" class="confirm-account" title="Confirm account" data-person-id="<?php echo $user->id ?>" data-person-name="<?php echo $user->getName() ?>"><span class="glyphicon glyphicon-check"></span></a>
					<?php endif; ?>
					<?php if(!(bool)$user->sales_rep_enabled): ?>
						<a href="#" class="enable-sales-rep" title="Enabled user as sales representative" data-person-id="<?php echo $user->id ?>" data-person-name="<?php echo $user->getName(); ?>"><span class="glyphicon glyphicon-gbp"></span></a>
					<?php endif; ?>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>

<div class="modal fade" id="modalConfirmed" tabindex="-1" role="dialog" aria-labelledby="modalConfirmedLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalConfirmedLabel">Confirm account “<span class="user_name"></span>”</h4>
			</div>

			<div class="modal-body">
				<?php echo Form::textarea('message', '', array('class' => 'form-control', 'placeholder' => '', 'id' => 'message')); ?>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary confirm-account" data-person-id="" data-url="<?php echo url('admin/users/confirm-account') ?>">Confirm</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="modalMessageLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalMessageLabel">Send message to “<span class="user_name"></span>”</h4>
			</div>

			<div class="modal-body">
				<?php echo Form::textarea('message-text', '', array('class' => 'form-control', 'placeholder' => '', 'id' => 'message-text')); ?>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary send-message" data-person-id="" data-url="<?php echo url('admin/users/send-message') ?>">Send message</button>
			</div>
		</div>
	</div>
</div>