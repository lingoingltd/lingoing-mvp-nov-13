<h1><?php echo $person->getName() ?>'s AtW support packages</h1>
<hr>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th>Given to</th>
			<th>National insurance (NI)</th>
			<th>Email address</th>
			<th>Support ends</th>
			<th>Status</th>
			<th>Attachment</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php if(count($packages) == 0): ?>
			<tr>
				<td colspan="7" class="text-center">Client has no support packages</td>
			</tr>
		<?php else: ?>
			<?php foreach($packages as $package): ?>
				<tr>
					<td><?php echo AtwSupportPackage::listGivenTo($package->given_to) ?></td>
					<td><?php echo $package->national_insurance ?></td>
					<td><?php echo $package->email_address ?></td>
					<td><?php echo date('d/m/Y', strtotime($package->support_end)) ?></td>
					
					<?php if($package->status == 'verified'): ?>
						<td class="success text-success">Verified</td>
					<?php elseif($package->status == 'in_review'): ?>
						<td class="bg-info text-info">In review</td>
					<?php elseif($package->status == 'canceled'): ?>
						<td class="danger text-danger">Canceled</td>
					<?php elseif($package->status == 'expired'): ?>
						<td class="danger text-danger"><strong>Expired</strong></td>
					<?php else: ?>
						<td class="bg-warning text-warning">Not verified</td>
					<?php endif; ?>
					
					<td><a href="<?php echo url('admin/users/package-download-attachment', array('id' => $package->id)); ?>">View document</a></td>
					<td class="text-right">
						<a href="<?php echo url('admin/users/atw-package', array('id' => $package->id)) ?>">View details</a>
					</td>
				</tr>
			<?php endforeach; ?>
		<?php endif; ?>
	</tbody>
</table>