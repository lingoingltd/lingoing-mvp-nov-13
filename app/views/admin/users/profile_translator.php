<?php
$s3 = App::make('aws')->get('s3');
?>
<h1><?php echo $person->getName() ?>'s profile</h1>

<div class="col-md-1 text-center img-container">
	<img src="<?php echo $person->getProfilePicture(); ?>" class="img-thumbnail" <?php if(empty($person->profile_picture_url)): ?>style="width:100%;"<?php endif; ?>>
</div>

<div class="col-md-10">
	<p class="lead">Common</p>
	<p><strong>Account type:</strong> <?php echo $person->accountTypeAsString() ?></p>
	<p><strong>Email:</strong> <a href="mailto:<?php echo $person->email ?>"><?php echo $person->email ?></a></p>
	<p>&nbsp;</p>
	
	<p class="lead">Name &amp; Phone</p>
	<p><strong>Title:</strong> <?php echo Person::renderValue($defaultAddress->title) ?></p>
	<p><strong>Name:</strong> <?php echo $defaultAddress->getName(); ?></p>
	<p><strong>Mobile number:</strong> <?php echo Person::renderValue($defaultAddress->mobile_number) ?></p>
	<p>&nbsp;</p>
	
	<p class="lead">Location</p>
	<p><strong>Address 1:</strong> <?php echo Person::renderValue($defaultAddress->address_1) ?></p>
	<p><strong>Address 2:</strong> <?php echo Person::renderValue($defaultAddress->address_2) ?></p>
	<p><strong>Town/City:</strong> <?php echo Person::renderValue($defaultAddress->city) ?></p>
	<p><strong>Country:</strong> <?php echo Person::renderValue(empty($defaultAddress->country) ? '' : Person::listCountries($defaultAddress->country)) ?></p>
	<p><strong>County:</strong> <?php echo Person::renderValue($defaultAddress->county) ?></p>
	<p><strong>Postcode:</strong> <?php echo Person::renderValue($defaultAddress->postcode) ?></p>
	<p>&nbsp;</p>
	
	<p class="lead">Language skills</p>
	<?php
		$sli_memberships = $person->memberships('sli');
		$sli2_memberships = $person->memberships('sli2');
		$lipspeaker_memberships = $person->memberships('lipspeaker');
		$sttr_memberships = $person->memberships('sttr');
		$slt_memberships = $person->memberships('slt');
		$notetaker_memberships = $person->memberships('notetaker');
	?>
	
	<?php if(count($sli_memberships) > 0 || count($sli2_memberships) > 0 || count($lipspeaker_memberships) > 0 || count($sttr_memberships) > 0 || count($slt_memberships) > 0 || count($notetaker_memberships) > 0): ?>		
		<ul>
			<?php if($person->spoken_language_interpreter): ?>
				<li><strong>Spoken language interpreter</strong>: 
					<table class="table table-stripped">
						<thead>
							<tr>
								<th>Membership body</th>
								<th>Reference number</th>
								<th>Expiry date</th>
								<th>Expired?</th>
								<th>Languages</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($sli_memberships) > 0): ?>
								<?php foreach ($sli_memberships as $membership): ?>
									<tr>
										<td><?php echo $membership->membership_body ?></td>
										<td><?php echo $membership->reference_number ?></td>
										<?php if(!empty($membership->expiry_date)): ?>
											<td><?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y'); ?></td>
										<?php else: ?>
											<td>Not set</td>
										<?php endif; ?>
										
										<?php if($membership->expired): ?>
											<td class="danger text-danger">Yes</td>
										<?php else: ?>
											<td class="bg-success text-success">No</td>
										<?php endif; ?>
										
										<td>
											<?php $languages = $membership->languages(); ?>
											<?php if(count($languages) > 0): ?>
												<?php foreach ($languages as $language): ?>
													<?php echo $language->language ?><br>
												<?php endforeach ?>
											<?php else: ?>
												No languages inserted
											<?php endif; ?>
										</td>
									</tr>
								<?php endforeach ?>
							<?php else: ?>
								<tr>
									<td colspan="5" class="text-center">LSP didn't insert any membership</td>
								</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
			<?php endif; ?>
		
			<?php if($person->sign_language_interpreter): ?>
				<li><strong>Sign language interpreter</strong>: 
					<table class="table table-stripped">
						<thead>
							<tr>
								<th>Membership body</th>
								<th>Reference number</th>
								<th>Expiry date</th>
								<th>Expired?</th>
								<th>Languages</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($sli2_memberships) > 0): ?>
								<?php foreach ($sli2_memberships as $membership): ?>
									<tr>
										<td><?php echo $membership->membership_body ?></td>
										<td><?php echo $membership->reference_number ?></td>

										<?php if(!empty($membership->expiry_date)): ?>
											<td><?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y'); ?></td>
										<?php else: ?>
											<td>Not set</td>
										<?php endif; ?>
										
										<?php if($membership->expired): ?>
											<td class="danger text-danger">Yes</td>
										<?php else: ?>
											<td class="bg-success text-success">No</td>
										<?php endif; ?>
										
										<td>
											<?php $languages = $membership->languages(); ?>
											<?php if(count($languages) > 0): ?>
												<?php foreach ($languages as $language): ?>
													<?php echo $language->language ?><br>
												<?php endforeach ?>
											<?php else: ?>
												No languages inserted
											<?php endif; ?>
										</td>
									</tr>
								<?php endforeach ?>
							<?php else: ?>
								<tr>
									<td colspan="5" class="text-center">LSP didn't insert any membership</td>
								</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
			<?php endif; ?>
		
			<?php if($person->lipspeaker): ?>
				<li><strong>Lipspeaker</strong>: 
					<table class="table table-stripped">
						<thead>
							<tr>
								<th>Membership body</th>
								<th>Reference number</th>
								<th>Expiry date</th>
								<th>Expired?</th>
								<th>Languages</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($lipspeaker_memberships) > 0): ?>
								<?php foreach ($lipspeaker_memberships as $membership): ?>
									<tr>
										<td><?php echo $membership->membership_body ?></td>
										<td><?php echo $membership->reference_number ?></td>
										
										<?php if(!empty($membership->expiry_date)): ?>
											<td><?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y'); ?></td>
										<?php else: ?>
											<td>Not set</td>
										<?php endif; ?>
										
										<?php if($membership->expired): ?>
											<td class="danger text-danger">Yes</td>
										<?php else: ?>
											<td class="bg-success text-success">No</td>
										<?php endif; ?>
										
										<td>
											<?php $languages = $membership->languages(); ?>
											<?php if(count($languages) > 0): ?>
												<?php foreach ($languages as $language): ?>
													<?php echo $language->language ?><br>
												<?php endforeach ?>
											<?php else: ?>
												No languages inserted
											<?php endif; ?>
										</td>
									</tr>
								<?php endforeach ?>
							<?php else: ?>
								<tr>
									<td colspan="5" class="text-center">LSP didn't insert any membership</td>
								</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
			<?php endif; ?>
		
			<?php if($person->speech_to_text_reporter): ?>
				<li><strong>Speech to text reporter</strong>: 
					<table class="table table-stripped">
						<thead>
							<tr>
								<th>Membership body</th>
								<th>Reference number</th>
								<th>Expiry date</th>
								<th>Expired?</th>
								<th>Languages</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($sttr_memberships) > 0): ?>
								<?php foreach ($sttr_memberships as $membership): ?>
									<tr>
										<td><?php echo $membership->membership_body ?></td>
										<td><?php echo $membership->reference_number ?></td>
										
										<?php if(!empty($membership->expiry_date)): ?>
											<td><?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y'); ?></td>
										<?php else: ?>
											<td>Not set</td>
										<?php endif; ?>
										
										<?php if($membership->expired): ?>
											<td class="danger text-danger">Yes</td>
										<?php else: ?>
											<td class="bg-success text-success">No</td>
										<?php endif; ?>
										
										<td>
											<?php $languages = $membership->languages(); ?>
											<?php if(count($languages) > 0): ?>
												<?php foreach ($languages as $language): ?>
													<?php echo $language->language ?><br>
												<?php endforeach ?>
											<?php else: ?>
												No languages inserted
											<?php endif; ?>
										</td>
									</tr>
								<?php endforeach ?>
							<?php else: ?>
								<tr>
									<td colspan="5" class="text-center">LSP didn't insert any membership</td>
								</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
			<?php endif; ?>
		
			<?php if($person->sign_language_translator): ?>
				<li><strong>Sign language translator</strong>: 
					<table class="table table-stripped">
						<thead>
							<tr>
								<th>Membership body</th>
								<th>Reference number</th>
								<th>Expiry date</th>
								<th>Expired?</th>
								<th>Languages</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($slt_memberships) > 0): ?>
								<?php foreach ($slt_memberships as $membership): ?>
									<tr>
										<td><?php echo $membership->membership_body ?></td>
										<td><?php echo $membership->reference_number ?></td>
										
										<?php if(!empty($membership->expiry_date)): ?>
											<td><?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y'); ?></td>
										<?php else: ?>
											<td>Not set</td>
										<?php endif; ?>
										
										<?php if($membership->expired): ?>
											<td class="danger text-danger">Yes</td>
										<?php else: ?>
											<td class="bg-success text-success">No</td>
										<?php endif; ?>
										
										<td>
											<?php $languages = $membership->languages(); ?>
											<?php if(count($languages) > 0): ?>
												<?php foreach ($languages as $language): ?>
													<?php echo $language->language ?><br>
												<?php endforeach ?>
											<?php else: ?>
												No languages inserted
											<?php endif; ?>
										</td>
									</tr>
								<?php endforeach ?>
							<?php else: ?>
								<tr>
									<td colspan="5" class="text-center">LSP didn't insert any membership</td>
								</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
			<?php endif; ?>
		
			<?php if($person->note_taker): ?>
				<li><strong>Note taker</strong>: 
					<table class="table table-stripped">
						<thead>
							<tr>
								<th>Membership body</th>
								<th>Reference number</th>
								<th>Expiry date</th>
								<th>Expired?</th>
								<th>Languages</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($notetaker_memberships) > 0): ?>
								<?php foreach ($notetaker_memberships as $membership): ?>
									<tr>
										<td><?php echo $membership->membership_body ?></td>
										<td><?php echo $membership->reference_number ?></td>
										
										<?php if(!empty($membership->expiry_date)): ?>
											<td><?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y'); ?></td>
										<?php else: ?>
											<td>Not set</td>
										<?php endif; ?>
										
										<?php if($membership->expired): ?>
											<td class="danger text-danger">Yes</td>
										<?php else: ?>
											<td class="bg-success text-success">No</td>
										<?php endif; ?>
										
										<td>
											<?php $languages = $membership->languages(); ?>
											<?php if(count($languages) > 0): ?>
												<?php foreach ($languages as $language): ?>
													<?php echo $language->language ?><br>
												<?php endforeach ?>
											<?php else: ?>
												No languages inserted
											<?php endif; ?>
										</td>
									</tr>
								<?php endforeach ?>
							<?php else: ?>
								<tr>
									<td colspan="5" class="text-center">LSP didn't insert any membership</td>
								</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
			<?php endif; ?>
		</ul>
	<?php else: ?>
		<p class="text-muted"><i>LSP didn't insert any language skills</i></p>
	<?php endif; ?>
	<p>&nbsp;</p>
	
	<p class="lead">Specialisms</p>
	<?php
		$specialisms = $person->specialisms();
	?>
	<?php if(count($specialisms) > 0): ?>
		<ul>
			<?php foreach($specialisms as $specialism): ?>
				<li><?php echo Person::listSpecialisms($specialism->specialism) ?></li>
			<?php endforeach; ?>
		</ul>
	<?php else: ?>
		<p class="text-muted"><i>LSP didn't insert any specialisms</i></p>
	<?php endif; ?>
	<p>&nbsp;</p>
	
	<p class="lead">Experiences</p>
	<?php $experiences = $person->experience; ?>
	<?php if(!empty($experiences)): ?>
		<p><?php echo nl2br(strip_tags($experiences)); ?></p>
	<?php else: ?>
		<p class="text-muted"><i>LSP didn't insert any experiences</i></p>
	<?php endif; ?>
	<p>&nbsp;</p>
	
	<p class="lead">Accreditations</p>
	<?php $accreditations = $person->accreditations; ?>
	<?php if(!empty($accreditations)): ?>
		<p><?php echo nl2br(strip_tags($accreditations)); ?></p>
	<?php else: ?>
		<p class="text-muted"><i>LSP didn't insert any accreditations</i></p>
	<?php endif; ?>
	<p>&nbsp;</p>
	
	<p class="lead">Payment details</p>
	<p><strong>Hourly rate:</strong> £<?php echo number_format($person->hourly_rate, 2) ?></p>
	<p><strong>Min callout (in hours):</strong> <?php echo Person::renderValue($person->min_callout); ?></p>
	<p><strong>How would you prefer to be paid?</strong> <?php echo Person::renderValue(empty($person->payment_type) ? '' : Person::listPaymentMethods($person->payment_type)); ?></p>
	<p><strong>What is your preferred payment period?</strong> <?php echo Person::renderValue(empty($person->payment_period) ? '' : Person::listPaymentPeriod($person->payment_period)); ?></p>
	<p>&nbsp;</p>
	
	<p class="lead">Insurance documents <small>(links are only valid for 1 hour)</small></p>
	<?php $memberships = $person->memberships(); ?>
	<?php if(count($memberships) > 0): ?>
		<?php foreach ($memberships as $membership): ?>
			<?php $documents = $membership->documents(); ?>
		
			<div>
				<h5>Membership body: <?php echo $membership->membership_body ?> <?php if(!empty($membership_body->expiry_date)): ?>(expire: <?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y'); ?>)<?php endif; ?> <?php if($membership->expired): ?><span class="required">expired</span><?php endif; ?></h5>
			</div>
			<div>
				<?php if(count($documents) == 0): ?>
					<p>Person didn't upload any documents yet<?php if($membership->membership_body != 'ASLI' && $membership->membership_body != 'VLP'): ?><span class="required">* Translator has to upload documents</span><?php endif; ?></p>
				<?php else: ?>
					<ul>
					<?php foreach ($documents as $document): ?>
						<li><a href="<?php echo $s3->getObjectUrl(Config::get('amazon.bucket_prefix').'docs', $document->filename, '+1 hour'); ?>"><?php echo $document->filename; ?></a> (<?php echo $document->humanReadableFilesize() ?>)</li>
					<?php endforeach ?>
					</ul>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	<?php else: ?>
		<p class="text-muted"><i>LSP isn't member of any membership body</i></p>
	<?php endif; ?>
	
	<?php if(count($insurance_documents) > 0): ?>
		<div>
			<h5>Other insurance documents</h5>
		</div>
		<div>
			<ul>
			<?php foreach ($insurance_documents as $attachment): ?>
				<li><a href="<?php echo $s3->getObjectUrl(Config::get('amazon.bucket_prefix').'docs', $attachment->filename, '+1 hour'); ?>"><?php echo $attachment->filename; ?></a> (<?php echo $attachment->humanReadableFilesize() ?>)</li>
			<?php endforeach ?>
			</ul>
		</div>
	<?php endif; ?>
	<p>&nbsp;</p>
	
	<p class="lead">CRB documents</p>
	<?php if(count($crb_documents) > 0): ?>
		<ul>
		<?php foreach ($crb_documents as $attachment): ?>
			<li><a href="<?php echo $s3->getObjectUrl(Config::get('amazon.bucket_prefix').'docs', $attachment->filename, '+1 hour'); ?>"><?php echo $attachment->filename; ?></a> (<?php echo $attachment->humanReadableFilesize() ?>)</li>
		<?php endforeach ?>
		</ul>
	<?php else: ?>
		<p class="text-muted"><i>LSP didn't upload any CRB documentations</i></p>
	<?php endif; ?>
</div>