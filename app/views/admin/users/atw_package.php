<h1><?php echo $person->getName() ?>'s AtW support package <small><a href="<?php echo url('admin/users/atw-packages', array('id' => $person->id)) ?>">View all user's packages</a></small></h1>
<hr>
<?php if($package->status == 'verified'): ?>
	<p class="bg-success text-success">AtW support package <strong>IS VERIFIED</strong>!</p>
<?php elseif($package->status == 'in_review'): ?>
	<p class="bg-info text-info">AtW support package <strong>IS IN REVIEW</strong>!</p>
<?php elseif($package->status == 'canceled'): ?>
	<p class="bg-danger text-danger">AtW support package <strong>HAS BEEN CANCELED</strong>!</p>
<?php elseif($package->status == 'expired'): ?>
	<p class="bg-danger text-danger">AtW support package <strong>EXPIRED</strong>!</p>
<?php else: ?>
	<p class="bg-warning text-warning">AtW support package <strong>IS NOT</strong> verified yet!</p>
<?php endif; ?>

<p><strong>Last updated:</strong> <?php echo date('d/m/Y H:i:s', strtotime($package->updated_at)); ?></p>
<p><strong>Given to:</strong> <?php echo AtwSupportPackage::listGivenTo($package->given_to) ?></p>
<p><strong>Address:</strong> 
	<?php if(!empty($address->organization_name)): echo $address->organization_name . ", "; elseif(!empty($address->first_name)): echo $address->first_name . ' ' . $address->last_name . ', '; endif; ?>
	<?php if(!empty($address->address_1)) echo $address->address_1 . ", "; ?>
	<?php if(!empty($address->address_2)) echo $address->address_2 . ", "; ?>
	<?php if(!empty($address->city)) echo $address->city . ", "; ?>
	<?php if(!empty($address->country)) echo Person::listCountries($address->country) . ", "; ?>
	<?php if(!empty($address->county)) echo $address->county . ", "; ?>
	<?php if(!empty($address->postcode)) echo $address->postcode; ?>
</p>
<p><strong>National insurance:</strong> <?php echo $package->national_insurance ?></p>
<p><strong>Email address:</strong> <?php echo $package->email_address ?></p>
<p><strong>Your budget in:</strong> <?php echo AtwSupportPackage::listYourBudgetIn($package->your_budget_in) ?></p>
<p><strong>Your budget:</strong> £<?php echo number_format($package->budget, 2) ?></p>
<p><strong>Type of language professional:</strong> <?php echo AtwSupportPackage::listTypeOfLanguageProfessional($package->type_of_lsp) ?></p>
<?php if($package->type_of_lsp == 'other'): ?>
	<p><strong>Type of language professional (Other):</strong> <?php echo $package->type_of_lsp_other; ?></p>
<?php endif; ?>
<p><strong>Do you want to receive the invoice:</strong> <?php echo AtwSupportPackage::listReceiveInvoice($package->receive_invoice) ?></p>
<p><strong>Any additional information:</strong> <?php echo $package->additional_information ?></p>
<p><strong>When does your support end:</strong> <?php echo date('d/m/Y', strtotime($package->support_end)) ?>
	<?php if($package->isExpired()): ?>
		<strong class="text-danger">Expired!</strong>
	<?php endif; ?>
</p>
<p><strong>Which address do you post your claim forms to:</strong> <?php echo AtwSupportPackage::listClaimAddresses($package->claim_address) ?></p>
<p><strong>How many hour(s) per:</strong> <?php echo AtwSupportPackage::listHowManyHoursPer($package->hours_per) ?></p>
<p><strong>How many hour(s):</strong> <?php echo number_format($package->hours_per_sum, 2) ?></p>
<p><strong>Attachment:</strong> <a href="<?php echo url('admin/users/package-download-attachment', array('id' => $package->id)); ?>">View document</a></p>

<?php if(!$package->isExpired()): ?>
	<?php echo Form::open(array('url' => url('admin/users/atw-package', array('id' => $package->id)), 'method' => 'post')); ?>
		<input type="hidden" name="package_id" value="<?php echo $package->id ?>">
		<button type="submit" name="verify_package" class="btn btn-primary">Verify package</button>
		<button type="submit" name="cancel_package" class="btn btn-primary">Cancel package</button>
		<button type="submit" name="in_review" class="btn btn-primary">Set package as "In review"</button>
	<?php echo Form::close(); ?>
<?php endif; ?>