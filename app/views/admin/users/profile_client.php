<h1><?php echo $person->getName() ?>'s profile</h1>

<div class="col-md-1 text-center img-container">
	<img src="<?php echo $person->getProfilePicture(); ?>" class="img-thumbnail" <?php if(empty($person->profile_picture_url)): ?>style="width:100%;"<?php endif; ?>>
</div>

<div class="col-md-10">
	<p class="lead">Common</p>
	<p><strong>Account type:</strong> <?php echo $person->accountTypeAsString() ?></p>
	<p><strong>Email:</strong> <a href="mailto:<?php echo $person->email ?>"><?php echo $person->email ?></a></p>
	<p>&nbsp;</p>
	
	<p class="lead">Name &amp; Phone</p>
	<p><strong>Title:</strong> <?php echo Person::renderValue($defaultAddress->title) ?></p>
	<p><strong>Name:</strong> <?php echo $defaultAddress->getName(); ?></p>
	<p><strong>Mobile number:</strong> <?php echo Person::renderValue($defaultAddress->mobile_number) ?></p>
	<p>&nbsp;</p>
	
	<p class="lead">Location</p>
	<p><strong>Address 1:</strong> <?php echo Person::renderValue($defaultAddress->address_1) ?></p>
	<p><strong>Address 2:</strong> <?php echo Person::renderValue($defaultAddress->address_2) ?></p>
	<p><strong>Town/City:</strong> <?php echo Person::renderValue($defaultAddress->city) ?></p>
	<p><strong>Country:</strong> <?php echo Person::renderValue(empty($defaultAddress->country) ? '' : Person::listCountries($defaultAddress->country)) ?></p>
	<p><strong>County:</strong> <?php echo Person::renderValue($defaultAddress->county) ?></p>
	<p><strong>Postcode:</strong> <?php echo Person::renderValue($defaultAddress->postcode) ?></p>
	<p>&nbsp;</p>
	
	<p class="lead">Billing address</p>
	<?php if(!empty($billingAddress)): ?>
		<p><strong>Title:</strong> <?php echo Person::renderValue($billingAddress->title) ?></p>
		<p><strong>First name:</strong> <?php echo Person::renderValue($billingAddress->first_name) ?></p>
		<p><strong>Last name:</strong> <?php echo Person::renderValue($billingAddress->last_name) ?></p>
		<p><strong>Mobile number:</strong> <?php echo Person::renderValue($billingAddress->mobile_number) ?></p>
		<p><strong>Phone number:</strong> <?php echo Person::renderValue($billingAddress->phone_number) ?></p>
		<p><strong>Fax number:</strong> <?php echo Person::renderValue($billingAddress->fax_number) ?></p>
		<p><strong>Billing organization name:</strong> <?php echo Person::renderValue($billingAddress->organization_name) ?></p>
		<p><strong>Billing reference:</strong> <?php echo Person::renderValue($billingAddress->billing_reference) ?></p>
		<p><strong>Address 1:</strong> <?php echo Person::renderValue($billingAddress->address_1) ?></p>
		<p><strong>Address 2:</strong> <?php echo Person::renderValue($billingAddress->address_2) ?></p>
		<p><strong>Town/City:</strong> <?php echo Person::renderValue($billingAddress->city) ?></p>
		<p><strong>Postcode:</strong> <?php echo Person::renderValue($billingAddress->postcode) ?></p>
		<p><strong>County:</strong> <?php echo Person::renderValue($billingAddress->county) ?></p>
		<p><strong>Country:</strong> <?php echo Person::renderValue(empty($billingAddress->country) ? '' : Person::listCountries($billingAddress->country)) ?></p>
	<?php else: ?>
		<p>Same as location</p>
	<?php endif; ?>
</div>