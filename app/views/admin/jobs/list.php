<h3>Jobs</h3>
<hr>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th>Client name</th>
			<th>Type</th>
			<th>Project budget</th>
			<th>Budget</th>
			<th>Status</th>
			<th>Date</th>
			<th>Related to job</th>
			<th>Payment method</th>
			<th>AtW package status</th>
			<th>Visible on list</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php if(count($jobs) > 0): ?>
			<?php foreach ($jobs as $job): ?>
				<tr>
					<td>
						<?php $address = $job->person->default_address(); ?>
						<a href="/admin/users/profile/<?php echo $job->person->id ?>"><?php echo $address->getName(); ?></a>
					</td>
					<td><a href="<?php echo url('jobs/view') ?>/<?php echo $job->id ?>"><?php echo ($job->type == 'other' ? $job->type_other : Job::listTypes($job->type)); ?></a></td>
					<td><?php echo Job::listProjectBudget($job->project_budget) ?></td>
					
					<?php if($job->project_budget == 'open_to_quotes'): ?>
						<td>/</td>
					<?php elseif($job->project_budget == 'fixed'): ?>
						<td>£<?php echo number_format((double)$job->budget, 2) ?></td>
					<?php else: ?>
						<td>£<?php echo number_format((double)$job->hourly_rate, 2) ?>/hr</td>
					<?php endif; ?>
					
					<td><?php echo Job::listStatuses($job->status) ?></td>
					<td><?php 
						$firstDate = $job->firstDate();
						if(!empty($firstDate))
							echo \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $firstDate->date . ' ' . $firstDate->start_time)->format('d.m.Y H:i') . ' (' . $job->getDurationInHours() . ' hour(s))';
					?></td>
					<td><?php
						if(!empty($job->parent_job_id))
						{
							$parentJob = Job::find($job->parent_job_id);
							if($parentJob != null)
								echo '<a href="/jobs/view/' . $job->parent_job_id . '">' . ($parentJob->type == 'other' ? $parentJob->type_other : Job::listTypes($parentJob->type)) . '</a>';
							
						}
						else
							echo "/";
					?></td>
					<td><?php echo Job::listPaymentMethods($job->payment_method) ?></td>
					
					<?php if($job->payment_method == 'atw_customer'): ?>
						<?php $package = $job->supportPackage(); ?>
						
						<?php if(!empty($package)): ?>
							<?php if($package->status == 'verified'): ?>
								<td class="success text-success">Verified
							<?php elseif($package->status == 'in_review'): ?>
								<td class="bg-info text-info">In review
							<?php elseif($package->status == 'canceled'): ?>
								<td class="danger text-danger">Canceled
							<?php elseif($package->status == 'expired'): ?>
								<td class="danger text-danger"><strong>Expired</strong>
							<?php else: ?>
								<td class="bg-warning text-warning">Not verified
							<?php endif; ?>
							 (<?php echo date('d/m/Y', strtotime($package->support_end)) ?>)</td>
							
						<?php else: ?>
							<td>Not set</td>
						<?php endif; ?>
					<?php else: ?>
						<td>/</td>
					<?php endif; ?>
					
					<?php if((bool)$job->confirmed): ?>
						<td class="success text-success">Visible</td>
					<?php else: ?>
						<td>Hidden</td>
					<?php endif; ?>
					
					<td class="text-right">
						<a href="<?php echo url('admin/jobs/change') ?>/<?php echo $job->id ?>">Change</a>
						<a href="#" class="text-danger unconfirm-job <?php if(!(bool)$job->confirmed): ?>hidden<?php endif; ?>" data-id="<?php echo $job->id ?>">Unconfirm</a>
						<a href="#" class="confirm-job <?php if((bool)$job->confirmed): ?>hidden<?php endif; ?>" data-id="<?php echo $job->id ?>">Confirm</a>
					</td>
				</tr>
			<?php endforeach ?>
		<?php else: ?>
			<tr>
				<td colspan="10" class="text-center">No jobs available</td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>