<h1>Job details “<?php echo Job::listTypes($job->type) ?>”</h1>
<hr>

<?php echo Form::open(array('url' => url('admin/jobs/change') . '/' . $job_id, 'method' => 'post')); ?>
	
	<h3>Payment method</h3>
	<p>
		<?php $field = 'payment_method' ?>
		<?php echo Form::select($field, array('' => '') + Job::listPaymentMethods(), Input::old($field, $job->payment_method), array('class' => 'form-control', 'placeholder' => 'Please select ...', 'id' => $field)); ?>
	</p>
	
	<button type="submit" name="save_changes" class="btn btn-primary">Save changes</button>
	
<?php echo Form::close(); ?>
