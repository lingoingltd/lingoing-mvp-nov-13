<!DOCTYPE html>
<html lang="en">
<head>
	<title>Lingoing</title>
	<meta name="description" content="">
	<meta name="language" content="en"/>
	<meta name="robots" content="index, follow">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8">
	
    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
	
	<?php echo HTML::style('css/bootstrap.min.css'); ?>
	<?php echo HTML::style('css/select2.css'); ?>
	<?php echo HTML::style('css/select2-bootstrap.css'); ?>
	<?php echo HTML::style('css/admin.css'); ?>
	
	<?php echo HTML::script('js/jquery-1.10.2.min.js'); ?>
	<?php echo HTML::script('js/select2.min.js'); ?>
	<?php echo HTML::script('js/bootstrap.min.js'); ?>
	<?php echo HTML::script('js/admin.js'); ?>
</head>
<body>
	
	<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">Lingoing</a>
		</div>
		
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">					
			<ul class="nav navbar-nav navbar-right">
				<?php if(Auth::check() && Auth::user()->admin): ?>
					<li <?php if(Route::getFacadeRoot()->currentRouteAction() == 'AdminController@getDashboard'): ?>class="active"<?php endif; ?>><a href="<?php echo action('AdminController@getDashboard') ?>">Dashboard</a></li>
					<li><a href="<?php echo action('FrontPageController@getLogout') ?>">Logout (<?php echo Auth::user()->getName(); ?>)</a></li>
				<?php else: ?>
					<li><a href="<?php echo action('AdminController@getLogin') ?>">Login</a></li>
				<?php endif; ?>
			</ul>
		</div>
	</nav>

	<div class="clearfix">
		<?php if(Auth::check() && (bool)Auth::user()->admin): ?>
			<?php if(Session::has('success')): ?>
				<div class="alert alert-success"><?php echo Session::get('success'); ?></div>
			<?php endif; ?>
			
			<div class="col-md-10">
				<!-- begin content -->
				<?php if(isset($content)) echo $content; ?>
				<!-- end content -->
			</div>
			<div class="col-md-2">
				<h4>Manage users</h4>
				<?php
					$unconfirmedClients = Person::countUnconfirmed('client');
					$unconfirmedTranslators = Person::countUnconfirmed('translator');
				?>
				<a href="<?php echo action('UsersAdminController@getClients') ?>">Manage clients <?php if($unconfirmedClients > 0): ?><span class="badge pull-right"><?php echo $unconfirmedClients ?></span><?php endif; ?></a><br>
				<a href="<?php echo action('UsersAdminController@getTranslators') ?>">Manage translators <?php if($unconfirmedTranslators > 0): ?><span class="badge pull-right"><?php echo $unconfirmedTranslators ?></span><?php endif; ?></a><br>
				<a href="<?php echo action('UsersAdminController@getSalesRep') ?>">Manage sales rep.</a>
                <div>&nbsp;</div>
                <h4>Manage jobs</h4>
				<a href="<?php echo action('JobsAdminController@getJobs') ?>">Jobs</a>
				<div>&nbsp;</div>
                <h4>Manage invoices</h4>
                <a href="<?php echo action('InvoicesAdminController@getInvoices') ?>">Client invoices (unpaid)</a><br>
                <a href="<?php echo action('InvoicesAdminController@getInvoices') ?>/paid">Client invoices (paid)</a><br>
				<a href="<?php echo action('InvoicesAdminController@getLspInvoices') ?>">LSP invoices (unpaid)</a><br>
                <a href="<?php echo action('InvoicesAdminController@getLspInvoices') ?>/paid">LSP invoices (paid)</a>
				<div>&nbsp;</div>
                <h4>Stats</h4>
				<a href="<?php echo action('StatsAdminController@getSalesRep') ?>">Sales representatives</a><br>
			</div>
		<?php else: ?>
			<!-- begin content -->
			<?php if(isset($content)) echo $content; ?>
			<!-- end content -->
		<?php endif; ?>
	</div>
	
	<hr/>
	<footer class="text-right">
		&copy; <?php echo date('Y'); ?> by Lingoing Ltd.
	</footer>
	
	<!--div class="debug">
	</div>
	<style>
		.debug:before{
			content:"xs";
		}
		@media (min-width: 768px) {
			.debug:before{
				content:"sm";
			}
		}
		@media (min-width: 992px) {
			.debug:before{
				content:"md";
			}
		}
		@media (min-width: 1200px) {
			.debug:before{
				content:"lg";
			}
		}
	</style-->
</body>
</html>