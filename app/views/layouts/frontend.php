<!DOCTYPE html>
<html lang="en">
<head>
	<title>Lingoing</title>
	<meta name="keywords" content="Interpreters, Translators, Language, Deaf, Access">
	<meta name="description" content="Lingoing is an online market place for language professionals and clients to work directly together. We take care of all the paperwork, reducing cost and time, so you don't have to worry. Lingoing Making Every Voice Heard.">
	<meta name="robots" content="index, follow">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
	  
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<?php echo HTML::style('css/bootstrap.min.css'); ?>
	<?php echo HTML::style('css/select2.css'); ?>
	<?php echo HTML::style('css/select2-bootstrap.css'); ?>
	<?php echo HTML::style('css/flick/jquery-ui-1.10.4.custom.min.css'); ?>
	<?php echo HTML::style('css/jquery.ui.timepicker.css'); ?>
	<?php echo HTML::style('css/font-awesome.min.css'); ?>
	<?php echo HTML::style('css/imgareaselect-animated.css'); ?>
	<?php echo HTML::style('css/lingoing.css'); ?>

	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,700,300' rel='stylesheet' type='text/css'>
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
	<?php echo HTML::script('js/jquery-1.10.2.min.js'); ?>
	<?php echo HTML::script('js/jquery-ui-1.10.3.custom.min.js'); ?>
	<?php echo HTML::script('js/jquery.ui.timepicker.js'); ?>
	<?php echo HTML::script('js/bootstrap.min.js'); ?>
	<?php echo HTML::script('js/select2.min.js'); ?>
	<?php echo HTML::script('js/jquery.ui.widget.js'); ?>
	<?php echo HTML::script('js/jquery.iframe-transport.js'); ?>
	<?php echo HTML::script('js/jquery.fileupload.js'); ?>
	<?php echo HTML::script('js/jquery.imgareaselect.min.js'); ?>
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<?php echo HTML::script('js/lingoing.js'); ?>
</head>
<body>	
	<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom:0;">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
	      <a class="logo" href="/"> <img class="logo" src="/img/logo.png"> </a> </div>
	    <div class="head_social_con">
	      <div class="head_social">
	        <div class="martop5"> 
	        <div class="top_links">
	            <ul>
					<li><a href="<?php echo url('about-us') ?>">About us</a></li>
					<li>|</li>
					<li><a class="recruitment" href="<?php echo url('recruitments') ?>">We're hiring</a></li>
					<li>|</li>
					<li><a href="<?php echo url('faq') ?>">FAQ</a></li>
					<li>|</li>
					<li><a href="<?php echo url('contact-us') ?>">Contact us</a></li>
	            </ul>
	        </div>
	        <div class="head_login_con">
				<?php if(Auth::check()): ?>
					<div class="fp_logedin_welcome text-right">
						Welcome, <strong><?php echo Auth::user()->getName(); ?>! <a href="<?php echo url('dashboard') ?>">Dashboard</a> <a href="<?php echo url('logout') ?>">Logout</a></strong>
					</div>
				<?php else: ?>
		        	<form method="post" action="<?php echo url('login') ?>">
		            	<div class="float_left">Email:<input type="text" class="head_input marright10 marleft10" name="email" /></div>
		               <div class="float_left">Password:<input type="password" class="head_input marright10 marleft10" name="password" /></div>
						<?php echo Form::token(); ?>
		               <div class="float_left"><input type="submit" value="login" class="head_submit" /></div>
		            </form>
				<?php endif; ?>
	        </div>
	        </div>
	      </div>
	    </div>
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="<?php echo url('for-interpreters') ?>">For interpreters / translators</a></li>
	        <li><a href="<?php echo url('for-clients') ?>">For clients</a></li>
	        <li><a href="<?php echo url('jobs') ?>">Find jobs</a></li>
	        <li><a href="<?php echo url('professionals') ?>">Find language professionals</a></li>
	      </ul>
	    </div>
	  </div>
	</nav>
	
	<div class="submenu">
		<div class="container">
			<a href="<?php echo url('jobs') ?>" class="btn btn-link" title="Find Jobs">Find Jobs</a>|<a href="<?php echo url('professionals') ?>" class="btn btn-link" title="Find Language Professionals">Find Language Professionals</a>
			<?php if(!Auth::check() || Auth::user()->account_type != 'translator'): ?>
				<?php if(Auth::check()): ?>
					<?php if(!(bool)Auth::user()->profile_completed): ?>
						<a href="<?php echo url('profile-not-completed') ?>" class="btn btn-primary pull-right post-a-job-button" title="Post a job">Post a Job</a>
					<?php elseif(!(bool)Auth::user()->confirmed): ?>
						<a href="<?php echo url('account-not-approved') ?>" class="btn btn-primary pull-right post-a-job-button" title="Post a job">Post a Job</a>
					<?php else: ?>
						<a href="<?php echo url('post-a-job') ?>" class="btn btn-primary pull-right post-a-job-button" title="Post a Job">Post a Job</a>
					<?php endif; ?>
				<?php else: ?>
					<a href="<?php echo url('post-a-job') ?>" class="btn btn-primary pull-right post-a-job-button" title="Post a Job">Post a Job</a>
				<?php endif; ?>
			<?php endif; ?>
			<div class="clearfix"></div>
		</div>
	</div>
	<?php if(Auth::check()): ?>
		<?php $unread_counter = Auth::user()
			->notifications()
			->where('read', '=', '0')
			->count(); ?>
		<div class="dashboard-line">
			<div class="container">
				<a href="<?php echo url('dashboard') ?>" class="btn btn-link">Dashboard</a>
				<a href="<?php echo url('my-jobs') ?>" class="btn btn-link">My jobs</a>
				<a href="<?php echo url('my-invoices') ?>" class="btn btn-link">My invoices</a>
			
				<?php if(Auth::user()->account_type == 'translator'): ?>
					<a href="<?php echo url('my-clients') ?>" class="btn btn-link">My clients</a>
				<?php elseif(Auth::user()->account_type == 'client'): ?>
					<a href="<?php echo url('my-professionals') ?>" class="btn btn-link">My professionals</a>
				<?php elseif(Auth::user()->account_type == 'agent'): ?>
					<a href="<?php echo url('my-clients') ?>" class="btn btn-link">My clients</a>
					<a href="<?php echo url('my-professionals') ?>" class="btn btn-link">My professionals</a>
				<?php endif; ?>
				
				<a href="<?php echo url('profile') ?>/<?php echo Auth::user()->id ?>" class="btn btn-link">My profile</a>
				<a href="<?php echo url('notifications') ?>" class="btn btn-link pull-right"><?php if($unread_counter > 0): ?><span class="badge badge-red"><?php echo $unread_counter; ?></span><?php endif; ?> Notifications</a>
			</div>
		</div>
	<?php endif; ?>

	<div class="container">
		<!-- begin content -->
		<?php if(isset($content)) echo $content; ?>
		<!-- end content -->
	</div>
	
	<hr/>
	<footer>
		<div class="container">
			<div class="col-md-12">
				&copy; <?php echo date('Y'); ?> by Lingoing Ltd. | <a href="<?php echo url('about-us') ?>" title="About us">About us</a> | <a href="<?php echo url('feedback') ?>" title="Feedback">Feedback</a> | <a href="<?php echo url('contact-us') ?>" title="Contact us">Contact us</a> | <a href="<?php echo url('cookies') ?>">Cookies Notice</a> | <a href="/files/Lingoing Website Terms of Use issued April 2014.pdf">Terms of Use</a> | <a href="/files/Lingoing Website Privacy Statement Issued April 2014.pdf">Privacy statement</a>
			</div>
		</div>
	</footer>

	<?php if(App::environment('production')): ?>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-30074073-6', 'lingoing.com');
		  ga('send', 'pageview');
		</script>
	<?php endif; ?>
</body>
</html>