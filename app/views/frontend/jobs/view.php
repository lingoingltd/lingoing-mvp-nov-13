<?php if(Session::has('success')): ?>
	<div class="alert alert-success complete-profile"><strong>Congrats!</strong> <?php echo Session::get('success'); ?></div>
<?php endif; ?>

<?php $show_status = false; ?>
<div class="pull-left job-title-client">
	<h3><?php echo $job->getTitle() ?> <?php if($job->isUserWorker(Auth::user()->id) || $job->person_id == Auth::user()->id): ?><small>by</small><?php endif; ?></h3>
	<p class="lead">
		<?php if($job->isUserWorker(Auth::user()->id) || $job->person_id == Auth::user()->id): ?>
			<img src="<?php echo $job->person->getProfilePicture() ?>" class="img-thumbnail"> <a href="<?php echo url('profile') ?>/<?php echo $job->person->id ?>" title="<?php echo $job->person->getName() ?>"><?php echo $job->person->getName() ?></a>
		<?php endif; ?>
	</p>
</div>
<div class="pull-right job-title-right-container">
	<?php if($job->person_id != Auth::user()->id): ?>
		<?php if(Auth::user()->account_type == 'translator'): ?>
			<?php if(!$job->hasUserApplied(Auth::user()->id)): ?>
				<?php if($job->status == 'awaiting_applications'): ?>
					
					<!-- Button trigger modal -->
					<?php if(!(bool)Auth::user()->profile_completed): ?>
						<a href="<?php echo url('profile-not-completed') ?>" class="btn btn-primary pull-right" title="Apply for the job">Apply for the job</a>
					<?php elseif(!(bool)Auth::user()->confirmed): ?>
						<a href="<?php echo url('account-not-approved') ?>" class="btn btn-primary pull-right" title="Apply for the job">Apply for the job</a>
					<?php else: ?>
						<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#applyForTheJob">Apply for the job</button>
					<?php endif; ?>
					
				<?php endif; ?>
			<?php else: ?>
				<?php if($job->status == 'confirmed' || $job->status == 'awaiting_applications'): ?>
					<h4 class="pull-right text-success text-right">
						<strong>You already applied!</strong>					
						<div style="margin-top:10px" class="clearfix">
							<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalResign">Resign from the job</button>
							<?php if(count($job->open_positions()) > 1): ?>
								<button class="btn btn-default pull-right" data-toggle="modal" data-target="#modalSendRecommendation" style="margin-right:10px;">Send the job as recommendation</button>
							<?php endif; ?>
						</div>
					</h4>
				<?php else: ?>
					<h4 class="pull-right text-success text-right"><strong>Job finished!</strong></h4>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>
	<?php else: ?>
		<h4 class="job-details-status <?php if($job->status == 'awaiting_applications'): ?>text-warning<?php elseif($job->status == 'confirmed' || $job->status == 'finished' || $job->status == 'invoice_sent'): ?>text-success<?php else: ?>text-danger<?php endif; ?>">
			<?php echo $job->getStatusAsString('<strong>', '</strong>'); ?>
		</h4>
	<?php endif; ?>
</div>
<div class="clearfix"></div>


<ul class="nav nav-tabs">
	<?php if(Auth::user()->id == $job->person_id): ?>
		<li <?php if($tab == 'details'): ?>class="active"<?php endif; ?>><a href="<?php echo url('jobs/view/' . $job->id) ?>">Job details</a></li>
		<li <?php if($tab == 'dates'): ?>class="active"<?php endif; ?>><a href="<?php echo url('jobs/view/' . $job->id) ?>/dates">Dates</a></li>
		<li <?php if($tab == 'applicants'): ?>class="active"<?php endif; ?>><a href="<?php echo url('jobs/view/' . $job->id . '/applicants') ?>">Applicants / Workers</a></li>
		<li <?php if($tab == 'overtime'): ?>class="active"<?php endif; ?>><a href="<?php echo url('jobs/view/' . $job->id . '/overtime') ?>">Overtime</a></li>
		<li <?php if($tab == 'messages'): ?>class="active"<?php endif; ?>><a href="<?php echo url('jobs/view/' . $job->id . '/messages') ?>">Messages</a></li>
		<li <?php if($tab == 'attachments'): ?>class="active"<?php endif; ?>><a href="<?php echo url('jobs/view/' . $job->id . '/attachments') ?>">Attachments</a></li>
	<?php else: ?>
		<li <?php if($tab == 'details'): ?>class="active"<?php endif; ?>><a href="<?php echo url('jobs/view/' . $job->id) ?>">Job details</a></li>
		<li <?php if($tab == 'dates'): ?>class="active"<?php endif; ?>><a href="<?php echo url('jobs/view/' . $job->id) ?>/dates">Dates</a></li>
		<?php if($job->isUserWorker(Auth::user()->id)): ?>
			<li <?php if($tab == 'overtime'): ?>class="active"<?php endif; ?>><a href="<?php echo url('jobs/view/' . $job->id . '/overtime') ?>">Overtime</a></li>
			<li <?php if($tab == 'messages'): ?>class="active"<?php endif; ?>><a href="<?php echo url('jobs/view/' . $job->id . '/messages') ?>">Messages</a></li>
			<li <?php if($tab == 'attachments'): ?>class="active"<?php endif; ?>><a href="<?php echo url('jobs/view/' . $job->id . '/attachments') ?>">Attachments</a></li>
		<?php endif; ?>
	<?php endif; ?>
</ul>
<?php
	$view_path = null;
	
	switch($tab)
	{
		case 'applicants':
			$view_path = 'frontend.jobs.tabs.applicants';
			break;
			
		case 'overtime':
			$view_path = 'frontend.jobs.tabs.overtime';
			break;
			
		case 'messages':
			$view_path = 'frontend.jobs.tabs.messages';
			break;
			
		case 'dates':
			$view_path = 'frontend.jobs.tabs.dates';
			break;
			
		case 'attachments':
			$view_path = 'frontend.jobs.tabs.attachments';
			break;
			
		default:
			$view_path = 'frontend.jobs.tabs.details';
			break;
	}
	
	echo View::make($view_path, array(
		'job' => $job
	));
?>

<!-- Modal -->
<div class="modal fade" id="applyForTheJob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Application for “<?php echo $job->getTitle() ?>”</h4>
			</div>

			<div class="modal-body">
				<?php if($job->project_budget == 'fixed' || $job->project_budget == 'fixed_hourly_rate'): ?>
					Please confirm your application for this job.
				<?php else: ?>
					<label>
					Enter your bid for the project: 
						<div class="input-group">
							<span class="input-group-addon">£</span>
							<?php $field = 'budget'; 
								$hours = ($job->getDurationInHours() < Auth::user()->min_callout ? Auth::user()->min_callout : $job->getDurationInHours());
							?>
							<?php echo Form::text($field, Input::old($field, $hours * Auth::user()->hourly_rate), array('class' => 'form-control', 'placeholder' => 'Your bid', 'id' => $field)); ?>
						</div>

					</label>
					<span class="text-danger hidden"></span>
				<?php endif; ?>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary apply-for-the-job" data-url="<?php echo url('jobs/apply') ?>/<?php echo $job->id ?>">Apply</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="applyForTheJobSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabelSuccess" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabelSuccess">Application successful!</h4>
			</div>

			<div class="modal-body">
				We have received your application. The client will be in touch shortly to let you know if your were selected for this job.<br><br>
				You can view the status of this job in <a href="<?php echo url('my-jobs/applied') ?>" title="My jobs">My jobs</a>.
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default button-ok" data-dismiss="modal">OK</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modalResign" tabindex="-1" role="dialog" aria-labelledby="modalResignLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalResignLabel">Resign from the job</h4>
			</div>

			<div class="modal-body">
				Please tell us the reason:<br>
				<div id="resign-reason">
					<?php echo Form::textarea('reason', '', array('id' => 'reason', 'class' => 'form-control')); ?>
				</div>
				<div class="hidden text-danger"></div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary resign-button" data-url="<?php echo url('jobs/resign') ?>/<?php echo $job->id ?>">Resign!</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modalSendRecommendation" tabindex="-1" role="dialog" aria-labelledby="modalSendRecommendationLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalSendRecommendationLabel">Send the job as recommendation</h4>
			</div>

			<div class="modal-body">
				Email address:<br>
				<div id="send-recommendation-email">
					<?php echo Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Email address ...', 'id' => 'email')); ?>
				</div>
				<div class="hidden text-danger"></div>
				<br><br>
				Message:<br>
				<div id="send-recommendation-message">
					<?php echo Form::textarea('message', '', array('id' => 'message', 'class' => 'form-control')); ?>
				</div>
				<div class="hidden text-danger"></div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary send-recommendation-button" data-url="<?php echo url('jobs/send-recommendation') ?>/<?php echo $job->id ?>">Send recommendation</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->