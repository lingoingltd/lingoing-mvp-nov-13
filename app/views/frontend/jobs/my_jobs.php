<h1>My jobs</h1>

<div class="col-md-3">
	<div class="btn-group-vertical my-jobs-menu">
		<a href="<?php echo url('my-jobs') ?>" class="btn <?php if($type == 'opened'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">Jobs in progress</a>
		<a href="<?php echo url('my-jobs/confirmed') ?>" class="btn <?php if($type == 'confirmed'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">All confirmed jobs</a>
		<a href="<?php echo url('my-jobs/completed') ?>" class="btn <?php if($type == 'completed'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">Completed jobs</a>
		<a href="<?php echo url('my-jobs/canceled') ?>" class="btn <?php if($type == 'canceled'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">Canceled jobs</a>
	</div>
	
</div>

<div class="col-md-9">
	<?php if(count($jobs) > 0): ?>
		<?php foreach ($jobs as $job): ?>
			<div class="job-container border-all">
				<div class="job-title border-bottom-gray">
					<div class="pull-left">
						<h4><small><?php echo Job::listTypes($job->type); ?></small></h4>
						<p class="lead"><a href="<?php echo url('jobs/view') ?>/<?php echo $job->id ?>"><?php echo $job->getTitle() ?></a></p>
						<?php if($job->payment_method == 'atw_customer'): ?>
							<p>Access to Work customer</p>
						<?php endif; ?>
					</div>
					<div class="pull-right text-right">
						<div class="<?php if($job->status == 'awaiting_applications'): ?>text-warning<?php elseif($job->status == 'finished' || $job->status == 'confirmed' || $job->status == 'invoice_sent'): ?>text-success<?php else: ?>text-danger<?php endif; ?>">
							<?php echo $job->getStatusAsString('<strong>', '</strong>'); ?>
						</div>
						<?php if($job->project_budget == 'fixed' || $job->project_budget == 'fixed_hourly_rate'): ?>
							<p class="lead text-right my-jobs-fixed-budget">£<?php echo number_format($job->getBudget(), 2) ?></p>
							<?php if($job->project_budget == 'fixed_hourly_rate'): ?>
								<p class="text-right note"><small class="text-muted">Fixed hourly rate</small></p>
							<?php else: ?>
								<p class="text-right note"><small class="text-muted">Fixed budget</small></p>
							<?php endif; ?>
						<?php endif; ?>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="job-details clearfix">
					<?php $date = $job->firstDate(); ?>
					<div class="col-md-4"><strong>BEGIN DATE:</strong> <?php echo date('d. M Y', strtotime($date->date)); ?></div>
					<div class="col-md-4"><strong>TIME:</strong> <?php echo date('H:i', strtotime($date->start_time)); ?></div>
					<div class="col-md-4 clearfix"><strong>DURATION:</strong> <?php echo $job->getDurationInHours(); ?> hours</div>
					<?php if(Auth::check() && Auth::user()->id == $job->person_id): ?>
						<div class="col-md-12"><strong>LOCATION:</strong> <?php echo $job->getLocation() ?></div>
					<?php elseif($type == 'confirmed'): ?>
						<?php if(Auth::check() && $job->isUserWorker(Auth::user()->id)): ?>
							<div class="col-md-12"><strong>LOCATION:</strong> <?php echo $job->getLocation() ?></div>
						<?php else: ?>
							<div class="col-md-12"><strong>LOCATION:</strong> <?php echo $job->getShortLocation() ?></div>
						<?php endif; ?>
					<?php else: ?>
						<div class="col-md-12"><strong>LOCATION:</strong> <?php echo $job->getShortLocation() ?></div>
					<?php endif; ?>
				</div>
				<div class="button-container clearfix">
					<!--a href="" class="btn btn-default pull-right">Edit</a-->
					<?php if($job->person_id == Auth::user()->id && $job->status == 'awaiting_applications'): ?>
						<a href="<?php echo url('jobs/delete') ?>/<?php echo $job->id ?>" class="btn btn-danger pull-right delete-button" data-title="<?php echo $job->getTitle() ?>" data-id="<?php echo $job->id ?>">Cancel</a>
					<?php endif; ?>
					<a href="<?php echo url('jobs/view') ?>/<?php echo $job->id ?>" class="btn btn-primary pull-right">View</a>
				</div>
			</div>
		<?php endforeach ?>
	<?php else: ?>
		<?php if($type == 'applied'): ?>
			<p class="text-center">You didn't apply for any job yet.</p>
		<?php elseif($type == 'completed'): ?>
			<p class="text-center">You didn't complete any job yet.</p>
		<?php elseif($type == 'canceled'): ?>
			<p class="text-center">You don't have canceled jobs yet.</p>
		<?php else: ?>
			<p class="text-center">You don't have any jobs yet.</p>
		<?php endif; ?>
	<?php endif; ?>
</div>

<div class="modal fade" id="modalCancelJob" tabindex="-1" role="dialog" aria-labelledby="modalCancelJobLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalCancelJobLabel">Confirmation</h4>
			</div>

			<div class="modal-body text-center">
				Do you want to cancel the job <strong class="job-title-dialog"></strong>?
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger cancel-the-job-confirmation" data-url="<?php echo url('jobs/cancel') ?>/" data-id="">Cancel the job</button>
			</div>
		</div>
	</div>
</div>