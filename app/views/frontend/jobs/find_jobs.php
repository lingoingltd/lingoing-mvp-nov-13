<h1>Find jobs</h1>

<div class="col-md-3">
	<?php echo Form::open(array('url' => '/jobs', 'method' => 'post', 'role' => 'form')); ?>
	<div class="white-container search-container">
		<p style="padding-top:10px;"><strong>Access to Work jobs</strong></p>
		<div>
			<label class="list-item">
				<?php echo Form::radio('atw_jobs', 'all', isset($filter) && isset($filter['atw_jobs']) ? $filter['atw_jobs'] == 'all' : true, array('id' => 'atw_jobs_all')) ?>
				All jobs
			</label>
		</div>
		<div>
			<label class="list-item">
				<?php echo Form::radio('atw_jobs', 'only_atw', isset($filter) && isset($filter['atw_jobs']) ? $filter['atw_jobs'] == 'only_atw' : false, array('id' => 'atw_jobs_atw')) ?>
				Only Access to Work jobs
			</label>
		</div>
		<div>
			<label class="list-item">
				<?php echo Form::radio('atw_jobs', 'without_atw', isset($filter) && isset($filter['atw_jobs']) ? $filter['atw_jobs'] == 'without_atw' : false, array('id' => 'atw_jobs_without_atw')) ?>
				Without Access to Work jobs
			</label>
		</div>
		<p style="padding-top:10px;"><strong>Budget</strong></p>
		<div>
			<label class="list-item">
				<?php echo Form::checkbox('fixed_budget', 'yes', isset($filter) && isset($filter['fixed_budget']) ? $filter['fixed_budget'] == 'yes' : false, array('id' => 'fixed_budget')) ?>
				Fixed budget
			</label>
		</div>
		<div>
			<label class="list-item">
				<?php echo Form::checkbox('fixed_hourly_rate', 'yes', isset($filter) && isset($filter['fixed_hourly_rate']) ? $filter['fixed_hourly_rate'] == 'yes' : false, array('id' => 'fixed_hourly_rate')) ?>
				Fixed hourly rate
			</label>
		</div>
		<div>
			<label class="list-item">
				<?php echo Form::checkbox('open_to_quotes', 'yes', isset($filter) && isset($filter['open_to_quotes']) ? $filter['open_to_quotes'] == 'yes' : false, array('id' => 'open_to_quotes')) ?>
				Open to quotes
			</label>
		</div>
		<p class="text-center" style="margin-top:20px;">
			<button type="submit" name="search" class="btn btn-primary text-center"><span class="glyphicon glyphicon-search"></span> Filter</button>
			<button type="submit" name="show_all" class="btn btn-default text-center">Show all</button>
		</p>
	</div>
	<?php echo Form::close(); ?>
</div>

<div class="col-md-9">
	<?php if(count($jobs) > 0): ?>
		<?php foreach($jobs as $job): ?>
			<div class="job-container border-all">
				<div class="job-client clearfix">
					<?php if(Auth::check() && ($job->isUserWorker(Auth::user()->id) || $job->person_id == Auth::user()->id)): ?>
						<div class="professional-picture">
							<img src="<?php echo $job->person->getProfilePicture() ?>" class="img-thumbnail">
						</div>
						<p class="lead professional-name">
							<a href="<?php echo url('profile') ?>/<?php echo $job->person->id ?>" title="<?php echo $job->person->getName() ?>'s public profile"><?php echo $job->person->getName() ?></a>
						</p>
					<?php endif; ?>
					<div class="pull-right">
						<?php if(Auth::check()): ?>
							<?php if(Auth::user()->account_type == 'translator' && $job->person_id != Auth::user()->id): ?>
								<?php if(!$job->hasUserApplied(Auth::user()->id)): ?>
									<a class="btn btn-primary pull-right" href="<?php echo url('jobs/view') ?>/<?php echo $job->id ?>" title="Apply">Apply</a>
								<?php else: ?>
									<span class="text-success"><strong>You already applied!</strong></span>
								<?php endif; ?>
							<?php endif; ?>
						<?php else: ?>
							<a class="btn btn-primary pull-right" href="<?php echo url('login') ?>" title="Apply">Apply</a>
						<?php endif; ?>
					</div>
				</div>
				
				<div class="job-title border-bottom-gray">
					<div class="pull-left col-md-7">
						<p class="lead title"><a href="<?php echo url('jobs/view') ?>/<?php echo $job->id ?>"><?php echo $job->getTitle(); ?></a></p>
						<?php if($job->payment_method == 'atw_customer'): ?>
							<?php 
								$date = \Carbon\Carbon::createFromFormat('Y-m-d', $job->firstDate()->date);
								
								$dateInvoice = '';
								$receiveInvoice = '';
								$package = $job->supportPackage();
								if($package->receive_invoice == 'job_by_job')
								{
									$receiveInvoice = 'job by job';
									
									$dateInvoice = $date->addDay()->addDays(30)->format('jS F');
								}
								elseif($package->receive_invoice == 'every_two_weeks')
								{
									$receiveInvoice = 'every two weeks';
									
									if($date->day < 15)
									{
										$date->day = 15;
										$dateInvoice = $date->addDays(30)->format('jS F');
									}
									else
										$dateInvoice = $date->addDays(20)->startOfMonth()->addDays(30)->format('jS F'); //20day => move to next month
								}
								else
								{
									$receiveInvoice = 'at the end of every month';
									$dateInvoice = $date->startOfMonth()->addMonth()->startOfMonth()->addDays(30)->format('jS F');
								}
							?>
							
							<p>Access to Work customer</p>
							<div class="">
								<p><small class="text-muted">Customer will send claim form <?php echo $receiveInvoice ?>, payment can be expected by the <?php echo $dateInvoice ?></small></p>
							</div>
						<?php endif; ?>
					</div>
					<div class="pull-right col-md-5">						
						<?php if($job->project_budget == 'fixed' || $job->project_budget == 'fixed_hourly_rate'): ?>
							<p class="lead text-right project-budget">£<?php echo number_format($job->getBudget(), 2) ?></p>
							<div class="col-md-8 pull-right row">
								<p class="text-right note"><small class="text-muted">Rate is all inclusive, due to fixed budget constraints. Any overtime must be agreed with client.</small></p>
							</div>
						<?php else: ?>
							<p class="lead text-right project-budget">Open to quotes</p>
						<?php endif; ?>
					</div>
					<div class="clearfix"></div>
				</div>
				
				<div class="job-details clearfix">
					<?php $date = $job->firstDate(); ?>
					<div class="col-md-4"><strong>Date:</strong> <?php echo date('d. M Y', strtotime($date->date)); ?></div>
					<div class="col-md-4"><strong>Time:</strong> <?php echo date('H:i', strtotime($date->start_time)); ?></div>
					<div class="col-md-4 clearfix"><strong>Duration:</strong> <?php echo $job->getDurationInHours(); ?> hours</div>
					<?php if(Auth::check() && Auth::user()->id == $job->person_id): ?>
						<div class="col-md-12"><strong>Location:</strong> <?php echo $job->getLocation() ?></div>
					<?php else: ?>
						<div class="col-md-12"><strong>Location:</strong> <?php echo $job->getShortLocation() ?></div>
					<?php endif; ?>
				</div>
			</div>
		<?php endforeach; ?>
	
		<?php echo View::make('frontend.common.paging', array(
			'page' => $page,
			'max_pages' => $max_pages,
			'url' => 'jobs'
		)) ?>
	<?php else: ?>
		<p class="text-center">Currently there are no jobs</p>
	<?php endif; ?>
</div>