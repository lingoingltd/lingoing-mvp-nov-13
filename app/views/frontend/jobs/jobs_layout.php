<div class="col-md-12">
	<h2>Post a new job</h2>
</div>

<div class="col-md-12">
	<ul class="nav nav-tabs">	
		<li class="disabled <?php if(Route::getFacadeRoot()->currentRouteAction() == 'JobsController@getStep1'): ?>active<?php endif; ?>"><a href="#" title="1. Job details">1. Job details</a></li>
		<li class="disabled <?php if(Route::getFacadeRoot()->currentRouteAction() == 'JobsController@getStep2'): ?>active<?php endif; ?>"><a href="#" title="2. Language requirements">2. Language requirements</a></li>
		<li class="disabled <?php if(Route::getFacadeRoot()->currentRouteAction() == 'JobsController@getStep3'): ?>active<?php endif; ?>"><a href="#" title="3. Address">3. Address</a></li>
		<li class="disabled <?php if(Route::getFacadeRoot()->currentRouteAction() == 'JobsController@getStep4' || Route::getFacadeRoot()->currentRouteAction() == 'JobsController@getReviewAllDates'): ?>active<?php endif; ?>"><a href="#" title="4. Dates">4. Dates</a></li>
		<li class="disabled <?php if(Route::getFacadeRoot()->currentRouteAction() == 'JobsController@getStep5'): ?>active<?php endif; ?>"><a href="#" title="5. Attachments">5. Attachments</a></li>
	</ul>
	
	<!-- begin profile -->
	<div class="profile_layout">
		<p class="text-right required">* - mandatory field</p>
		<?php if(isset($content)) echo $content; ?>
	</div>
	<!-- end profile -->
</div>

<!--
<?php if(!(bool)Auth::user()->profile_completed): ?>
	<div class="alert alert-info complete-profile">You have to <strong>complete profile</strong> before continue using our services.</div>
<?php elseif(Session::has('success')): ?>
	<div class="alert alert-success complete-profile"><?php echo Session::get('success'); ?></div>
<?php endif; ?>
-->

<!--div class="col-md-2">
	<div class="my-panel">
		<div class="title">Progress</div>
		
		<?php 
			$max_steps = null;
			$step_no = null;
			
			$action = Route::getFacadeRoot()->currentRouteAction();
			
			$max_steps = 4;
			if($action == 'JobsController@getStep1')
				$step_no = 1;
			elseif($action == 'JobsController@getStep2')
				$step_no = 2;
			elseif($action == 'JobsController@getStep3')
				$step_no = 3;
			elseif($action == 'JobsController@getStep4' || $action == 'JobsController@getReviewAllDates')
				$step_no = 4;
			
			$percentage = 0;
			if((int)$max_steps > 0)
				$percentage = round((100 / $max_steps) * $step_no);
			?>
		<div class="progress">
			<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $percentage ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percentage ?>%;">
				<span class="sr-only">Step <?php echo $step_no ?> of <?php echo $max_steps ?></span>
			</div>
		</div>
		<div class="text-center"><small>Step <?php echo $step_no ?> of <?php echo $max_steps ?></small></div>
	</div>
</div-->