<?php 
	$hired = array();
	$dates = $job->dates();
	
	$open_positions = count($job->open_positions()) > 0;
	foreach ($job->positions as $position): ?>
	<?php $review = $position->review(); ?>
	
	<div class="col-md-12 applicants-container" style="border:0;margin-top:10px;">
		<span class="text-uppercase">
			<strong>Hired language professionals</strong>
		</span>
	</div>
	
	<?php if(empty($position->person)): ?>
		<div class="col-md-12 application-container text-center">
			Language professional required.
		</div>
	<?php else: ?>		
		<?php $hired[] = $position->person->id; ?>
		<div class="ratings-container border-all clearfix">
			<div class="ratings-header clearfix">
				<div class="pull-left">
					<div class="professional-picture">
						<img src="<?php echo $position->person->getProfilePicture() ?>" class="img-thumbnail">
					</div>
					<p class="lead professional-name">
						<a href="<?php echo url('profile') ?>/<?php echo $position->person->id ?>" title="<?php echo $position->person->getName() ?>"><?php echo $position->person->getName() ?></a>
					</p>
				</div>
				<div class="pull-right">
					<?php if($job->status == 'awaiting_applications'): ?>						
						<button class="btn btn-danger fire-lsp" data-name="<?php echo $position->person->getName() ?>" data-position-id="<?php echo $position->id ?>">Remove</button>
					<?php elseif($job->status == 'invoice_sent' || $job->status == 'finished'): ?>
						
						<?php 
							$isRated = $position->person->isPersonRated($job->id);
							$attended = $position->lspDidAttend();
						?>
						
						<?php if($attended && !$isRated): ?>
							<button class="btn btn-danger lsp-did-not-attend" data-name="<?php echo $position->person->getName() ?>" data-position-id="<?php echo $position->id ?>">Did <strong>NOT</strong> attend!</button>
						<?php endif; ?>
						
						<?php if(!$isRated): ?>
							<?php if($position->lspDidAttend()): ?>
								<button class="btn btn-primary rate-lsp" data-name="<?php echo $position->person->getName() ?>" data-position-id="<?php echo $position->id ?>">Rate</button>
							<?php else: ?>
								<span class="">Translator did <strong>NOT</strong> attend!</span>
							<?php endif; ?>
						<?php elseif($attended): ?>
							<?php
								$rating = $review->stars;
							?>
							<strong>Rated:</strong> 
							<?php for ($i=1; $i <= $rating; $i++): ?> 
								<span class="glyphicon glyphicon-star gold-star"></span> 
							<?php endfor ?>

							<?php for($i=$rating + 1; $i <= 5; $i++): ?>
								<span class="glyphicon glyphicon-star"></span> 
							<?php endfor; ?>
						<?php endif; ?>
					<?php endif; ?>
				</div>
			</div>
			
			<?php if(!empty($review->review)): ?>
				<div class="reviews-review">
					<?php echo $review->review; ?>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	
<?php endforeach ?>

<?php if(count($job->open_positions()) > 0): ?>
	<div class="col-md-12 applicants-container">
		<span class="text-uppercase">
			<strong>Applicants</strong>
		</span>
	</div>
	
	<?php if (count($job->applicants) > 0): ?>
		<?php foreach ($job->applicants as $application): ?>
			<?php if(in_array($application->person->id, $hired)) continue; ?>
			
			<div class="application">
				<div class="professional-container border-all bottom-space">
					<div class="job-title border-bottom-gray clearfix">
						<?php if($job->status == 'awaiting_applications' && $open_positions): ?>
							<button class="btn btn-primary pull-right hire-lsp" data-person-id="<?php echo $application->person_id ?>" data-name="<?php echo $application->person->getName() ?>" data-job="<?php echo $job->getTitle() ?>">Hire</button>
						<?php endif; ?>

						<div class="professional-picture">
							<img src="<?php echo $application->person->getProfilePicture() ?>" class="img-thumbnail">
						</div>
						<p class="lead professional-name">
							<a href="<?php echo url('profile') ?>/<?php echo $application->person->id ?>" title="<?php echo $application->person->getName() ?>"><?php echo $application->person->getName() ?></a>
						</p>
					</div>
					<div class="professional-languages border-bottom-gray clearfix">
						<?php
							if($application->person->spoken_language_interpreter): 
								$languages = $application->person->membershipsLanguages('sli');
								$count = count($languages);
								$index = 0;
							?>

							<strong>Spoken language interpreter:</strong> 
							<?php foreach ($languages as $language): ?>
								<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
								<?php $index++; ?>
							<?php endforeach ?>
							<br>
						<?php endif;


							if($application->person->sign_language_interpreter): 
								$languages = $application->person->membershipsLanguages('sli2');
								$count = count($languages);
								$index = 0;
							?>

							<strong>Sign language interpreter:</strong> 
							<?php foreach ($languages as $language): ?>
								<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
								<?php $index++; ?>
							<?php endforeach ?>
							<br>	
						<?php endif;

							if($application->person->lipspeaker):
								$languages = $application->person->membershipsLanguages('lipspeaker');
								$count = count($languages);
								$index = 0;
							?>

							<strong>Lipspeaker:</strong> 
							<?php foreach ($languages as $language): ?>
								<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
								<?php $index++; ?>
							<?php endforeach ?>
							<br>
						<?php endif;

							if($application->person->speech_to_text_reporter):						
								$languages = $application->person->membershipsLanguages('sttr');
								$count = count($languages);
								$index = 0;
							?>

							<strong>Speech to text reporter:</strong> 
							<?php foreach ($languages as $language): ?>
								<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
								<?php $index++; ?>
							<?php endforeach ?>
							<br>
						<?php endif;

							if($application->person->sign_language_translator):						
								$languages = $application->person->membershipsLanguages('slt');
								$count = count($languages);
								$index = 0;
							?>

							<strong>Sign language translator:</strong> 
							<?php foreach ($languages as $language): ?>
								<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
								<?php $index++; ?>
							<?php endforeach ?>
							<br>
						<?php endif;

							if($application->person->note_taker):
								$languages = $application->person->membershipsLanguages('notetaker');
								$count = count($languages);
								$index = 0;
							?>

							<strong>Note taker:</strong> 
							<?php foreach ($languages as $language): ?>
								<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
								<?php $index++; ?>
							<?php endforeach ?>
							<br>
						<?php endif; ?>

						<?php
							$specialisms = $application->person->specialisms();
							$count = count($specialisms);
							if($count > 0):
								$index = 0;
						?>

							<strong>Specialisms:</strong>
							<?php foreach ($specialisms as $specialism): ?>
								<?php echo $specialism->specialism; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
								<?php $index++; ?>
							<?php endforeach ?>

						<?php endif; ?>
					</div>
					<div class="professional-details clearfix">
						<div class="col-md-4 border-right-gray">
						<?php
							$rating = floor($application->person->getRating());
						?>
							<strong>Rating:</strong> 
							<?php for ($i=1; $i <= $rating; $i++): ?> 
								<span class="glyphicon glyphicon-star gold-star"></span> 
							<?php endfor ?>

							<?php for($i=$rating + 1; $i <= 5; $i++): ?>
								<span class="glyphicon glyphicon-star"></span> 
							<?php endfor; ?>
						</div>
						<div class="col-md-4 border-right-gray">
							<?php $defaultAddress = $application->person->default_address(); ?>
							<strong>Location:</strong> <?php echo $defaultAddress->getShortLocation(); ?>
						</div>
						<div class="col-md-4">
							<?php if($job->project_budget == 'open_to_quotes'): ?>
								Bid: <strong>£<?php echo number_format((double)$application->budget_show, 2); ?></strong>
							<?php else: ?>
								&nbsp;
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach ?>
	<?php else: ?>
		<div class="col-md-12 text-center">
			There are no applicants yet.
		</div>
	<?php endif; ?>
<?php endif; ?>

<div class="modal fade" id="modalDidNotAttend" tabindex="-1" role="dialog" aria-labelledby="modalDidNotAttendLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalDidNotAttendLabel">Interpreter did not attend!</h4>
			</div>

			<div class="modal-body text-center">
				<strong class="person-name-did-not-attend"></strong> did not attend on the job. Please give us your opinion about situation:<br>
				<div class="dna-container">
					<?php echo Form::textarea('did-not-attend-message', '', array('id' => 'did-not-attend-message', 'class' => 'form-control')); ?>
					<div class="text-left text-danger did-not-respond-error hidden"></div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary send-did-not-attend" data-position-id="" data-url="<?php echo url('jobs/did-not-attend') ?>">Send</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalDidNotAttendSuccess" tabindex="-1" role="dialog" aria-labelledby="modalDidNotAttendSuccessLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalDidNotAttendSuccessLabel">Interpreter did not attend!</h4>
			</div>

			<div class="modal-body">
				<p>Dear <strong><?php echo $job->person->getName(); ?></strong>,</p>
				<p>We received your message and would like to apologise for this situation.</p>
				<p>Thank you for your response.</p>
				<p>Lingoing Team</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-close-dna" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalHireLspConfirmation" tabindex="-1" role="dialog" aria-labelledby="modalHireLspConfirmationLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalHireLspConfirmationLabel">Confirmation</h4>
			</div>

			<div class="modal-body text-center">
				Please confirm <strong class="lsp-name"></strong> for <strong><?php echo $job->getTitle() ?>  (<?php $firstDate = $job->firstDate(); echo \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $firstDate->date . ' ' . $firstDate->start_time)->format('d/m/Y H:i') ?>)</strong>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary confirm-lsp" data-person-id="" data-url="<?php echo url('jobs/hire-lsp') ?>/<?php echo $job->id ?>">Confirm</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modalFireLspConfirmation" tabindex="-1" role="dialog" aria-labelledby="modalFireLspConfirmationLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalFireLspConfirmationLabel">Confirmation</h4>
			</div>

			<div class="modal-body">
				<p class="text-center">Do you want to remove <strong class="lsp-fire-name"></strong> from <strong><?php echo $job->getTitle() ?>  (<?php $firstDate = $job->firstDate(); echo \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $firstDate->date . ' ' . $firstDate->start_time)->format('d/m/Y H:i') ?>)</strong></p>
				<p>&nbsp;</p>
				<div class="text-center">
					<div class="text-muted"><small>For security reasons you have to insert your current login password!</small> <span class="required">*</span></div>
					<div class="col-md-6 col-md-offset-3 password-fire-entry">
						<?php echo Form::password('password', array('class' => 'form-control text-center', 'placeholder' => 'Your login password', 'id' => 'password')); ?>
						<div class="text-danger control-error hidden"></div>
					</div>
				</div>
				<p>&nbsp;</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger confirm-fire-lsp" data-position-id="" data-url="<?php echo url('jobs/fire-lsp') ?>/<?php echo $job->id ?>">Remove</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modalRateLSP" tabindex="-1" role="dialog" aria-labelledby="modalRateLSPLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalRateLSPLabel">Rate LSP's work</h4>
			</div>

			<div class="modal-body">
				Please rate <strong class="lsp-name"></strong>'s work.
				<div class="row line stars text-center">
					<span id="star-1" class="glyphicon glyphicon-star" data-star="1"></span>
					<span id="star-2" class="glyphicon glyphicon-star" data-star="2"></span>
					<span id="star-3" class="glyphicon glyphicon-star" data-star="3"></span>
					<span id="star-4" class="glyphicon glyphicon-star" data-star="4"></span>
					<span id="star-5" class="glyphicon glyphicon-star" data-star="5"></span>
				</div>
				<?php echo Form::hidden('stars-value', '0', array('id' => 'stars-value')) ?>
				
				<div class="review-body">
					Review (optional):<br>
					<?php echo Form::textarea('review', '', array('id' => 'review', 'class' => 'form-control')); ?>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary confirm-rate-lsp" data-person-id="" data-url="<?php echo url('jobs/rate-lsp') ?>/<?php echo $job->id ?>">Rate</button>
			</div>
		</div>
	</div>
</div>