<div class="col-md-9 top-space">
	<div class="white-container">
		<p class="lead">Language requirements</p>
		<?php
			$positions = $job->positions;
		?>
		<p>The following language professionals required:</p>
		
		<?php foreach ($positions as $position):
			$sli_languages = $position->searchOptionsAsArray('sli');
			$sli2_languages = $position->searchOptionsAsArray('sli2');
			$lipspeaker_languages = $position->searchOptionsAsArray('lipspeaker');
			$sttr_languages = $position->searchOptionsAsArray('sttr');
			$slt_languages = $position->searchOptionsAsArray('slt');
			$note_taker_languages = $position->searchOptionsAsArray('note_taker');
			$specialisms = $position->searchOptionsAsArray('specialism');
		?>
			<div class="opened_position">
				<?php if(count($specialisms) > 0): ?>
					<span class="text-uppercase"><strong>Specialisms:</strong></span>
					<?php $count = 0; ?> 
					<?php foreach($specialisms as $specialism): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo Person::listSpecialisms($specialism); ?><?php $count++; ?><?php endforeach; ?>
					<br>
				<?php endif; ?>
				<span class="text-uppercase"><strong>Language skills required:</strong></span>
				<ul>
					<?php if($position->spoken_language_interpreter): ?>
						<li><strong>Spoken language interpreter</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($sli_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
					
					<?php if($position->sign_language_interpreter): ?>
						<li><strong>Sign language interpreter</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($sli2_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
					
					<?php if($position->lipspeaker): ?>
						<li><strong>Lipspeaker</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($lipspeaker_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
					
					<?php if($position->speech_to_text_reporter): ?>
						<li><strong>Speech to text reporter</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($sttr_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
					
					<?php if($position->sign_language_translator): ?>
						<li><strong>Sign language translator</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($slt_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
					
					<?php if($position->note_taker): ?>
						<li><strong>Note taker</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($note_taker_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
					
					<?php if($position->number_of_audience == 'one'): ?>
						<li>The interpreter is working only with one person</li>
					<?php else: ?>
						<li>The interpreter is working for a group of <?php echo $position->no_in_groups ?> persons</li>
						<?php if((bool)$position->coworker): ?>
							<li>The interpreter will have a co-worker</li>
						<?php endif; ?>
					<?php endif; ?>
				</ul>
			</div>
		<?php endforeach ?>
	</div>
	
	<?php if(!empty($job->description)): ?>
		<div class="white-container top-space">
			<p class="lead">Job description</p>
			<?php echo nl2br($job->description); ?>
		</div>
	<?php endif; ?>
	
	<?php if(!empty($job->related_bookings)): ?>
		<?php $booking = Job::find($job->related_bookings); ?>
		<div class="white-container top-space">
			<p class="lead">Related booking</p>
			<a href="<?php echo action('JobsController@getView') ?>/<?php echo $booking->id ?>" title="<?php echo $booking->title ?>"><?php echo $booking->title ?></a>
		</div>
	<?php endif; ?>
</div>

<div class="col-md-3 top-space">
	<div class="white-container budget-container">
		<p class="lead budget-label">Budget</p>
		<?php if($job->project_budget == 'open_to_quotes'): ?>
			<h3>Open to quotes</h3>
		<?php else: ?>
			<h3>£<?php echo number_format($job->getBudget(), 2); ?></h3>
			<?php if($job->project_budget == 'fixed_hourly_rate'): ?>
				<small>Fixed hourly rate</small>
			<?php else: ?>
				<small>Fixed budget</small>
			<?php endif; ?>
		<?php endif; ?>
	</div>
	
	<div class="white-container top-space">
		<p class="lead">Job dates</p>
		
		<?php 
			$counter = 0;
			$dates = $job->dates();
		?>
		<?php foreach ($dates as $date): ?>
			<?php if($counter == 3) break; ?>
			
			<div class="date">
				<span class="text-uppercase"><strong>Date:</strong></span> <?php echo date('d M Y', strtotime($date->date)) ?><br>
				<span class="text-uppercase"><strong>Start time:</strong></span> <?php echo date('H:i', strtotime($date->start_time)) ?><br>
				<span class="text-uppercase"><strong>End time:</strong></span> <?php echo date('H:i', strtotime($date->end_time)) ?><br>
			</div>
			
			<?php $counter++; ?>
		<?php endforeach ?>
		
		<div class="duration">
			<span class="text-uppercase"><strong>Duration:</strong></span> <?php echo $job->getDurationInHours(); ?> hours
		</div>
		
		<?php if($job->date_type == 'weekly'): ?>
			<div class="repeat_job">
				This is weekly job and we have <?php echo $job->countChildJobs(); ?> similar jobs waiting for you.
			</div>
		<?php endif; ?>
		
		<div class="view-all-dates text-center">
			<a href="<?php echo url('jobs/view') ?>/<?php echo $job->id ?>/dates">View all job dates</a>
		</div>
	</div>
	
	<div class="white-container top-space">
		<?php if(Auth::check() && (Auth::user()->id == $job->person_id || $job->isUserWorker(Auth::user()->id))): ?>
			<div class="googleMaps" id="mapCanvas" data-address="<?php echo $job->getLocation(); ?>"></div>
			<p class="lead">Job address</p>
		
			<?php echo $job->address_1; ?>
			<?php if(!empty($job->address_2)): ?>
				, <?php echo $job->address_2 ?>
			<?php endif; ?><br>
			<?php if(!empty($job->county)): ?>
				, <?php echo $job->county; ?>
			<?php endif; ?>
			<?php echo $job->city; ?><br>
			<?php echo $job->postcode; ?><br>
			<?php echo Person::listCountries($job->country); ?>
		<?php else: ?>
			<div class="googleMaps" id="mapCanvas" data-address="<?php echo $job->getShortLocation(); ?>"></div>
			<p class="lead">Job address</p>
		
			<?php echo $job->city; ?><br>
			<?php echo $job->postcode; ?><br>
			<?php echo Person::listCountries($job->country); ?>
		<?php endif; ?>
	</div>
</div>