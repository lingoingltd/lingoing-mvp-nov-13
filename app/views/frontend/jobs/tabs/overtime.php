<?php
$overtimes_requests = null;
$overtimes_approve = null;

if(Auth::user()->id == $job->person_id)
{
	$overtimes_requests = $job->overtimes()
		->where('status', '=', 'open')
		->orderBy('created_at', 'DESC')
		->get();
	
	$overtimes_approve = $job->overtimes()
		->where('status', '=', 'approved')
		->orderBy('created_at', 'DESC')
		->get();
}
else
{
	$overtimes_requests = $job->overtimes()
		->where('person_id', '=', Auth::user()->id)
		->where('status', '=', 'open')
		->orderBy('created_at', 'DESC')
		->get();
		
	$overtimes_approve = $job->overtimes()
		->where('person_id', '=', Auth::user()->id)
		->where('status', '=', 'approved')
		->orderBy('created_at', 'DESC')
		->get();
}
?>

<?php if($job->status == 'confirmed' || $job->status == 'finished'): ?>
	<div class="col-md-12 apply-for-overtime-container">
		<button class="btn btn-primary" data-toggle="modal" data-target="#requestOvertime">Apply for overtime</button> <small>Your request will be sent to the client for approval</small>
	</div>
<?php endif; ?>

<?php if($job->project_budget == 'fixed_hourly_rate'): ?>
	<div class="col-md-12 overtime-rate-text">
		<strong>IMPORTANT NOTICE:</strong> Overtime will be charged at agreed rate <strong>£<?php echo number_format($job->person_id == Auth::user()->id ? $job->hourly_rate : $job->hourly_rate_lsp_only, 2); ?>/hr</strong>.
	</div>
<?php endif; ?>

<div class="col-md-12 overtime-container-text">
	<span class="text-uppercase">
		<strong>Overtime requests</strong>
	</span>
</div>

<?php if($overtimes_requests->count() > 0): ?>
	<?php foreach ($overtimes_requests as $overtime): ?>
		<div class="overtime-container border-all clearfix">
			<div class="overtime-picture">
				<img src="<?php echo $overtime->person->getProfilePicture() ?>" class="img-thumbnail">
			</div>
			<p class="overtime-name">
				<a href="#" title="<?php echo $overtime->person->getName() ?>"><?php echo $overtime->person->getName() ?></a> has requested of <strong><?php 
					if(!empty($overtime->minutes) && $overtime->minutes != 0.0 && !empty($overtime->hours) && $overtime->hours != 0.0)
						echo $overtime->hours . ' hours and ' . ($overtime->minutes * 60) . ' minutes';
					elseif(!empty($overtime->minutes) && $overtime->minutes != 0.0)
						echo ($overtime->minutes * 60) . ' minutes';
					elseif(!empty($overtime->hours) && $overtime->hours != 0.0)
						echo $overtime->hours . ' hours';
				?></strong> overtime for this job - <strong>£<?php echo number_format(($overtime->hours + $overtime->minutes) * $overtime->hourly_rate, 2); ?></strong>. <small><?php echo $overtime->showTimePassedCreated() ?></small>
			</p>
			<?php if(Auth::user()->id == $job->person_id): ?>
				<div class="pull-right">
					<button class="btn btn-default reject-overtime" data-url="<?php echo url('jobs/reject-overtime') ?>/<?php echo $job->id ?>" data-overtime-id="<?php echo $overtime->id ?>">Reject Request</button>
					<button class="btn btn-primary approve-overtime" data-url="<?php echo url('jobs/approve-overtime') ?>/<?php echo $job->id ?>" data-overtime-id="<?php echo $overtime->id ?>">Approve Request</button>
				</div>
			<?php endif; ?>
		</div>
	<?php endforeach ?>
<?php else: ?>
	<p class="text-center">You don't have any open overtime requests</p>
<?php endif; ?>

<?php if($overtimes_approve->count() > 0): ?>
	<div class="col-md-12 overtime-container-text">
		<span class="text-uppercase">
			<strong>Approved overtime</strong>
		</span>
	</div>
	
	<?php foreach ($overtimes_approve as $overtime): ?>
		<div class="overtime-container border-all clearfix">
			<div class="overtime-picture">
				<img src="<?php echo $overtime->person->getProfilePicture() ?>" class="img-thumbnail">
			</div>
			<p class="overtime-name">
				<a href="<?php echo url('profile') ?>/<?php echo $overtime->person->id ?>" title="<?php echo $overtime->person->getName() ?>"><?php echo (Auth::user()->id != $job->person_id ? 'You' : $overtime->person->getName()) ?></a> had overtime of <strong><?php 
					if(!empty($overtime->minutes) && $overtime->minutes != 0.0 && !empty($overtime->hours) && $overtime->hours != 0.0)
						echo $overtime->hours . ' hours and ' . ($overtime->minutes * 60) . ' minutes';
					elseif(!empty($overtime->minutes) && $overtime->minutes != 0.0)
						echo ($overtime->minutes * 60) . ' minutes';
					elseif(!empty($overtime->hours) && $overtime->hours != 0.0)
						echo $overtime->hours . ' hours';
				?></strong> approved for this job - <strong>£<?php echo number_format(($overtime->hours + $overtime->minutes) * $overtime->hourly_rate, 2); ?></strong>. <small><?php echo $overtime->showTimePassedUpdated() ?></small>
			</p>
		</div>
	<?php endforeach ?>
<?php endif; ?>

<div class="modal fade" id="requestOvertime" tabindex="-1" role="dialog" aria-labelledby="requestOvertimeSuccess" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="sendMessageSuccess">Request overtime for <?php echo $job->getTitle() ?></h4>
			</div>

			<div class="modal-body clearfix">
				<div>
					<label>Enter the amount of overtime you are requesting:</label>
				</div>
				
				<form class="form-horizontal" role="form">
					<div id="hours-group" class="form-group">
						<label for="hours" class="col-md-2 control-label">hours</label>
						<div class="col-md-3">
							<?php echo Form::text('hours', Input::old('hours'), array('class' => 'form-control', 'placeholder' => '', 'id' => 'hours')); ?>
						</div>
						<div class="col-md-7 hidden text-danger">
						</div>
					</div>
					<div id="minutes-group" class="form-group">
						<label for="minutes" class="col-md-2 control-label">minutes</label>
						<div class="col-md-3">
							<?php echo Form::select('minutes', array('' => '', '0.25' => '15 minutes', '0.5' => '30 minutes', '0.75' => '45 minutes'), Input::old('minutes'), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => 'minutes')); ?>
						</div>
						<div class="col-md-7 hidden text-danger">
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary request-overtime" data-url="<?php echo url('jobs/request-overtime') ?>/<?php echo $job->id ?>">Send request</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->