<?php
	$attachments = $job->attachments();
?>
<div class="dates-container">
	<?php if (count($attachments) > 0): ?>
		<?php foreach ($attachments as $attachment): ?>
			<p>
				<?php if(!empty($attachment->description)): ?>
					<?php echo $attachment->description ?><br>
				<?php endif; ?>
				<a href="/uploads_jobs/<?php echo $job->id ?>/<?php echo $attachment->filename; ?>" target="_blank"><?php echo $attachment->filename; ?></a>
			</p>
		<?php endforeach ?>
	<?php else: ?>
		<p class="text-center">No attachments available</p>
	<?php endif; ?>
</div>