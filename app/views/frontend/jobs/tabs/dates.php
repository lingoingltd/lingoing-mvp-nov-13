<?php
	$dates = $job->dates();
?>
<div class="dates-container">
	<div class="clearfix review-dates-header">
		<div class="col-md-3">
			<h5>Day</h5>
		</div>
		<div class="col-md-3">
			<h5>Date</h5>
		</div>
		<div class="col-md-3">
			<h5>Start time</h5>
		</div>
		<div class="col-md-3">
			<h5>End time</h5>
		</div>
	</div>
	
	<?php $counter = 0; ?>
	<?php foreach ($dates as $date): ?>
		<div class="review-date-row clearfix <?php echo ($counter % 2 == 0) ? 'gray-background' : ''; ?>">
			<div class="clearfix">
				<div class="col-md-3">
					<?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $date->date)->format('l'); ?>
				</div>
				<div class="col-md-3">
					<?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $date->date)->format('d/m/Y'); ?>
				</div>
				<div class="col-md-3">
					<?php echo Carbon\Carbon::createFromFormat('H:i:s', $date->start_time)->format('H:i'); ?>
				</div>
				<div class="col-md-3">
					<?php echo Carbon\Carbon::createFromFormat('H:i:s', $date->end_time)->format('H:i'); ?>
				</div>
			</div>
			<?php 
				$attachments = $date->attachments();
				
				$show_notes_attachments = false;
				if(count($attachments) > 0 || !empty($date->notes))
					$show_notes_attachments = (Auth::check() && ($job->isUserWorker(Auth::user()->id) || $job->person_id == Auth::user()->id));
			?>
			<div class="notes-attachments-container clearfix <?php if(!$show_notes_attachments): ?>hidden<?php endif; ?>">
				<div class="col-md-7">
					<p><strong>Notes</strong></p>
					<?php if(empty($date->notes)): ?>
						<small><em>No notes</em></small>
					<?php else: ?>
						<p><?php echo nl2br($date->notes); ?></p>
					<?php endif; ?>
				</div>
				<div class="col-md-5">
					<p><strong>Attachments</strong></p>
					<?php if(count($attachments) > 0): ?>
						<?php foreach ($attachments as $attachment): ?>
							<p><a href="/uploads_jobs/<?php echo $job->id ?>/dates/<?php echo $date->id ?>/<?php echo $attachment->filename; ?>" target="_blank"><?php echo $attachment->filename; ?></a></p>
						<?php endforeach ?>
					<?php else: ?>
						<small><em>No attachments</em></small>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php $counter++; ?>
	<?php endforeach; ?>
	
	<?php $childJobs = $job->childJobs(); ?>
	<?php if(count($childJobs) > 0): ?>
		<h5 style="margin: 15px 0;">Related bookings <small class="text-muted">We split your dates in separate jobs</small></h5>

		<?php $counter = 0; ?>
		<?php foreach ($childJobs as $childJob): ?>
			<div class="<?php echo ($counter % 2 == 0) ? 'gray-background' : ''; ?>">
				<div class="clearfix col-md-12 related-booking-title">
					<h4><a href="<?php echo url('jobs/view') ?>/<?php echo $childJob->id ?>"><?php echo $childJob->getTitle(); ?></a></h4>
				</div>

				<?php $dates = $childJob->dates(); ?>
				<?php foreach ($dates as $date): ?>
					<div class="review-date-row clearfix <?php echo ($counter % 2 == 0) ? 'gray-background' : ''; ?>">
						<div class="clearfix">
							<div class="col-md-3">
								<?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $date->date)->format('l'); ?>
							</div>
							<div class="col-md-3">
								<?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $date->date)->format('d/m/Y'); ?>
							</div>
							<div class="col-md-3">
								<?php echo Carbon\Carbon::createFromFormat('H:i:s', $date->start_time)->format('H:i'); ?>
							</div>
							<div class="col-md-3">
								<?php echo Carbon\Carbon::createFromFormat('H:i:s', $date->end_time)->format('H:i'); ?>
							</div>
						</div>
						<?php 
							$attachments = $date->attachments(); 

							$show_notes_attachments = false;
							if(count($attachments) > 0 || !empty($date->notes))
								$show_notes_attachments = (Auth::check() && ($job->isUserWorker(Auth::user()->id) || $childJob->person_id == Auth::user()->id));
						?>
						<div class="notes-attachments-container clearfix <?php if(!$show_notes_attachments): ?>hidden<?php endif; ?>">
							<div class="col-md-7">
								<p><strong>Notes</strong></p>
								<?php if(empty($date->notes)): ?>
									<small><em>No notes</em></small>
								<?php else: ?>
									<p><?php echo nl2br($date->notes); ?></p>
								<?php endif; ?>
							</div>
							<div class="col-md-5">
								<p><strong>Attachments</strong></p>
								<?php if(count($attachments) > 0): ?>
									<?php foreach ($attachments as $attachment): ?>
										<p><a href="/uploads_jobs/<?php echo $job->id ?>/dates/<?php echo $date->id ?>/<?php echo $attachment->filename; ?>"><?php echo $attachment->filename; ?></a></p>
									<?php endforeach ?>
								<?php else: ?>
									<small><em>No attachments</em></small>
								<?php endif; ?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>

			<?php $counter++; ?>
		<?php endforeach ?>
	<?php endif; ?>
</div>