<?php $messages = $job->messages()->orderBy('created_at', 'DESC')->get(); ?>
<?php if(count($messages) > 0): ?>
	<p class="send-message-button-placeholder text-right"><button class="btn btn-primary" data-toggle="modal" data-target="#sendMessage">Send message</button></p>
	<?php foreach($messages as $message): ?>
		<div class="col-md-12 message-container border-all">
	
			<div class="col-md-12 message-header">
				<p class="lead"><?php echo $message->person->getName(); ?></p>
				<p><small><?php echo date('d. M Y H:i', strtotime($job->created_at)) ?></small></p>
			</div>
			
			<div class="col-md-12 message">
				<?php echo nl2br(strip_tags($message->message)); ?>
			</div>
		</div>
	<?php endforeach; ?>
<?php else: ?>
	<p class="text-center top-space">There are no messages yet.</p>
	<p class="text-center"><button class="btn btn-primary" data-toggle="modal" data-target="#sendMessage">Send message</button></p>
<?php endif; ?>

<div class="modal fade" id="sendMessage" tabindex="-1" role="dialog" aria-labelledby="sendMessageSuccess" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="sendMessageSuccess">Send message</h4>
			</div>

			<div class="modal-body">
				Message:
				<?php echo Form::textarea('message', Input::old('message'), array('class' => 'form-control', 'id' => 'message')); ?>
				<span class="text-danger hidden"></span>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary send-message" data-url="<?php echo url('jobs/send-message') ?>/<?php echo $job->id ?>">Send message</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->