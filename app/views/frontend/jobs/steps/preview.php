<?php echo Form::open(array('action' => 'JobsController@postPreviewJob', 'method' => 'post')); ?>

<h1>Post a new job</h1>

<div class="yellow_container">
	<p class="lead text-center">The below is a preview of your Job Posting. Please ensure you check all the details below are correct before posting.</p>
	
	<?php $firstDate = $job->firstDate(); ?>
	<p class="text-center"><strong>Note:</strong> If you don't hire any Language professional by the <?php echo date('d/m/Y', strtotime($firstDate->date)) . ' ' . date('H:i', strtotime($firstDate->start_time)); ?>, your job posting will be automatically canceled.</p>
	
	<p class="text-center">
		<a class="btn btn-default" href="<?php echo action('JobsController@getStep5') ?>">&lt; Previous</a>
		<button class="btn btn-primary" role="button" type="submit" name="next" title="Confirm and post a job">Confirm and post a job</button>
	</p>
</div>

<h1><?php echo $job->getTitle() ?></h1>
<div class="col-md-9">
	<div class="white-container">
		<p class="lead">Language requirements</p>
		<?php
			$positions = $job->positions;
		?>
		<p>The following language professionals required:</p>
		
		<?php foreach ($positions as $position):
			$sli_languages = $position->searchOptionsAsArray('sli');
			$sli2_languages = $position->searchOptionsAsArray('sli2');
			$lipspeaker_languages = $position->searchOptionsAsArray('lipspeaker');
			$sttr_languages = $position->searchOptionsAsArray('sttr');
			$slt_languages = $position->searchOptionsAsArray('slt');
			$note_taker_languages = $position->searchOptionsAsArray('note_taker');
			$specialisms = $position->searchOptionsAsArray('specialism');
		?>
			<div class="opened_position">
				<?php if(count($specialisms) > 0): ?>
					<span class="text-uppercase"><strong>Specialisms:</strong></span> 
					<?php $count = 0; ?> 
					<?php foreach($specialisms as $specialism): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo Person::listSpecialisms($specialism); ?><?php $count++; ?><?php endforeach; ?>
					<br>
				<?php endif; ?>
				<span class="text-uppercase"><strong>Language skills required:</strong></span>
				<ul>
					<?php if($position->spoken_language_interpreter): ?>
						<li><strong>Spoken language interpreter</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($sli_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
					
					<?php if($position->sign_language_interpreter): ?>
						<li><strong>Sign language interpreter</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($sli2_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
					
					<?php if($position->lipspeaker): ?>
						<li><strong>Lipspeaker</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($lipspeaker_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
					
					<?php if($position->speech_to_text_reporter): ?>
						<li><strong>Speech to text reporter</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($sttr_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
					
					<?php if($position->sign_language_translator): ?>
						<li><strong>Sign language translator</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($slt_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
					
					<?php if($position->note_taker): ?>
						<li><strong>Note taker</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($note_taker_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
					
					<?php if($position->number_of_audience == 'one'): ?>
						<li>The interpreter is working only with one person</li>
					<?php else: ?>
						<li>The interpreter is working for a group of <?php echo $position->no_in_groups ?> persons</li>
						<?php if((bool)$position->coworker): ?>
							<li>The interpreter will have a co-worker</li>
						<?php endif; ?>
					<?php endif; ?>
				</ul>
			</div>
		<?php endforeach ?>
	</div>
	
	<?php if(!empty($job->description)): ?>
		<div class="white-container top-space">
			<p class="lead">Job description</p>
			<?php echo nl2br($job->description); ?>
		</div>
	<?php endif; ?>
	
	<div class="white-container top-space">
		<p class="lead">Job dates</p>
		
		<?php $dates = $job->dates(); ?>
		<div class="clearfix review-dates-header">
			<div class="col-md-3">
				<h5>Day</h5>
			</div>
			<div class="col-md-3">
				<h5>Date</h5>
			</div>
			<div class="col-md-2">
				<h5>Start time</h5>
			</div>
			<div class="col-md-2">
				<h5>End time</h5>
			</div>
			<div class="col-md-2">
				<h5>Duration</h5>
			</div>
		</div>
		
		<?php $counter = 0; ?>
		<?php foreach ($dates as $date): ?>
			<div class="preview-date <?php echo ($counter % 2 == 0) ? 'gray-background' : ''; ?>">
				<div class="clearfix date">
					<div class="col-md-3">
						<?php echo date('l', strtotime($date->date)); ?>
					</div>
					<div class="col-md-3">
						<?php echo date('d/m/Y', strtotime($date->date)); ?>
					</div>
					<div class="col-md-2">
						<?php echo date('H:i', strtotime($date->start_time)); ?>
					</div>
					<div class="col-md-2">
						<?php echo date('H:i', strtotime($date->end_time)); ?>
					</div>
					<div class="col-md-2">
						<?php echo $job->getDurationInHours(); ?> hours
					</div>
				</div>
				<?php $attachments = $date->attachments(); ?>
				<?php if(count($attachments) > 0 || !empty($date->notes)): ?>
					<div class="clearfix note-attachment-container">
						<div class="col-md-7">
							<p><strong>Notes</strong></p>
							<?php if(empty($date->notes)): ?>
								<small><em>No notes</em></small>
							<?php else: ?>
								<p><?php echo nl2br($date->notes); ?></p>
							<?php endif; ?>
						</div>
						<div class="col-md-5">
							<p><strong>Attachments</strong></p>
							<?php if(count($attachments) > 0): ?>
								<?php foreach ($attachments as $attachment): ?>
									<p><a href="/uploads_jobs/<?php echo $job->id ?>/dates/<?php echo $date->id ?>/<?php echo $attachment->filename; ?>"><?php echo $attachment->filename; ?></a></p>
								<?php endforeach ?>
							<?php else: ?>
								<small><em>No attachments</em></small>
							<?php endif; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<?php $counter++; ?>
		<?php endforeach ?>
		
		<?php $childJobs = $job->childJobs(); ?>
		<?php if(count($childJobs) > 0): ?>
			<h5 style="margin: 15px 0;">Related bookings <small class="text-muted">We split your dates in separate jobs</small></h5>
			
			<?php $counter = 0; ?>
			<?php foreach ($childJobs as $childJob): ?>
				<div class="<?php echo ($counter % 2 == 0) ? 'gray-background' : ''; ?>">
					<?php $dates = $childJob->dates(); ?>
					<?php foreach ($dates as $date): ?>
						<div class="review-date-row clearfix <?php echo ($counter % 2 == 0) ? 'gray-background' : ''; ?>">
							<div class="clearfix">
								<div class="col-md-3">
									<?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $date->date)->format('l'); ?>
								</div>
								<div class="col-md-3">
									<?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $date->date)->format('d/m/Y'); ?>
								</div>
								<div class="col-md-2">
									<?php echo Carbon\Carbon::createFromFormat('H:i:s', $date->start_time)->format('H:i'); ?>
								</div>
								<div class="col-md-2">
									<?php echo Carbon\Carbon::createFromFormat('H:i:s', $date->end_time)->format('H:i'); ?>
								</div>
								<div class="col-md-2">
									<?php echo $childJob->getDurationInHours(); ?> hours
								</div>
							</div>
							<?php $attachments = $date->attachments(); ?>
							<div class="notes-attachments-container clearfix <?php if(count($attachments) == 0 && empty($date->notes)): ?>hidden<?php endif; ?>">
								<div class="col-md-7">
									<p><strong>Notes</strong></p>
									<?php if(empty($date->notes)): ?>
										<small><em>No notes</em></small>
									<?php else: ?>
										<p><?php echo nl2br($date->notes); ?></p>
									<?php endif; ?>
								</div>
								<div class="col-md-5">
									<p><strong>Attachments</strong></p>
									<?php if(count($attachments) > 0): ?>
										<?php foreach ($attachments as $attachment): ?>
											<p><a href="/uploads_jobs/<?php echo $childJob->id ?>/dates/<?php echo $date->id ?>/<?php echo $attachment->filename; ?>"><?php echo $attachment->filename; ?></a></p>
										<?php endforeach ?>
									<?php else: ?>
										<small><em>No attachments</em></small>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>

				<?php $counter++; ?>
			<?php endforeach ?>
		<?php endif; ?>
	</div>
	
	<?php $attachments = $job->attachments(); ?>
	<?php if(count($attachments) > 0): ?>
		<div class="white-container top-space">
			<p class="lead">Attachments</p>
			<p>
				<?php foreach ($attachments as $attachment): ?>
					<?php if(!empty($attachment->description)): ?>
						<?php echo $attachment->description ?><br>
					<?php endif; ?>
					<a href="/uploads_jobs/<?php echo $job->id ?>/<?php echo $attachment->filename; ?>"><?php echo $attachment->filename; ?></a>
				<?php endforeach ?>
			</p>
		</div>
	<?php endif; ?>
	
	<?php if(!empty($job->related_bookings)): ?>
		<?php $booking = Job::find($job->related_bookings); ?>
		<div class="white-container top-space">
			<p class="lead">Related booking</p>
			<a href="#" title="<?php echo $booking->title ?>"><?php echo $booking->title ?></a>
		</div>
	<?php endif; ?>
</div>

<div class="col-md-3">
	<div class="white-container budget-container">
		<p class="lead budget-label">Budget</p>
		<?php if($job->project_budget == 'open_to_quotes'): ?>
			<h3>Open to quotes</h3>
		<?php else: ?>
			<h3>£<?php echo number_format($job->budget, 2); ?></h3>
			<?php if($job->project_budget == 'fixed_hourly_rate'): ?>
				<small>Fixed hourly rate</small>
			<?php else: ?>
				<small>Fixed budget</small>
			<?php endif; ?>
		<?php endif; ?>
	</div>	
	
	<div class="white-container top-space">
		<div class="googleMaps" id="mapCanvas" data-address="<?php echo $job->getLocation(); ?>"></div>
		<p class="lead">Job address</p>
		
		<?php echo $job->address_1; ?>
		<?php if(!empty($job->address_2)): ?>
			, <?php echo $job->address_2 ?>
		<?php endif; ?><br>
		<?php if(!empty($job->county)): ?>
			, <?php echo $job->county; ?>
		<?php endif; ?>
		<?php echo $job->city; ?><br>
		<?php echo $job->postcode; ?><br>
		<?php echo Person::listCountries($job->country); ?>
	</div>
</div>

<?php echo Form::close(); ?>