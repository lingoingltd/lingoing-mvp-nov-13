<?php echo Form::open(array('action' => 'JobsController@postStep2', 'method' => 'post')); ?>

<h3>Enter your language requirements</h3>

<?php
	$field = 'no_of_professionals';
	$field_errors = $errors->get($field);
?>
<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
	<div class="col-md-8">
		<?php echo Form::label($field, 'How many language professionals do you require?', array('class' => 'control-label')) ?> <span class="required">*</span>
		<?php echo Form::select($field, array('' => '', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10'), Input::old($field, empty($positions) ? '' : count($positions)), array('class' => 'form-control no_of_professionals', 'placeholder' => 'Please select ...', 'id' => $field)); ?>
	</div>
	
	<?php if(count($field_errors) > 0): ?>
		<div class="col-md-4">
			<?php foreach ($errors->get($field) as $message): ?>
				<div class="text-left text-danger"><?php echo $message; ?></div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>

<div class="language_requirements">
	<?php 
	$visible = null;
	if(Input::old('no_of_professionals') != null)
		$visible = (int)Input::old('no_of_professionals');
	elseif(count($positions) > 0)
		$visible = count($positions);
		
	$i = 10;
	for ($i=1; $i <= 10; $i++): 
		$position = null;
		if(count($positions) >= $i)
			$position = $positions[$i - 1];
	?>
		
	<?php echo View::make('frontend.jobs.steps.step2_partial', array(
		'index' => $i,
		'visible' => $visible,
		'position' => $position
	)) ?>
		
	<?php endfor; ?>
</div>

<div class="row line">
	<div class="col-md-12">
		<div class="pull-left">
			<a class="btn btn-default" href="<?php echo action('JobsController@getStep1') ?>">&lt; Previous</a>
		</div>
		<div class="pull-right">
			<small>You'll enter the job address on the next page</small>
			<button class="btn btn-primary" role="button" type="submit" name="next" title="Next">Next &gt;</button>
		</div>
	</div>
</div>

<?php echo Form::close(); ?>