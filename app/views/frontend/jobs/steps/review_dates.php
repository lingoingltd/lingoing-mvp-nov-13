<?php echo Form::open(array('action' => 'JobsController@postReviewAllDates', 'method' => 'post')); ?>

<h3>Review dates</h3>

<div class="clearfix review-dates-header">
	<div class="col-md-2">
		<h5>Day</h5>
	</div>
	<div class="col-md-2">
		<h5>Date</h5>
	</div>
	<div class="col-md-2">
		<h5>Start time</h5>
	</div>
	<div class="col-md-2">
		<h5>End time</h5>
	</div>
	<div class="col-md-4">
		<h5>&nbsp;</h5>
	</div>
</div>

<?php $counter = 0; ?>
<?php foreach ($dates as $date): ?>
	<div class="review-date-row clearfix <?php echo ($counter % 2 == 0) ? 'gray-background' : ''; ?>">
		<div class="clearfix">
			<?php 
				$field = 'date_' . $date->id;
				$field_errors = $errors->get($field);
			?>
			<div class="col-md-2 day">
				<?php echo \Carbon\Carbon::createFromFormat('d/m/Y', Input::old($field, isset($date) ? \Carbon\Carbon::createFromFormat('Y-m-d', $date->date)->format('d/m/Y') : null))->format('l'); ?>
			</div>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, isset($date) ? \Carbon\Carbon::createFromFormat('Y-m-d', $date->date)->format('d/m/Y') : ''), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)); ?>
		
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
	
			<?php 
				$field = 'start_time_' . $date->id;
				$field_errors = $errors->get($field);
			?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, isset($date) ? Carbon\Carbon::createFromFormat('H:i:s', $date->start_time)->format('H:i') : ''), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
	
			<?php 
				$field = 'end_time_' . $date->id;
				$field_errors = $errors->get($field);
			?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, isset($date) ? Carbon\Carbon::createFromFormat('H:i:s', $date->end_time)->format('H:i') : ''), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<div class="col-md-4 review-dates-buttons">
				<a href="#" class="btn btn-sm btn-link remove-date" data-id="<?php echo $date->id ?>">Remove date</a> <a href="#" class="btn btn-sm btn-link add-notes-attachments" data-id="<?php echo $date->id ?>">Show notes or attachments</a>
			</div>
		</div>
		<?php $attachments = $date->attachments(); ?>
		<div class="notes-attachments-container clearfix <?php if(count($attachments) == 0 && (!isset($date) || empty($date->notes))): ?>hidden<?php endif; ?>">
			<div class="col-md-7">
				<?php $field = 'notes_' . $date->id; ?>
				<?php echo Form::textarea($field, Input::old($field, isset($date) ? $date->notes : ''), array('class' => 'form-control time-helper', 'placeholder' => 'Any additional notes', 'id' => $field)) ?>
			</div>
			<div class="col-md-5">
				<strong>Attachments</strong>
				<?php $field = 'attachment'; ?>
				<p><?php echo Form::file($field, array('class' => 'upload-date-attachment', 'data-url' => '/jobs/upload-job-attachment/job_date/' . $date->id . '/' . $job->id)); ?></p>
				<p class="hidden"><img src="/img/ajax-loader.gif"> Uploading, please wait ...</p>
				<div class="attachments">
					<?php if(count($attachments) > 0): ?>
						<?php echo View::make('frontend.jobs.steps.review_dates_partial_attachments_list', array(
							'attachments' => $attachments,
							'date' => $date,
							'job' => $job,
							'target' => 'job_date'
						)) ?>
					<?php else: ?>
						<div class="text-center">No attachments uploaded yet</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php $counter++; ?>
<?php endforeach; ?>


<div class="row line button-line">
	<div class="col-md-12">
		<div class="pull-left">
			<a class="btn btn-default" href="<?php echo action('JobsController@getStep4') ?>">&lt; Previous</a>
		</div>
		<div class="pull-right">
			<small class="short-description">You'll see a preview of your job posting on the next page</small>
			<button class="btn btn-primary btn-date-next" role="button" type="submit" name="next">Next &gt;</button>
		</div>
	</div>
</div>

<?php echo Form::close(); ?>