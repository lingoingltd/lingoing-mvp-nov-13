<?php echo Form::open(array('action' => 'JobsController@postStep3', 'method' => 'post')); ?>

<h3>Enter the address of the job</h3>

<div class="row">
	<div class="col-md-12">
		<?php $job_address_type = Input::old('job_address_type', Session::get('job_address_type')); ?>
		<?php echo Form::radio('job_address_type', 'existing', empty($job_address_type) || $job_address_type == 'existing', array('id' => 'job_address_type_existing')) ?> <?php echo Form::label('job_address_type_existing', 'Use existing address', array('class' => 'control-label')) ?>
	</div>
</div>

<div class="row line" id="job_address_type_existing_container">
	<?php 
		$field = 'existing_address';
		$field_errors = $errors->get($field);
	?>
	<div class="col-md-8 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<?php echo Form::select($field, array('' => '') + Auth::user()->addressesAsArray(), Input::old($field, Session::get('existing_address_id')), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
	</div>
	
	<?php if(count($field_errors) > 0): ?>
		<div class="col-md-4">
			<?php foreach ($errors->get($field) as $message): ?>
				<div class="text-left text-danger"><?php echo $message; ?></div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>

<div class="row">
	<div class="col-md-12">
		<?php echo Form::radio('job_address_type', 'new', $job_address_type == 'new', array('id' => 'job_address_type_new')) ?> <?php echo Form::label('job_address_type_new', 'Add new address', array('class' => 'control-label')) ?>
	</div>
</div>

<div class="row line hidden" id="job_address_type_new_container">
	<div class="col-md-12">
		<?php
			$field = 'address_1';
			$field_errors = $errors->get($field);
		?>
		<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<div class="col-md-8">		
				<?php echo Form::label($field, 'Address 1', array('class' => 'control-label')) ?> <span class="required">*</span><br>
				<?php echo Form::text($field, Input::old($field, $job->address_1), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
			</div>
	
			<?php if(count($field_errors) > 0): ?>
				<div class="col-md-4">
					<?php foreach ($errors->get($field) as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>

		<?php
			$field = 'address_2';
			$field_errors = $errors->get($field);
		?>
		<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<div class="col-md-8">
				<?php echo Form::label($field, 'Address 2', array('class' => 'control-label')) ?><br>
				<?php echo Form::text($field, Input::old($field, $job->address_2), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
			</div>
	
			<?php if(count($field_errors) > 0): ?>
				<div class="col-md-4">
					<?php foreach ($errors->get($field) as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>

		<?php
			$field = 'city';
			$field_errors = $errors->get($field);
		?>
		<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<div class="col-md-8">
				<?php echo Form::label($field, 'Town/City', array('class' => 'control-label')) ?> <span class="required">*</span><br>
				<?php echo Form::text($field, Input::old($field, $job->city), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
			</div>
	
			<?php if(count($field_errors) > 0): ?>
				<div class="col-md-4">
					<?php foreach ($errors->get($field) as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>

		<?php
			$field = 'country';
			$field_errors = $errors->get($field);
		?>
		<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<div class="col-md-8">
				<?php echo Form::label($field, 'Country', array('class' => 'control-label')) ?> <span class="required">*</span><br>
				<?php echo Form::select($field, array('' => '') + Person::listCountries(), Input::old($field, $job->country), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
			</div>
	
			<?php if(count($field_errors) > 0): ?>
				<div class="col-md-4">
					<?php foreach ($errors->get($field) as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>

		<?php
			$field = 'county';
			$field_errors = $errors->get($field);
		?>
		<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<div class="col-md-8">
				<?php echo Form::label($field, 'County', array('class' => 'control-label')) ?><br>
				<?php echo Form::select($field, array('' => '') + Person::listCounties(), Input::old($field, $job->county), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
			</div>
	
			<?php if(count($field_errors) > 0): ?>
				<div class="col-md-4">
					<?php foreach ($errors->get($field) as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>

		<?php
			$field = 'postcode';
			$field_errors = $errors->get($field);
		?>
		<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<div class="col-md-8">
				<?php echo Form::label($field, 'Postcode', array('class' => 'control-label')) ?> <span class="required">*</span><br>
				<?php echo Form::text($field, Input::old($field, $job->postcode), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
			</div>
	
			<?php if(count($field_errors) > 0): ?>
				<div class="col-md-4">
					<?php foreach ($errors->get($field) as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<div>&nbsp;</div>

<div class="row line">
	<div class="col-md-12">
		<div class="pull-left">
			<a class="btn btn-default" href="<?php echo action('JobsController@getStep2') ?>">&lt; Previous</a>
		</div>
		<div class="pull-right">
			<small>You'll enter job deadlines on the next page</small>
			<button class="btn btn-primary" role="button" type="submit" name="next" title="Next">Next &gt;</button>
		</div>
	</div>
</div>

<?php echo Form::close(); ?>