<?php if(count($attachments) > 0): ?>
	<?php if($target == 'job_date'): ?>
		<?php foreach ($attachments as $attachment): ?>
			<p><a href="/uploads_jobs/<?php echo $job->id ?>/dates/<?php echo $date->id ?>/<?php echo $attachment->filename; ?>" target="_blank"><?php echo $attachment->filename; ?></a> <a href="#" class="remove-attachment" data-id="<?php echo $attachment->id ?>" data-name="<?php echo $attachment->filename ?>">Remove</a></p>
		<?php endforeach ?>
	<?php else: ?>
		<?php foreach ($attachments as $attachment): ?>
			<p><a href="/uploads_jobs/<?php echo $job->id ?>/<?php echo $attachment->filename; ?>" target="_blank"><?php echo $attachment->filename; ?></a> <a href="#" class="remove-attachment" data-id="<?php echo $attachment->id ?>" data-name="<?php echo $attachment->filename ?>">Remove</a></p>
		<?php endforeach ?>
	<?php endif; ?>
<?php else: ?>
	<p class="text-center">No attachments uploaded yet</p>
<?php endif; ?>