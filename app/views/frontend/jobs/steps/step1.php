<?php echo Form::open(array('action' => 'JobsController@postStep1', 'method' => 'post')); ?>
	
	<?php
		$field = 'type_of_job';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Job type', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => '') + Job::listTypes(), Input::old($field, empty($job) ? '' : $job->type), array('class' => 'form-control', 'placeholder' => 'Please select ...', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	<?php
		$field = 'type_other';
		$field_errors = $errors->get($field);
	?>
	<div id="type_other_container" class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> <?php if(Input::old('type_of_job') != 'other'): ?>hidden<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::text($field, Input::old($field, empty($job) ? '' : $job->type_other), array('class' => 'form-control', 'placeholder' => 'Please specify ...', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'job_description';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Description of Work - additional notes', array('class' => 'control-label')) ?> <br>
			<?php echo Form::textarea($field, Input::old($field, empty($job) ? '' : $job->description), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'payment_method';
		$field_errors = $errors->get($field);
		
		$payment_methods = Job::listPaymentMethods();
		if(Auth::user()->how_to_pay_bills == 'paying_card')
			unset($payment_methods['atw_customer']);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Which payment do you prefer?', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => '') + $payment_methods, Input::old($field, empty($job) ? '' : $job->payment_method), array('class' => 'form-control', 'placeholder' => 'Please select ...', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<div class="atw_customer <?php if(Input::old('payment_method') != 'atw_customer' || empty(Input::old('payment_method'))): ?>hidden<?php endif; ?>">
		
		<?php
			$field = 'support_package';
			$field_errors = $errors->get($field);
			
			$support_packages = Auth::user()->atwVerifiedPackagesAsArray();
		?>
		<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<div class="col-md-8">
				<?php echo Form::label($field, 'Which Access to Work support package will you use for this job?', array('class' => 'control-label')) ?> <span class="required">*</span>
				<?php if(count($support_packages) == 0): ?>
					<p class="text-danger">You don't have any <strong>verified</strong> Access to Work support package. You'll <strong>CAN'T</strong> post an AtW job.</p>
				<?php endif; ?>
				<?php echo Form::select($field, array('' => '') + $support_packages, Input::old($field, empty($job) ? '' : $job->type), array('class' => 'form-control', 'placeholder' => 'Please select ...', 'id' => $field)); ?>
			</div>

			<?php if(count($field_errors) > 0): ?>
				<div class="col-md-4">
					<?php foreach ($errors->get($field) as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
		
	</div>

	<?php
		$field = 'related_bookings';
		$field_errors = $errors->get($field);
	?>
	<!--div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Is this booking related to a previous booking? If yes select which one', array('class' => 'control-label')) ?>
			<?php echo Form::select($field, array('' => '') + Auth::user()->jobsConfirmedAsArray(), Input::old($field, empty($job) ? '' : $job->related_bookings), array('class' => 'form-control', 'placeholder' => 'Select existing job ...', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div-->
	
	<?php
		$field = 'budget_type';
	?>
	<div class="row line">
		<div class="col-md-8 account_types">
			<?php echo Form::label($field, 'Job budget', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::radio($field, 'open_to_quotes', Input::old($field, empty($job) ? true : $job->project_budget == 'open_to_quotes'), array('class' => 'project_budget', 'id' => $field . '_open_to_quotes')); ?> <?php echo Form::label($field . '_open_to_quotes', 'Open to quotes', array('class' => 'account_type')) ?><br>
			<?php echo Form::radio($field, 'fixed', Input::old($field, empty($job) ? false : $job->project_budget == 'fixed'), array('class' => 'project_budget', 'id' => $field . '_fixed')); ?> <?php echo Form::label($field . '_fixed', 'Fixed budget (the total you can pay for this job)', array('class' => 'account_type')) ?><br>		
			
			<?php $budget_type = Input::old('budget_type', !empty($job) ? $job->project_budget : null); ?>
			<?php
				$field = 'budget';
				$field_errors = $errors->get($field);
			?>
			<div id="budget_type_fixed_container" class="row line <?php if(empty($budget_type) || $budget_type != 'fixed'): ?>hidden<?php endif; ?>">
				<div class="col-md-5 budget_type <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> ">
					<div class="input-group">
					  	<span class="input-group-addon">£</span>
						<?php echo Form::text($field, Input::old($field, empty($job) ? false : $job->budget), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
					</div>
				</div>
			
				<?php if(count($field_errors) > 0): ?>
					<div class="col-md-6">
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php echo Form::radio('budget_type', 'fixed_hourly_rate', Input::old('budget_type', empty($job) ? false : $job->project_budget == 'fixed_hourly_rate'), array('class' => 'project_budget', 'id' => 'budget_type_fixed_hourly_rate')); ?> <?php echo Form::label('budget_type_fixed_hourly_rate', 'Fixed hourly rate', array('class' => 'account_type')) ?><br>
			
			<?php
				$field = 'budget_hr';
				$field_errors = $errors->get($field);
			?>
			<div id="budget_type_fixed_hourly_rate_container" class="row line <?php if(empty($budget_type) || $budget_type != 'fixed_hourly_rate'): ?>hidden<?php endif; ?>">
				<div class="col-md-5 budget_type <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> ">
					<div class="input-group">
					  	<span class="input-group-addon">£</span>
						<?php echo Form::text($field, Input::old($field, empty($job) ? false : $job->budget), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
					  	<span class="input-group-addon">/hr</span>
					</div>
				</div>
			
				<?php if(count($field_errors) > 0): ?>
					<div class="col-md-6">
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
	<!--div class="row line">
		<div class="col-md-8 account_types">
			<?php echo Form::label('', 'Additional arrangements for assignment', array('class' => 'control-label')) ?><br>
			<?php echo Form::checkbox('travel_expenses', 'yes', Input::old('travel_expenses', empty($job) ? false : $job->travel_expenses), array('id' => 'travel_expenses')); ?> <?php echo Form::label('travel_expenses', 'Agreement to pay travel expenses, including a taxi where necessary', array('class' => 'account_type')) ?><br>
			<?php echo Form::checkbox('co_worker', 'yes', Input::old('co_worker', empty($job) ? false : $job->co_worker), array('id' => 'co_worker')); ?> <?php echo Form::label('co_worker', 'Request a co-worker for this assignment', array('class' => 'account_type')) ?><br>
			<?php echo Form::checkbox('co_worker_or_double_rate', 'yes', Input::old('co_worker_or_double_rate', empty($job) ? false : $job->co_worker_or_double_rate), array('id' => 'co_worker_or_double_rate')); ?> <?php echo Form::label('co_worker_or_double_rate', 'Request a co-worker for this assignment, if not I have the right to double rate', array('class' => 'account_type')) ?><br>
			<?php echo Form::checkbox('unsociable_hours', 'yes', Input::old('unsociable_hours', empty($job) ? false : $job->unsociable_hours), array('id' => 'unsociable_hours')); ?> <?php echo Form::label('unsociable_hours', 'If work falls into unsociable hours (from )', array('class' => 'account_type')) ?><br>
			<?php echo Form::checkbox('other_arrangement', 'yes', Input::old('other_arrangement', empty($job) ? false : $job->other_arrangement), array('id' => 'other_arrangement')); ?> <?php echo Form::label('other_arrangement', 'Other, please state', array('class' => 'account_type')) ?><br>
		</div>
	</div-->
	
	<?php
		$field = 'other_arrangement_text';
		$field_errors = $errors->get($field);
	?>
	<div class="row line">
		<div class="col-md-8 other_arrangement <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> <?php if(Input::old('other_arrangement') != 'yes'): ?>hidden<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, empty($job) ? false : $job->other_arrangement_text), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<div class="row line">
		<div class="col-md-12">
			<div class="pull-right">
				<small>You'll enter your language requirements on the next page</small>
				<button class="btn btn-primary" role="button" type="submit" name="next" title="Next">Next &gt;</button>
			</div>
		</div>
	</div>
<?php echo Form::close(); ?>