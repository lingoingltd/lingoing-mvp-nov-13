<?php echo Form::open(array('action' => 'JobsController@postStep4', 'method' => 'post')); ?>

<h3>Date of Job</h3>
<?php 
	$field_radio = 'date_type';
?>
<?php echo Form::radio($field_radio, 'one_day', Input::old($field_radio, !empty($job) ? $job->date_type == 'one_day': true), array('id' => $field_radio . '_one_day', 'class' => $field_radio)) ?> <?php echo Form::label($field_radio . '_one_day', 'One day booking') ?><br>

<?php if(count($errors->get('one_day_error')) > 0): ?>
	<div class="col-md-12 one_day_error_container">
	<?php foreach ($errors->get('one_day_error') as $message): ?>
		<div class="text-left text-danger"><?php echo $message; ?></div>
	<?php endforeach; ?>
	</div>
<?php endif; ?>

<div class="one_day_booking row clearfix <?php if((Input::old($field_radio, !empty($job) ? $job->date_type : 'one_day')) != 'one_day'): ?>hidden<?php endif; ?>">	
	<div class="one_day_booking_header clearfix">
		<div class="col-md-2">
			<label>Date</label>
		</div>
		
		<div class="col-md-2">
			<label>Start time</label>
		</div>
		
		<div class="col-md-2">
			<label>End time</label>
		</div>
	</div>

	<?php for($i = 1; $i <= 20; $i++): ?>
		<?php
			echo View::make('frontend.jobs.steps.step4_partial_one_day', array(
				'index' => $i,
				'errors' => $errors,
				'visible' => isset($dates[$i]),
				'dates' => $dates
			));
		?>
	<?php endfor ?>
	
	<div class="one_day_booking_add_item row clearfix">
		<div class="col-md-12">
			<a class="btn btn-link add-another-date-button" href="#">+ (add another date to this job)</a>
		</div>
	</div>
</div>

<?php 
	$field_radio = 'date_type';
?>
<?php echo Form::radio($field_radio, 'weekly', Input::old($field_radio, !empty($job) ? $job->date_type == 'weekly' : false), array('id' => $field_radio . '_weekly', 'class' => $field_radio)) ?> <?php echo Form::label($field_radio . '_weekly', 'Weekly bookings') ?><br>

<div class="weekly_bookings row clearfix <?php if(Input::old($field_radio, !empty($job) ? $job->date_type : 'weekly') != 'weekly'): ?>hidden<?php endif; ?>">
	<?php
		$field = 'start_date_weekly';
		$field_errors = $errors->get($field);
	?>
	<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> clearfix">
		<div class="col-md-3">
			<?php echo Form::label($field, 'Start Date', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, Session::get('start_date_weekly')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)); ?>
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
	<?php
		$field = 'end_date_weekly';
		$field_errors = $errors->get($field);
	?>
	<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> clearfix">
		<div class="col-md-3">
			<?php echo Form::label($field, 'End Date', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, Session::get('end_date_weekly')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)); ?>
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="weekly_bookings_header clearfix">
		<div class="col-md-2">
			<label>Day in week</label>
		</div>
		
		<div class="col-md-2">
			<label>Start time</label>
		</div>
		
		<div class="col-md-2">
			<label>End time</label>
		</div>
	</div>
	
	<?php if(count($errors->get('weekly_error')) > 0): ?>
		<div class="col-md-12 weekly_error_container">
		<?php foreach ($errors->get('weekly_error') as $message): ?>
			<div class="text-left text-danger"><?php echo $message; ?></div>
		<?php endforeach; ?>
		</div>
	<?php endif; ?>
	
	<div class="weekly_bookings_item clearfix">
		<div class="col-md-2 day">
			Monday
		</div>
		<?php
			$field = 'start_time_mon';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<?php
			$field = 'end_time_mon';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="weekly_bookings_item clearfix">
		<div class="col-md-2 day">
			Tuesday
		</div>
		<?php
			$field = 'start_time_tue';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<?php
			$field = 'end_time_tue';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="weekly_bookings_item clearfix">
		<div class="col-md-2 day">
			Wednesday
		</div>
		<?php
			$field = 'start_time_wed';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<?php
			$field = 'end_time_wed';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="weekly_bookings_item clearfix">
		<div class="col-md-2 day">
			Thursday
		</div>
		<?php
			$field = 'start_time_thu';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<?php
			$field = 'end_time_thu';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="weekly_bookings_item clearfix">
		<div class="col-md-2 day">
			Friday
		</div>
		<?php
			$field = 'start_time_fri';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<?php
			$field = 'end_time_fri';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="weekly_bookings_item clearfix">
		<div class="col-md-2 day">
			Saturday
		</div>
		<?php
			$field = 'start_time_sat';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<?php
			$field = 'end_time_sat';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="weekly_bookings_item clearfix">
		<div class="col-md-2 day">
			Sunday
		</div>
		<?php
			$field = 'start_time_sun';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<?php
			$field = 'end_time_sun';
			$field_errors = $errors->get($field);
		?>
		<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, Session::get($field)), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>
		
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
</div>

<div class="row line">
	<div class="col-md-12">
		<div class="pull-left">
			<a class="btn btn-default" href="<?php echo action('JobsController@getStep3') ?>">&lt; Previous</a>
		</div>
		<div class="pull-right">
			<small class="short-description">You can upload any additional attachments on next page</small>
			<button class="btn btn-primary btn-date-next" role="button" type="submit" name="next">Review all dates &gt;</button>
		</div>
	</div>
</div>

<?php echo Form::close(); ?>