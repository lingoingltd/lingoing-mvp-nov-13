<div class="one_day_booking_item clearfix <?php if(!$visible): ?>hidden<?php endif; ?>">
	<?php
		$field = 'start_date_' . $index;
		$field_errors = $errors->get($field);
	?>
	<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<?php echo Form::text($field, Input::old($field, isset($dates[$index]) ? $dates[$index]['date'] : ''), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)); ?>
		<?php if(count($field_errors) > 0): ?>
			<?php foreach ($field_errors as $message): ?>
				<div class="text-left text-danger"><?php echo $message; ?></div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'start_time_' . $index;
		$field_errors = $errors->get($field);
	?>
	<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<?php echo Form::text($field, Input::old($field, isset($dates[$index]) ? $dates[$index]['start_time'] : ''), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>

		<?php if(count($field_errors) > 0): ?>
			<?php foreach ($field_errors as $message): ?>
				<div class="text-left text-danger"><?php echo $message; ?></div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'end_time_' . $index;
		$field_errors = $errors->get($field);
	?>
	<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<?php echo Form::text($field, Input::old($field, isset($dates[$index]) ? $dates[$index]['end_time'] : ''), array('class' => 'form-control time-helper', 'placeholder' => 'HH:mm', 'id' => $field)); ?>

		<?php if(count($field_errors) > 0): ?>
			<?php foreach ($field_errors as $message): ?>
				<div class="text-left text-danger"><?php echo $message; ?></div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	<div class="col-md-2">
		<a class="btn btn-link remove-one-day-booking" href="#" data-index="<?php echo $index ?>">Remove</a>
	</div>
</div>