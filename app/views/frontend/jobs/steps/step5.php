<?php echo Form::open(array('action' => 'JobsController@postStep5', 'method' => 'post')); ?>

<h3>Job's Attachments</h3>

<div class="attachments-upload-container">
	<?php $field = 'attachment'; ?>
	<p><?php echo Form::file($field, array('id' => $field, 'class' => 'upload-date-attachment', 'data-url' => '/jobs/upload-job-attachment/job/' . $job->id)); ?></p>
	<p class="hidden"><img src="/img/ajax-loader.gif"> Uploading, please wait ...</p>

	<div>
		<?php echo View::make('frontend.jobs.steps.review_dates_partial_attachments_list', array(
			'attachments' => $attachments,
			'job' => $job,
			'target' => 'job'
		)) ?>
	</div>
</div>

<div class="row line">
	<div class="col-md-12">
		<div class="pull-left">
			<a class="btn btn-default" href="<?php echo action('JobsController@getStep4') ?>">&lt; Previous</a>
		</div>
		<div class="pull-right">
			<small class="short-description">You'll see all dates on the next page</small>
			<button class="btn btn-primary btn-date-next" role="button" type="submit" name="next">Preview</button>
		</div>
	</div>
</div>

<?php echo Form::close(); ?>