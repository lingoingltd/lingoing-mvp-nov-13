<div class="requirements <?php if($visible == null || $visible < $index): ?>hidden<?php endif; ?>" data-index="<?php echo $index ?>">
	<p class="lead">Requirements for language professional #<?php echo $index ?></p>
	
	<?php $req_errors = $errors->get('requirements_' . $index); ?>
	<?php if(count($req_errors) > 0): ?>
		<?php foreach ($req_errors as $message): ?>
			<div class="text-left text-danger error-requirements"><?php echo $message; ?></div>
		<?php endforeach; ?>
	<?php endif; ?>

	<div class="row line">
		<div class="col-md-8 <?php if(count($req_errors) > 0): ?>has-error<?php endif; ?>">
			<?php 
				$field = 'spoken_language_interpreter_' . $index;
			?>
			<?php echo Form::checkbox($field, 'yes', Input::old($field, empty($position) ? false : $position->spoken_language_interpreter), array('id' => $field, 'class' => 'requirement')); ?> <?php echo Form::label($field, 'Spoken Language Interpreter', array('class' => 'control-label')) ?>
		</div>
	</div>
	
	<?php
		$field = 'sli_languages_' . $index;
		$field_errors = $errors->get('sli_languages_'.$index);
	?>
	<div class="row line <?php if(Input::old('spoken_language_interpreter_' . $index) != 'yes' && (empty($position) ? true : $position->spoken_language_interpreter == 0)): ?>hidden<?php endif; ?> <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($position) ? '' : $position->searchOptionsAsArray('sli')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<div class="row line">
		<div class="col-md-8 <?php if(count($req_errors) > 0): ?>has-error<?php endif; ?>">
			<?php 
				$field = 'sign_language_interpreter_' . $index;
			?>
			<?php echo Form::checkbox($field, 'yes', Input::old($field, empty($position) ? false : $position->sign_language_interpreter), array('id' => $field, 'class' => 'requirement')); ?> <?php echo Form::label($field, 'Sign Language Interpreter', array('class' => 'control-label')) ?>
		</div>
	</div>
	
	<?php
		$field = 'sli2_languages_' . $index;
		$field_errors = $errors->get('sli2_languages_'.$index);
	?>
	<div class="row line <?php if(Input::old('sign_language_interpreter_' . $index) != 'yes' && (empty($position) ? true : $position->sign_language_interpreter == 0)): ?>hidden<?php endif; ?> <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::select($field.'[]', Person::listSignLanguageInterpreter(), Input::old($field, empty($position) ? '' : $position->searchOptionsAsArray('sli2')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<div class="row line">
		<div class="col-md-8 <?php if(count($req_errors) > 0): ?>has-error<?php endif; ?>">
			<?php 
				$field = 'lipspeaker_' . $index;
			?>
			<?php echo Form::checkbox($field, 'yes', Input::old($field, empty($position) ? false : $position->lipspeaker), array('id' => $field, 'class' => 'requirement')); ?> <?php echo Form::label($field, 'Lipspeaker', array('class' => 'control-label')) ?>
		</div>
	</div>

	<?php
		$field = 'lipspeaker_languages_' . $index;
		$field_errors = $errors->get('lipspeaker_languages_'.$index);
	?>
	<div class="row line <?php if(Input::old('lipspeaker_' . $index) != 'yes' && (empty($position) ? true : $position->lipspeaker == 0)): ?>hidden<?php endif; ?> <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($position) ? '' : $position->searchOptionsAsArray('lipspeaker')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<div class="row line">
		<div class="col-md-8 <?php if(count($req_errors) > 0): ?>has-error<?php endif; ?>">
			<?php 
				$field = 'speech_to_text_reporter_' . $index;
			?>
			<?php echo Form::checkbox($field, 'yes', Input::old($field, empty($position) ? false : $position->speech_to_text_reporter), array('id' => $field, 'class' => 'requirement')); ?> <?php echo Form::label($field, 'Speech to Text Reporter', array('class' => 'control-label')) ?>
		</div>
	</div>
	
	<?php
		$field = 'sttr_languages_' . $index;
		$field_errors = $errors->get('sttr_languages_'.$index);
	?>
	<div class="row line <?php if(Input::old('speech_to_text_reporter_' . $index) != 'yes' && (empty($position) ? true : $position->speech_to_text_reporter == 0)): ?>hidden<?php endif; ?> <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($position) ? '' : $position->searchOptionsAsArray('sttr')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<div class="row line">
		<div class="col-md-8 <?php if(count($req_errors) > 0): ?>has-error<?php endif; ?>">
			<?php 
				$field = 'sign_language_translator_' . $index;
			?>
			<?php echo Form::checkbox($field, 'yes', Input::old($field, empty($position) ? false : $position->sign_language_translator), array('id' => $field, 'class' => 'requirement')); ?> <?php echo Form::label($field, 'Sign Language Translator', array('class' => 'control-label')) ?>
		</div>
	</div>
	
	<?php
		$field = 'slt_languages_' . $index;
		$field_errors = $errors->get('slt_languages_'.$index);
	?>
	<div class="row line <?php if(Input::old('sign_language_translator_' . $index) != 'yes' && (empty($position) ? true : $position->sign_language_translator == 0)): ?>hidden<?php endif; ?> <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::select($field.'[]', Person::listSignLanguageInterpreter(), Input::old($field, empty($position) ? '' : $position->searchOptionsAsArray('slt')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<div class="row line">
		<div class="col-md-8 <?php if(count($req_errors) > 0): ?>has-error<?php endif; ?>">
			<?php 
				$field = 'note_taker_' . $index;
			?>
			<?php echo Form::checkbox($field, 'yes', Input::old($field, empty($position) ? false : $position->note_taker), array('id' => $field, 'class' => 'requirement')); ?> <?php echo Form::label($field, 'Note taker', array('class' => 'control-label')) ?>
		</div>
	</div>
	
	<?php
		$field = 'note_taker_languages_' . $index;
		$field_errors = $errors->get('note_taker_languages_'.$index);
	?>
	<div class="row line <?php if(Input::old('note_taker_' . $index) != 'yes' && (empty($position) ? true : $position->note_taker == 0)): ?>hidden<?php endif; ?> <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($position) ? '' : $position->searchOptionsAsArray('note_taker')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<p class="lead">Specialisms</p>

	<div class="row line">
		<div class="col-md-8">
			<?php $field = 'specialisms_' . $index; ?>
			<?php echo Form::select($field.'[]', Person::listSpecialisms(), Input::old($field, empty($position) ? '' : $position->searchOptionsAsArray('specialism')), array('class' => 'form-control', 'placeholder' => 'Choose specialisms ...', 'id' => $field, 'multiple')) ?><br>
		</div>
	</div>
	
	<p class="lead">Number of people translating/interpreting for</p>
	
	<div class="row line">
		<div class="col-md-8">
			<?php 
				$field = 'number_or_audience_' . $index;
				$value = (Input::old($field, 'one') == 'one');
				if(!empty($position))
					$value = ($position->number_of_audience == 'one');
			?>
			
			<?php echo Form::radio($field, 'one', $value, array('id' => $field . '_one', 'class' => 'number_of_audience')) ?> <?php echo Form::label($field . '_one', 'The interpreter is working only with one person') ?><br>
			<?php echo Form::radio($field, 'group', !$value, array('id' => $field . '_group', 'class' => 'number_of_audience')) ?> <?php echo Form::label($field . '_group', 'The interpreter is working for a group') ?><br>
		</div>
	</div>
	
	<div class="row line <?php if($value): ?>hidden<?php endif; ?>">
		<?php
	        $field = 'no_of_people_' . $index;
	        $field_errors = $errors->get($field);
		?>
		<div class="col-md-8 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> coworkers clearfix">
			<div class="clearfix">
				<?php echo Form::label($field, 'How many people are in the group?', array('class' => 'control-label')) ?> <span class="required">*</span>
			</div>
			<div class="clearfix">
				<div class="col-md-5">			
					<?php echo Form::text($field, Input::old($field, empty($position) ? '' : $position->no_in_groups), array('class' => 'form-control', 'id' => $field)); ?>
				</div>
				<?php if(count($field_errors) > 0): ?>
					<div class="col-md-7">
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="col-md-8 clearfix">
			<div class="col-md-8">
				<div class="clearfix">
					<div>
						<label>Is there a co-worker?</label>
					</div>
					<div class="col-md-12">
						<?php 
							$field = 'coworker_' . $index;
							$value = (Input::old($field, 'no') == 'yes');
							if(!empty($position))
								$value = (bool)$position->coworker;
						?>
						
						<?php echo Form::radio($field, 'yes', $value, array('id' => $field . '_yes', 'class' => 'coworker')) ?> <?php echo Form::label($field . '_yes', 'Yes', array('class' => 'control-label')) ?><br>
						<?php echo Form::radio($field, 'no', !$value, array('id' => $field . '_no', 'class' => 'coworker')) ?> <?php echo Form::label($field . '_no', 'No', array('class' => 'control-label')) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>