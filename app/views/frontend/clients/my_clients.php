<h1>My client</h1>

<div class="col-md-3">
	<div class="btn-group-vertical my-jobs-menu">
		<a href="<?php echo url('my-clients') ?>" class="btn <?php if($type != 'preferred' && $type != 'blocked'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">Clients</a>
		<!--a href="<?php echo url('my-clients/preferred') ?>" class="btn <?php if($type == 'preferred'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">Preferred clients</a>
		<a href="<?php echo url('my-clients/blocked') ?>" class="btn <?php if($type == 'blocked'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">Blocked clients</a-->
	</div>
</div>

<div class="col-md-9">
	<?php if(count($clients) > 0): ?>
		<?php foreach ($clients as $professional): ?>
			<div class="professional-container border-all bottom-space">
				<div class="job-title border-bottom-gray clearfix">
					<div class="professional-picture">
						<img src="<?php echo $professional->getProfilePicture() ?>" class="img-thumbnail">
					</div>
					<p class="lead professional-name">
						<a href="<?php echo url('profile') ?>/<?php echo $professional->id ?>" title="<?php echo $professional->getName() ?>"><?php echo $professional->getName() ?></a>
					</p>
				</div>
			</div>
		<?php endforeach ?>
	<?php else: ?>
		<?php if($type == 'preferred'): ?>
			<p class="text-center">You don't have any preferred clients yet.</p>
		<?php elseif($type == 'blocked'): ?>
			<p class="text-center">You don't have any blocked clients yet.</p>
		<?php else: ?>
			<p class="text-center">You don't have any clients yet.</p>
		<?php endif; ?>
	<?php endif; ?>
</div>