<?php if($max_pages > 1): ?>
	<?php
		$begin = 1;
		$prikaziLevoDesno = 3;

		$beginVidno = $page - $prikaziLevoDesno;
		if($beginVidno < 1)
			$beginVidno = 1;
		$endVidno = $page + $prikaziLevoDesno;
		if($endVidno > $max_pages)
			$endVidno = $max_pages;
	?>
	<div class="text-center">
		<ul class="pagination">
			<?php if($page == 1): ?>
				<li class="disabled"><a href="#">&laquo;</a></li>
			<?php else: ?>
				<li><a href="<?php echo url($url) . '/' . ($page - 1); ?>">&laquo;</a></li>
			<?php endif; ?>
			
			<?php if($begin < $beginVidno): ?>
				<?php if($begin === $page): ?>
					<li class="active"><span class="current_page"><?php echo $begin ?></span></li>
				<?php else: ?>
					<li><a href="<?php echo url($url) . '/' . $begin; ?>"><?php echo $begin ?></a></li>
				<?php endif; ?>
				<li><span class="pager_dots">...</span></li>
			<?php endif; ?>
			
			<?php for($i = $beginVidno; $i <= $endVidno; $i++): ?>
				<?php if($i == $page): ?>
					<li class="active"><span class="current_page"><?php echo $i ?></span></li>
				<?php else: ?>
					<li><a href="<?php echo url($url) . '/' . $i; ?>"><?php echo $i ?></a></li>
				<?php endif; ?>
			<?php endfor; ?>
			
			<?php if($max_pages > $endVidno): ?>
				<li><span class="pager_dots">...</span></li>
				<?php if($max_pages == $page): ?>
					<li class="active"><span class="current_page"><?php echo $max_pages ?></span></li>
				<?php else: ?>
					<li><a href="<?php echo url($url) . '/' . $max_pages; ?>"><?php echo $max_pages ?></a></li>
				<?php endif; ?>
			<?php endif; ?>
			
			<?php if($max_pages == $page): ?>
				<li class="disabled"><span class="nextprev_page">&raquo;</span></li>
			<?php else: ?>
				<li><a class="nextprev_page" href="<?php echo url($url) . '/' . ($page + 1); ?>">&raquo;</a></li>
			<?php endif; ?>
		</ul>
	</div>
<?php endif; ?>