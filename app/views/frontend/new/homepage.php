<div class="gallery_con">
	<div class="container">
		<div class="martop50" style="margin-bottom:50px;">
			<div class="row">
				<div class="col-md-8"> 
					<span class="head_title">Every voice</span> <br>
					<span class="head_title">will be heard</span> 
					<div style="background:rgba(0, 0, 0, 0.7); width:240px; color:#FFFFFF;">
						<div class="pad20 font12">
							<div class="martop10 marbot10 bold font22">Lingoing</div>
							<p style="margin:0;">At Lingoing we believe that every voice has the right to be heard.  It's why we created an online platform that connects the community to qualified language professionals in a no fuss, value for money, simple to use way. By uniting both parties we aim to remove the barriers to language isolation so that language communities feel visible, valued, connect and heard.<div style="margin-top:5px;">Help us end the isolation.</div></p>
						</div>
					</div>
				</div>
				
				<div class="col-md-4">
					<div>
						<div class="marright20 marleft20 text-center">
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="row mar0">
	    <div class="col-md-12 head_textbox">
	      <div class="container">

	        <div class="cycle-slideshow" 
	    data-cycle-fx="scrollHorz" 
	    data-cycle-timeout="0"
	    data-cycle-slides="> div"
	    data-cycle-pager="#no-template-pager"
	    data-cycle-pager-template=""
	    >

	          <div class="txt_slide">
	            <h1 class="text-center font32 mar0">Clients</h1>
	            <div class="martop20">
	              <p>Lingoing is an online service aimed to support, connect and unite the deaf and hearing communities with fully qualified language professionals in the UK.  By registering on the platform you can quickly post a job request and get access to relevant matches in your local area.</p>
	            </div>
	          </div>

	          <div class="txt_slide">
	            <h1 class="text-center font32 mar0">Language professionals</h1>
	            <div class="martop20">
	              <p>Lingoing provides an attractive proposition to Language Service Professionals - its free to use, saves time, organises admin and bookings plus collect payment.</p>
	            </div>
	          </div>

	        </div>

	        <div class="txt_nav cycle-pager external" id=no-template-pager>
	          <div class="txt_nav_btn" style="border-right:1px #aeaeae solid;"> Clients </div>
	          <div class="txt_nav_btn"> Language professionals </div>
	        </div>

	      </div>
	    </div>
	  </div>
	</div>
	</div>
	<div class="clearfix">
	  <div class="col-md-12 wayra_section">
	    <div class="container pink_txt font60 bold upper">
	      <div class="martop90" style="max-width:690px;"> We’re a start-up with a difference </div>
	    </div>
	    <div class="row">
	      <div class="col-md-12 wayra_bottom">
	        <div class="container">
	          <div class="row">
	            <div class="col-md-7">
	              <div class="padtop20 padbot20"> Lingoing is a social enterprise start-up and one of the first digital companies to be selected as part of the government backed Wayra accelerator academy. On October 14th, 2014, Deputy Prime Minister Nick Clegg officially opened the new space and received a quick lesson in British Sign Language (BSL) from one of our founders, Saduf Naqvi. </div>
	            </div>
	            <div class="col-md-5">
	              <div class="row">
	                <div class="col-xs-4 padtop10 padbot10"> <img src="/img/wayra-logo.png" width="120" height="75" alt="wayra" /> </div>
	                <div class="col-xs-4 padtop10 padbot10"> <img src="/img/unltd.png" width="187" height="54" alt="UnLtd" /> </div>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="clearfix">
	  <div class="col-md-12 team_section">
	    <div class="container team_con">
	      <div class="row martop20 marbot20">
	        <div class="col-sm-6 col-sm-offset-3">
	          <div class="txt_center bold font32 upper padbot20" style="border-bottom:2px solid #FFFFFF;">Our team</div>
	        </div>
	      </div>
	      <div class="row martop30 marbot30">
	        <div class="col-sm-4"> <img src="/img/sadaqat.png" width="246" height="315" alt="Sadaqat" /> </div>
	        <div class="col-sm-8 martop30">
	          <div class="font43 light marbot20">Sadaqat</div>
	          <div>Sadaqat Ali is deaf and has worked with language professionals all his life. He understands the complexity of the market as well as the pitfalls so is ideally placed to identify the issues plus problem solve appropriate solutions. Sadaqat completed his degrees in Deaf Studies, Youth and Community, PGCE in 16+ teaching as well as British Sign Language.</div>
	          <div class="martop30"><img src="/img/sadaqat-sign.png" alt="Sadaqat Sign" /></div>
	        </div>
	      </div>
	      <div class="row martop30 marbot30">
	        <div class="col-sm-8">
	          <div class="font43 light marbot20">Saduf</div>
	          <div>Saduf Naqvi is a trainee language professional, she understands the market from the inside including its problems and inconsistencies. She has extensive experience and insight into the needs and demands of deaf people gained from interaction with long- term clients.  Saduf ran an interpreting agency so she can identify the gaps in the market that this online solution will meet including providing interpreters, fair pay for language professionals, reducing costs, and provision of data management for users.  Saduf completed her degree in Computing and Information Systems, and a PhD in sign language technologies and is on the final level towards qualifying to become a British Sign Language interpreter</div>
	          <div class="martop30"><img src="/img/saduf-sign.png" width="322" height="56" alt="Saduf Sign" /></div>
	        </div>
	        <div class="col-sm-4"> <img src="/img/saduf.png" width="246" height="315" alt="Saduf" /> </div>
	      </div>
	    </div>
	  </div>
	</div>

	<?php echo View::make('frontend.static.partial.get_in_touch')->render(); ?>