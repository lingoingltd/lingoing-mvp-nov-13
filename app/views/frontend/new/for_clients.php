<div class="container padbot30">
	<div class="row martop50">
		<div class="col-md-12">
			<div class="font38 light pink_txt marbot30">For Clients</div>
		</div>
	</div>
	
	<div class="row marbot30">
		<div class="col-md-12">
			<div class="font14 grey_txt">
				We are currently using all your brilliant feedback to help improve the Lingoing platform. Further ideas and feedback are always welcome. In the mean-time please fill in our quick form and we will be in touch to connect you with trusted language service providers (LSP):
			</div>
		</div>
	</div>
	
	<?php if(!empty($flashSuccess)): ?>
		<p class="lead text-center"><?php echo $flashSuccess ?></p>
	<?php endif; ?>
	
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<?php echo Form::open(array('url' => '/for-clients', 'method' => 'post', 'role' => 'form')); ?>

				<?php
					$label = 'Client full name';
					$field = 'client_name';
					$field_errors = $errors->get($field);
				?>
				<div class="form-group <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> clearfix">
					<div class="col-md-4">
						<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>:
						<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'id' => $field)); ?>
					</div>
				
					<?php if(count($field_errors) > 0): ?>
						<div class="col-md-8">
							<?php foreach ($field_errors as $message): ?>
								<div class="text-left text-danger"><?php echo $message; ?></div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			
				<?php
					$label = 'Email address';
					$field = 'email_address';
					$field_errors = $errors->get($field);
				?>
				<div class="form-group <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> clearfix">
					<div class="col-md-4">
						<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>:
						<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'id' => $field)); ?>
					</div>
				
					<?php if(count($field_errors) > 0): ?>
						<div class="col-md-8">
							<?php foreach ($field_errors as $message): ?>
								<div class="text-left text-danger"><?php echo $message; ?></div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			
				<?php
					$label = 'Contact number';
					$field = 'contact_number';
					$field_errors = $errors->get($field);
				?>
				<div class="form-group <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> clearfix">
					<div class="col-md-4">
						<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>:
						<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'id' => $field)); ?>
					</div>
				
					<?php if(count($field_errors) > 0): ?>
						<div class="col-md-8">
							<?php foreach ($field_errors as $message): ?>
								<div class="text-left text-danger"><?php echo $message; ?></div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
				
				<?php
					$label = 'What support are you looking for/How can we help you?';
					$field = 'comments';
					$field_errors = $errors->get($field);
				?>
				<div class="form-group <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
					<div class="col-md-12">
						<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>
						<?php echo Form::textarea($field, Input::old($field), array('class' => 'form-control', 'id' => $field)); ?>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-12 text-center">
						<button class="btn btn-submit" role="button" type="submit" name="submit">Submit</button>
					</div>
				</div>

			<?php echo Form::close(); ?>
		</div>
	</div>

</div>
</div>