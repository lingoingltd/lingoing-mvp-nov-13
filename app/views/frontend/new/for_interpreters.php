<div class="container padbot30">
	<div class="row martop50">
		<div class="col-md-12">
			<div class="font38 light blue_txt marbot30">For Interpreters/Translators</div>
		</div>
	</div>
	
	<div class="row marbot30">
		<div class="col-md-12">
			<div class="font14 grey_txt">
				Your amazing feedback has helped us to come up with new, exciting features that make you life easier. We are currently maintaining the Lingoing platform, whilst we integrate our exciting new plans. Filling in the form will connect you to new clients and make you part of our growing community. All feedback and ideas are welcome - thank you.
			</div>
		</div>
	</div>
	
	<?php if(!empty($flashSuccess)): ?>
		<p class="lead text-center"><?php echo $flashSuccess ?></p>
	<?php endif; ?>
	
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<?php echo Form::open(array('url' => '/for-interpreters', 'method' => 'post', 'role' => 'form')); ?>

				<?php
					$label = 'Interpreter full name';
					$field = 'interpreter_name';
					$field_errors = $errors->get($field);
				?>
				<div class="form-group <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> clearfix">
					<div class="col-md-4">
						<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>:
						<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'id' => $field)); ?>
					</div>
				
					<?php if(count($field_errors) > 0): ?>
						<div class="col-md-8">
							<?php foreach ($field_errors as $message): ?>
								<div class="text-left text-danger"><?php echo $message; ?></div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			
				<?php
					$label = 'Email address';
					$field = 'email_address';
					$field_errors = $errors->get($field);
				?>
				<div class="form-group <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> clearfix">
					<div class="col-md-4">
						<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>:
						<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'id' => $field)); ?>
					</div>
				
					<?php if(count($field_errors) > 0): ?>
						<div class="col-md-8">
							<?php foreach ($field_errors as $message): ?>
								<div class="text-left text-danger"><?php echo $message; ?></div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			
				<?php
					$label = 'Contact number';
					$field = 'contact_number';
					$field_errors = $errors->get($field);
				?>
				<div class="form-group <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> clearfix">
					<div class="col-md-4">
						<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>:
						<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'id' => $field)); ?>
					</div>
				
					<?php if(count($field_errors) > 0): ?>
						<div class="col-md-8">
							<?php foreach ($field_errors as $message): ?>
								<div class="text-left text-danger"><?php echo $message; ?></div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
				
				<?php
					$label = 'Location/Region';
					$field = 'location_region';
					$field_errors = $errors->get($field);
				?>
				<div class="form-group <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> clearfix">
					<div class="col-md-4">
						<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>
						<?php echo Form::select($field, array('' => '') + Person::listCounties(), Input::old($field), array('class' => 'form-control', 'placeholder' => 'Please select ...', 'id' => $field)); ?>
					</div>

					<?php if(count($field_errors) > 0): ?>
						<div class="col-md-8">
							<?php foreach ($field_errors as $message): ?>
								<div class="text-left text-danger"><?php echo $message; ?></div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
				
				<?php
					$label = 'Language skills';
					$field = 'language_skills';
					$field_errors = $errors->get($field);
				?>
				<div class="form-group <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> clearfix">
					<div class="col-md-4">
						<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>
						<?php echo Form::select($field . '[]', array('' => '') + Person::listLanguageSkills(), Input::old($field, array()), array('class' => 'form-control', 'placeholder' => 'Please select ...', 'id' => $field, 'multiple')); ?>
					</div>

					<?php if(count($field_errors) > 0): ?>
						<div class="col-md-8">
							<?php foreach ($field_errors as $message): ?>
								<div class="text-left text-danger"><?php echo $message; ?></div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
				
				<?php $counter = 0; ?>
				<?php foreach ($bodies as $body): ?>
					<div class="form-group clearfix">
						<?php
							$label = 'Membership body';
							$field = 'membership_body_' . $body;
							$field_errors = $errors->get($field);
						?>
						<div class="col-md-3 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
							<?php if($counter == 0): ?>
								<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>
							<?php endif; ?>
							
							<?php echo Form::text($field, $body, array('class' => 'form-control', 'id' => $field, 'disabled')); ?>

							<?php if(count($field_errors) > 0): ?>
								<?php foreach ($field_errors as $message): ?>
									<div class="text-left text-danger"><?php echo $message; ?></div>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>

						<?php
							$label = 'Reference number';
							$field = 'reference_number_' . $body;
							$field_errors = $errors->get($field);
						?>
						<div class="col-md-3 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
							<?php if($counter == 0): ?>
								<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>
							<?php endif; ?>
							
							<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'id' => $field)); ?>

							<?php if(count($field_errors) > 0): ?>
								<?php foreach ($field_errors as $message): ?>
									<div class="text-left text-danger"><?php echo $message; ?></div>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>

						<?php
							$label = 'Expiration date';
							$field = 'expiration_date_' . $body;
							$field_errors = $errors->get($field);
						?>
						<div class="col-md-3 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
							<?php if($counter == 0): ?>
								<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>
							<?php endif; ?>
							
							<?php echo Form::text($field, Input::old($field), array('class' => 'form-control date-helper', 'id' => $field)); ?>

							<?php if(count($field_errors) > 0): ?>
								<?php foreach ($field_errors as $message): ?>
									<div class="text-left text-danger"><?php echo $message; ?></div>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>

						<?php
							$label = 'Languages';
							$field = 'membership_languages_' . $body;
							$field_errors = $errors->get($field);
						?>
						<div class="col-md-3 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
							<?php if($counter == 0): ?>
								<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>
							<?php endif; ?>
							
							<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'id' => $field)); ?>

							<?php if(count($field_errors) > 0): ?>
								<?php foreach ($field_errors as $message): ?>
									<div class="text-left text-danger"><?php echo $message; ?></div>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
					</div>
					<?php $counter++; ?>
				<?php endforeach ?>
				
				<?php
					$label = 'Hourly rate (£)';
					$field = 'hourly_rate';
					$field_errors = $errors->get($field);
				?>
				<div class="form-group <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> clearfix">
					<div class="col-md-4">
						<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>:
						<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'id' => $field)); ?>
					</div>
				
					<?php if(count($field_errors) > 0): ?>
						<div class="col-md-8">
							<?php foreach ($field_errors as $message): ?>
								<div class="text-left text-danger"><?php echo $message; ?></div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			
				<?php
					$label = 'Qualified/Trainee/CSW (level)?';
					$field = 'expert_level';
					$field_errors = $errors->get($field);
				?>
				<div class="form-group <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> clearfix">
					<div class="col-md-4">
						<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>
						<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'id' => $field)); ?>
					</div>
				
					<?php if(count($field_errors) > 0): ?>
						<div class="col-md-8">
							<?php foreach ($field_errors as $message): ?>
								<div class="text-left text-danger"><?php echo $message; ?></div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
				
				<?php
					$label = 'Specialisms';
					$field = 'specialisms';
					$field_errors = $errors->get($field);
				?>
				<div class="form-group <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?> clearfix">
					<div class="col-md-4">
						<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>
						<?php echo Form::select($field . '[]', array('' => '') + Person::listSpecialisms(), Input::old($field, array()), array('class' => 'form-control', 'placeholder' => 'Please select ...', 'id' => $field, 'multiple')); ?>
					</div>

					<?php if(count($field_errors) > 0): ?>
						<div class="col-md-8">
							<?php foreach ($field_errors as $message): ?>
								<div class="text-left text-danger"><?php echo $message; ?></div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				</div>
			
				<?php
					$label = 'Additional Qualification/Accreditations/Comments';
					$field = 'comments';
					$field_errors = $errors->get($field);
				?>
				<div class="form-group <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
					<div class="col-md-12">
						<?php echo Form::label($field, $label, array('class' => 'control-label')) ?>:
						<?php echo Form::textarea($field, Input::old($field), array('class' => 'form-control', 'id' => $field)); ?>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-12 text-center">
						<button class="btn btn-submit-blue" role="button" type="submit" name="submit">Submit</button>
					</div>
				</div>

			<?php echo Form::close(); ?>
		</div>
	</div>

</div>
</div>