<h1>My invoices</h1>

<div class="col-md-3">
	<div class="btn-group-vertical my-jobs-menu">
		<a href="<?php echo url('my-invoices') ?>" class="btn <?php if($type == 'unpaid'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">Unpaid invoices</a>
		<a href="<?php echo url('my-invoices/paid') ?>" class="btn <?php if($type == 'paid'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">Paid invoices</a>
		<?php if(Auth::user()->account_type == 'client'): ?>
			<a href="<?php echo url('my-invoices/pending') ?>" class="btn <?php if($type == 'pending'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">Pending invoices</a>
		<?php endif; ?>
	</div>
</div>

<div class="col-md-9">
	<?php if(count($invoices) > 0): ?>
		<?php foreach ($invoices as $invoice): ?>
			<div class="invoice-container">
				<div class="invoice-client">
					<?php $job = $invoice->job; ?>
					<?php echo $job->type == 'other' ? $job->type_other : Job::listTypes($job->type); ?>, <?php 
						if(empty($invoice->atw_package_id))
						{
							$date = $invoice->firstDate();
							echo date('d/m/Y', strtotime($date->date));
						}
						else
						{
							if($invoice->atw_date_begin != $invoice->atw_date_end)
								echo date('d/m/Y', strtotime($invoice->atw_date_begin)) . ' - ' . date('d/m/Y', strtotime($invoice->atw_date_end));
						} 
					?>, <?php echo $invoice->getDurationInHoursAsString(); ?>
				</div>
				<div class="invoice-body">
					<a title="Click to download invoice in PDF" href="<?php echo url('invoices/download') ?>/<?php echo $invoice->id ?>" class="lead pull-left">Invoice <?php echo $invoice->number ?></a>
                    <?php if(Auth::user()->account_type == 'client'): ?>
						<p class="lead price pull-right">£ <?php echo number_format((double)$invoice->total_cost,2) ?></p>
                    <?php else: ?>
                    	<p class="lead price pull-right">£ <?php echo number_format((float)$invoice->lsp_cost + (float)$invoice->lsp_cost_overtime, 2) ?></p>
                    <?php endif; ?>
					<div class="clearfix"></div>
					
					<?php if(!empty($invoice->atw_package_id)): ?>
						<p>Access to Work customer</p>
					<?php endif; ?>
					
					<?php if((Auth::user()->account_type == 'client' && empty($invoice->atw_package_id) && ($invoice->status == 'unpaid' || $invoice->status == 'pending')) || (Auth::user()->account_type == 'translator' && ($invoice->status_worker == 'unpaid' || $invoice->status_worker == 'pending'))): ?>
						<small class="<?php if($invoice->isOverdue()): ?>text-danger<?php else: ?>text-muted<?php endif; ?>"><?php echo $invoice->renderDueDate() ?></small>
					<?php endif; ?>
				</div>
				
				<?php if(Auth::user()->account_type == 'client' && ($invoice->status == 'unpaid' || (!empty($invoice->atw_package_id) && !(bool)$invoice->atw_form_sent))): ?>
					<div class="button-container clearfix">
						<div class="invoice-actions">
							<?php if(empty($invoice->atw_package_id) && $invoice->status == 'unpaid'): ?>
								<div class="btn-group pull-right">
								  	<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
								    	Pay by <span class="caret"></span>
								  	</button>
								  	<ul class="dropdown-menu" role="menu">
										<li><a href="<?php echo url('invoices/pay') ?>/<?php echo $invoice->id ?>/credit" style="margin:0;">Credit card</a></li>
										<li><a href="<?php echo url('invoices/pay') ?>/<?php echo $invoice->id ?>/debit" style="margin:0;">Debit card</a></li>
								  	</ul>
								</div>
							<?php endif; ?>
							
							<?php if(!empty($invoice->atw_package_id) && !(bool)$invoice->atw_form_sent): ?>
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-primary atw-form-sent-user" data-id="<?php echo $invoice->id ?>">I posted my AtW forms</button>
								</div>
							<?php endif; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?php endforeach ?>
	<?php else: ?>
		<?php if($type == 'unpaid'): ?>
			<p class="text-center">You don't have any unpaid invoices.</p>
		<?php elseif($type == 'pending'): ?>
			<p class="text-center">You don't have any pending invoices.</p>
		<?php else: ?>
			<p class="text-center">You don't have any paid invoices.</p>
		<?php endif; ?>
	<?php endif; ?>
</div>