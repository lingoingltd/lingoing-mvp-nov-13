<h1>Paying by debit card</h1>
<p class="col-md-12 lead">You just canceled a payment!</p>
<p class="col-md-12">Your payment for invoice <?php echo $invoice->number ?> was canceled!</p>
<p class="col-md-12 text-muted">You will be redirected to <a href="<?php echo url('my-invoices/paid'); ?>">My invoices</a> in 10 seconds.</p>

<script type="text/javascript">
	setTimeout(function(){
		window.location.href = "<?php echo url('my-invoices'); ?>";
	}, 10000);
</script>