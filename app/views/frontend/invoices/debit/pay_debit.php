<h1>Paying by debit card</h1>
<p class="col-md-12">To pay your invoice via debit card please click on the button below.</p>
<div class="text-center">
	<p class="lead">Invoice number <strong><?php echo $invoice->number; ?></strong></p>
    <a href="<?php echo $bill_url; ?>" class="stripe-button-el">
    	<span style="display: block; min-height: 30px;">Pay with Card</span>
    </a>
</div>