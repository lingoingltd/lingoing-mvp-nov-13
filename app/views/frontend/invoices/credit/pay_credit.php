<h1>Paying by credit card</h1>
<p class="col-md-12">To pay your invoice via credit card please click on the button below.</p>
<div class="text-center">
	<p class="lead">Invoice number <strong><?php echo $invoice->number; ?></strong></p>
    <form action="" method="POST">
      <script
        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
        data-key="<?php echo Config::get('pricing.stripe_publishable_api_key'); ?>"
        data-amount="<?php echo round((float)$invoice->total_cost * 100, 0); ?>"
		data-currency="GBP"
        data-name="Lingoing.com"
        data-description="Lingoing.com's invoice <?php echo $invoice->number; ?>"
        data-image="/img/logo_60x60.png">
      </script>
    </form>
</div>