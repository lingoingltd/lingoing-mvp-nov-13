<h1>Paying by credit/debit card</h1>
<p class="col-md-12 lead">Thank you for paying invoice <strong><?php echo $invoice->number; ?></strong>.</p>
<p class="col-md-12 text-muted">You will be redirected to <a href="<?php echo url('my-invoices/paid'); ?>">My invoices</a> in 5 seconds.</p>

<script type="text/javascript">
	setTimeout(function(){
		window.location.href = "<?php echo url('my-invoices'); ?>";
	}, 5000);
</script>