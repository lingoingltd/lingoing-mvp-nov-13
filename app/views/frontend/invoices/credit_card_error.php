<h1>Paying by credit card</h1>
<p class="col-md-12 lead">An error occured during payment!</p>
<p class="col-md-12">Please check your credit card is <strong>VALID</strong> and <strong>NOT</strong> expired! Then please try again.</p>
<p class="col-md-12">If the problem repeats please contact us for support.</p>