<div class="container padbot30">
	<div class="row martop50">
		<div class="col-md-12">
			<div class="font38 light blue_txt marbot30">For Clients</div>
		</div>
	</div>
   
	<div class="row marbot30">
		<div class="col-md-7">
			<div class="font14 grey_txt">
				It’s not always easy to find the right match of language professionals for the job at hand.<br><br>
				You have a conference, meeting or event coming up and need someone who can interpret or translate, but how do know who to go to, and are they qualified enough?<br><br>
				In the language community you often find very capable, honest and hard working language professionals that are interested in delivering the best but, amongst all the bad practice it’s hard to find them.<br><br>
				Lingoing helps make it easy and we only want to work with the best in the industry.<br><br>
				Why?<br><br>
				We believe not only should the best be supported to shine through, but also our clients deserve nothing less.<br><br>
				Register today and join the Lingoing community<br><br>
				
				<ul class="blue_home_bullets">
					<li>Lingoing lets you choose the right supplier</li>
					<li>Lingoing saves you time, effort and money</li>
					<li>Lingoing helps you be part of a community.</li>
					<li>Lingoing understands your needs and concerns</li>
					<li><strong>Specifically for organisations</strong><br>Lingoing helps you track usage and helps you manage your budget</li>
				</ul>
			</div>
		</div>
		
		<div class="col-md-4 col-md-offset-1">
			<?php if(!Auth::check()): ?>
				<div class="sign-in-con">
					<div class="marright20 marleft20"> 
						<!-- begin content -->
						<div class="row">
							<div class="col-md-12 social-login" style="background:none; padding:0; height:auto;">
								<p class="text-center"><a id="btn-facebook" class="btn" role="button" href="https://www.facebook.com/dialog/oauth?client_id=<?php echo Config::get('social.facebook.app_id') ?>&amp;redirect_uri=http://<?php echo $_SERVER['HTTP_HOST'] ?>/facebook&amp;scope=<?php echo Config::get('social.facebook.scope') ?>"><i class="fa fa-facebook-square" style="font-size:20px"></i> Login with Facebook</a></p>
								<p class="text-center"><a id="btn-linked-in" class="btn" role="button" href="http://www.lingoing.com/linkedin"><i class="fa fa-linkedin-square" style="font-size:20px"></i> Login with LinkedIn</a></p>
							</div>
						</div>
					
						<div class="text-center" id="login-or" style="margin:0;">OR</div>
						<div class="row">
							<div class="col-md-12 email-signup" style="background:none;">
								<form method="post" action="<?php echo url('register') ?>" accept-charset="UTF-8" role="form" class="form-horizontal">
									<div class="form-group">
										<div class="col-sm-12">
											<input class="form-control" placeholder="First name" id="first_name" name="first_name" type="text">
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<input class="form-control" placeholder="Last name" id="last_name" name="last_name" type="text">
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<input class="form-control" placeholder="Email address" id="email" name="email" type="text">
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<input class="form-control" placeholder="Password" id="password" name="password" type="password" value="">
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<input class="form-control" placeholder="Confirm password" id="confirm_password" name="confirm_password" type="password" value="">
										</div>
									</div>
									<div class="form-group">
				                      <div class="col-sm-12">
				                        <input class="form-control" placeholder="Business name (optional)" id="business_name" name="business_name" type="text" value="">
				                      </div>
				                    </div>
									<div class="form-group">
				                      <div class="col-sm-12">
				                        <input class="form-control" placeholder="Referral code (optional)" id="ref_code" name="ref_code" type="text" value="">
				                      </div>
				                    </div>
									<div class="form-group" style="margin-bottom:10px;">
										<div class="col-sm-12 account_types">
											<input id="account_type_client" checked="checked" name="account_type" type="radio" value="client">
											<label for="account_type_client" class="account_type font14">I'm looking to hire language professionals</label>
											<br>
											<input id="account_type_translator" name="account_type" type="radio" value="translator">
											<label for="account_type_translator" class="account_type font14">I'm a language professional</label>
											<br>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12 terms">
											<input id="terms_agreed" name="terms_agreed" type="checkbox" value="yes">
											<label for="terms_agreed" class="account_type">I agree to Lingoing's <a href="/files/Lingoing Terms of Business Issued April 2014.pdf" title="Terms Of Business (click to read)">Terms Of Business (click to read)</a></label>
										</div>
									</div>
									<div class="row line text-center">
										<?php echo Form::token(); ?>
										<button class="btn btn-primary" role="button" type="submit" title="Create Account">Create Account</button>
									</div>
								</form>
							</div>
						</div>
						<!-- end content --> 
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div> 

<?php echo View::make('frontend.static.partial.get_in_touch')->render(); ?>