<div class="gallery_con">
  <div class="container">
    <div class="martop50">
      <div class="row">
        <div  class="col-md-8"> 
        <span class="head_title">Every voice</span> <br />
          <span class="head_title">will be heard</span> 
          <div style="background:rgba(0, 0, 0, 0.7); width:240px; color:#FFFFFF;">
          	<div class="pad20 font12">
            <div class="martop10 marbot10 bold font22">Lingoing</div>
          <p style="margin:0;">At Lingoing we believe that every voice has the right to be heard.  It's why we created an online platform that connects the community to qualified language professionals in a no fuss, value for money, simple to use way. By uniting both parties we aim to remove the barriers to language isolation so that language communities feel visible, valued, connect and heard.<div style="margin-top:5px;">Help us end the isolation.</div></p>
          </div>
          </div>
          </div>
        <div class="col-md-4">
          <div class="sign-in-con">
            <div class="marright20 marleft20"> 
              <!-- begin content -->
              <div class="row">
                <div class="col-md-12 social-login" style="background:none; padding:0; height:auto;">
                  <p class="text-center"><a id="btn-facebook" class="btn" role="button" href="https://www.facebook.com/dialog/oauth?client_id=<?php echo Config::get('social.facebook.app_id') ?>&amp;redirect_uri=http://<?php echo isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'lingoing.com' ?>/facebook&amp;scope=<?php echo Config::get('social.facebook.scope') ?>"><i class="fa fa-facebook-square" style="font-size:20px"></i> Login with Facebook</a></p>
                  <p class="text-center"><a id="btn-linked-in" class="btn" role="button" href="http://www.lingoing.com/linkedin"><i class="fa fa-linkedin-square" style="font-size:20px"></i> Login with LinkedIn</a></p>
                </div>
              </div>
              <div class="text-center" id="login-or" style="margin:0;">OR</div>
              <div class="row">
                <div class="col-md-12 email-signup" style="background:none;">
                  <form method="post" action="<?php echo url('register') ?>" accept-charset="UTF-8" role="form" class="form-horizontal">
                    <div class="form-group">
                      <div class="col-sm-12">
                        <input class="form-control" placeholder="First name" id="first_name" name="first_name" type="text">
                      </div>
                    </div>
					<div class="form-group">
                      <div class="col-sm-12">
                        <input class="form-control" placeholder="Last name" id="last_name" name="last_name" type="text">
                      </div>
                    </div>
					<div class="form-group">
                      <div class="col-sm-12">
                        <input class="form-control" placeholder="Email address" id="email" name="email" type="text">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                        <input class="form-control" placeholder="Password" id="password" name="password" type="password" value="">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                        <input class="form-control" placeholder="Confirm password" id="confirm_password" name="confirm_password" type="password" value="">
                      </div>
                    </div>
					<div class="form-group">
                      <div class="col-sm-12">
                        <input class="form-control" placeholder="Business name (optional)" id="business_name" name="business_name" type="text" value="">
                      </div>
                    </div>
					<div class="form-group">
                      <div class="col-sm-12">
                        <input class="form-control" placeholder="Referral code (optional)" id="ref_code" name="ref_code" type="text" value="">
                      </div>
                    </div>
                    <div class="form-group" style="margin-bottom:10px;">
                      <div class="col-sm-12 account_types">
                        <input id="account_type_client" checked="checked" name="account_type" type="radio" value="client">
                        <label for="account_type_client" class="account_type font14">I'm looking to hire language professionals</label>
                        <br>
                        <input id="account_type_translator" name="account_type" type="radio" value="translator">
                        <label for="account_type_translator" class="account_type font14">I'm a language professional</label>
                        <br>
                        <!--<input id="account_type_agent" name="account_type" type="radio" value="agent"> <label for="account_type_agent" class="account_type">I&#039;m an agent</label>--> 
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12 terms">
                        <input id="terms_agreed" name="terms_agreed" type="checkbox" value="yes">
                        <label for="terms_agreed" class="account_type">I agree to Lingoing's <a href="/files/Lingoing Terms of Business Issued April 2014.pdf" title="Terms Of Business (click to read)">Terms Of Business (click to read)</a></label>
                      </div>
                    </div>
                    <div class="row line text-center">
						<?php echo Form::token(); ?>
                      <button class="btn btn-primary" role="button" type="submit" title="Create Account">Create Account</button>
                    </div>
                  </form>
                </div>
              </div>
              <!-- end content --> 

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mar0">
    <div class="col-md-12 head_textbox">
      <div class="container">

        <div class="cycle-slideshow" 
    data-cycle-fx="scrollHorz" 
    data-cycle-timeout="0"
    data-cycle-slides="> div"
    data-cycle-pager="#no-template-pager"
    data-cycle-pager-template=""
    >

          <div class="txt_slide">
            <h1 class="text-center font32 mar0">Clients</h1>
            <div class="martop20">
              <p>Lingoing is an online service aimed to support, connect and unite the deaf and hearing communities with fully qualified language professionals in the UK.  By registering on the platform you can quickly post a job request and get access to relevant matches in your local area.</p>
            </div>
          </div>

          <div class="txt_slide">
            <h1 class="text-center font32 mar0">Language professionals</h1>
            <div class="martop20">
              <p>Lingoing provides an attractive proposition to Language Service Professionals - its free to use, saves time, organises admin and bookings plus collect payment.</p>
            </div>
          </div>

        </div>

        <div class="txt_nav cycle-pager external" id=no-template-pager>
          <div class="txt_nav_btn" style="border-right:1px #aeaeae solid;"> Clients </div>
          <div class="txt_nav_btn"> Language professionals </div>
        </div>

      </div>
    </div>
  </div>
</div>
</div>

<div class="clearfix">
  <div class="col-md-12 club_section">
    <div class="container">
      <div class="row martop40 marbot20">
        <div class="col-sm-6 col-sm-offset-3">
          <div class="txt_center light font43 upper">The 100 Club</div>
        </div>
      </div>
      <div class="txt_center font14"> Register today and we will invite you to take part<br />in the ongoing development of our platform.
        <div class="row martop30 marbot30">
          <div class="col-sm-2 col-sm-offset-5"> <a href="<?php echo url('register') ?>">
            <div class="pink_btn">Sign up <span><img src="/img/white_arrow.png" /></span></div>
            </a> </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix">
  <div class="col-md-12 wayra_section">
    <div class="container pink_txt font60 bold upper">
      <div class="martop90" style="max-width:690px;"> We’re a start-up with a difference </div>
    </div>
    <div class="row">
      <div class="col-md-12 wayra_bottom">
        <div class="container">
          <div class="row">
            <div class="col-md-7">
              <div class="padtop20 padbot20"> Lingoing is a social enterprise start-up and one of the first digital companies to be selected as part of the government backed Wayra accelerator academy. On October 14th, 2014, Deputy Prime Minister Nick Clegg officially opened the new space and received a quick lesson in British Sign Language (BSL) from one of our founders, Saduf Naqvi. </div>
            </div>
            <div class="col-md-5">
              <div class="row">
                <div class="col-xs-4 padtop10 padbot10"> <img src="/img/wayra-logo.png" width="120" height="75" alt="wayra" /> </div>
                <div class="col-xs-4 padtop10 padbot10"> <img src="/img/unltd.png" width="187" height="54" alt="UnLtd" /> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix">
  <div class="col-md-12 team_section">
    <div class="container team_con">
      <div class="row martop20 marbot20">
        <div class="col-sm-6 col-sm-offset-3">
          <div class="txt_center bold font32 upper padbot20" style="border-bottom:2px solid #FFFFFF;">Our team</div>
        </div>
      </div>
      <div class="row martop30 marbot30">
        <div class="col-sm-4"> <img src="/img/sadaqat.png" width="246" height="315" alt="Sadaqat" /> </div>
        <div class="col-sm-8 martop30">
          <div class="font43 light marbot20">Sadaqat</div>
          <div>Sadaqat Ali is deaf and has worked with language professionals all his life. He understands the complexity of the market as well as the pitfalls so is ideally placed to identify the issues plus problem solve appropriate solutions. Sadaqat completed his degrees in Deaf Studies, Youth and Community, PGCE in 16+ teaching as well as British Sign Language.</div>
          <div class="martop30"><img src="/img/sadaqat-sign.png" alt="Sadaqat Sign" /></div>
        </div>
      </div>
      <div class="row martop30 marbot30">
        <div class="col-sm-8">
          <div class="font43 light marbot20">Saduf</div>
          <div>Saduf Naqvi is a trainee language professional, she understands the market from the inside including its problems and inconsistencies. She has extensive experience and insight into the needs and demands of deaf people gained from interaction with long- term clients.  Saduf ran an interpreting agency so she can identify the gaps in the market that this online solution will meet including providing interpreters, fair pay for language professionals, reducing costs, and provision of data management for users.  Saduf completed her degree in Computing and Information Systems, and a PhD in sign language technologies and is on the final level towards qualifying to become a British Sign Language interpreter</div>
          <div class="martop30"><img src="/img/saduf-sign.png" width="322" height="56" alt="Saduf Sign" /></div>
        </div>
        <div class="col-sm-4"> <img src="/img/saduf.png" width="246" height="315" alt="Saduf" /> </div>
      </div>
    </div>
  </div>
</div>

<?php echo View::make('frontend.static.partial.get_in_touch')->render(); ?>