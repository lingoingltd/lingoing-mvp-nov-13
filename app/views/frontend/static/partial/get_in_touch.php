<div class="clearfix darkgrey_bg">
	<div class="col-md-4" style="padding:0; height:350px;">
		<div class="address_con">
			<div class="font32 xlight martop30 upper marbot20">Get in touch</div>
			<div class="martop30"> <span class="bold font16">Phone:</span><br /><span class="font20">+44 (0)20 7084 6373</span> </div>
			<div class="martop30"> <span class="bold font16">Address:</span><br /><span class="font20">Wayra Shropshire House<br />2-10 Capper St<br />London, WC1E 6JA</span> </div>
		</div>
		<div class="footer_social">
			<div class="address_con">
				<fb:like href="https://www.facebook.com/lingoingltd" width="200" layout="button_count" action="like" show_faces="false" share="false"></fb:like>
				<a href="https://twitter.com/share" class="twitter-share-button" data-via="lingoingltd">Tweet</a> 
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script> 
			</div>
		</div>
	</div>
	<div class="col-md-8 map_con">
		<iframe scrolling="no" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2482.490983250042!2d-0.13545945!3d51.52255389999999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761b2ed4bd8f71%3A0xc72fd77b6c4b5c6e!2sWayra!5e0!3m2!1sen!2suk!4v1393929614218" width="100%" height="350" frameborder="0" style="border:0; position:absolute;"></iframe>
	</div>
</div>