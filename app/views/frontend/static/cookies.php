<div class="container padbot30">
	<div class="row martop50">
		<div class="col-md-12">
			<div class="font38 light blue_txt marbot30">Cookies Notice</div>
		</div>
	</div>
   
	<div class="row marbot30">
		<div class="col-md-12">
			<div class="font14 grey_txt">
				<p>When you create or log in to an online account on our website we advise you of our use of cookies and provide the advice and option for you to leave the site if you do not agree to the use of cookies. If you agree to continue using this website you should be aware that we may collect information by using 'cookies'. The use of cookies is as described in this cookies notice.</p>
				<p>Cookies are pieces of text that are downloaded to your computer when you visit a website and the majority of websites use them. Your browser communicates to the website via these cookies each time you visit the site in order to recognise you and manage what you see on the screen.</p>
				<p>There are different uses for cookies, but they fall into the following main groups:</p>
				<ul>
					<li>Essential cookies that enable you to move around the website and access its features. For example some are used to keep you logged in during your visit. These types of cookies do not collect information for marketing purposes and don’t record where you've been on the internet.</li>
					<li>Cookies to improve your browsing experience, for example remembering preferences and settings and determining whether you are a first time visitor or a returning visitor.</li>
					<li>Analytic cookies – these are used to collect information about how people use the website. They do not collect information that may identify you, rather they are identify trends of website usage across visitors.</li>
					<li>Advertising cookies – we may place adverts on our website and these adverts themselves often use cookies which are used to collect information regarding your response to posted adverts. The advertiser uses the browsing information collected from these cookies to collect information about your browsing. This data is not linked to you as a person.</li>
				</ul>
				<p>The management of cookies varies from browser to browser but if you have concerns about the use of different types of cookies you should visit your browser help file for assistance with managing cookies via your selected browser.</p>
			</div>
		</div>
	</div>
</div> 

<?php echo View::make('frontend.static.partial.get_in_touch')->render(); ?>