<h1>Recruitment</h1>

<div class="col-md-3">
	<?php echo Form::open(array('url' => '/recruitments', 'method' => 'post', 'role' => 'form')); ?>
	<div class="white-container search-container">
		<p style="padding-top:10px;"><strong>Jobs available</strong></p>
		<div>
			<label class="list-item">
				<?php echo Form::radio('job_type', 'all', isset($filter) && isset($filter['job_type']) ? $filter['job_type'] == 'all' : true, array('id' => 'job_type_all')) ?>
				All careers
			</label>
		</div>
		<div>
			<label class="list-item">
				<?php echo Form::radio('job_type', 'part_time', isset($filter) && isset($filter['job_type']) ? $filter['job_type'] == 'part_time' : false, array('id' => 'job_type_part_time')) ?>
				Part time
			</label>
		</div>
		<div>
			<label class="list-item">
				<?php echo Form::radio('job_type', 'full_time', isset($filter) && isset($filter['job_type']) ? $filter['job_type'] == 'full_time' : false, array('id' => 'job_type_full_time')) ?>
				Full time
			</label>
		</div>
		<div>
			<label class="list-item">
				<?php echo Form::radio('job_type', 'volunteer', isset($filter) && isset($filter['job_type']) ? $filter['job_type'] == 'volunteer' : false, array('id' => 'job_type_volunteer')) ?>
				Volunteer
			</label>
		</div>
		<p class="text-center" style="margin-top:20px;">
			<button type="submit" name="search" class="btn btn-primary text-center"><span class="glyphicon glyphicon-search"></span> Filter</button>
			<button type="submit" name="show_all" class="btn btn-default text-center">Show all</button>
		</p>
	</div>
	<?php echo Form::close(); ?>
</div>

<div class="col-md-9">
	<?php if(count($recruitments) > 0): ?>
		<?php foreach($recruitments as $recruitment): ?>
			<div class="job-container border-all">
				<div class="job-client clearfix">
					<p class="lead"><a href="<?php echo url('recruitment') ?>/<?php echo $recruitment->id ?>"><?php echo $recruitment->title; ?></a></p>
				</div>
				
				<div class="job-title border-bottom-gray">
					<div class="pull-left col-md-8">
						<?php echo nl2br($recruitment->short_description); ?>
					</div>
					<div class="pull-right col-md-4 text-right">
						<a class="btn btn-primary" href="<?php echo url('recruitment') ?>/<?php echo $recruitment->id ?>">See more</a>
					</div>
					<div class="clearfix"></div>
				</div>
				
				<div class="job-details clearfix">
					<div class="col-md-4"><strong>DATE POSTED:</strong> <?php echo date('d/m/Y', strtotime($recruitment->created_at)); ?></div>
					<div class="col-md-4"><strong>JOB TYPE:</strong> <?php echo Recruitment::listJobTypes($recruitment->job_type); ?></div>
					<div class="col-md-4 clearfix"><strong>SALARY:</strong> <?php echo $recruitment->salary ?></div>
					<div class="col-md-4"><strong>LOCATION:</strong> <?php echo $recruitment->location; ?></div>
				</div>
			</div>
		<?php endforeach; ?>
	<?php else: ?>
		<p class="text-center">We are not hiring right now</p>
	<?php endif; ?>
</div>