<h1>Recruitment</h1>

<?php if(Session::has('recruitment_error')): ?>
	<div class="alert alert-danger"><strong>Ups!</strong> <?php echo Session::get('recruitment_error'); ?></div>
<?php elseif(Session::has('recruitment_success')): ?>
	<div class="alert alert-success"><strong>Congrats!</strong> <?php echo Session::get('recruitment_success'); ?></div>
<?php endif; ?>

<div class="col-md-9 clearfix">
	<div class="job-container border-all">
		<div class="job-client clearfix">
			<p class="lead"><a href="<?php echo url('recruitment') ?>/<?php echo $recruitment->id ?>"><?php echo $recruitment->title; ?></a></p>
		</div>
		
		<div class="job-details clearfix">
			<div class="col-md-4"><strong>DATE POSTED:</strong> <?php echo date('d/m/Y', strtotime($recruitment->created_at)); ?></div>
			<div class="col-md-4"><strong>JOB TYPE:</strong> <?php echo Recruitment::listJobTypes($recruitment->job_type); ?></div>
			<div class="col-md-4 clearfix"><strong>SALARY:</strong> <?php echo $recruitment->salary; ?></div>
			<div class="col-md-4"><strong>LOCATION:</strong> <?php echo $recruitment->location; ?></div>
		</div>
		
		<div class="job-details clearfix">
			<div class="col-md-12">
				<?php echo nl2br($recruitment->description); ?>
			</div>
		</div>
	</div>
</div>

<?php echo Form::open(array('url' => '/recruitment/' . $recruitment->id, 'method' => 'post', 'role' => 'form', 'files' => true)); ?>
<div class="col-md-9 clearfix">
	<h4>Apply for the job</h4>
	<div class="job-container border-all" style="padding:15px 0;">
		<div class="clearfix line">
			<div class="col-md-2">
				<?php
					$field = 'name';
					$field_errors = $errors->get($field);
				?>
				<?php echo Form::label($field, 'Name', array('class' => 'control-label')) ?> <span class="required">*</span>
			</div>
			<div class="col-md-4">
				<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => 'John Doe', 'id' => $field)); ?>
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<div class="col-md-2">
				<?php
					$field = 'email';
					$field_errors = $errors->get($field);
				?>
				<?php echo Form::label($field, 'Email', array('class' => 'control-label')) ?> <span class="required">*</span>
			</div>
			<div class="col-md-4">
				<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => 'john.doe@domain.com', 'id' => $field)); ?>
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="clearfix line">
			<div class="col-md-2">
				<?php
					$field = 'address';
					$field_errors = $errors->get($field);
				?>
				<?php echo Form::label($field, 'Address', array('class' => 'control-label')) ?> <span class="required">*</span>
			</div>
			<div class="col-md-4">
				<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => 'Some street 123', 'id' => $field)); ?>
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<div class="col-md-2">
				<?php
					$field = 'phone';
					$field_errors = $errors->get($field);
				?>
				<?php echo Form::label($field, 'Phone', array('class' => 'control-label')) ?> <span class="required">*</span>
			</div>
			<div class="col-md-4">
				<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => '(123) 1234 1234', 'id' => $field)); ?>
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="clearfix line">
			<div class="col-md-2">
				<?php
					$field = 'answer';
					$field_errors = $errors->get($field);
				?>
				<?php echo Form::label($field, 'Our Question to You', array('class' => 'control-label')) ?> <span class="required">*</span>
			</div>
			<div class="col-md-4">
				<p><?php echo $recruitment->question ?></p>
				<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => 'Your answer', 'id' => $field, 'maxlength' => 255)); ?>
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<div class="col-md-2">
				<?php
					$field = 'cv';
					$field_errors = $errors->get($field);
				?>
				<?php echo Form::label($field, 'CV', array('class' => 'control-label')) ?> <span class="required">*</span>
			</div>
			<div class="col-md-4">
				<?php echo Form::file($field, Input::old($field), array('class' => 'form-control', 'id' => $field)); ?>
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
		
		<div class="col-md-12 line text-right">
			<button type="submit" name="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
</div>
<?php echo Form::close(); ?>