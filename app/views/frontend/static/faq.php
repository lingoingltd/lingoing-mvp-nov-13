<div class="container padbot30">
	<div class="row martop50">
		<div class="col-md-12">
			<div class="font38 light blue_txt marbot30">FAQ</div>
		</div>
	</div>

	<div class="row marbot30">
		<div class="col-md-7">
			<div class="font14 grey_txt">
				
				<div class="blue_txt text-center font24">Service Levels</div>

				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">What and who is Lingoing? Can I be sure that they fully understand the needs of interpreters and the needs of their clients?</span>
					<p>Lingoing, by providing this online service is motivated wholly by a desire to improve the cost effectiveness and the efficiency of providing interpreting services. Lingoing has been set up by a group of people with extensive experience in the interpreting market. As users of language services and trained as language professionals, they have examined the market from both sides: the client and the language service professionals (LSPs). A key aim of the Lingoing process is to create an informed approach to interpreting needs to make it easy for language service professionals and their clients to find and communicate with each other, always in the most cost effective way. See “<a href="<?php echo url('about-us') ?>">about us</a>” for more details.</p>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">What is the standard of those providing services via Lingoing?</span>
					<p>All (LSPs) registered to provide services are fully qualified in one or more fields of interpreting and hold recognised certification. Lingoing makes careful checks with membership bodies to ensure that LSPs hold relevant certification.</p>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">I am classified as a vulnerable person/work with vulnerable people. Can I be confident that my/their needs will be respected?</span>
					<p>This is a key concern for Lingoing and an area where sometimes too little attention is paid towards those with special needs. Lingoing vets the LSP qualifications carefully and ensures that matters such as CRB certifications are held to the correct level. If you don’t have a CRB you can apply via Lingoing with a CRB agency.</p>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">How can I be sure my LSP will be qualified for the specialist type of service I need?</span>
					<p>All LSPs registered to provide services by Lingoing hold memberships with governing bodies. This means that LSPs are responsible and aware of the ethics involved before presenting their suitability for an assignment. In addition to this, Lingoing can help validate their suitability to work within specific fields such as medical or legal work. Lingoing provides a very broad range of specialisations and the languages and disciplines covered will continue to grow. If there is something specific that you need let us know and we’ll find it for you.</p>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">What if I wish to cancel my registration?</span>
					<p>There is no minimum period for a client or LSP to engage with Lingoing. In the event you wish to deregister simply go to your personal account page. Lingoing retains no personal details other than those associated with work in progress or details required for statutory purposes.</p>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">What if I decide Lingoing isn't for me?</span>
					<p>We are confident that you will be very satisfied with the services available from Lingoing. Nevertheless, if for any reason you are not satisfied we would like to hear from you and, of course, you remain free to cancel your registration at any time.</p>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">What information about me is held by Lingoing and who has access to it?</span>
					<p>For LSPs we maintain records detailing qualifications and categorisation levels as well as the usual contact details. For clients we maintain records of names and addresses. All records are held in a secure password protected environment. Financial information such as credit card details are not retained by Lingoing. All such transactions are processed externally to the main Lingoing site in a fully encrypted environment operated by the FSA regulated payment processing organisation.</p>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">How do I get feedback from clients?</span>
					<p>Lingoing encourages all users of the site to record their experience and levels of satisfaction with feedback contributing to the record of the particular LSP to create a star rating system. Lingoing wants to work with the best LSPs in the market and actively encourages feedback to support those high standards, ensuring also that LSPs get the acknowledgment that they deserve.<br><br>
					So please do let us know what you think. And if you’re not happy and prefer to contact Lingoing directly you are welcome to do so in the knowledge that Lingoing will share constructive criticism among the community to help us all to reach our full potential.</p>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">I am an LSP. What if I have a booking and on the day, I can't make it?</span>
					<p>Lingoing expects the highest standards of service from LSPs but we recognise that unforeseen issues may sometimes arise. It is imperative that the LSP provides notice directly to the client and works with the client to arrange an alternative date or to provide an alternative service provider. Where a substitute service provider is nominated it is expected that the agreed fee structure, in all but the most unusual of circumstances, be maintained. If there is sufficient time interval it may be possible, with the client’s agreement, for the client to re-post the assignment. Working together in this way means we can improve the marketplace connections and ensure better support for all of us.</p>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">I am a client. What if I need to cancel or change my booking?</span>
					<p>In the first instance Lingoing would expect the client and LSP to contact each other and ideally, arrange an alternative date and time. Clients should be aware that a short notice or an outright cancellation by the client may be subject to a cancellation fee as a proportion of the overall fee, subject to the notice provided.</p>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">What if I leave the members organisation that I currently belong to? And how will Lingoing know?</span>
					<p>Lingoing maintains contact with member organisations to maintain up to date records. However as a professional service provider it is expected that the LSP will keep Lingoing advised of changes to their credentials if and when they arise.</p>
				</div>
				
				<div class="faq-title blue_txt text-center font24">Payments</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">As a client, how and when do I pay for the services?</span>
					<p>This depends on the type of assignment. The normal process for a “one off” assignment would be to take a secure credit card payment for the anticipated fee at the time of assignment award. This payment is held by Lingoing and is subject to the satisfactory sign off by the client before release to the LSP. For repeat assignments the usual process is to establish a direct debit which would be drawn down against monthly invoices over the period of the assignments.</p>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">I am a LSP, How do I get paid?</span>
					<p>The client and LSP agree the nature of the work required and agree an appropriate fee. On completion of the work the LSP will present the final billing amount to the client via the Lingoing site. This will match the assignment completion certificate signed by the client which will include any additional amount agreed in writing by the parties. On notification of acceptance by the client Lingoing will take payment from the client and forward payment to the LSP less the agreed commission.</p>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">When do I get paid?</span>
					<p>Once the assignment is completed the LSP will advise the client via a completion notice which will include agreed charges. The client is required to approve or query the completion within seven days. In the absence of a response from the client the completion notice will be deemed as approved on the seventh day after issue. Client approved assignments will be invoiced at the earliest of two invoicing periods per month with payment due on presentation of the invoice. The LSP will be paid by Lingoing within three days of receipt of funds from the client.</p>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">Can I charge for extras over and above the quoted fee?</span>
					<p>Yes there is full provision to charge for agreed extras – additional hours, additional travel etc. This is detailed on the assignment completion sheet to be signed off between client and LSP and which forms the basis of the final client approval.</p>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">As a language service professional providing services via Lingoing do I have to be registered for VAT?</span>
					<p>This is not a Lingoing requirement and depends entirely on your own business setup. For those who are VAT registered Lingoing will need details of the VAT registration and invoicing will be processed in accordance with HMRC VAT requirements.</p>
				</div>
				
				<div class="faq-title blue_txt text-center font24">Disputes</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">What if an LSP has a complaint about a client or a client about an LSP?</span>
					<p>Normally we would expect that in the first instance the parties would deal directly with each other in an effort to resolve any disputes. In our experience most of these arise from misunderstandings about arrangements and are usually resolved by reasonableness on each side. However Lingoing has a dispute resolution process which is available to clients and to LSPs in circumstances where differences cannot be resolved directly between the parties. As a normal course of events, once engaged, Lingoing would expect both parties to be bound by the outcome of the dispute resolution process.</p>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="break"></div>
					</div>
				</div> 

				<div>
					<span class="bold font18">What if I have a complaint about Lingoing?</span>
					<p>If at any time you are dissatisfied or concerned about any of the Lingoing processes or actions of course we want to hear from you without delay. Lingoing is committed to the highest levels of service to both clients and to LSPs and undertakes to abide scrupulously to the terms of business.</p>
				</div>
			</div>
		</div>
		
		<div class="col-md-4 col-md-offset-1">
			<form method="post" action="">
				<div class="font20 xlight martop30 upper marbot10"><label for="">Your email address</label></div>
				<div id="email_address_container" class="form_box_container">
					<?php echo Form::text('email_address', '', array('class' => 'form_box', 'id' => 'email_address')); ?>
					<div class="text-danger hidden"></div>
				</div>
				
				<div class="font20 xlight upper marbot10"><label for="">your question</label></div>
				<div id="your_question_container" class="form_box_container">
					<?php echo Form::textarea('your_question', '', array('class' => 'form_box', 'id' => 'your_question')) ?>
					<div class="text-danger hidden"></div>
				</div>
				
				<input type="submit" class="pink_btn light send-faq-question" style="background:none;" value="Send question" />
			</form>
			<p class="lead text-center hidden">Your messages has been sent!</p>
		</div>
	</div>
</div> 

<?php echo View::make('frontend.static.partial.get_in_touch')->render(); ?>