
<div class="container padbot30">
   
   <div class="row martop50 marbot30">
   		<div class="col-md-12">
        <div class="font38 light blue_txt marbot30">Contact us</div>
        <div class="font14 grey_txt">
       
       	<div class="row">
       		<div class="col-md-6">
            <div class="padright20">
<p>At Lingoing we believe that every voice has the right to be heard.  It's why we created an online platform that connects the community to qualified language professionals in a no fuss, value for money, simple to use way.</p>

<p>We would love to hear from you if you have any questions, problems or feedback.</p>


<p class="martop20">
<strong>Phone:</strong><br />
+44 (0)20 7084 6373
</p>

<p>
<strong>Address:</strong><br />
Wayra Shropshire House<br />
2-10 Capper St<br />
London, WC1E 6JA
</p>
			</div>
       		</div>
            <div class="col-md-6">
	            <form method="post" action="">
					<div class="bold font18"><label for="name">Name<label></div>
					<div id="name_container" class="form_box_container">
						<?php echo Form::text('name', '', array('class' => 'form_box', 'id' => 'name')); ?>
						<div class="text-danger hidden"></div>
					</div>
					
					<div class="bold font18"><label for="email_address">Email</label></div>
					<div id="email_address_container" class="form_box_container">
						<?php echo Form::text('email_address', '', array('class' => 'form_box', 'id' => 'email_address')); ?>
						<div class="text-danger hidden"></div>
					</div>
					
					<div class="bold font18"><label for="message">Message</label></div>
					<div id="message_container" class="form_box_container">
						<?php echo Form::textarea('message', '', array('class' => 'form_box', 'id' => 'message')) ?>
						<div class="text-danger hidden"></div>
					</div>
					<input type="submit" value="Send message" style="background:none;" class="pink_btn light send-contact-us" />
	            </form>
				<p class="lead text-center hidden">Your messages has been sent!</p>
       		</div>
       </div>
        
        </div>
       </div>
   </div>
   
   </div> 

<?php echo View::make('frontend.static.partial.get_in_touch')->render(); ?>