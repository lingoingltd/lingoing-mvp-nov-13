<div class="container padbot30">
	<div class="row martop50">
		<div class="col-md-12">
			<div class="font38 light blue_txt marbot30">Feedback</div>
		</div>
	</div>

	<div class="row marbot30">
		<div class="col-md-7">
			<div class="font14 grey_txt">
				<p>In our continuous effort to improve our site, we look forward to your thoughts, questions and feedback on how we can better tailor the features to your needs.</p>
				<p>We are keen to co-create and co-design this space with our community to make this something that can exceed our expectations.</p>
				<p>Feel free to get in touch, we would love to hear from you.</p>
			</div>
		</div>
		
		<div class="col-md-4 col-md-offset-1">
			<form method="post" action="">
				<div class="font20 xlight upper marbot10"><label for="">Your name</label></div>
				<div id="name_container" class="form_box_container">
					<?php echo Form::text('name', '', array('class' => 'form_box', 'id' => 'name')); ?>
					<div class="text-danger hidden"></div>
				</div>
				
				<div class="font20 xlight martop30 upper marbot10"><label for="">Your email address</label></div>
				<div id="email_address_container" class="form_box_container">
					<?php echo Form::text('email_address', '', array('class' => 'form_box', 'id' => 'email_address')); ?>
					<div class="text-danger hidden"></div>
				</div>
				
				<div class="font20 xlight upper marbot10"><label for="">feedback</label></div>
				<div id="feedback_container" class="form_box_container">
					<?php echo Form::textarea('feedback', '', array('class' => 'form_box', 'id' => 'feedback')) ?>
					<div class="text-danger hidden"></div>
				</div>
				
				<input type="submit" class="pink_btn light send-feedback" style="background:none;" value="Send feedback" />
			</form>
			<p class="lead text-center hidden">Your messages has been sent!</p>
		</div>
	</div>
</div> 

<?php echo View::make('frontend.static.partial.get_in_touch')->render(); ?>