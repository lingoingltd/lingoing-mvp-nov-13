<div class="container padbot30">

   <div class="row martop50 marbot30">
   		<div class="col-md-12">
        <div class="font38 light blue_txt marbot30">About us</div>
        <div class="font14 grey_txt">
	
			<p><strong>Vision:</strong> Making every voice heard</p>
			<p><strong>Purpose:</strong> Remove language barriers that often lead to isolation, exclusion and poverty in order for individuals, families and communities to feel visible, valued, connected and heard.</p>
			<p><strong>Mission:</strong> To do this by uniting and connecting a global community of language professionals and clients via an interactive online platform that is transparent, useful and simple-to-use.</p>
			<p><strong>Social Aims:</strong></p>
			<ol>
				<li>To represent our communities who experience language as a barrier</li>
				<li>To ensure our community is always fully informed of the level of qualification our interpreters have in the market</li>
				<li>To ensure our interpreters are supported and to encourage a fair rate of pay</li>
				<li>To ensure our clients are charged reasonably within the market and can make informed choices</li>
				<li>To support agents on the ground to use the platform and network to connect language professionals to jobs and vice versa</li>
			</ol>
        </div>
       </div>
   </div>

</div> 

<div class="clearfix">
	<div class="col-md-12 team_section">
		<div class="container team_con">
			<div class="row martop20 marbot20">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="txt_center bold font32 upper padbot20" style="border-bottom:2px solid #FFFFFF;">Our team</div>
				</div>
			</div>
			
			<div class="row martop30 marbot30">
				<div class="col-sm-4"> <img src="/img/sadaqat.png" width="246" height="315" alt="Sadaqat" /> </div>
				<div class="col-sm-8 martop30">
					<div class="font43 light marbot20">Sadaqat</div>
					<div>Sadaqat Ali is deaf and has worked with language professionals all his life. He understands the complexity of the market as well as the pitfalls so is ideally placed to identify the issues plus problem solve appropriate solutions. Sadaqat completed his degrees in Deaf Studies, Youth and Community, PGCE in 16+ teaching as well as British Sign Language.</div>
					<div class="martop30"><img src="/img/sadaqat-sign.png" alt="Sadaqat Sign" /></div>
				</div>
			</div>
			
			<div class="row martop30 marbot30">
				<div class="col-sm-8">
					<div class="font43 light marbot20">Saduf</div>
					<div>Saduf Naqvi is a trainee language professional, she understands the market from the inside including its problems and inconsistencies. She has extensive experience and insight into the needs and demands of deaf people gained from interaction with long- term clients.  Saduf ran an interpreting agency so she can identify the gaps in the market that this online solution will meet including providing interpreters, fair pay for language professionals, reducing costs, and provision of data management for users.  Saduf completed her degree in Computing and Information Systems, and a PhD in sign language technologies and is on the final level towards qualifying to become a British Sign Language interpreter</div>
					<div class="martop30"><img src="/img/saduf-sign.png" width="322" height="56" alt="Saduf Sign" /></div>
				</div>
				<div class="col-sm-4"> <img src="/img/saduf.png" width="246" height="315" alt="Saduf" /> </div>
			</div>
			
			<div class="row martop20 marbot20">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="txt_center bold font32 upper padbot20" style="border-bottom:2px solid #FFFFFF;"></div>
				</div>
			</div>
			
			<div class="row martop30 marbot30">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="text-center"><img src="/img/fabio59.png" alt="Fabio LaFranca" width="250" height="248"></div>
					<div class="font43 light text-center bio-name">Fabio LaFranca</div>
					<div class="text-center bio-pos">CFO and Advisor</div>
					<div class="bio">Fabio brings more than a decade of finance experience to Lingoing where, as CFO, he is responsible for all strategic financial decisions. Fabio has an MBA from the University of Exeter and qualified as an ACA while working for PWC in London prior to joining Lingoing.</div>
				</div>
				<div class="col-sm-4 col-sm-offset-2">
					<div class="text-center"><img src="/img/vojko_voga.png" alt="Vojko Voga" width="246" height="248"></div>
					<div class="font43 light text-center bio-name">Vojko Voga</div>
					<div class="text-center bio-pos">Lead programmer</div>
					<div class="bio">I began working as Web developer almost 10 years ago, based in Slovenia I have worked with a range of projects.  Specializing in PHP and MySQL, I have vaste experience in building transactional systems and am enjoying the Lingoing journey with the team.  I joined the team in November 2013 as the lead programmer, helping bring the team vision to the public and the aspirations of the founders.</div>
				</div>
			</div>
			
			<div class="row martop20 marbot20">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="txt_center bold font32 upper padbot20" style="border-bottom:2px solid #FFFFFF;"></div>
				</div>
			</div>
			
			<div class="row martop30 marbot30">
				<div class="col-sm-4">
					<div class="text-center"><img src="/img/jinea_bw.png" alt="Jinea McFarlane" width="248" height="248"></div>
					<div class="font43 light text-center bio-name">Jinea McFarlane</div>
					<div class="text-center bio-pos">Admin Intern</div>
					<div class="bio">Hello, my name is Jinea and I am the administration intern at Lingoing. My role requires a lot of organisation and planning, written and spoken communication. Having recently graduated with a BA Hons in Human resource management and with previous experience in sectors such as sales, retail, administration and human resources, I have been able to further develop my skills in customer service, administration and PR and at Lingoing although my role has allowed me to cover a whole range of areas which is what makes working here so interesting and I really enjoy being a part of the Lingoing team.</div>
				</div>
				<div class="col-sm-4">
					<div class="text-center"><img src="/img/ovaisBW.png" alt="Ovais Khan" width="248" height="248"></div>
					<div class="font43 light text-center bio-name">Ovais Khan</div>
					<div class="text-center bio-pos">User Experience and Graphics Design</div>
					<div class="bio">Hello, my name is Ovais, I oversee the social media function at Lingoing, I have a BA (Hons) in graphic design as well as an extensive understanding of user experience design. Since working with Lingoing, I have been given the opportunity to further expand my skills and engage with audiences through social media. I enjoy working with others and consider myself to be very approachable and work well within a team of people. I enjoy challenges and coming up with new ideas and solutions, which Lingoing has allowed me to do.</div>
				</div>
				<div class="col-sm-4">
					<div class="text-center"><img src="/img/abi_bw.png" alt="Adi Plahar" width="248" height="248"></div>
					<div class="font43 light text-center bio-name">Adi Plahar</div>
					<div class="text-center bio-pos">Book Keeping</div>
					<div class="bio">Hello, my name is Adi and I oversee the finance function at Lingoing, I am deaf myself and am very aware of the disadvantageous issues affecting users of the language experience through my first-hand experience in regards to my budget. I was very interested in Lingoing’s concept and wanted to be involved, as a member of the Lingoing team I bring skills such as auditing, business administration and financial budgeting and my interpersonal skills such as team work has helped me become a fully integrated member of the Lingoing team. Outside of work I am an avid gym goer and my interest fitness, nutrition and healthy living.</div>
				</div>
			</div>
			
			<div class="row martop20 marbot20">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="txt_center bold font32 upper padbot20" style="border-bottom:2px solid #FFFFFF;"></div>
				</div>
			</div>
			
			<div class="row martop30 marbot30">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="text-center"><img src="/img/nick_magliocchetti.png" alt="Nick Magliocchetti" width="248" height="248"></div>
					<div class="font43 light text-center bio-name">Nick Magliocchetti</div>
					<div class="text-center bio-pos">Advisor</div>
					<div class="bio">Nick enjoys investing in Talent. Having turned around the fortunes of numerous businesses over the past 10 years, his focus today is all about adding value to companies at various stages of their development world wide.  Nick has been an advisor and mentor to The Princes Trust for the past 7 years and also works with London based Charities Homeless Link and Virgin Unite.</div>
				</div>
				<div class="col-sm-4 col-sm-offset-2">
					<div class="text-center"><img src="/img/esther_foreman.png" alt="Esther Foreman" width="248" height="248"></div>
					<div class="font43 light text-center bio-name">Esther Foreman</div>
					<div class="text-center bio-pos">Advisor</div>
					<div class="bio">Esther is a Director of the Social Change Agency, a systemic change innovation agency, dedicated to supporting, guiding and working with organisations, people and boards to bring about positive social change. Esther specialises in designing and building movements of people as well as coaching social change agents. She has over 15 years experience in the not for profit area, working for large charities, social enterprises and universities and has a policy background in disability rights and social justice.</div>
				</div>
			</div>
			
			<div class="row martop30 marbot30">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="text-center"><img src="/img/louise_fellows.png" alt="Louise Fellows" width="248" height="248"></div>
					<div class="font43 light text-center bio-name">Louise Fellows</div>
					<div class="text-center bio-pos">Advisor</div>
					<div class="bio">With more than 12 years’ experience in the Telecoms industry, and 8 fantastic years at Nortel I moved to O2 where I have account managed and serviced some of O2’s Top 44 Retail & Service accounts.  In 2010 I co-founded The Local Government Futures Forum which was a ground breaking transformation programme for Local Government is part of the Telefonica Public Sector strategy over the next 3 years.  I am pivotal in helping SME’s on their digital growth journey by helping them embrace the digital world.</div>
				</div>
				<div class="col-sm-4 col-sm-offset-2">
					<div class="text-center"><img src="/img/jonathan_may.png" alt="Jonathan May" width="248" height="248"></div>
					<div class="font43 light text-center bio-name">Jonathan May</div>
					<div class="text-center bio-pos">Advisor</div>
					<div class="bio">Jonathan is a social entrepreneur, and the CEO and founder of hubbub, the movement powering the future of education. He was a member of the 2013 WayraUK cohort, and also a winner of the £100k Big Venture Challenge run by UnLtd. Originally from a technology background, he has previously worked on projects as diverse as semiconductor verification and table football import and distribution.</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<?php echo View::make('frontend.static.partial.get_in_touch')->render(); ?>