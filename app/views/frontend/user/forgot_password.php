<h1>I forgot my password</h1>

<div class="col-md-4 col-md-offset-4 email-signup">
	<?php echo Form::open(array('action' => 'FrontPageController@postForgotPassword', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')); ?>

	<div class="form-group">
		<p><small>Insert your email address and we will send you new password</small></p>
		<div class="col-sm-12">
			<?php 
			$errs = $errors->get('email');
		
		 	$class = 'form-control';
			if(count($errs) > 0)
				$class .= ' error-form-control';
			echo Form::text('email', Input::old('email'), array('class' => $class, 'placeholder' => 'Email address ...', 'id' => 'email')); ?>
		
			<?php if(Session::has('error')): ?>
				<div class="text-left text-danger control-error"><?php echo trans(Session::get('reason')) ?></div>
			<?php else: ?>
				<?php foreach ($errs as $message): ?>
					<div class="text-left text-danger control-error"><?php echo $message; ?></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="row line text-center">
		<input class="btn btn-primary" role="button" type="submit" value="Request new password">
	</div>
	
	<?php echo Form::close(); ?>
</div>