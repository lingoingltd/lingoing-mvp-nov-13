<h1>Reset your password</h1>

<div class="col-md-4 col-md-offset-4 email-signup">
<?php echo Form::open(array('action' => array('FrontPageController@postPasswordReset', $token), 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')); ?>
	<input type="hidden" name="token" value="<?php echo $token ?>">
	
	<?php if(Session::has('error')): ?>
		<div class="text-left text-danger"><?php echo trans(Session::get('reason')) ?></div>
	<?php endif; ?>
	
	<p><small>Insert your email address and new password</small></p>

	<div class="form-group">
		<div class="col-sm-12">
			<?php echo Form::text('email', Input::old('email'), array('class' => 'form-control green-border', 'placeholder' => 'Email address ...', 'id' => 'email')); ?>
			
			<?php foreach ($errors->get('email') as $message): ?>
				<div class="text-left text-danger"><?php echo $message; ?></div>
			<?php endforeach; ?>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-12">
			<?php echo Form::password('password', array('class' => 'form-control green-border', 'placeholder' => 'New password ...', 'id' => 'password')); ?>
			
			<?php foreach ($errors->get('password') as $message): ?>
				<div class="text-left text-danger"><?php echo $message; ?></div>
			<?php endforeach; ?>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-12">
			<?php echo Form::password('password_confirmation', array('class' => 'form-control green-border', 'placeholder' => 'Confirm password ...', 'id' => 'password_confirmation')); ?>
			
			<?php foreach ($errors->get('password_confirmation') as $message): ?>
				<div class="text-left text-danger"><?php echo $message; ?></div>
			<?php endforeach; ?>
		</div>
	</div>
	
	<div class="row line text-center">
		<input class="btn btn-primary" role="button" type="submit" value="Reset my password">
	</div>
<?php echo Form::close(); ?>
</div>