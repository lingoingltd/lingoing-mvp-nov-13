<h1>I forgot my password</h1>
<p class="text-center">We have send you an email message to reset your password.</p>
<p class="text-center"><strong>Please note:</strong> Reset link we just send you is active <strong>24 hours (<?php echo \Carbon\Carbon::now()->format('d/m/Y H:i:s'); ?>)</strong>. Then you have to request a new one.</p>