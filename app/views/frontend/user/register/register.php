<div class="row">
	<div class="col-md-12 text-center">
		<h1>Register with Lingoing for free</h1>
	</div>
</div>
<div class="row">
	<div class="col-md-4 col-md-offset-4 social-login">
		<p class="text-center" id="quickest-way">The quickest ways to sign-up.</p>
		<p class="text-center"><a id="btn-facebook" class="btn" role="button" href="https://www.facebook.com/dialog/oauth?client_id=<?php echo Config::get('social.facebook.app_id') ?>&amp;redirect_uri=http://<?php echo $_SERVER['HTTP_HOST'] ?>/facebook&amp;scope=<?php echo Config::get('social.facebook.scope') ?>"><i class="fa fa-facebook-square" style="font-size:20px"></i> Login with Facebook</a></p>
		<p class="text-center"><a id="btn-linked-in" class="btn" role="button" href="<?php echo action('FrontPageController@getLinkedin') ?>"><i class="fa fa-linkedin-square" style="font-size:20px"></i> Login with LinkedIn</a></p>
	</div>
</div>
<p class="text-center" id="login-or">OR</p>
<div class="row">
	<div class="col-md-4 col-md-offset-4 email-signup">
		<?php echo Form::open(array('action' => 'FrontPageController@postRegister', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')); ?>

			<div class="form-group">
				<div class="col-sm-12">
					<?php
						$field = 'first_name';
						$errs = $errors->get($field);
					
					 	$class = 'form-control';
						if(count($errs) > 0)
							$class .= ' error-form-control';
						echo Form::text($field, Input::old($field, !empty($first_name) ? $first_name : ''), array('class' => $class, 'placeholder' => 'First name', 'id' => $field)); 
					?>

					<?php foreach ($errs as $message): ?>
						<div class="text-left text-danger control-error"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-12">
					<?php
						$field = 'last_name';
						$errs = $errors->get($field);
					
					 	$class = 'form-control';
						if(count($errs) > 0)
							$class .= ' error-form-control';
						echo Form::text($field, Input::old($field, !empty($last_name) ? $last_name : ''), array('class' => $class, 'placeholder' => 'Last name', 'id' => $field)); 
					?>

					<?php foreach ($errs as $message): ?>
						<div class="text-left text-danger control-error"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-12">
					<?php
						$field = 'email';
						$errs = $errors->get($field);
					
					 	$class = 'form-control';
						if(count($errs) > 0)
							$class .= ' error-form-control';
						echo Form::text($field, Input::old($field, !empty($email) ? $email : ''), array('class' => $class, 'placeholder' => 'Email address', 'id' => $field)); 
					?>

					<?php foreach ($errs as $message): ?>
						<div class="text-left text-danger control-error"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-12">
					<?php
						$field = 'password';
						$errs = $errors->get($field);
					
					 	$class = 'form-control';
						if(count($errs) > 0)
							$class .= ' error-form-control';
				
						echo Form::password($field, array('class' => $class, 'placeholder' => 'Password', 'id' => $field));
					?>

					<?php foreach ($errs as $message): ?>
						<div class="text-left text-danger control-error"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-12">
					<?php 
						$field = 'confirm_password';
						$errs = $errors->get($field);
				
					 	$class = 'form-control';
						if(count($errs) > 0)
							$class .= ' error-form-control';
				
						echo Form::password($field, array('class' => $class, 'placeholder' => 'Confirm password', 'id' => $field));
					?>

					<?php foreach ($errs as $message): ?>
						<div class="text-left text-danger control-error"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-12">
					<?php
						$field = 'business_name';
						$errs = $errors->get($field);
					
					 	$class = 'form-control';
						if(count($errs) > 0)
							$class .= ' error-form-control';
						echo Form::text($field, Input::old($field), array('class' => $class, 'placeholder' => 'Business name (optional)', 'id' => $field)); 
					?>

					<?php foreach ($errs as $message): ?>
						<div class="text-left text-danger control-error"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-12">
					<?php
						$field = 'ref_code';
						$errs = $errors->get($field);
					
					 	$class = 'form-control';
						if(count($errs) > 0)
							$class .= ' error-form-control';
						echo Form::text($field, Input::old($field, isset($ref_code) ? $ref_code : ''), array('class' => $class, 'placeholder' => 'Refereral code (optional)', 'id' => $field)); 
					?>

					<?php foreach ($errs as $message): ?>
						<div class="text-left text-danger control-error"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-12 account_types">
					<?php echo Form::radio('account_type', 'client', true, array('id' => 'account_type_client')); ?> <?php echo Form::label('account_type_client', 'I\'m looking to hire language professionals', array('class' => 'account_type')) ?><br>
					<?php echo Form::radio('account_type', 'translator', false, array('id' => 'account_type_translator')); ?> <?php echo Form::label('account_type_translator', 'I\'m a language professional', array('class' => 'account_type')) ?><br>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-12 terms">
					<?php echo Form::checkbox('terms_agreed', 'yes', false, array('id' => 'terms_agreed')); ?> <label for="terms_agreed" class="account_type">I agree to Lingoing's <a href="/files/Lingoing Terms of Business Issued April 2014.pdf" title="Terms Of Business (click to read)">Terms Of Business (click to read)</a></label>

					<?php foreach ($errors->get('terms_agreed') as $message): ?>
						<div class="text-left text-danger control-error"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			</div>

			<div class="row line text-center">
				<button class="btn btn-primary" role="button" type="submit" title="Create Account">Create Account</button>
			</div>

		<?php echo Form::close(); ?>
	</div>
</div>