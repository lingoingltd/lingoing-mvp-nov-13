<h1 class="text-center">Register with LinkedIn</h1>

<div class="col-md-4 col-md-offset-4 email-signup">
<?php echo Form::open(array('action' => 'FrontPageController@postRegisterLinkedin', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')); ?>

	<div class="form-group">
		<div class="col-sm-12">
			<?php echo Form::text('first_name', $person->first_name, array('class' => 'form-control', 'placeholder' => 'First name', 'id' => 'first_name', 'disabled' => 'disabled')); ?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-12">
			<?php echo Form::text('last_name', $person->last_name, array('class' => 'form-control', 'placeholder' => 'Last name', 'id' => 'last_name', 'disabled' => 'disabled')); ?>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-12">
			<?php echo Form::text('email', $person->email, array('class' => 'form-control', 'placeholder' => 'Email address ...', 'id' => 'email', 'disabled' => 'disabled')); ?>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-12 account_types">
			<?php echo Form::radio('account_type', 'client', true, array('id' => 'account_type_client')); ?> <?php echo Form::label('account_type_client', 'I\'m looking to hire language professionals', array('class' => 'account_type')) ?><br>
			<?php echo Form::radio('account_type', 'translator', false, array('id' => 'account_type_translator')); ?> <?php echo Form::label('account_type_translator', 'I\'m a language professional', array('class' => 'account_type')) ?><br>
			<!--<?php echo Form::radio('account_type', 'agent', false, array('id' => 'account_type_agent')); ?> <?php echo Form::label('account_type_agent', 'I\'m an agent', array('class' => 'account_type')) ?>-->
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-12 terms">
			<?php echo Form::checkbox('terms_agreed', 'yes', false, array('id' => 'terms_agreed')); ?> <label for="terms_agreed" class="account_type">I agree to Lingoing's <a href="/files/Lingoing Terms of Business Issued April 2014.pdf" title="Terms Of Business (click to read)">Terms Of Business (click to read)</a></label>

			<?php foreach ($errors->get('terms_agreed') as $message): ?>
				<div class="text-left text-danger control-error"><?php echo $message; ?></div>
			<?php endforeach; ?>
		</div>
	</div>
	
	<div class="row line text-center">
		<button class="btn btn-primary" role="button" type="submit" title="Create Account">Create Account</button>
	</div>
	
<?php echo Form::close(); ?>
</div>