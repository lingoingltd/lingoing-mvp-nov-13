<h1><?php echo Auth::user()->getName(); ?>'s notifications</h1>

<div class="col-md-12">
	<div class="notifications-container">
		<?php if($notifications->count() > 0): ?>
			<?php foreach ($notifications as $notification): ?>
				<?php if(empty($notification->sender_id)): ?>
					<div class="notification-container <?php if(!(bool)$notification->read): ?>unread<?php endif; ?>">
						<div class="notification-text">
							<?php echo $notification->renderText(); ?><br>
							<?php if(!empty($notification->link)): ?> <a href="<?php echo url($notification->renderLink()) ?>"><?php echo $notification->link_title ?></a><br><?php endif; ?>
							<small><?php echo $notification->showTimePassedCreated() ?></small>
						</div>
					</div>
				<?php else: ?>
					<div class="notification-container <?php if(!(bool)$notification->read): ?>unread<?php endif; ?> clearfix">
						<div class="professional-picture">
							<img src="<?php echo $notification->sender->getProfilePicture() ?>" class="img-thumbnail">
						</div>
						<div class="notification-text-with-image">
							<?php echo $notification->renderText(); ?><br>
							<?php if(!empty($notification->link)): ?> <a href="<?php echo url($notification->renderLink()) ?>"><?php echo $notification->link_title ?></a><br><?php endif; ?>
							<small><?php echo $notification->showTimePassedCreated() ?></small>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach ?>
		
			<?php if($max_pages > 1): ?>
			<div class="text-center">
				<ul class="pagination">
					<li <?php if($page == 1): ?>class="disabled"<?php endif; ?>><a href="<?php if($page == 1): ?>#<?php else: ?><?php echo url('notifications') . '/' . ($page - 1); ?><?php endif; ?>">&laquo;</a></li>
			
					<?php if($page - 2 > 0): ?>
						<li><a href="<?php echo url('notifications') . '/' . ($page - 2); ?>"><?php echo $page - 2; ?></a></li>
					<?php endif; ?>
			
					<?php if($page - 1 > 0): ?>
						<li><a href="<?php echo url('notifications') . '/' . ($page - 1); ?>"><?php echo $page - 1; ?></a></li>
					<?php endif; ?>
			
					<li class="active"><a href="#"><?php echo $page ?></a></li>
			
					<?php if($page + 1 <= $max_pages): ?>
						<li><a href="<?php echo url('notifications') . '/' . ($page + 1); ?>"><?php echo $page + 1; ?></a></li>
					<?php endif; ?>
			
					<?php if($page + 2 <= $max_pages): ?>
						<li><a href="<?php echo url('notifications') . '/' . ($page + 2); ?>"><?php echo $page + 2; ?></a></li>
					<?php endif; ?>
		  
					<li <?php if($page == $max_pages): ?>class="disabled"<?php endif; ?>><a href="<?php if($page == $max_pages): ?>#<?php else: ?><?php echo url('notifications') . '/' . ($page + 1); ?><?php endif; ?>">&raquo;</a></li>
				</ul>
			</div>
			<?php endif; ?>
		<?php else: ?>
			<p class="text-center">You don't have any notification yet</p>
		<?php endif; ?>
	</div>
</div>