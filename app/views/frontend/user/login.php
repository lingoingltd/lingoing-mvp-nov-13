<div class="row">
	<div class="col-md-12 text-center">
		<h1>Login to Lingoing</h1>
	</div>
</div>
<div class="row">
	<div class="col-md-4 col-md-offset-4 social-login">
		<p class="text-center"><a id="btn-facebook" class="btn" role="button" href="https://www.facebook.com/dialog/oauth?client_id=<?php echo Config::get('social.facebook.app_id') ?>&amp;redirect_uri=http://<?php echo $_SERVER['HTTP_HOST'] ?>/facebook&amp;scope=<?php echo Config::get('social.facebook.scope') ?>"><i class="fa fa-facebook-square" style="font-size:20px"></i> Login with Facebook</a></p>
		<p class="text-center"><a id="btn-linked-in" class="btn" role="button" href="<?php echo action('FrontPageController@getLinkedin') ?>"><i class="fa fa-linkedin-square" style="font-size:20px"></i> Login with LinkedIn</a></p>
	</div>
</div>
<p class="text-center" id="login-or">OR</p>
<div class="row">
	<div class="col-md-4 col-md-offset-4 email-signup">
		<?php echo Form::open(array('action' => 'FrontPageController@postLogin', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal')); ?>

			<div class="form-group">
				<div class="col-sm-12">
					<?php
						$errs = $errors->get('email');
					
					 	$class = 'form-control';
						if(count($errs) > 0)
							$class .= ' error-form-control';
						echo Form::text('email', Input::old('email'), array('class' => $class, 'placeholder' => 'Email address', 'id' => 'email')); 
					?>

					<?php foreach ($errs as $message): ?>
						<div class="text-left text-danger control-error"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-12">
					<?php
						$errs = $errors->get('password');
					
					 	$class = 'form-control';
						if(count($errs) > 0)
							$class .= ' error-form-control';
				
						echo Form::password('password', array('class' => $class, 'placeholder' => 'Password', 'id' => 'password'));
					?>

					<?php foreach ($errs as $message): ?>
						<div class="text-left text-danger control-error"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			</div>

			<div class="row line text-center">
				<button class="btn btn-primary" role="button" type="submit" title="Login">Login</button>
			</div>

		<?php echo Form::close(); ?>
	</div>
</div>
<div class="text-center col-md-12 forgot-text">
	<a href="<?php echo action('FrontPageController@getForgotPassword') ?>" title="I forgot my password">I forgot my password</a>
</div>