<?php if(Session::has('success')): ?>
	<div class="col-md-12">
		<div class="alert alert-success complete-profile"><?php echo Session::get('success'); ?></div>
	</div>
<?php endif; ?>

<h1>Account not confirmed yet</h1>

<div class="col-md-12">
<?php if((bool)Auth::user()->profile_completed): ?>
	<p>We need to do a few more checks and then we'll approve your account.<br>
	You should hear from us within 2 business days.</p>
	<p>If you have any questions in the meantime, please email <a href='mailto:support@lingoing.com'>support@lingoing.com</a><br> or
	call  us on (+44) 20 7084 6373.</p>
<?php else: ?>
	<p class="text-center">You need to <strong><a href="<?php echo url('my-profile') ?>">complete profile</a></strong> before using our service.</p>
<?php endif; ?>
</div>