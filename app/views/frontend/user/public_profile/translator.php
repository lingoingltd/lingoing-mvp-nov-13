<h1>
	<?php if(Auth::check() && Auth::user()->id == $person->id): ?>
		<div class="pull-right">
			<small><a href="<?php echo url('my-profile') ?>">Edit profile</a></small>
		</div>
	<?php endif; ?>
	
	<div><?php echo $person->getName() ?>'s profile</div>
</h1>

<div class="col-md-2 text-center img-container">
	<img src="<?php echo $person->getProfilePicture(); ?>" class="img-thumbnail" <?php if(empty($person->profile_picture_url)): ?>style="width:100%;"<?php endif; ?>>
</div>

<div class="col-md-7">
	<?php
		$defaultAddress = $person->default_address();
		$location = $defaultAddress->getShortLocation();
	?>
	<h2 class="profile-name"><?php echo $person->getName() ?></h2>
	<p><strong>Account type:</strong> <?php echo $person->accountTypeAsString() ?></p>
	<p><strong>Location:</strong> <?php if(empty($location)): ?>/<?php else: ?> <?php echo $location; ?> <?php endif; ?></p>
	
	<?php
		$sli_languages = $person->membershipsLanguages('sli');
		$sli2_languages = $person->membershipsLanguages('sli2');
		$lipspeaker_languages = $person->membershipsLanguages('lipspeaker');
		$sttr_languages = $person->membershipsLanguages('sttr');
		$slt_languages = $person->membershipsLanguages('slt');
		$note_taker_languages = $person->membershipsLanguages('notetaker');
	?>
	
	<?php if(count($sli_languages) > 0 || count($sli2_languages) > 0 || count($lipspeaker_languages) > 0 || count($sttr_languages) > 0 || count($slt_languages) > 0 || count($note_taker_languages) > 0): ?>
		<div class="public-profile-language-skills black-border-top">	
			<p class="lead">Language skills</p>
		
			<div class="opened_position">
				<ul>
					<?php if($person->spoken_language_interpreter): ?>
						<li><strong>Spoken language interpreter</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($sli_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language->language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
				
					<?php if($person->sign_language_interpreter): ?>
						<li><strong>Sign language interpreter</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($sli2_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language->language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
				
					<?php if($person->lipspeaker): ?>
						<li><strong>Lipspeaker</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($lipspeaker_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language->language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
				
					<?php if($person->speech_to_text_reporter): ?>
						<li><strong>Speech to text reporter</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($sttr_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language->language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
				
					<?php if($person->sign_language_translator): ?>
						<li><strong>Sign language translator</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($slt_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language->language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
				
					<?php if($person->note_taker): ?>
						<li><strong>Note taker</strong>: 
							<?php $count = 0; ?>
							<?php foreach ($note_taker_languages as $language): ?><?php if($count > 0): ?>, <?php endif; ?><?php echo $language->language; ?><?php $count++; ?><?php endforeach ?>
						</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	<?php endif; ?>
	
	<?php
		$specialisms = $person->specialisms();
	?>
	<?php if(count($specialisms) > 0): ?>
		<div class="public-profile-language-skills black-border-top">		
			<p class="lead">Specialisms</p>
			<div class="opened_position">
				<ul>
					<?php foreach($specialisms as $specialism): ?>
						<li><?php echo Person::listSpecialisms($specialism->specialism) ?></li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	<?php endif; ?>
	
	<?php $experiences = $person->experience; ?>
	<?php if(!empty($experiences)): ?>
		<div class="public-profile-language-skills black-border-top">		
			<p class="lead">Experiences</p>
			<div class="opened_position">
				<?php echo nl2br(strip_tags($experiences)); ?>
			</div>
		</div>
	<?php endif; ?>
	
	<?php $accreditations = $person->accreditations; ?>
	<?php if(!empty($accreditations)): ?>
		<div class="public-profile-language-skills black-border-top">		
			<p class="lead">Accreditations</p>
			<div class="opened_position">
				<?php echo nl2br(strip_tags($accreditations)); ?>
			</div>
		</div>
	<?php endif; ?>
	
	<?php if(count($insurance_documents) > 0 || count($crb_documentation) > 0): ?>
		<div class="public-profile-language-skills black-border-top">
			<p class="lead">Other</p>
			<div class="opened_position">
				<ul>
					<?php if(count($insurance_documents) > 0): ?>
						<li>LSP uploaded insurance documents</li>
					<?php endif; ?>
					
					<?php if(count($crb_documentation) > 0): ?>
						<li>LSP uploaded CRB documents</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	<?php endif; ?>
</div>

<?php $rating = $person->getRating(); ?>
<div class="col-md-3">
	<div class="white-container public-profile-rating">
		<p class="text-center lead">Rating</p>
		<p class="text-center rating-exact"><?php echo $rating; ?></p>
		<p class="text-center public-profile-stars">
			<?php for ($i=1; $i <= floor($rating); $i++): ?> 
				<span class="glyphicon glyphicon-star gold-star"></span> 
			<?php endfor ?>
		
			<?php for($i=floor($rating) + 1; $i <= 5; $i++): ?>
				<span class="glyphicon glyphicon-star"></span> 
			<?php endfor; ?>
		</p>
		<?php if($person->no_of_votes == 0): ?>
			<p class="text-center note"><small class="text-muted">LSP have no ratings</small></p>
		<?php else: ?>
			<p class="text-center note"><small class="text-muted"><?php echo $person->no_of_votes . ($person->no_of_votes == 1 ? ' vote' : ' votes') ?></small></p>
		<?php endif; ?>
	</div>
	
	<div class="white-container top-space public-profile-jobs-worked">
		<?php
			if((int)$person->no_of_bookings > 0)
			{
				$percentage = round(((int)$person->no_of_bookings-(int)$person->no_of_non_attendings)/(int)$person->no_of_bookings * 100);
				$class = '';
				if($percentage >= 90)
					$class = 'progress-bar-success';
				elseif($percentage >= 70 && $percentage < 90)
					$class = ''; //default blue
				elseif($percentage >= 50 && $percentage < 70)
					$class = 'progress-bar-warning';
				elseif($percentage > 0 && $percentage < 50)
					$class = 'progress-bar-danger';
				else
					$class = 'progress-bar-dead';
			}
		?>
		<p class="text-center lead">Jobs stats</p>
		<div class="pull-left booking-exact">Booked: </div>
		<div class="pull-right booking-exact"><?php echo (int)$person->no_of_bookings ?></div>
		<div class="clearfix"></div>
		<div class="pull-left booking-exact">Not attended: </div>
		<div class="pull-right booking-exact"><?php echo (int)$person->no_of_non_attendings ?></div>
		<div class="clearfix"></div>
		
		<?php if((int)$person->no_of_bookings > 0): ?>
			<div class="progress progress-dna">
				<div class="progress-bar <?php echo $class ?>" role="progressbar" aria-valuenow="<?php echo $percentage ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percentage ?>%;">
					<?php echo $percentage ?>%
				</div>
			</div>
		<?php endif; ?>
	</div>
	
	<?php $reviews = $person->reviews()->orderBy('created_at', 'DESC')->take(3)->get(); ?>
	<?php if(count($reviews) > 0): ?>
		<div class="white-container top-space public-profile-jobs-worked">
			<p class="text-center lead">Reviews</p>
			<?php foreach ($reviews as $review): ?>
				<?php if(empty($review->review)) continue; ?>
				<div class="review-container">
					<p class="review">“<?php echo $review->review; ?>”</p>
					<p class="review-name text-right"><?php echo $review->client->getName() ?></p>
				</div>
			<?php endforeach ?>
			<p class="text-center">
				<a href="<?php echo url('reviews') ?>/<?php echo $person->id ?>">View all reviews</a>
			</p>
		</div>
	<?php endif; ?>
</div>