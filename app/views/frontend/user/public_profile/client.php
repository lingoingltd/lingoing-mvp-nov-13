<h1>
	<?php if(Auth::check() && Auth::user()->id == $person->id): ?>
		<div class="pull-right">
			<small><a href="<?php echo url('my-profile') ?>">Edit profile</a></small>
		</div>
	<?php endif; ?>
	
	<div><?php echo $person->getName() ?>'s profile</div>
</h1>

<div class="col-md-2 text-center img-container">
	<img src="<?php echo $person->getProfilePicture(); ?>" class="img-thumbnail" <?php if(empty($person->profile_picture_url)): ?>style="width:100%;"<?php endif; ?>>
</div>

<div class="col-md-7">
	<?php
		$defaultAddress = $person->default_address();
		$location = $defaultAddress->getShortLocation();
	?>
	<h2 class="profile-name"><?php echo $person->getName() ?></h2>
	<p><strong>Account type:</strong> <?php echo $person->accountTypeAsString() ?></p>
	<p><strong>Location:</strong> <?php if(empty($location)): ?>/<?php else: ?> <?php echo $location; ?> <?php endif; ?></p>
</div>

<div class="col-md-3">	
	<div class="white-container top-space public-profile-jobs-worked">
		<p class="text-center lead">Jobs posted</p>
		<p class="text-center rating-exact"><?php echo $person->jobs()
			->count(); ?>
		</p>
	</div>
</div>