<h1>Reset your password</h1>
<p class="text-center">Your password reset URL <strong>has expired</strong>!<br>Please <a href="<?php echo url('forgot-password'); ?>">request a new reset URL</a>.</p>