<h1><?php echo $person->getName() ?>'s reviews</h1>

<div class="col-md-2 text-center img-container">
	<img src="<?php echo $person->getProfilePicture(); ?>" class="img-thumbnail" <?php if(empty($person->profile_picture_url)): ?>style="width:100%;"<?php endif; ?>>
</div>

<div class="col-md-10 reviews-container">
	<?php foreach ($reviews as $review): ?>
		<?php $professional = $review->client; ?>
		<div class="professional-container border-all bottom-space">
			<div class="job-title border-bottom-gray clearfix">
				<div class="pull-left">
					<div class="professional-picture">
						<img src="<?php echo $professional->getProfilePicture() ?>" class="img-thumbnail">
					</div>
					<p class="lead professional-name">
						<a href="<?php echo url('profile') ?>/<?php echo $professional->id ?>" title="<?php echo $professional->getName() ?>"><?php echo $professional->getName() ?></a>
					</p>
				</div>
				
				<div class="pull-right review-stars">
					<?php for ($i = 1; $i <= $review->stars; $i++): ?> 
						<span class="glyphicon glyphicon-star gold-star"></span> 
					<?php endfor ?>

					<?php for($i = $review->stars + 1; $i <= 5; $i++): ?>
						<span class="glyphicon glyphicon-star"></span> 
					<?php endfor; ?>
				</div>
			</div>
			<div class="reviews-review">
				<?php echo $review->review; ?>
			</div>
		</div>
	<?php endforeach ?>
</div>