<?php echo Form::open(array('action' => 'FrontPageController@postProfileStep4', 'method' => 'post')); ?>

	<div class="row line">
		<div class="col-md-12">
			<?php $field = 'experience'; ?>
			<?php echo Form::label($field, 'Add a summary of your experience here', array('class' => 'control-label')) ?><br>
			<?php echo Form::textarea($field, Input::old($field, Auth::user()->experience), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
	</div>
	
	<div class="row line">
		<div class="col-md-12">
			<?php $field = 'accreditations'; ?>
			<?php echo Form::label($field, 'Add any accreditations you have here', array('class' => 'control-label')) ?><br>
			<?php echo Form::textarea($field, Input::old($field, Auth::user()->accreditations), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
	</div>

	<div class="row line">
		<div class="col-md-12">
			<div class="pull-left">
				<a class="btn btn-default" href="<?php echo action('FrontPageController@getProfileStep3') ?>">Previous</a>
			</div>
			<div class="pull-right">
				<?php
					$label = 'Save';
					if(!Auth::user()->profile_completed)
						$label = 'Save &amp; Continue';
				?>
				<button class="btn btn-primary" role="button" type="submit" name="next" title="<?php echo $label ?>"><?php echo $label ?></button>
			</div>
		</div>
	</div>
<?php echo Form::close(); ?>