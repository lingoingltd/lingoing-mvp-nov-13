<p>This information is used purely to process your AtW details for invoices and claim forms and are treated as confidential.</p>
<p>&nbsp;</p>
<div class="row clearfix">
	<div class="col-md-5">
		<?php
			$field = 'atw_support_type';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'This support was given to me as', array('class' => 'control-label')) ?>
			<?php echo Form::select($field, array('' => '', 'freelancer' => 'Freelance person', 'company' => 'Company'), Input::old($field), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
		</div>
		
		<div class="company_data">
			<?php
				$field = 'atw_company';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'Company name', array('class' => 'control-label')) ?>
				<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
			</div>
		
			<?php
				$field = 'atw_company_address_1';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'Address 1', array('class' => 'control-label')) ?>
				<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
			</div>
		
			<?php
				$field = 'atw_company_address_2';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'Address 2', array('class' => 'control-label')) ?>
				<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
			</div>
		
			<?php
				$field = 'atw_company_city';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'City', array('class' => 'control-label')) ?>
				<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
			</div>
		
			<?php
				$field = 'atw_company_country';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'Country', array('class' => 'control-label')) ?>
				<?php echo Form::select($field, array('' => '') + Person::listCountries(), Input::old($field, Auth::user()->country), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
			</div>
		
			<?php
				$field = 'atw_company_county';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'County', array('class' => 'control-label')) ?>
				<?php echo Form::select($field, array('' => '') + Person::listCounties(), Input::old($field, Auth::user()->county), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
			</div>
		
			<?php
				$field = 'atw_company_postcode';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'Postcode', array('class' => 'control-label')) ?>
				<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
			</div>
		</div>
		
		<?php
			$field = 'national_insurance';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'National insurance (NI)', array('class' => 'control-label')) ?>
			<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
		</div>
		
		<?php
			$field = 'atw_email_address';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Email address', array('class' => 'control-label')) ?>
			<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
		</div>
		
		<?php
			$field = 'your_budget';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Your budget in', array('class' => 'control-label')) ?>
			<?php echo Form::select($field, array('' => '', 'total' => 'Total', 'per_hour' => 'Per hour'), Input::old($field), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
		</div>
		
		<?php
			$field = 'your_budget_other';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
		</div>
		
		<?php
			$field = 'type_of_language_professional';
			$field_errors = $errors->get($field);
			
			$values = array(
				'' => '', 
				'british_sign_language' => 'British Sign Language', 
				'communication_support_worker' => 'Communication Support Worker',
				'note_taker' => 'Note Taker',
				'lip_speaker' => 'Lip Speaker',
				'other' => 'Other'
			);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Type of language professional', array('class' => 'control-label')) ?>
			<?php echo Form::select($field, $values, Input::old($field), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
		</div>
		
		<?php
			$field = 'type_of_language_professional_other';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
		</div>
		
		<?php
			$field = 'type_of_language_professional';
			$field_errors = $errors->get($field);
			
			$values = array(
				'' => '', 
				'end_of_month' => 'End of month', 
				'every_two_weeks' => 'Every two weeks',
				'job_by_job' => 'Job by Job'
			);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Do you want to receive the invoice', array('class' => 'control-label')) ?>
			<?php echo Form::select($field, $values, Input::old($field), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
		</div>
		
		<?php
			$field = 'additional_info';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Any additional information?', array('class' => 'control-label')) ?>
			<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
		</div>
	</div>
	<div class="col-md-5 col-md-offset-1">
		<?php
			$field = 'atw_confirmation_letter';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Upload confirmation letter of your AtW', array('class' => 'control-label')) ?>
			<?php echo Form::file($field, array('class' => '', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php
			$field = 'support_ends';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'When does your support end?', array('class' => 'control-label')) ?>
			<?php echo Form::text($field, Input::old($field), array('class' => 'form-control date-helper', 'placeholder' => 'dd/mm/yyyy', 'id' => $field)) ?>
		</div>
		
		<?php
			$field = 'atw_address';
			$field_errors = $errors->get($field);
			
			$values = array(
				'' => ''
			);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Which address do you post your claim forms to?', array('class' => 'control-label')) ?>
			<?php echo Form::select($field, $values, Input::old($field), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
		</div>
		
		<?php
			$field = 'how_many_hours_per';
			$field_errors = $errors->get($field);
			
			$values = array(
				'' => '', 
				'week' => 'Week', 
				'month' => 'Month',
				'year' => 'Year'
			);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Hom many hour(s) per ...', array('class' => 'control-label')) ?>
			<?php echo Form::select($field, $values, Input::old($field), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
		</div>
		
		<?php
			$field = 'how_many_hours_per_value';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
		</div>
		
		<?php
			$field = 'additional_support_package';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::checkbox($field, 'yes', false, array('id' => $field)); ?> <?php echo Form::label($field, 'I have an additional support package that I would like to use on Lingoing', array('style' => 'display:inline;')) ?>
		</div>
	</div>
</div>

<div>&nbsp;</div>

<div class="row line">
	<div class="col-md-12">
		<div class="pull-left">
			<a class="btn btn-default" href="<?php echo action('FrontPageController@getProfileStep3') ?>">Previous</a>
		</div>
		<div class="pull-right">
			<?php
				$label = 'Save';
				if(!Auth::user()->profile_completed)
					$label = 'Complete profile';
				else
				{
					if(Auth::user()->how_to_pay_bills == 'atw_customer')
						$label = 'Save &amp; Continue';
				}
			?>
			<button class="btn btn-primary" id="submit-next" role="button" type="submit" name="next" title="<?php echo $label ?>"><?php echo $label ?></button>
		</div>
	</div>
</div>