<?php echo Form::open(array('url' => url('change-email-address'), 'method' => 'post')); ?>

	<p class="lead">Change email address</p>

	<?php
		$field = 'new_email_address';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'New email address', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, '', array('class' => 'form-control', 'placeholder' => 'New email address', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'new_email_address_repeat';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Repeat new email address', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, '', array('class' => 'form-control', 'placeholder' => 'Repeat new email address', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<p>&nbsp;</p>

	<div class="row line">
		<div class="col-md-12">
			<div class="pull-left">
				<a class="btn btn-default" href="<?php echo action('FrontPageController@getProfileStep1') ?>">Cancel</a>
			</div>
			<div class="pull-right">
				<button class="btn btn-primary" role="button" type="submit" name="next">Change email address</button>
			</div>
		</div>
	</div>

<?php echo Form::close(); ?>