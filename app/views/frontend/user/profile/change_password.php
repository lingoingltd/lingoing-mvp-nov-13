<?php echo Form::open(array('url' => url('change-password'), 'method' => 'post')); ?>

	<p class="lead">Change password</p>

	<?php
		$field = 'new_password';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'New password', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::password($field, array('class' => 'form-control', 'placeholder' => 'New password', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'new_password_repeat';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Repeat new password', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::password($field, array('class' => 'form-control', 'placeholder' => 'Repeat new password', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<p>&nbsp;</p>

	<div class="row line">
		<div class="col-md-12">
			<div class="pull-left">
				<a class="btn btn-default" href="<?php echo action('FrontPageController@getProfileStep1') ?>">Cancel</a>
			</div>
			<div class="pull-right">
				<button class="btn btn-primary" role="button" type="submit" name="next">Change password</button>
			</div>
		</div>
	</div>

<?php echo Form::close(); ?>