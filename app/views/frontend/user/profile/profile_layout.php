<div class="col-md-12">
	<h1>My profile</h1>
</div>

<?php if(!(bool)Auth::user()->profile_completed): ?>
	<div class="col-md-12">
		<div class="alert alert-warning complete-profile">You have to <strong>complete profile</strong> before continue using our services. Your profile <strong>will not be public visible</strong> lists until you complete it.</div>
	</div>
<?php elseif(empty(Auth::user()->profile_picture_url)): ?>
	<div class="col-md-12">
		<div class="alert alert-warning complete-profile"><strong>Profile picture</strong> is <strong>mandatory</strong>. We can't approve you account without profile picture. You can upload it <strong><a href="<?php echo url('my-profile'); ?>#profile-picture">here</a></strong>.</div>
	</div>
<?php endif; ?>

<?php if(Session::has('success')): ?>
	<div class="col-md-12">
		<div class="alert alert-success complete-profile"><?php echo Session::get('success'); ?></div>
	</div>
<?php endif; ?>

<div class="col-md-12">
	<ul class="nav nav-tabs">
		<?php if(Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getChangeEmailAddress'): ?>
			<li class="active"><a href="<?php echo url('change-email-address') ?>">Change email address</a></li>
		<?php elseif(Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getChangePassword'): ?>
			<li class="active"><a href="<?php echo url('change-password') ?>">Change password</a></li>
		<?php else: ?>
			<li <?php if(Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getProfileStep1'): ?>class="active"<?php elseif(!(bool)Auth::user()->profile_completed): ?>class="disabled"<?php endif; ?>><a href="<?php if(!(bool)Auth::user()->profile_completed): ?>#<?php else: ?><?php echo action('FrontPageController@getProfileStep1') ?><?php endif; ?>">Basic information</a></li>
			<li <?php if(Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getMyAddressBook' || Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getChangeAddress'): ?>class="active"<?php elseif(!(bool)Auth::user()->profile_completed): ?>class="disabled"<?php endif; ?>><a href="<?php if(!(bool)Auth::user()->profile_completed): ?>#<?php else: ?><?php echo action('FrontPageController@getMyAddressBook') ?><?php endif; ?>">My address book</a></li>
			<?php if(Auth::user()->account_type == 'translator'): ?>
				<li <?php if(Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getProfileStep2'): ?>class="active"<?php elseif(!(bool)Auth::user()->profile_completed): ?>class="disabled"<?php endif; ?>><a href="<?php if(!(bool)Auth::user()->profile_completed): ?>#<?php else: ?><?php echo action('FrontPageController@getProfileStep2') ?><?php endif; ?>">Language skills</a></li>
				<li <?php if(Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getProfileStep3'): ?>class="active"<?php elseif(!(bool)Auth::user()->profile_completed): ?>class="disabled"<?php endif; ?>><a href="<?php if(!(bool)Auth::user()->profile_completed): ?>#<?php else: ?><?php echo action('FrontPageController@getProfileStep3') ?><?php endif; ?>">Documents</a></li>
				<li <?php if(Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getProfileStep4'): ?>class="active"<?php elseif(!(bool)Auth::user()->profile_completed): ?>class="disabled"<?php endif; ?>><a href="<?php if(!(bool)Auth::user()->profile_completed): ?>#<?php else: ?><?php echo action('FrontPageController@getProfileStep4') ?><?php endif; ?>">Experience</a></li>
				<li <?php if(Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getProfileStep5'): ?>class="active"<?php elseif(!(bool)Auth::user()->profile_completed): ?>class="disabled"<?php endif; ?>><a href="<?php if(!(bool)Auth::user()->profile_completed): ?>#<?php else: ?><?php echo action('FrontPageController@getProfileStep5') ?><?php endif; ?>">Payment details</a></li>
			<?php elseif(Auth::user()->account_type == 'client'): ?>
				<?php if((bool)Auth::user()->different_billing_address): ?>
					<li <?php if(Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getProfileStep2'): ?>class="active"<?php elseif(!(bool)Auth::user()->profile_completed): ?>class="disabled"<?php endif; ?>><a href="<?php if(!(bool)Auth::user()->profile_completed): ?>#<?php else: ?><?php echo action('FrontPageController@getProfileStep2') ?><?php endif; ?>">Billing details</a></li>
				<?php endif; ?>
				<li <?php if(Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getProfileStep3'): ?>class="active"<?php elseif(!(bool)Auth::user()->profile_completed): ?>class="disabled"<?php endif; ?>><a href="<?php if(!(bool)Auth::user()->profile_completed): ?>#<?php else: ?><?php echo action('FrontPageController@getProfileStep3') ?><?php endif; ?>">Payment details</a></li>
				<?php if(Auth::user()->how_to_pay_bills == 'atw_customer'): ?>
					<li <?php if(Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getAtwPackages' || Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getChangeAtw'): ?>class="active"<?php elseif(!(bool)Auth::user()->profile_completed): ?>class="disabled"<?php endif; ?>><a href="<?php if(!(bool)Auth::user()->profile_completed): ?>#<?php else: ?><?php echo action('FrontPageController@getAtwPackages') ?><?php endif; ?>">Access to Work packages</a></li>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>
		
		<?php if((bool)Auth::user()->sales_rep_enabled && !empty(Auth::user()->referer_code)): ?>
			<li <?php if(Route::getFacadeRoot()->currentRouteAction() == 'FrontPageController@getCustomerInvite'): ?>class="active"<?php endif; ?>><a href="<?php echo action('FrontPageController@getCustomerInvite') ?>">Customer invite</a></li>
		<?php endif; ?>
	</ul>
	
	<!-- begin profile -->
	<div class="profile_layout">
		<p class="text-right"><span class="required">* - mandatory field</span></p>
		<?php if(isset($profile)) echo $profile; ?>
	</div>
	<!-- end profile -->
</div>