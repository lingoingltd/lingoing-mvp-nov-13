<?php echo Form::open(array('action' => 'FrontPageController@postProfileStep3', 'method' => 'post')); ?>

	<p class="lead">Payment details</p>
	
	<?php
		$field = 'how_to_pay_bills';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'How do you want to pay bills?', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => '') + Invoice::listBillPayingOptions(), Input::old($field, Auth::user()->how_to_pay_bills), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<div class="row line">
		<div class="col-md-12">
			<div class="pull-left">
				<?php if(Session::get('wizard') == 'true'): ?>
					<?php if(Session::get('different_billing_address') == 'yes'): ?>
						<?php
							$billing_address = Auth::user()->billing_address();
						?>
						<a class="btn btn-default" href="<?php echo action('FrontPageController@getChangeAtw') ?>/<?php echo $billing_address->id ?>">Previous</a>
					<?php else: ?>
						<a class="btn btn-default" href="<?php echo action('FrontPageController@getProfileStep1') ?>">Previous</a>
					<?php endif; ?>
				<?php else: ?>
					<a class="btn btn-default" href="<?php echo action('FrontPageController@getMyAddressBook') ?>">Previous</a>
				<?php endif; ?>
			</div>
			<div class="pull-right">
				<?php
					$label = 'Save';
					if(Auth::user()->how_to_pay_bills == 'atw_customer')
						$label = 'Save &amp; Continue';
					else
						$label = 'Complete profile';
				?>
				<button class="btn btn-primary" id="submit-next" role="button" type="submit" name="next" title="<?php echo $label ?>"><?php echo $label ?></button>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		$("#how_to_pay_bills").change(function(){
			if($(this).val() == 'paying_card')
			<?php if(!(bool)Auth::user()->profile_completed): ?>
				$("#submit-next").html('Complete profile');
			<?php else: ?>
				$("#submit-next").html('Save');
			<?php endif; ?>
			else
				$("#submit-next").html('Save &amp; Continue');
		});
	</script>
	
<?php echo Form::close(); ?>