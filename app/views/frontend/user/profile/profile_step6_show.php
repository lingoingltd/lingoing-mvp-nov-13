<h1>Organization details</h1>
<hr>

<address>
	<span class="lead"><?php echo $organization->name; ?></span><br>
	<?php echo $organization->address_1 ?><br>
	
	<?php if(!empty($organization->address_2)): ?>
		<?php echo $organization->address_2 ?><br>
	<?php endif; ?>
	
	<?php echo $organization->city ?> <?php echo $organization->postcode ?><br>
	
	<?php if(!empty($organization->county)): ?>
		<?php echo $organization->county ?><br>
	<?php endif; ?>
	
	<?php echo Person::listCountries($organization->country) ?><br>
	
	<?php if(!empty($organization->phone)): ?>
		<abbr title="Phone">P:</abbr> <?php echo $organization->phone ?><br>
	<?php endif; ?>
	
	<?php if(!empty($organization->fax)): ?>
		<abbr title="Fax">F:</abbr> <?php echo $organization->fax ?><br>
	<?php endif; ?>
</address>

<address>
	Organization type: <strong><?php echo Organization::listTypes($organization->organization_type) ?></strong><br>
	
	<?php if(!empty($organization->registration_number)): ?>
		Registration number: <strong><?php echo $organization->registration_number ?></strong><br>
	<?php endif; ?>
	
	VAT registered: <strong><?php echo ($organization->vat_registered ? 'Yes' : 'No'); ?></strong><br>
	
	<?php if(!empty($organization->vat_number)): ?>
		VAT number: <strong><?php echo $organization->vat_number ?></strong>
	<?php endif; ?>
</address>

<?php echo Form::open(array('action' => 'FrontPageController@postProfileStep6', 'method' => 'post', 'id' => 'remove_organization')); ?>
	<div class="row line">
		<div class="col-md-12">
			<div class="text-center">
				<button class="btn btn-danger" role="button" type="submit" name="next" title="Remove organization">Remove organization</button>
			</div>
		</div>
	</div>

	<input type="hidden" name="action" value="remove_organization" id="action">
<?php echo Form::close(); ?>