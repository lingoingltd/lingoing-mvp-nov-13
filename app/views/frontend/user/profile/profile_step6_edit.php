<?php if($organization == null): ?>
	<p class="lead">Select organization ...</p>
	
	<?php echo Form::open(array('action' => 'FrontPageController@postProfileStep6', 'method' => 'post', 'id' => 'search_for_it')); ?>
	
		<?php
			$field = 'organization';
			$field_errors = $errors->get($field);
		?>
		<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<div class="col-md-8">
				<?php echo Form::label($field, 'Organization', array('class' => 'control-label')) ?> <span class="required">*</span><br>
				<?php echo Form::select($field, array('' => '') + Organization::listAll(), '', array('class' => 'form-control disabledable', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
			</div>
			
			<?php if(count($field_errors) > 0): ?>
				<div class="col-md-4">
					<?php foreach ($errors->get($field) as $message): ?>
						<div class="text-left text-danger"><?php echo $message; ?></div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	
		<div class="row line">
			<div class="col-md-8">
				<div class="text-center">
					<button class="btn btn-primary" role="button" type="submit" name="next" title="Add organization">Add organization</button>
				</div>
			</div>
		</div>
	
		<input type="hidden" name="action" value="add_existing" id="action">
	<?php echo Form::close(); ?>

	<p class="lead">... or add new one</p>
<?php else: ?>
	<p class="lead">Change organization</p>
<?php endif; ?>

<?php echo Form::open(array('action' => 'FrontPageController@postProfileStep6', 'method' => 'post', 'id' => 'add_new_one')); ?>

	<?php 
		$field = 'name';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Organization name', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::text($field, Input::old($field, (isset($organization) ? $organization->name : '')), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php 
		$field = 'address_1';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Address 1', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::text($field, Input::old($field, (isset($organization) ? $organization->address_1 : '')), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'address_2';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Address 2', array('class' => 'control-label')) ?>
			<?php echo Form::text($field, Input::old($field, (isset($organization) ? $organization->address_2 : '')), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'city';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">		
			<?php echo Form::label($field, 'City', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::text($field, Input::old($field, (isset($organization) ? $organization->city : '')), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'postcode';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Postcode', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::text($field, Input::old($field, (isset($organization) ? $organization->postcode : '')), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'county';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'County', array('class' => 'control-label')); ?>
			<?php echo Form::select($field, array('' => '') + Person::listCounties(), Input::old($field, (isset($organization) ? $organization->county : '')), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'country';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Country', array('class' => 'control-label')); ?> <span class="required">*</span><br>
			<?php echo Form::select($field, array('' => '') + Person::listCountries(), Input::old($field, (isset($organization) ? $organization->country : '')), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'phone';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Phone number', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, (isset($organization) ? $organization->phone : '')), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'fax';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Fax number', array('class' => 'control-label')) ?><br>
			<?php echo Form::text($field, Input::old($field, (isset($organization) ? $organization->fax : '')), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'organization_type';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Organization type', array('class' => 'control-label')); ?><br>
			<?php echo Form::select($field, Organization::listTypes(), Input::old($field, (isset($organization) ? $organization->ogranization_type : '')), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'registration_number';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Registration number', array('class' => 'control-label')) ?><br>
			<?php echo Form::text($field, Input::old($field, (isset($organization) ? $organization->registration_number : '')), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php 
		$disabled = 'disabled';
		if(Input::old('vat_registered') == 'yes' || (isset($organization) && (bool)$organization->vat_registered))
			$disabled = '';
	?>
	<div class="row line">
		<div class="col-md-8">
			<?php echo Form::label('vat_registered', 'VAT registered', array('class' => 'control-label')) ?><br>
			<?php echo Form::radio('vat_registered', 'yes', Input::old('vat_registered', (isset($organization) && (bool)$organization->vat_registered ? 'yes' : 'no')) == 'yes', array('id' => 'vat_registered_yes', 'class' => 'vat_registered')); ?> <?php echo Form::label('vat_registered_yes', 'Yes', array('class' => 'control-label')) ?> 
			<?php echo Form::radio('vat_registered', 'no', Input::old('vat_registered', (isset($organization) && (bool)$organization->vat_registered ? 'yes' : 'no')) == 'no', array('id' => 'vat_registered_no', 'class' => 'vat_registered')); ?> <?php echo Form::label('vat_registered_no', 'No', array('class' => 'control-label')) ?>
		</div>
	</div>
	
	<?php
		$field = 'vat_number';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'VAT number', array('class' => 'control-label')) ?><br>
			<?php echo Form::text($field, Input::old($field, (isset($organization) ? $organization->vat_number : '')), array('class' => 'form-control', 'placeholder' => '', 'id' => $field, $disabled)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php if($organization == null): ?>
		<div class="row line">
			<div class="col-md-8">
				<div class="text-center">
					<button class="btn btn-primary" role="button" type="submit" name="add" title="Add organization">Add organization</button>
				</div>
			</div>
		</div>
		
		<input type="hidden" name="action" value="add_new" id="action">
	<?php else: ?>
		<div class="row line">
			<div class="col-md-8">
				<div class="text-center">				
					<?php if($organization->person_id == Auth::user()->id): ?>
						<button class="btn btn-primary" role="button" type="submit" name="update" value="update" title="Update organization">Update organization</button>
					<?php endif; ?>
				
					<button class="btn btn-danger" role="button" type="submit" name="remove" value="remove" title="Remove organization">Remove organization</button>
				</div>
			</div>
		</div>
		
		<input type="hidden" name="action" value="update_remove" id="action">
	<?php endif; ?>
<?php echo Form::close(); ?>