<?php echo Form::open(array('url' => url('customer-invite'), 'method' => 'post')); ?>

	<p class="lead">Send customer invitation</p>

	<?php
		$field = 'first_name';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'First name', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => 'John', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'last_name';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Last name', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => 'Doe', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'email_address';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Email address', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field), array('class' => 'form-control', 'placeholder' => 'john.doe@domain.com', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'message';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Message', array('class' => 'control-label')) ?><br>
			<?php echo Form::textarea($field, Input::old($field), array('class' => 'form-control', 'placeholder' => 'Your message ...', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<p>&nbsp;</p>

	<div class="row line">
		<div class="col-md-12 text-center">
			<button type="submit" name="send_invitation" class="btn btn-primary">Send invitation</button>
		</div>
	</div>

<?php echo Form::close(); ?>