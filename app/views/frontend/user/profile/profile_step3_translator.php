<?php echo Form::open(array('action' => 'FrontPageController@postProfileStep3', 'method' => 'post')); ?>
	
	<p class="lead">Insurance documents</p>

	<?php $field = 'insurance_documents'; ?>
	<?php echo Form::file($field, array('id' => $field, 'class' => 'hidden')); ?>

	<?php foreach ($memberships as $membership): ?>
		<?php $documents = $membership->documents(); ?>
		<div class="membership_body">
			<h5>Membership body: <?php echo $membership->membership_body ?> <?php if(!empty($membership->expiry_date)): ?>(expire: <?php echo \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y'); ?>)<?php endif; ?></h5>
		</div>
		<div class="membership_body_documents">
			<div class="insurance_container">
				<?php if(count($documents) == 0): ?>
					<div class="membership_body_documents_empty text-center">You don't have any documents<?php if($membership->membership_body != 'ASLI' && $membership->membership_body != 'VLP'): ?><br><span class="required">* We need you to upload your insurance documents</span><?php endif; ?></div>
				<?php else: ?>
					<?php echo View::make('frontend.user.profile.profile_step3_partial_list', array(
						'documents' => $documents,
						'type' => 'insurance'
					)) ?>
				<?php endif; ?>
			</div>
			<div class="insurance_upload_container text-center">
				<a href="#" class="btn btn-sm btn-blue upload-insurance-document" data-id="<?php echo $membership->id ?>">Upload new document</a>
			</div>
			<div class="insurance_upload_indicator text-center hidden">
				<img src="/img/loader2.gif"> Uploading, please wait ...
			</div>
		</div>
	<?php endforeach ?>
		
	<div class="membership_body">
		<h5>Other insurance documents</h5>
	</div>
	<div class="membership_body_documents">
		<div class="insurance_container">
			<?php if (count($insurance_documents) > 0): ?>
				<?php echo View::make('frontend.user.profile.profile_step3_partial_list', array(
					'documents' => $insurance_documents,
					'type' => 'insurance'
				)) ?>
			<?php else: ?>
				<div class="text-center">You don't have any documents</div>
			<?php endif ?>
		</div>
		<div class="insurance_upload_container text-center">
			<a href="#" class="btn btn-sm btn-blue upload-crb-document" data-id="">Upload new document</a>
		</div>
		<div class="insurance_upload_indicator text-center hidden">
			<img src="/img/loader2.gif"> Uploading, please wait ...
		</div>
	</div>
	
	<p class="lead">CRB documents</p>

	<?php $field = 'crb_documents'; ?>
	<?php echo Form::file($field, array('id' => $field, 'class' => 'hidden')); ?>

	<div class="membership_body_documents">
		<div class="insurance_container">
			<?php if(count($crb_documents) == 0): ?>
				<div class="membership_body_documents_empty text-center">You don't have any CRB documents</div>
			<?php else: ?>
				<?php echo View::make('frontend.user.profile.profile_step3_partial_list', array(
					'documents' => $crb_documents,
					'type' => 'insurance'
				)) ?>
			<?php endif; ?>
		</div>
		<div class="insurance_upload_container text-center">
			<a href="#" class="btn btn-sm btn-blue upload-crb-document">Upload new document</a>
		</div>
		<div class="insurance_upload_indicator text-center hidden">
			<img src="/img/loader2.gif"> Uploading, please wait ...
		</div>
	</div>
	
	<div class="row line">
		<div class="col-md-12">
			<div class="pull-left">
				<a class="btn btn-default" href="<?php echo action('FrontPageController@getProfileStep2') ?>">Previous</a>
			</div>
			<div class="pull-right">
				<a class="btn btn-primary" href="<?php echo action('FrontPageController@getProfileStep4') ?>">Next</a>
			</div>
		</div>
	</div>

<?php echo Form::close(); ?>