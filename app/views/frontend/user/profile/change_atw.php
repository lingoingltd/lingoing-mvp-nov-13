<?php echo Form::open(array('url' => url('change-atw', array('id' => $package_id)), 'method' => 'post', 'files' => true)); ?>

<?php if($package == null): ?>
	<p class="lead">Add new Access to Work support package</p>
<?php else: ?>
	<p class="lead">Change Access to Work support package</p>
<?php endif; ?>

<p>This information is used purely to process your AtW details for invoices and claim forms and are treated as confidential.</p>
<p><strong>Note:</strong> before using this support package our support team will have to verify it.</p>
<p>&nbsp;</p>
<div class="row clearfix">
	<div class="col-md-5">
		<?php
			$field = 'atw_support_type';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'This support was given to me as', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => '') + AtwSupportPackage::listGivenTo(), Input::old($field, !empty($package) ? $package->given_to : ''), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<?php echo Form::radio('atw_address_type', 'existing', Input::old('atw_address_type', true), array('id' => 'atw_address_existing', 'class' => 'atw_address_type')) ?> <?php echo Form::label('atw_address_existing', 'Use existing address', array('class' => 'control-label')) ?>
			</div>
		</div>
		
		<?php
			$field = 'atw_address';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>" id="atw_address_existing_container">
			<?php echo Form::select($field, array('' => '') + Auth::user()->addressesAsArray(), Input::old($field, !empty($package) ? $package->address_id : ''), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<?php echo Form::radio('atw_address_type', 'new', Input::old('atw_address_type', false), array('id' => 'atw_address_new', 'class' => 'atw_address_type')) ?> <?php echo Form::label('atw_address_new', 'Add new address', array('class' => 'control-label')) ?>
			</div>
		</div>
		
		<div class="new_address line <?php if(Input::old('atw_address_type', 'existing') == 'existing'): ?>hidden<?php endif; ?>" id="atw_address_new_container">
			<?php
				$field = 'atw_first_name';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'First name', array('class' => 'control-label')) ?> <span class="required">*</span>
				<?php echo Form::text($field, Input::old($field, !empty($package) ? $package->organization_name : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $error): ?>
						<div class="text-danger"><?php echo $error ?></div>
					<?php endforeach ?>
				<?php endif; ?>
			</div>
			
			<?php
				$field = 'atw_last_name';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'Last name', array('class' => 'control-label')) ?> <span class="required">*</span>
				<?php echo Form::text($field, Input::old($field, !empty($package) ? $package->organization_name : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $error): ?>
						<div class="text-danger"><?php echo $error ?></div>
					<?php endforeach ?>
				<?php endif; ?>
			</div>
			
			<?php
				$field = 'atw_company';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'Company name', array('class' => 'control-label')) ?> <span class="required">*</span>
				<?php echo Form::text($field, Input::old($field, !empty($package) ? $package->organization_name : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $error): ?>
						<div class="text-danger"><?php echo $error ?></div>
					<?php endforeach ?>
				<?php endif; ?>
			</div>
		
			<?php
				$field = 'atw_address_1';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'Address 1', array('class' => 'control-label')) ?> <span class="required">*</span>
				<?php echo Form::text($field, Input::old($field, !empty($package) ? $package->address_1 : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $error): ?>
						<div class="text-danger"><?php echo $error ?></div>
					<?php endforeach ?>
				<?php endif; ?>
			</div>
		
			<?php
				$field = 'atw_address_2';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'Address 2', array('class' => 'control-label')) ?>
				<?php echo Form::text($field, Input::old($field, !empty($package) ? $package->address_2 : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $error): ?>
						<div class="text-danger"><?php echo $error ?></div>
					<?php endforeach ?>
				<?php endif; ?>
			</div>
		
			<?php
				$field = 'atw_city';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'City', array('class' => 'control-label')) ?> <span class="required">*</span>
				<?php echo Form::text($field, Input::old($field, !empty($package) ? $package->city : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $error): ?>
						<div class="text-danger"><?php echo $error ?></div>
					<?php endforeach ?>
				<?php endif; ?>
			</div>
		
			<?php
				$field = 'atw_country';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'Country', array('class' => 'control-label')) ?> <span class="required">*</span>
				<?php echo Form::select($field, array('' => '') + Person::listCountries(), Input::old($field, !empty($package) ? $package->country : ''), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
				
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $error): ?>
						<div class="text-danger"><?php echo $error ?></div>
					<?php endforeach ?>
				<?php endif; ?>
			</div>
		
			<?php
				$field = 'atw_county';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'County', array('class' => 'control-label')) ?>
				<?php echo Form::select($field, array('' => '') + Person::listCounties(), Input::old($field, !empty($package) ? $package->county : ''), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
				
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $error): ?>
						<div class="text-danger"><?php echo $error ?></div>
					<?php endforeach ?>
				<?php endif; ?>
			</div>
		
			<?php
				$field = 'atw_postcode';
				$field_errors = $errors->get($field);
			?>
			<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::label($field, 'Postcode', array('class' => 'control-label')) ?> <span class="required">*</span>
				<?php echo Form::text($field, Input::old($field, !empty($package) ? $package->postcode : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<?php foreach ($field_errors as $error): ?>
						<div class="text-danger"><?php echo $error ?></div>
					<?php endforeach ?>
				<?php endif; ?>
			</div>
		</div>
		
		<?php
			$field = 'national_insurance';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'National insurance (NI)', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::text($field, Input::old($field, !empty($package) ? $package->national_insurance : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
		
		<?php
			$field = 'atw_email_address';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Email address', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::text($field, Input::old($field, !empty($package) ? $package->email_address : Auth::user()->email), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
		
		<?php
			$field = 'your_budget_in';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Your budget in', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => '') + AtwSupportPackage::listYourBudgetIn(), Input::old($field, !empty($package) ? $package->your_budget_in : ''), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
		
		<?php
			$field = 'your_budget';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, !empty($package) ? $package->budget : ''), array('class' => 'form-control', 'placeholder' => 'Your budget', 'id' => $field)) ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
		
		<?php
			$field = 'type_of_language_professional';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Type of language professional', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => '') + AtwSupportPackage::listTypeOfLanguageProfessional(), Input::old($field, !empty($package) ? $package->type_of_lsp : ''), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
		
		<?php
			$field = 'type_of_language_professional_other';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(!(Input::old('type_of_language_professional') == 'other' || (!empty($package) && $package->type_of_lsp == 'other'))): ?>hidden<?php endif; ?> <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, !empty($package) ? $package->type_of_lsp_other : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
		
		<?php
			$field = 'receive_invoice';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Do you want to receive the invoice', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => '') + AtwSupportPackage::listReceiveInvoice(), Input::old($field, !empty($package) ? $package->receive_invoice : ''), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
		
		<?php
			$field = 'additional_info';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Any additional information?', array('class' => 'control-label')) ?>
			<?php echo Form::text($field, Input::old($field, !empty($package) ? $package->additional_information : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)) ?>
		</div>
	</div>
	<div class="col-md-5 col-md-offset-1">
		<?php
			$field = 'atw_confirmation_letter';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Upload confirmation letter of your AtW', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::file($field, array('class' => '', 'placeholder' => '', 'id' => $field)); ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
		
		<?php
			$field = 'support_ends';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'When does your support end?', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::text($field, Input::old($field, !empty($package) ? date('d/m/Y', strtotime($package->support_end)) : ''), array('class' => 'form-control date-helper', 'placeholder' => 'dd/mm/yyyy', 'id' => $field)) ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
		
		<?php
			$field = 'atw_address_claim';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Which address do you post your claim forms to?', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => '') + AtwSupportPackage::listClaimAddresses(), Input::old($field, !empty($package) ? $package->claim_address : ''), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
		
		<?php
			$field = 'hours_per';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::label($field, 'Hom many hour(s) per ...', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => '') + AtwSupportPackage::listHowManyHoursPer(), Input::old($field, !empty($package) ? $package->hours_per : ''), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)) ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
		
		<?php
			$field = 'hours_per_value';
			$field_errors = $errors->get($field);
		?>
		<div class="line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
			<?php echo Form::text($field, Input::old($field, !empty($package) ? $package->hours_per_sum : ''), array('class' => 'form-control', 'placeholder' => 'No of hours', 'id' => $field)) ?>
			
			<?php if(count($field_errors) > 0): ?>
				<?php foreach ($field_errors as $error): ?>
					<div class="text-danger"><?php echo $error ?></div>
				<?php endforeach ?>
			<?php endif; ?>
		</div>
	</div>
</div>

<div>&nbsp;</div>

<div class="row line">
	<div class="col-md-12">
		<div class="pull-left">
			<?php if(Session::get('wizard') == 'true'): ?>
				<a class="btn btn-default" href="<?php echo action('FrontPageController@getProfileStep3') ?>">Previous</a>
			<?php else: ?>
				<a class="btn btn-default" href="<?php echo action('FrontPageController@getAtwPackages') ?>">Cancel</a>
			<?php endif; ?>
		</div>
		<div class="pull-right">
			<?php if(Session::get('wizard') == 'true'): ?>
				<button class="btn btn-primary" id="submit-next" role="button" type="submit" name="next">Complete profile</button>
			<?php else: ?>
				<button class="btn btn-primary" id="submit-next" role="button" type="submit" name="next">Save</button>
			<?php endif; ?>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#additional_support_package").change(function(){
			if($(this).is(':checked'))
				$("#submit-next").html('Save &amp; Continue');
			else
		<?php if(Session::get('wizard') == 'true'): ?>
			$("#submit-next").html('Complete profile');
		<?php else: ?>
			$("#submit-next").html('Save');
		<?php endif; ?>
	});
</script>

<?php echo Form::close(); ?>