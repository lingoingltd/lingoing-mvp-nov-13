<div style="margin-bottom:10px;">
	<a class="btn btn-link" href="<?php echo url('change-address') ?>">Add new address</a>
</div>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Address type</th>
			<th>Name / Organization name</th>
			<th>Address</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$addresses = Auth::user()->addresses;
			foreach($addresses as $address):
		?>
			<tr>
				<td><?php echo $address->getAddressTypeAsString(); ?></td>
				<td><?php echo $address->getName(); ?></td>
				<td><?php 
					echo !empty($address->address_1) ? $address->address_1 . ', ' : '';
					echo !empty($address->address_2) ? $address->address_2 . ', ' : '';
					echo !empty($address->postcode) ? $address->postcode . ' ' : '';
					echo !empty($address->city) ? $address->city . ', ' : '';
					echo !empty($address->county) ? $address->county . ', ' : '';
					echo !empty($address->country) ? Person::listCountries($address->country) : '';
				?></td>
				<td class="text-right">
					<a href="<?php echo url('change-address') ?>/<?php echo $address->id ?>">Change</a>
					<?php if($address->address_type != 'default'): ?>&nbsp;<a href="<?php echo url('remove-address') ?>/<?php echo $address->id ?>" onclick="return confirm('Do you really want to remove address?');" class="text-danger">Remove</a><?php endif; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>