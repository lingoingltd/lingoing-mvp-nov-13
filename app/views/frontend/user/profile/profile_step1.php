<?php echo Form::open(array('action' => 'FrontPageController@postProfileStep1', 'method' => 'post')); ?>
	
	<p class="lead">Login details - email address &amp; password</p>
	
	<?php
		$field = 'email_address';
	?>
	<div class="row line">
		<div class="col-md-8 email-address-container">
			<div>
				<label>Current email address:</label> <small>(all email notifications are send to this email address)</small><br>
				<span class="email-address"><?php echo AutH::user()->email ?></span>
			</div>
			<div style="margin-top:5px;">
				<a class="btn btn-blue" href="<?php echo url('change-email-address') ?>">Change email address</a>
			</div>
		</div>

		<div class="clearfix"></div>
		<div>&nbsp;</div>

		<div class="col-md-8 email-address-container">
			<div>
				<label>Password:</label>
			</div>
			<div>
				<a class="btn btn-blue" href="<?php echo url('change-password') ?>">Change password</a>
			</div>
		</div>
	</div>

	<p>&nbsp;</p>
	
	<p class="lead">Name &amp; Phone</p>
	
	<?php
		$field = 'title';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Title', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => '') + Person::listTitles(), Input::old($field, $default_address->title), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'first_name';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'First name', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, $default_address->first_name), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'last_name';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Last name', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, $default_address->last_name), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'job_title';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Job title', array('class' => 'control-label')) ?><br>
			<?php echo Form::text($field, Input::old($field, Auth::user()->job_title), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'mobile_number';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Mobile number', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, $default_address->mobile_number), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<p class="lead">Address</p>
	
	<?php
		$field = 'address_1';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">		
			<?php echo Form::label($field, 'Address 1', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, $default_address->address_1), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'address_2';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Address 2', array('class' => 'control-label')) ?><br>
			<?php echo Form::text($field, Input::old($field, $default_address->address_2), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'city';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Town/City', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, $default_address->city), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'country';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Country', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::select($field, array('' => '') + Person::listCountries(), Input::old($field, $default_address->country), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'county';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'County', array('class' => 'control-label')) ?><br>
			<?php echo Form::select($field, array('' => '') + Person::listCounties(), Input::old($field, $default_address->county), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'postcode';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Postcode', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, $default_address->postcode), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php if(Auth::user()->account_type == 'client'): ?>
		<p class="lead">Billing address</p>
	
		<div class="row line">
			<div class="col-md-8">
				<?php echo Form::checkbox('different_billing_address', 'yes', Input::old('different_billing_address', Session::get('different_billing_address')), array('id' => 'different_billing_address')); ?> <?php echo Form::label('different_billing_address', 'Use a different billing address?') ?>
			</div>
		</div>
	<?php endif; ?>
	
	<a name="profile-picture"></a>
	<p class="lead">Profile picture <span class="required">*</span></p>
	
	<div class="row line">
		<div class="col-md-8 my-profile-picture">
			<img src="<?php echo Auth::user()->getProfilePicture(); ?>"><br><a class="delete-profile-picture <?php if(empty(Auth::user()->profile_picture_url)): ?>hidden<?php endif; ?>" href="#">Delete picture</a><br>
			<small>Click on picture to change it (150x150px)</small>
			<?php echo Form::file('profile-picture', array('class' => '', 'placeholder' => '', 'id' => 'profile-picture-upload')); ?>
		</div>
	</div>

	<div class="row line">
		<div class="col-md-12">
			<div class="pull-right">
				<?php
					$label = 'Save';
					if(!(bool)Auth::user()->profile_completed)
						$label = 'Save &amp; Continue';
						
				?>
				<button class="btn btn-primary" role="button" type="submit" name="next" title="<?php echo $label ?>"><?php echo $label ?></button>
			</div>
		</div>
	</div>
<?php echo Form::close(); ?>

<div class="modal fade" id="modalRemoveProfilePicture" tabindex="-1" role="dialog" aria-labelledby="modalRemoveProfilePictureLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalRemoveProfilePictureLabel">Confirmation</h4>
			</div>

			<div class="modal-body text-center">
				Do you want to delete your <strong>profile picture</strong>?
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger delete-profile-picture-confirmation" data-url="<?php echo action('FrontPageController@getDeleteProfilePicture') ?>">Delete profile picture</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="showProfilePicture" tabindex="-1" role="dialog" aria-labelledby="showProfilePictureLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="showProfilePictureLabel">Profile picture</h4>
			</div>

			<div class="modal-body text-center">
				<img class="img-thumbnail profile-img" src="">

				<div class="text-left" style="margin-top:30px;margin-left:30px;">
					<label><input type="radio" name="profile-type" class="profile-type" value="select-area" checked="checked" id="profile-type-select-area"> select area you want ot use as profile picture</label><br>
					<label><input type="radio" name="profile-type" class="profile-type" value="whole-picture" id="profile-type-whole-picture"> use whole picture as profile picture</label><br>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary set-profile-picture-thumbnail" data-url="<?php echo action('FrontPageController@postSetProfilePicturePosition') ?>" data-position-x="" data-position-y="">Confirm</button>
			</div>
		</div>
	</div>
</div>