<div style="margin-bottom:10px;">
	<a class="btn btn-link" href="<?php echo url('change-atw') ?>">Add new support package</a>
</div>
<?php $message = Session::get('atw-success'); ?>
<?php $error_message = Session::get('atw-error'); ?>

<?php if(!empty($error_message)): ?>
	<p class="bg-error text-error">
		<?php echo $error_message ?>
	</p>
<?php endif; ?>
<?php if(!empty($message)): ?>
	<p class="bg-success text-success">
		<?php echo $message ?>
	</p>
<?php endif; ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Support given to</th>
			<th>National Insurance (NI)</th>
			<th>Email address</th>
			<th>Support ends</th>
			<th>Status</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$packages = Auth::user()->atw_packages();
			if(count($packages) > 0):
				foreach($packages as $package):
			?>
				<tr>
					<td><?php echo AtwSupportPackage::listGivenTo($package->given_to); ?></td>
					<td><?php echo $package->national_insurance; ?></td>
					<td><?php echo $package->email_address; ?></td>
					<td><?php echo date('d/m/Y', strtotime($package->support_end)); ?></td>
					
					<?php if($package->status == 'verified'): ?>
						<td class="success text-success">Verified</td>
					<?php elseif($package->status == 'in_review'): ?>
						<td class="bg-info text-info">In review</td>
					<?php elseif($package->status == 'canceled'): ?>
						<td class="danger text-danger">Canceled</td>
					<?php elseif($package->status == 'expired'): ?>
						<td class="danger text-danger"><strong>Expired</strong></td>
					<?php else: ?>
						<td class="bg-warning text-warning">Not verified</td>
					<?php endif; ?>
					
					<td class="text-right">
						<?php if($package->status != 'in_review' && $package->status != 'canceled'): ?>
							<a href="<?php echo url('change-atw', array('id' => $package->id)) ?>">Change</a>
						<?php endif; ?>
						
						<?php if($package->status == 'not_verified' || $package->status == 'canceled' || $package->isExpired()): ?>
							&nbsp;<a href="<?php echo url('remove-atw', array('id' => $package->id)) ?>" class="text-danger" onclick="return confirm('Are you sure you want to remove current AtW support package?');">Remove</a>
						<?php endif; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="6" class="text-center">You don't have any support package<br><a href="<?php echo url('change-atw') ?>">Add new support package</a></td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>
<p>Every support package has to be <strong>verified</strong> by our support team. You can't use it without verification.</p>