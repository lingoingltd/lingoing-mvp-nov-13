
<?php echo Form::open(array('action' => 'FrontPageController@postProfileStep5', 'method' => 'post')); ?>

	<?php
		$field = 'hourly_rate';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'What is your standard hourly rate?', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<div class="input-group">
				<span class="input-group-addon">£</span>
				<?php echo Form::text($field, Input::old($field, Auth::user()->hourly_rate), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
			</div>
			<small>Excluding VAT if applicable. This is ONLY for internal calculations, we will never disclose this figure to clients.</small>
		</div>
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'callout_time';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'What is your minimum call out time?', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => 'Choose') + Person::listCalloutTimes(), Input::old($field, Auth::user()->min_callout), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
			<small>in Hours</small>
		</div>
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'payment_method';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">	
		<div class="col-md-8">
			<?php echo Form::label($field, 'How would you prefer to be paid?', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => 'Choose') + Person::listPaymentMethods(), Input::old($field, Auth::user()->payment_type), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
			<small>We will contact you separately for your Stripe details.</small>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
    
    <div class="row line <?php if(Auth::user()->payment_type != "bacs"): ?>hidden<?php endif; ?>" id="profile-bacs">	
    	<?php 
			$subfield = "bacs_account_number";
			$suberrors = $errors->get($subfield);
		?>
    	<div class="clearfix <?php if(count($suberrors) > 0): ?>has-error<?php endif; ?>" style="margin-bottom:10px;">
            <div class="col-md-8">
                <?php echo Form::label("bacs_account_info", 'BACS account info', array('class' => 'control-label')) ?> <span class="required">*</span>
                <?php echo Form::text($subfield, Input::old($field, Auth::user()->bacs_account_number), array('class' => 'form-control', 'placeholder' => 'BACS Account number', 'id' => $subfield)); ?>
            </div>
            
            <?php if(count($suberrors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($suberrors as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
        </div>
        
        <?php 
			$subfield = "bacs_sort_code";
			$suberrors = $errors->get($subfield);
		?>
    	<div class="clearfix <?php if(count($suberrors) > 0): ?>has-error<?php endif; ?>">
            <div class="col-md-8">
                <?php echo Form::text($subfield, Input::old($field, Auth::user()->bacs_sort_code), array('class' => 'form-control', 'placeholder' => 'BACS sort code', 'id' => $subfield)); ?>
            </div>
            
            <?php if(count($suberrors) > 0): ?>
                <div class="col-md-4">
                    <?php foreach ($suberrors as $message): ?>
                        <div class="text-left text-danger"><?php echo $message; ?></div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
	</div>
    
    <?php 
			$subfield = "paypal_email";
			$suberrors = $errors->get($subfield);
		?>
    <div class="row line <?php if(Auth::user()->payment_type != "paypal"): ?>hidden<?php endif; ?> <?php if(count($suberrors) > 0): ?>has-error<?php endif; ?>" id="profile-paypal">	
    	<div class="col-md-8">
			<?php echo Form::label($subfield, 'PayPal account info', array('class' => 'control-label')) ?> <span class="required">*</span>
            <?php echo Form::text($subfield, Input::old($field, Auth::user()->paypal_email), array('class' => 'form-control', 'placeholder' => 'PayPal email', 'id' => $subfield)); ?>
        </div>
        
        <?php if(count($suberrors) > 0): ?>
            <div class="col-md-4">
                <?php foreach ($suberrors as $message): ?>
                    <div class="text-left text-danger"><?php echo $message; ?></div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
	</div>
	
	<?php
		$field = 'payment_period';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">	
		<div class="col-md-8">
			<?php echo Form::label($field, 'What is your preferred payment period?', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => 'Choose') + Person::listPaymentPeriod(), Input::old($field, Auth::user()->payment_period), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
		
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'lingoing_address';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">	
		<div class="col-md-8">
			<?php echo Form::label($field, 'Use Lingoing as my corresponding address on my invoices, as I would like to keep my details confidential', array('class' => 'control-label')) ?><br>
			<?php echo Form::select($field, array('0' => 'No', '1' => 'Yes'), Input::old($field, Auth::user()->use_lingoing_address), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
	</div>

	<div class="row line">
		<div class="col-md-12">
			<div class="pull-left">
				<a class="btn btn-default" href="<?php echo action('FrontPageController@getProfileStep4') ?>">Previous</a>
			</div>
			<div class="pull-right">
				<?php
					$label = 'Save';
					if(!Auth::user()->profile_completed)
						$label = 'Save &amp; Continue';
				?>
				<button class="btn btn-primary" role="button" type="submit" name="next" title="<?php echo $label ?>"><?php echo $label ?></button>
			</div>
		</div>
	</div>

<?php echo Form::close(); ?>