<?php echo Form::open(array('action' => 'FrontPageController@postProfileStep2', 'method' => 'post', 'class' => 'form-horizontal')); ?>

	<p class="lead">Language Skills</p>

	<div class="row line">
		<div class="col-md-8">
			<?php $field = 'sli'; ?>
			<?php echo Form::checkbox($field, 'yes', Input::old($field, Auth::user()->spoken_language_interpreter), array('id' => $field, 'class' => 'profile-languages')); ?> <?php echo Form::label($field, 'Spoken Language Interpreter', array('class' => 'control-label')) ?>
		</div>
	</div>
	
	<div class="sli_languages <?php if(Input::old('sli') != 'yes' && (!Auth::user()->spoken_language_interpreter)): ?>hidden<?php endif; ?>">
		<div class="row line">
			<div class="col-md-2 text-center">
				<strong>Membership body</strong>
			</div>
			<div class="col-md-2">
				<strong>Reference number</strong>
			</div>
			<div class="col-md-2">
				<strong>Expiry date</strong>
			</div>
			<div class="col-md-4">
				<strong>Language(s)</strong>
			</div>
			<div class="col-md-2">
				<strong>Insurance documents</strong>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('sli', 'AIIC'); ?>
			<?php echo Form::hidden('sli_aiic_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				AIIC
			</div>
			
			<?php $field = 'sli_aiic_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli_aiic_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">				
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli_aiic_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('sli', 'CiOL'); ?>
			<?php echo Form::hidden('sli_ciol_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				CiOL
			</div>
			
			<?php $field = 'sli_ciol_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli_ciol_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli_ciol_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('sli', 'ITI'); ?>
			<?php echo Form::hidden('sli_iti_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				ITI
			</div>
			
			<?php $field = 'sli_iti_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli_iti_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli_iti_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('sli', 'NRPSI'); ?>
			<?php echo Form::hidden('sli_nrpsi_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				NRPSI
			</div>
			
			<?php $field = 'sli_nrpsi_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli_nrpsi_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli_nrpsi_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
	</div>
	
	<div class="row line">
		<div class="col-md-8">
			<?php $field = 'sli2'; ?>
			<?php echo Form::checkbox($field, 'yes', Input::old($field, Auth::user()->sign_language_interpreter), array('id' => $field, 'class' => 'profile-languages')); ?> <?php echo Form::label($field, 'Sign Language Interpreter', array('class' => 'control-label')) ?>
		</div>
	</div>
	
	<div class="sli2_languages <?php if(Input::old('sli2') != 'yes' && (!Auth::user()->sign_language_interpreter)): ?>hidden<?php endif; ?>">
		<div class="row line">
			<div class="col-md-2 text-center">
				<strong>Membership body</strong>
			</div>
			<div class="col-md-2">
				<strong>Reference number</strong>
			</div>
			<div class="col-md-2">
				<strong>Expiry date</strong>
			</div>
			<div class="col-md-4">
				<strong>Language(s)</strong>
			</div>
			<div class="col-md-2">
				<strong>Insurance documents</strong>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('sli2', 'AIIC'); ?>
			<?php echo Form::hidden('sli2_aiic_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				AIIC
			</div>
			
			<?php $field = 'sli2_aiic_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli2_aiic_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli2_aiic_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSignLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('sli2', 'ASLI'); ?>
			<?php echo Form::hidden('sli2_asli_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				ASLI
			</div>
			
			<?php $field = 'sli2_asli_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli2_asli_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli2_asli_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSignLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				&nbsp;
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('sli2', 'NRCPD'); ?>
			<?php echo Form::hidden('sli2_nrcpd_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				NRCPD
			</div>
			
			<?php $field = 'sli2_nrcpd_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli2_nrcpd_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli2_nrcpd_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSignLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('sli2', 'VLP'); ?>
			<?php echo Form::hidden('sli2_vlp_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				VLP
			</div>
			
			<?php $field = 'sli2_vlp_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli2_vlp_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sli2_vlp_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSignLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				&nbsp;
			</div>
		</div>
	</div>
	
	<div class="row line">
		<div class="col-md-8">
			<?php $field = 'lipspeaker'; ?>
			<?php echo Form::checkbox($field, 'yes', Input::old($field, Auth::user()->lipspeaker), array('id' => $field, 'class' => 'profile-languages')); ?> <?php echo Form::label($field, 'Lipspeaker', array('class' => 'control-label')) ?>
		</div>
	</div>
	
	<div class="lipspeaker_languages <?php if(Input::old('lipspeaker') != 'yes' && (!Auth::user()->lipspeaker)): ?>hidden<?php endif; ?>">
		<div class="row line">
			<div class="col-md-2 text-center">
				<strong>Membership body</strong>
			</div>
			<div class="col-md-2">
				<strong>Reference number</strong>
			</div>
			<div class="col-md-2">
				<strong>Expiry date</strong>
			</div>
			<div class="col-md-4">
				<strong>Language(s)</strong>
			</div>
			<div class="col-md-2">
				<strong>Insurance documents</strong>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('lipspeaker', 'AIIC'); ?>
			<?php echo Form::hidden('lipspeaker_aiic_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				AIIC
			</div>
			
			<?php $field = 'lipspeaker_aiic_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'lipspeaker_aiic_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'lipspeaker_aiic_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('lipspeaker', 'CiOL'); ?>
			<?php echo Form::hidden('lipspeaker_ciol_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				CiOL
			</div>
			
			<?php $field = 'lipspeaker_ciol_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'lipspeaker_ciol_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'lipspeaker_ciol_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('lipspeaker', 'ITI'); ?>
			<?php echo Form::hidden('lipspeaker_iti_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				ITI
			</div>
			
			<?php $field = 'lipspeaker_iti_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'lipspeaker_iti_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'lipspeaker_iti_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('lipspeaker', 'NRPSI'); ?>
			<?php echo Form::hidden('lipspeaker_nrpsi_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				NRPSI
			</div>
			
			<?php $field = 'lipspeaker_nrpsi_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'lipspeaker_nrpsi_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'lipspeaker_nrpsi_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
	</div>
	
	<div class="row line">
		<div class="col-md-8">
			<?php $field = 'sttr'; ?>
			<?php echo Form::checkbox($field, 'yes', Input::old($field, Auth::user()->speech_to_text_reporter), array('id' => $field, 'class' => 'profile-languages')); ?> <?php echo Form::label($field, 'Speech to Text Reporter', array('class' => 'control-label')) ?>
		</div>
	</div>
	
	<div class="sttr_languages <?php if(Input::old('sttr') != 'yes' && (!Auth::user()->speech_to_text_reporter)): ?>hidden<?php endif; ?>">
		<div class="row line">
			<div class="col-md-2 text-center">
				<strong>Membership body</strong>
			</div>
			<div class="col-md-2">
				<strong>Reference number</strong>
			</div>
			<div class="col-md-2">
				<strong>Expiry date</strong>
			</div>
			<div class="col-md-4">
				<strong>Language(s)</strong>
			</div>
			<div class="col-md-2">
				<strong>Insurance documents</strong>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('sttr', 'AIIC'); ?>
			<?php echo Form::hidden('sttr_aiic_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				AIIC
			</div>
			
			<?php $field = 'sttr_aiic_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sttr_aiic_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sttr_aiic_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('sttr', 'CiOL'); ?>
			<?php echo Form::hidden('sttr_ciol_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				CiOL
			</div>
			
			<?php $field = 'sttr_ciol_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sttr_ciol_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sttr_ciol_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('sttr', 'ITI'); ?>
			<?php echo Form::hidden('sttr_iti_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				ITI
			</div>
			
			<?php $field = 'sttr_iti_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sttr_iti_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sttr_iti_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('sttr', 'NRPSI'); ?>
			<?php echo Form::hidden('sttr_nrpsi_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				NRPSI
			</div>
			
			<?php $field = 'sttr_nrpsi_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sttr_nrpsi_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'sttr_nrpsi_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
	</div>
	
	<div class="row line">
		<div class="col-md-8">
			<?php $field = 'slt'; ?>
			<?php echo Form::checkbox($field, 'yes', Input::old($field, Auth::user()->sign_language_translator), array('id' => $field, 'class' => 'profile-languages')); ?> <?php echo Form::label($field, 'Sign Language Translator', array('class' => 'control-label')) ?>
		</div>
	</div>
	
	<div class="slt_languages <?php if(Input::old('slt') != 'yes' && (!Auth::user()->sign_language_translator)): ?>hidden<?php endif; ?>">
		<div class="row line">
			<div class="col-md-2 text-center">
				<strong>Membership body</strong>
			</div>
			<div class="col-md-2">
				<strong>Reference number</strong>
			</div>
			<div class="col-md-2">
				<strong>Expiry date</strong>
			</div>
			<div class="col-md-4">
				<strong>Language(s)</strong>
			</div>
			<div class="col-md-2">
				<strong>Insurance documents</strong>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('slt', 'AIIC'); ?>
			<?php echo Form::hidden('slt_aiic_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				AIIC
			</div>
			
			<?php $field = 'slt_aiic_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'slt_aiic_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'slt_aiic_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSignLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('slt', 'ASLI'); ?>
			<?php echo Form::hidden('slt_asli_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				ASLI
			</div>
			
			<?php $field = 'slt_asli_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'slt_asli_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'slt_asli_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSignLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				&nbsp;
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('slt', 'NRCPD'); ?>
			<?php echo Form::hidden('slt_nrcpd_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				NRCPD
			</div>
			
			<?php $field = 'slt_nrcpd_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'slt_nrcpd_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'slt_nrcpd_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSignLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('slt', 'VLP'); ?>
			<?php echo Form::hidden('slt_vlp_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				VLP
			</div>
			
			<?php $field = 'slt_vlp_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'slt_vlp_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'slt_vlp_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSignLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				&nbsp;
			</div>
		</div>
	</div>
	
	<div class="row line">
		<div class="col-md-8">
			<?php $field = 'notetaker'; ?>
			<?php echo Form::checkbox($field, 'yes', Input::old($field, Auth::user()->note_taker), array('id' => $field, 'class' => 'profile-languages')); ?> <?php echo Form::label($field, 'Note taker', array('class' => 'control-label')) ?>
		</div>
	</div>
	
	<div class="notetaker_languages <?php if(Input::old('notetaker') != 'yes' && (!Auth::user()->note_taker)): ?>hidden<?php endif; ?>">
		<div class="row line">
			<div class="col-md-2 text-center">
				<strong>Membership body</strong>
			</div>
			<div class="col-md-2">
				<strong>Reference number</strong>
			</div>
			<div class="col-md-2">
				<strong>Expiry date</strong>
			</div>
			<div class="col-md-4">
				<strong>Language(s)</strong>
			</div>
			<div class="col-md-2">
				<strong>Insurance documents</strong>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('notetaker', 'AIIC'); ?>
			<?php echo Form::hidden('notetaker_aiic_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				AIIC
			</div>
			
			<?php $field = 'notetaker_aiic_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'notetaker_aiic_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'notetaker_aiic_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('notetaker', 'CiOL'); ?>
			<?php echo Form::hidden('notetaker_ciol_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				CiOL
			</div>
			
			<?php $field = 'notetaker_ciol_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'notetaker_ciol_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'notetaker_ciol_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('notetaker', 'ITI'); ?>
			<?php echo Form::hidden('notetaker_iti_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				ITI
			</div>
			
			<?php $field = 'notetaker_iti_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'notetaker_iti_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'notetaker_iti_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
		<div class="row line">
			<?php $membership = Auth::user()->searchForMembership('notetaker', 'NRPSI'); ?>
			<?php echo Form::hidden('notetaker_nrpsi_id', empty($membership) ? '' : $membership->id); ?>
			<div class="col-md-2 text-center membership-body">
				NRPSI
			</div>
			
			<?php $field = 'notetaker_nrpsi_reference_number'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) ? null : $membership->reference_number), array('class' => 'form-control', 'placeholder' => 'Ref. number ...', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'notetaker_nrpsi_expiry_date'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-2 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::text($field, Input::old($field, empty($membership) || empty($membership->expiry_date) ? null : \Carbon\Carbon::createFromFormat('Y-m-d', $membership->expiry_date)->format('d/m/Y')), array('class' => 'form-control date-helper', 'placeholder' => 'DD/MM/YYYY', 'id' => $field)) ?>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<?php $field = 'notetaker_nrpsi_languages'; ?>
			<?php $field_errors = $errors->get($field); ?>
			<div class="col-md-4 <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
				<?php echo Form::select($field.'[]', Person::listSpokenLanguageInterpreter(), Input::old($field, empty($membership) ? null : $membership->languages()->lists('language')), array('class' => 'form-control', 'placeholder' => 'Choose languages ...', 'id' => $field, 'multiple')) ?><br>
				
				<?php if(count($field_errors) > 0): ?>
					<div>
						<?php foreach ($errors->get($field) as $message): ?>
							<div class="text-left text-danger"><?php echo $message; ?></div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-2 insurance-document-required">
				<span class="required-small">* <strong>Required</strong> on next step</span>
			</div>
		</div>
	</div>
	
	<p class="lead">Specialisms</p>
	
	<div class="row line">		
		<div class="col-md-8">
			<?php $field = 'specialisms'; ?>
			<?php echo Form::select($field.'[]', Person::listSpecialisms(), Input::old($field, Auth::user()->specialisms()->lists('specialism')), array('class' => 'form-control multiple-selection', 'placeholder' => 'Choose specialisms ...', 'id' => $field, 'multiple')) ?><br>
		</div>
	</div>
	
	<div class="row line">
		<div class="col-md-12">
			<div class="pull-left">
				<a class="btn btn-default" href="<?php echo action('FrontPageController@getProfileStep1') ?>">Previous</a>
			</div>
			
			<div class="pull-right">
				<small>Please upload your insurance documents on next step</small>
				<button class="btn btn-primary" role="button" type="submit" name="next">Save &amp; Continue</button>
			</div>
		</div>
	</div>

<?php echo Form::close(); ?>