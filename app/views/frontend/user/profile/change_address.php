<?php echo Form::open(array('url' => url('change-address', array('id' => $address_id)), 'method' => 'post')); ?>

	<?php if($address == null): ?>
		<?php if(Session::get('address_type') == 'billing'): ?>
			<p class="lead">Add new billing address</p>
		<?php else: ?>
			<p class="lead">Add new address</p>
		<?php endif; ?>
	<?php else: ?>
		<?php if($address->address_type == 'default'): ?>
			<p class="lead">Change my <strong>default</strong> address</p>
		<?php elseif($address->address_type == 'billing'): ?>
			<p class="lead">Change my <strong>billing</strong> address</p>
		<?php else: ?>
			<p class="lead">Change my address</p>
		<?php endif; ?>
	<?php endif; ?>

	<?php
		$field = 'title';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Title', array('class' => 'control-label')) ?> <span class="required">*</span>
			<?php echo Form::select($field, array('' => '') + Person::listTitles(), Input::old($field, !empty($address) ? $address->title : ''), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'first_name';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'First name', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, !empty($address) ? $address->first_name : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'last_name';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Last name', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, !empty($address) ? $address->last_name : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'organization_name';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Organization Name', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, !empty($address) ? $address->organization_name : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'address_1';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">		
			<?php echo Form::label($field, 'Address 1', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, !empty($address) ? $address->address_1 : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'address_2';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Address 2', array('class' => 'control-label')) ?><br>
			<?php echo Form::text($field, Input::old($field, !empty($address) ? $address->address_2 : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'city';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Town/City', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, !empty($address) ? $address->city : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'country';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Country', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::select($field, array('' => '') + Person::listCountries(), Input::old($field, !empty($address) ? $address->country : ''), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'county';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'County', array('class' => 'control-label')) ?><br>
			<?php echo Form::select($field, array('' => '') + Person::listCounties(), Input::old($field, !empty($address) ? $address->county : ''), array('class' => 'form-control', 'placeholder' => 'Choose ...', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'postcode';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Postcode', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, !empty($address) ? $address->postcode : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php
		$field = 'mobile_number';
		$field_errors = $errors->get($field);
	?>
	<div class="row line <?php if(count($field_errors) > 0): ?>has-error<?php endif; ?>">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Mobile number', array('class' => 'control-label')) ?> <span class="required">*</span><br>
			<?php echo Form::text($field, Input::old($field, !empty($address) ? $address->mobile_number : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'phone_number';
		$field_errors = $errors->get($field);
	?>
	<div class="row line">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Phone number') ?><br>
			<?php echo Form::text($field, Input::old($field, !empty($address) ? $address->phone_number : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
		$field = 'fax_number';
		$field_errors = $errors->get($field);
	?>
	<div class="row line">
		<div class="col-md-8">
			<?php echo Form::label($field, 'Fax number') ?><br>
			<?php echo Form::text($field, Input::old($field, !empty($address) ? $address->fax_number : ''), array('class' => 'form-control', 'placeholder' => '', 'id' => $field)); ?>
		</div>
	
		<?php if(count($field_errors) > 0): ?>
			<div class="col-md-4">
				<?php foreach ($errors->get($field) as $message): ?>
					<div class="text-left text-danger"><?php echo $message; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	
	<?php if($address == null || $address->address_type != 'default'): ?>
		<?php
			$params = array('id' => 'billing_address');
			$check = null;
			if(Session::get('address_type') == 'billing')
			{
				$check = true;
				$params['disabled'] = 'disabled';
			}
			else
				$check = Input::old('billing_address', !empty($address) ? $address->address_type == 'billing' : false);
		?>
		<div class="row line">
			<div class="col-md-8">
				<?php echo Form::checkbox('billing_address', 'yes', $check, $params); ?> <label for="billing_address">Set as <strong>Billing</strong> address</label>
			</div>
		</div>
	<?php endif; ?>

	<div class="row line">
		<div class="col-md-12">
			<div class="pull-left">
				<?php if(Session::get('wizard') == 'true'): ?>
					<a class="btn btn-default" href="<?php echo action('FrontPageController@getProfileStep1') ?>">Previous</a>
				<?php else: ?>
					<a class="btn btn-default" href="<?php echo action('FrontPageController@getMyAddressBook') ?>">Cancel</a>
				<?php endif; ?>
			</div>
			<div class="pull-right">
				<?php if(Session::get('wizard') == 'true'): ?>
					<button class="btn btn-primary" role="button" type="submit" name="next">Save &amp; Continue</button>
				<?php else: ?>
					<button class="btn btn-primary" role="button" type="submit" name="next">Save</button>
				<?php endif; ?>
			</div>
		</div>
	</div>

<?php echo Form::close(); ?>