<?php
$s3 = App::make('aws')->get('s3');
?>
<table class="table table-striped">
	<thead>
		<tr>
			<th class="col-md-8">Document</th>
			<th class="col-md-2">Uploaded</th>
			<th class="col-md-2 text-right">Size</th>
		</tr>
	</thead>
	
	<tbody>
		<?php foreach ($documents as $document): ?>
			<tr>
				<td><a href="<?php echo $s3->getObjectUrl(
					Config::get('amazon.bucket_prefix') . 'docs',
					$document->filename,
					'+1 hour'
				); ?>"><?php echo $document->filename ?></a></td>
				<td><?php echo \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $document->created_at)->format('d/m/Y H:i'); ?></td>
				<td class="text-right"><?php echo $document->humanReadableFilesize(); ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>
