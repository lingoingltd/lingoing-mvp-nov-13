<h1><?php echo Auth::user()->getName(); ?>'s dashboard</h1>

<div class="col-md-6">
	<div class="border notification-center">
		<p class="lead">Notifications</p>
		<?php if($notifications->count() > 0): ?>
			<?php foreach ($notifications as $notification): ?>
				<?php if(empty($notification->sender_id)): ?>
					<div class="notification-container <?php if(!(bool)$notification->read): ?>unread<?php endif; ?>">
						<div class="notification-text">
							<?php echo $notification->renderText(); ?><br>
							<?php if(!empty($notification->link)): ?> <a href="<?php echo url($notification->renderLink()) ?>"><?php echo $notification->link_title ?></a><br><?php endif; ?>
							<small><?php echo $notification->showTimePassedCreated() ?></small>
						</div>
					</div>
				<?php else: ?>
					<div class="notification-container <?php if(!(bool)$notification->read): ?>unread<?php endif; ?> clearfix">
						<div class="professional-picture">
							<img src="<?php echo $notification->sender->getProfilePicture() ?>" class="img-thumbnail">
						</div>
						<div class="notification-text-with-image">
							<?php echo $notification->renderText(); ?><br>
							<?php if(!empty($notification->link)): ?> <a href="<?php echo url($notification->renderLink()) ?>"><?php echo $notification->link_title ?></a><br><?php endif; ?>
							<small><?php echo $notification->showTimePassedCreated() ?></small>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach ?>
			
			<p class="text-center">
				<a href="<?php echo url('notifications') ?>">View all notifications &gt; &gt;</a>
			</p>
		<?php else: ?>
			<p class="text-center">You don't have any notification yet</p>
		<?php endif; ?>
	</div>
</div>
<div class="col-md-6">
	<div class="border profile-summary">
		<p class="lead">Profile summary</p>
		<div class="image-profile">
			<img src="<?php echo Auth::user()->getProfilePicture(); ?>" class="img-thumbnail">
		</div>
		<div class="profile-summary-details clearfix">
			<?php if(Auth::user()->profile_completed): ?>
				<h4><a href="<?php echo url('profile') ?>/<?php echo Auth::user()->id ?>"><?php echo Auth::user()->getName() ?></a></h4>
			<?php else: ?>
				<h4><a href="<?php echo action('FrontPageController@getProfileStep1') ?>"><?php echo Auth::user()->getName(); ?></a></h4>
			<?php endif; ?>
			<div class="profile-account-type"><strong>Account type</strong>: <?php echo Auth::user()->accountTypeAsString(); ?></div>
		</div>
	</div>
	
	<div class="border my-jobs-center top-space">
		<p class="lead">My jobs</p>
		
		<?php if(count($jobs) > 0): ?>
			<?php foreach ($jobs as $job): ?>
				<div class="dashboard_job">
					<p><a href="<?php echo action('JobsController@getView') ?>/<?php echo $job->id ?>"><?php echo $job->getTitle(); ?></a></p>
					<p><strong>Status:</strong> <?php echo Job::listStatuses($job->status); ?></p>
				</div>
			<?php endforeach ?>
			
			<?php if(Auth::user()->account_type == 'client'): ?>
				<?php if(!(bool)Auth::user()->profile_completed): ?>
					<a href="<?php echo url('profile-not-completed') ?>" class="btn btn-primary" title="Post a job">Post a Job</a>
				<?php elseif(!(bool)Auth::user()->confirmed): ?>
					<a href="<?php echo url('account-not-approved') ?>" class="btn btn-primary" title="Post a job">Post a Job</a>
				<?php else: ?>
					<a href="<?php echo url('post-a-job') ?>" class="btn btn-primary" title="Post a Job">Post a Job</a>
				<?php endif; ?>
			<?php else: ?>
				<a href="<?php echo url('my-jobs') ?>" class="btn btn-primary" title="My jobs">My jobs</a>
			<?php endif; ?>
		<?php else: ?>
			<p class="text-center">You don't have any jobs yet</p>
			<?php if(Auth::user()->account_type == 'client'): ?>
				<p class="text-center">
					<?php if(!(bool)Auth::user()->profile_completed): ?>
						<a href="<?php echo url('profile-not-completed') ?>" class="btn btn-primary" title="Post a job">Post a Job</a>
					<?php elseif(!(bool)Auth::user()->confirmed): ?>
						<a href="<?php echo url('account-not-approved') ?>" class="btn btn-primary" title="Post a job">Post a Job</a>
					<?php else: ?>
						<a href="<?php echo url('post-a-job') ?>" class="btn btn-primary" title="Post a Job">Post a Job</a>
					<?php endif; ?>
				</p>
			<?php else: ?>
				<p class="text-center"><a href="<?php echo url('my-jobs') ?>" class="btn btn-primary" title="My jobs">My jobs</a></p>
			<?php endif; ?>
		<?php endif; ?>
	</div>
	
	<div class="border my-invoices-center top-space">
		<p class="lead">My invoices</p>
		<p class="text-center">You don't have any invoices yet</p>
	</div>
</div>