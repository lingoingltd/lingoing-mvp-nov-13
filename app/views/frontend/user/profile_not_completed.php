<?php if(Session::has('success')): ?>
	<div class="col-md-12">
		<div class="alert alert-success complete-profile"><?php echo Session::get('success'); ?></div>
	</div>
<?php endif; ?>

<h1>Profile not completed!</h1>

<div class="col-md-12">
	<p class="text-center">You need to <strong><a href="<?php echo url('my-profile') ?>">complete profile</a></strong> before using our service.</p>
</div>