<h1>My professionals</h1>

<div class="col-md-3">
	<div class="btn-group-vertical my-jobs-menu">
		<a href="<?php echo url('my-professionals') ?>" class="btn <?php if($type != 'preferred' && $type != 'blocked'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">Professionals</a>
		<a href="<?php echo url('my-professionals/preferred') ?>" class="btn <?php if($type == 'preferred'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">Preferred professionals</a>
		<a href="<?php echo url('my-professionals/blocked') ?>" class="btn <?php if($type == 'blocked'): ?>btn-primary<?php else: ?>btn-default<?php endif; ?>">Blocked professionals</a>
	</div>
</div>

<div class="col-md-9">
	<?php if(count($professionals) > 0): ?>
		<?php foreach ($professionals as $p): ?>
			<?php
				$professional = null;
				if($type == 'preferred' || $type == 'blocked')
					$professional = $p->client_lsp;
				else
					$professional = $p;
			?>
			<div class="professional-container border-all bottom-space">
				<div class="job-title border-bottom-gray clearfix">
					<?php if(Auth::user()->id != $professional->id): ?>
						<?php if($type != 'blocked'): ?>
							<!--a class="btn btn-primary pull-right" href="#" title="Hire">Hire</a-->
						<?php endif; ?>
						
						<div class="btn-group <?php if($type == 'blocked'): ?>no-margin<?php endif; ?> pull-right">
						  	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						    	Options <span class="caret"></span>
						  	</button>
						  	<ul class="dropdown-menu" role="menu">
								<?php if(!in_array($professional->id, $preferred_list)): ?>
									<li><a href="#" class="add-to-preferred-list" data-id="<?php echo $professional->id ?>">Add to my preferred list</a></li>
								<?php else: ?>
									<li><a href="#" class="remove-from-preferred-list" data-id="<?php echo $professional->id ?>">Remove from my preferred list</a></li>
								<?php endif; ?>
								
								<?php if(!in_array($professional->id, $blocked_list)): ?>
									<li><a href="#" class="add-to-block-list" data-id="<?php echo $professional->id ?>">Add to my block list</a></li>
								<?php else: ?>
									<li><a href="#" class="remove-from-block-list" data-id="<?php echo $professional->id ?>">Remove from my block list</a></li>
								<?php endif; ?>
						  	</ul>
						</div>
						
					<?php endif; ?>

					<div class="professional-picture">
						<img src="<?php echo $professional->getProfilePicture() ?>" class="img-thumbnail">
					</div>
					<p class="lead professional-name">
						<a href="<?php echo url('profile') ?>/<?php echo $professional->id ?>" title="<?php echo $professional->getName() ?>"><?php echo $professional->getName() ?></a>
					</p>
				</div>
				<div class="professional-languages border-bottom-gray clearfix">
					<?php
						if($professional->spoken_language_interpreter): 
							$languages = $professional->membershipsLanguages('sli');
							$count = count($languages);
							$index = 0;
						?>
					
						<strong>Spoken language interpreter:</strong> 
						<?php foreach ($languages as $language): ?>
							<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
						<br>
					<?php endif;
					
					
						if($professional->sign_language_interpreter): 
							$languages = $professional->membershipsLanguages('sli2');
							$count = count($languages);
							$index = 0;
						?>
					
						<strong>Sign language interpreter:</strong> 
						<?php foreach ($languages as $language): ?>
							<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
						<br>	
					<?php endif;
					
						if($professional->lipspeaker):
							$languages = $professional->membershipsLanguages('lipspeaker');
							$count = count($languages);
							$index = 0;
						?>
					
						<strong>Lipspeaker:</strong> 
						<?php foreach ($languages as $language): ?>
							<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
						<br>
					<?php endif;
					
						if($professional->speech_to_text_reporter):						
							$languages = $professional->membershipsLanguages('sttr');
							$count = count($languages);
							$index = 0;
						?>
					
						<strong>Speech to text reporter:</strong> 
						<?php foreach ($languages as $language): ?>
							<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
						<br>
					<?php endif;
					
						if($professional->sign_language_translator):						
							$languages = $professional->membershipsLanguages('slt');
							$count = count($languages);
							$index = 0;
						?>
					
						<strong>Sign language translator:</strong> 
						<?php foreach ($languages as $language): ?>
							<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
						<br>
					<?php endif;
					
						if($professional->note_taker):
							$languages = $professional->membershipsLanguages('notetaker');
							$count = count($languages);
							$index = 0;
						?>
					
						<strong>Note taker:</strong> 
						<?php foreach ($languages as $language): ?>
							<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
						<br>
					<?php endif; ?>
				
					<?php
						$specialisms = $professional->specialisms();
						$count = count($specialisms);
						if($count > 0):
							$index = 0;
					?>
				
						<strong>Specialisms:</strong>
						<?php foreach ($specialisms as $language): ?>
							<?php echo $language->specialism; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
				
					<?php endif; ?>
				</div>
				<div class="professional-details clearfix">
					<div class="col-md-4 border-right-gray">
					<?php
						$rating = floor($professional->getRating());
					?>
						<strong>Rating:</strong> 
						<?php for ($i=1; $i <= $rating; $i++): ?> 
							<span class="glyphicon glyphicon-star gold-star"></span> 
						<?php endfor ?>
					
						<?php for($i=$rating + 1; $i <= 5; $i++): ?>
							<span class="glyphicon glyphicon-star"></span> 
						<?php endfor; ?>
					</div>
					<div class="col-md-4 border-right-gray">
						<?php if(!empty($professional->city) && !empty($professional->country)): ?>
							<strong>Location:</strong> <?php echo $professional->getShortLocation(); ?>
						<?php else: ?>
							<strong>Location:</strong> /
						<?php endif; ?>
					</div>
					<div class="col-md-4">
						
					</div>
				</div>
			</div>
		<?php endforeach ?>
	<?php else: ?>
		<?php if($type == 'preferred'): ?>
			<p class="text-center">You don't have any preferred professionals yet.</p>
		<?php elseif($type == 'blocked'): ?>
			<p class="text-center">You don't have any blocked professionals yet.</p>
		<?php else: ?>
			<p class="text-center">You don't have any professionals yet.</p>
		<?php endif; ?>
	<?php endif; ?>
</div>