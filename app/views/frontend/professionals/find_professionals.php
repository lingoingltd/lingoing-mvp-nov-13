<h1>Find language professionals</h1>

<div class="col-md-3">
	<?php echo Form::open(array('url' => '/professionals', 'method' => 'post', 'role' => 'form')); ?>
	<div class="white-container filter-container">
		<p class="lead">Filter professionals</p>
		
		<p>
			<?php echo Form::text('search_string', isset($filter) && isset($filter['search_string']) ? $filter['search_string'] : '', array('class' => 'form-control', 'id' => 'search_string', 'placeholder' => 'Professionals name ...')); ?>
		</p>
		
		<hr>
		
		<p><strong>Language skills</strong></p>
		<div>
			<label class="list-item">
				<?php echo Form::checkbox('spoken_language_interpreter', 'yes', isset($filter) && isset($filter['sli']) ? $filter['sli'] == 'yes' : false, array('id' => 'spoken_language_interpreter', 'class' => 'list-item-checkbox')) ?>
				Spoken Language Interpreter
			</label>
			<div <?php if(!(isset($filter) && isset($filter['sli']) && $filter['sli'] == 'yes')): ?>class="hidden"<?php endif; ?>>
				<?php echo Form::select('sli_language[]', Person::listSpokenLanguageInterpreter(), isset($filter) && isset($filter['sli_languages']) ? $filter['sli_languages'] : null, array('id' => 'sli_language', 'class' => 'form-control', 'placeholder' => 'All languages', 'multiple' => 'multiple')) ?>
			</div>
		</div>
		<div>
			<label class="list-item">
				<?php echo Form::checkbox('sign_language_interpreter', 'yes', isset($filter) && isset($filter['sli2']) ? $filter['sli2'] == 'yes' : false, array('id' => 'sign_language_interpreter', 'class' => 'list-item-checkbox')) ?>
				Sign Language Interpreter
			</label>
			<div <?php if(!(isset($filter) && isset($filter['sli2']) && $filter['sli2'] == 'yes')): ?>class="hidden"<?php endif; ?>>
				<?php echo Form::select('sli2_language[]', Person::listSignLanguageInterpreter(), isset($filter) && isset($filter['sli2_languages']) ? $filter['sli2_languages'] : null, array('id' => 'sli2_language', 'class' => 'form-control', 'placeholder' => 'All languages', 'multiple' => 'multiple')) ?>
			</div>
		</div>
		<div>
			<label class="list-item">
				<?php echo Form::checkbox('lipspeaker', 'yes', isset($filter) && isset($filter['lipspeaker']) ? $filter['lipspeaker'] == 'yes' : false, array('id' => 'lipspeaker', 'class' => 'list-item-checkbox')) ?>
				Lipspeaker
			</label>
			<div <?php if(!(isset($filter) && isset($filter['lipspeaker']) && $filter['lipspeaker'] == 'yes')): ?>class="hidden"<?php endif; ?>>
				<?php echo Form::select('lipspeaker_language[]', Person::listLipspeaker(), isset($filter) && isset($filter['lipspeaker_languages']) ? $filter['lipspeaker_languages'] : null, array('id' => 'lipspeaker_language', 'class' => 'form-control', 'placeholder' => 'All languages', 'multiple' => 'multiple')) ?>
			</div>
		</div>
		<div>
			<label class="list-item">
				<?php echo Form::checkbox('speech_to_text_reporter', 'yes', isset($filter) && isset($filter['sttr']) ? $filter['sttr'] == 'yes' : false, array('id' => 'speech_to_text_reporter', 'class' => 'list-item-checkbox')) ?>
				Speech to Text Reporter
			</label>
			<div <?php if(!(isset($filter) && isset($filter['sttr']) && $filter['sttr'] == 'yes')): ?>class="hidden"<?php endif; ?>>
				<?php echo Form::select('sttr_language[]', Person::listSpokenLanguageInterpreter(), isset($filter) && isset($filter['sttr_languages']) ? $filter['sttr_languages'] : null, array('id' => 'sttr_language', 'class' => 'form-control', 'placeholder' => 'All languages', 'multiple' => 'multiple')) ?>
			</div>
		</div>
		<div>
			<label class="list-item">
				<?php echo Form::checkbox('sign_language_translator', 'yes', isset($filter) && isset($filter['slt']) ? $filter['slt'] == 'yes' : false, array('id' => 'sign_language_translator', 'class' => 'list-item-checkbox')) ?>
				Sign Language Translator
			</label>
			<div <?php if(!(isset($filter) && isset($filter['slt']) && $filter['slt'] == 'yes')): ?>class="hidden"<?php endif; ?>>
				<?php echo Form::select('slt_language[]', Person::listSignLanguageInterpreter(), isset($filter) && isset($filter['slt_languages']) ? $filter['slt_languages'] : null, array('id' => 'slt_language', 'class' => 'form-control', 'placeholder' => 'All languages', 'multiple' => 'multiple')) ?>
			</div>
		</div>
		<div>
			<label class="list-item">
				<?php echo Form::checkbox('notetaker', 'yes', isset($filter) && isset($filter['notetaker']) ? $filter['notetaker'] == 'yes' : false, array('id' => 'notetaker', 'class' => 'list-item-checkbox')) ?>
				Note taker
			</label>
			<div <?php if(!(isset($filter) && isset($filter['notetaker']) && $filter['notetaker'] == 'yes')): ?>class="hidden"<?php endif; ?>>
				<?php echo Form::select('notetaker_language[]', Person::listSpokenLanguageInterpreter(), isset($filter) && isset($filter['notetaker_languages']) ? $filter['notetaker_languages'] : null, array('id' => 'notetaker_language', 'class' => 'form-control', 'placeholder' => 'All languages', 'multiple' => 'multiple')) ?>
			</div>
		</div>
		
		<hr>
		<p><strong>Specialism</strong></p>
		<div>
			<?php echo Form::select('specialism[]', Person::listSpecialisms(), isset($filter) && isset($filter['specialism']) ? $filter['specialism'] : null, array('id' => 'specialism', 'class' => 'form-control', 'placeholder' => 'All specialisms', 'multiple' => 'multiple')) ?>
		</div>
		
		<!--hr>
		<p><strong>Location</strong></p>
		<div>
			<?php echo Form::select('location', array('' => '') + Person::listCountries(), isset($filter) && isset($filter['location']) ? $filter['location'] : '', array('id' => 'location', 'class' => 'form-control', 'placeholder' => 'All locations')) ?>
		</div-->
		<p>&nbsp;</p>
		
		<p class="text-center">
			<button type="submit" name="search" class="btn btn-primary text-center"><span class="glyphicon glyphicon-search"></span> Search</button>
			<button type="submit" name="show_all" class="btn btn-default text-center">Show all</button>
		</p>
	</div>
	<?php echo Form::close(); ?>
</div>

<div class="col-md-9">
	<?php if(count($professionals) > 0): ?>
		<?php foreach ($professionals as $professional): ?>
			<div class="professional-container border-all bottom-space">
				<div class="job-title border-bottom-gray clearfix">
					<?php if(!Auth::check() || (Auth::user()->id != $professional->id)): ?>
						<!--<?php if(Auth::check()): ?>
							<a class="btn btn-primary pull-right" href="#" title="Hire">Hire</a>
						<?php else: ?>
							<a class="btn btn-primary pull-right" href="<?php echo url('register') ?>" title="Hire">Hire</a>
						<?php endif; ?>-->

						<?php if(Auth::check()): ?>
							<div class="btn-group pull-right">
							  	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
							    	Options <span class="caret"></span>
							  	</button>
							  	<ul class="dropdown-menu" role="menu">
									<?php if(!in_array($professional->id, $preferred_list)): ?>
										<li><a href="#" class="add-to-preferred-list" data-id="<?php echo $professional->id ?>">Add to my preferred list</a></li>
									<?php else: ?>
										<li><a href="#" class="remove-from-preferred-list" data-id="<?php echo $professional->id ?>">Remove from my preferred list</a></li>
									<?php endif; ?>

									<?php if(!in_array($professional->id, $blocked_list)): ?>
										<li><a href="#" class="add-to-block-list" data-id="<?php echo $professional->id ?>">Add to my block list</a></li>
									<?php else: ?>
										<li><a href="#" class="remove-from-block-list" data-id="<?php echo $professional->id ?>">Remove from my block list</a></li>
									<?php endif; ?>
							  	</ul>
							</div>
						<?php endif; ?>

					<?php endif; ?>

					<div class="professional-picture">
						<img src="<?php echo $professional->getProfilePicture() ?>" class="img-thumbnail">
					</div>
					<p class="lead professional-name">
						<a href="<?php echo url('profile') ?>/<?php echo $professional->id ?>"><?php 
							if(Auth::check()):
								echo $professional->getName();
							else:
								$address = $professional->default_address();
								if($address != null)
									echo $address->first_name;
								else
									echo $professional->email;
							endif;
						?></a>
					</p>
				</div>
				<div class="professional-languages border-bottom-gray clearfix">
					<?php
						if($professional->spoken_language_interpreter): 
							$languages = $professional->membershipsLanguages('sli');
							$count = count($languages);
							$index = 0;
						?>
					
						<strong>Spoken language interpreter:</strong> 
						<?php foreach ($languages as $language): ?>
							<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
						<br>
					<?php endif;
					
					
						if($professional->sign_language_interpreter): 
							$languages = $professional->membershipsLanguages('sli2');
							$count = count($languages);
							$index = 0;
						?>
					
						<strong>Sign language interpreter:</strong> 
						<?php foreach ($languages as $language): ?>
							<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
						<br>	
					<?php endif;
					
						if($professional->lipspeaker):
							$languages = $professional->membershipsLanguages('lipspeaker');
							$count = count($languages);
							$index = 0;
						?>
					
						<strong>Lipspeaker:</strong> 
						<?php foreach ($languages as $language): ?>
							<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
						<br>
					<?php endif;
					
						if($professional->speech_to_text_reporter):						
							$languages = $professional->membershipsLanguages('sttr');
							$count = count($languages);
							$index = 0;
						?>
					
						<strong>Speech to text reporter:</strong> 
						<?php foreach ($languages as $language): ?>
							<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
						<br>
					<?php endif;
					
						if($professional->sign_language_translator):						
							$languages = $professional->membershipsLanguages('slt');
							$count = count($languages);
							$index = 0;
						?>
					
						<strong>Sign language translator:</strong> 
						<?php foreach ($languages as $language): ?>
							<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
						<br>
					<?php endif;
					
						if($professional->note_taker):
							$languages = $professional->membershipsLanguages('notetaker');
							$count = count($languages);
							$index = 0;
						?>
					
						<strong>Note taker:</strong> 
						<?php foreach ($languages as $language): ?>
							<?php echo $language->language; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
						<br>
					<?php endif; ?>
				
					<?php
						$specialisms = $professional->specialisms();
						$count = count($specialisms);
						if($count > 0):
							$index = 0;
					?>
				
						<strong>Specialisms:</strong>
						<?php foreach ($specialisms as $language): ?>
							<?php echo $language->specialism; ?><?php if($index < $count - 1): ?>, <?php endif; ?>
							<?php $index++; ?>
						<?php endforeach ?>
				
					<?php endif; ?>
				</div>
				<div class="professional-details clearfix">
					<div class="col-md-4 border-right-gray">
					<?php
						$rating = floor($professional->getRating());
					?>
						<strong>Rating:</strong> 
						<?php for ($i=1; $i <= $rating; $i++): ?> 
							<span class="glyphicon glyphicon-star gold-star"></span> 
						<?php endfor ?>

						<?php for($i=$rating + 1; $i <= 5; $i++): ?>
							<span class="glyphicon glyphicon-star"></span> 
						<?php endfor; ?>
					</div>
					<div class="col-md-4 border-right-gray">
						<?php $defaultAddress = $professional->default_address(); ?>
						<?php if(!empty($defaultAddress->city) && !empty($defaultAddress->country)): ?>
							<strong>Location:</strong> <?php echo $defaultAddress->getShortLocation() ?>
						<?php else: ?>
							<strong>Location:</strong> /
						<?php endif; ?>
					</div>
					<div class="col-md-4">
						
					</div>
				</div>
			</div>
		<?php endforeach ?>
	
		<?php echo View::make('frontend.common.paging', array(
			'page' => $page,
			'max_pages' => $max_pages,
			'url' => 'professionals'
		)) ?>
	<?php else: ?>
		<p class="text-center">Currently there are no language professionals</p>
	<?php endif; ?>
</div>