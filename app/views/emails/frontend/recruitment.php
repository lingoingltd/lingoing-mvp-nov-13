<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Somebody apply for the recruitment job “<?php echo $recruitment_title ?>”</p>
<p>
	Name: <?php echo $application_name; ?><br>
	Email: <?php echo $application_email; ?><br>
	Address: <?php echo $application_address ?><br>
	Phone: <?php echo $application_phone ?><br>
	Our question to you: <?php echo $recruitment_question ?><br>
	Answer: <?php echo $application_answer ?><br>
	Attachment: <a href="<?php echo $attachment_url ?>"><?php echo $filename ?></a><br><br>
</p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>