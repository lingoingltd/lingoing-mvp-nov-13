<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Dear <?php echo $person_name ?>,</p>
<p>Welcome to Lingoing.com. Hope you will <strong>enjoy it</strong>!</p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>