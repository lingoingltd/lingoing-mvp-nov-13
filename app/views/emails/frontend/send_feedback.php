<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Somebody sent a feedback:</p>
<p>
	Name: <?php echo $name ?><br>
	Email: <?php echo $email ?><br>
	Question: <br>
	<?php echo nl2br($text); ?>
</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>