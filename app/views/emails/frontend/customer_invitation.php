<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Dear <?php echo $client_name ?>,</p>
<p><?php echo $person_name ?> send you an invitation for registering to Lingoing.com.</p>

<?php if(!empty($text)): ?>
	<p><?php echo nl2br($text); ?></p>
<?php endif; ?>

<p>Registration link:</p>
<p><?php echo URL::to('register', array('token' => $token)) ?></p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>