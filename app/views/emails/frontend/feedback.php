<?php echo View::make('layouts.email_newsletter_header'); ?>

<p><strong><?php echo $name ?></strong> sent you a feedback:</p>
<p><?php echo nl2br($comment) ?></p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>