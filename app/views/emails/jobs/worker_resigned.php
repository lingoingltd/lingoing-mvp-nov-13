<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>LSP resigned</h2>
		
		<div>
			<?php echo $worker_name ?> just resigned from the job “<?php echo $job_name ?>”. Job status has changed to <strong>Awaiting applications</strong>. The job is back on the market.<br>
			<a href="<?php echo URL::to('jobs/view') ?>/<?php echo $job_id ?>/applicants">Job “<?php echo $job_name ?>” applicants</a><br><br>
			Lingoing.com team
		</div>
	</body>
</html>