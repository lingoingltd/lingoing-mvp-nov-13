<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Dear <?php echo $person_name ?>,</p>
<p>You just send an application for job “<?php echo $job_name ?>”</p>
<p><a href="<?php echo URL::to('jobs/view') ?>/<?php echo $job_id ?>">Job “<?php echo $job_name ?>”</a></p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>