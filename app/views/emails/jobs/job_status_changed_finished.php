<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Your job “<?php echo $job_name ?>” comes to an end. Job status changed to <strong>Finished</strong>.</p>
<p><a href="<?php echo URL::to('jobs/view') ?>/<?php echo $job_id ?>">Job “<?php echo $job_name ?>” details</a></p>
<p>King regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>