<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>You were just hired you for a job “<?php echo $job_name ?>”.</p>
<p><a href="<?php echo URL::to('jobs/view') ?>/<?php echo $job_id ?>">Job “<?php echo $job_name ?>” details</a></p>
<p>King regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>