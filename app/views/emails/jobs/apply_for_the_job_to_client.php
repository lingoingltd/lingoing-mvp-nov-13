<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>
	<img src="<?php echo $profile_image ?>" style="float:left;margin-right:15px;margin-bottom:15px;background-color:#FFFFFF;border:1px solid #DDDDDD;border-radius:4px;padding:4px;width:90px;">
	Dear <?php echo $client_name ?>,<br><br>
	My name is <?php echo $worker_name ?> and I just send an application for your job “<?php echo $job_name ?>”.<br>
	You are welcome to check out my <a href="<?php echo $profile_link ?>">profile</a>.
</p>
<p><a href="<?php echo URL::to('jobs/view') ?>/<?php echo $job_id ?>/applicants">Job “<?php echo $job_name ?>”</a></p>
<p>Kind regards<br><?php echo $worker_name ?></p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>