<?php echo View::make('layouts.email_newsletter_header'); ?>

<p><?php echo $worker_name ?> just want recommend you the job “<?php echo $job_name ?>”.</p>
<p><a href="<?php echo URL::to('jobs/view') ?>/<?php echo $job_id ?>">Job “<?php echo $job_name ?>”</a></p>
<p>
	<strong>Here is his/her message:</strong><br>
	<?php echo nl2br($text) ?>
</p>
<p>King regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>