<?php echo View::make('layouts.email_newsletter_header'); ?>

<p><?php echo $person_name ?> just send a message for a job.</p>
<p><a href="<?php echo URL::to('jobs/view') ?>/<?php echo $job_id ?>/messages">Job “<?php echo $job_name ?>” messages</a></p>
<p>King regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>