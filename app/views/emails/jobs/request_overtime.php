<?php echo View::make('layouts.email_newsletter_header'); ?>

<p><?php echo $worker_name ?> just requested an overtime for job “<?php echo $job_name ?>”.</p>
<p><a href="<?php echo URL::to('jobs/view') ?>/<?php echo $job_id ?>/overtime">Job “<?php echo $job_name ?>” overtimes</a></p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>