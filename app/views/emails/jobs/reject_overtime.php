<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Somebody apply for the recruitment job “<?php echo $recruitment_title ?>”</p>
<p><?php echo $client_name ?> just rejected your request for overtime on job “<?php echo $job_name ?>”.</p>
<p><a href="<?php echo URL::to('jobs/view') ?>/<?php echo $job_id ?>/overtime">Job “<?php echo $job_name ?>” overtimes</a></p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>