<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Job status changed to <strong>Confirmed</strong>.</p>
<p><a href="<?php echo URL::to('jobs/view') ?>/<?php echo $job_id ?>">Job “<?php echo $job_name ?>” details</a></p>
<p>King regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>