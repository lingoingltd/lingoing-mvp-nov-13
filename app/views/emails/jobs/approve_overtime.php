<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Overtime request approved</h2>
		
		<div>
			<?php echo $client_name ?> just approved your request for overtime on job “<?php echo $job_name ?>”.<br>
			<a href="<?php echo URL::to('jobs/view') ?>/<?php echo $job_id ?>/overtime">Job “<?php echo $job_name ?>” overtimes</a><br><br>
			Lingoing.com team
		</div>
	</body>
</html>