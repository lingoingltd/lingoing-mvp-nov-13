<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>
Dear Lingoists,<br>
<br>
Great to have you registered on our site and we look forward to working together.<br>
<br>
We have a whole range of jobs requesting British Sign Language interpreters this week. These are just a few:<br>
<br>
<ul style="line-height:1.3em;">
	<?php foreach ($jobs as $job): ?>
		<?php
			$type = $job->type == 'other' ? $job->type_other : $job::listTypes($job->type);
			$date = $job->firstDate();
			
			$start_date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date->date . ' ' . $date->start_time);
			$end_date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date->date . ' ' . $date->end_time);
			
			$duration = $job->getDurationInHours();
		?>
		<li><a href="http://lingoing.com/jobs/view/<?php echo $job->id ?>"><?php echo $type ?></a>, <?php echo $start_date->format('d. M Y') ?>, <?php echo $start_date->format('H:i') ?> - <?php echo $end_date->format('H:i') ?> (<?php echo $duration; ?> <?php echo $duration == 1 ? 'hour' : 'hours'; ?>)<?php if($job->payment_method == 'atw_customer'): ?>, Access to Work customer<?php endif; ?> - <strong><?php if($job->project_budget == 'open_to_quotes'): ?>Open to quotes budget<?php else: ?>£<?php echo number_format($job->budget_lsp_only, 2) ?><?php endif; ?></strong></li>
	<?php endforeach ?>
</ul>
<br>
Visit <a href="http://lingoing.com">www.lingoing.com</a> to apply and there are many more jobs on their way.<br>
<br>
Have a fantastic day!<br>
<br>
Team Lingoing
</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>