<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Your payment for invoice at Lingoing.com - <strong><?php echo $invoice->number ?></strong> is <strong>pending</strong>.</p>
<p>It takes about 5 working days before transaction is confirmed.</p>
<p>We will keep you posted.</p>
<p><a href="<?php echo URL::to('my-invoices') ?>/paid">My invoices</a></p>
<p>King regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>