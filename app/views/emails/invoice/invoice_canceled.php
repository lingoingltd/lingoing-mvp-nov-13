<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Your payment for invoice at Lingoing.com - <strong><?php echo $invoice->number ?></strong> has been <strong>CANCELED</strong>.</p>
<p><a href="<?php echo URL::to('my-invoices') ?>/paid">My invoices</a></p>
<p>King regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>