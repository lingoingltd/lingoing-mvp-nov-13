<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>You just received an invoice for the job “<?php echo $job_name ?>”.</p>
<p><a href="<?php echo URL::to('my-invoices') ?>">My invoices</a></p>
<p>King regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>