<?php echo (string)View::make('layouts.email_newsletter_header'); ?>

<p>Somebody sent you a message via “For Clients” form:</p>
<p><strong>Client full name:</strong> <?php echo $client_name ?></p>
<p><strong>Email address:</strong> <?php echo $email_address ?></p>
<p><strong>Contact number:</strong> <?php echo $contact_number ?></p>
<p><strong>What support are you looking for/How can we help you?</strong><br> <?php echo nl2br($comments); ?></p>

<?php echo (string)View::make('layouts.email_newsletter_footer'); ?>