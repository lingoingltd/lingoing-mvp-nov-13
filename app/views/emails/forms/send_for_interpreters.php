<?php echo (string)View::make('layouts.email_newsletter_header'); ?>

<p>Somebody sent you a message via “For Interpreters” form:</p>
<p><strong>Interpreter full name:</strong> <?php echo $interpreter_name ?></p>
<p><strong>Email address:</strong> <?php echo $email_address ?></p>
<p><strong>Contact number:</strong> <?php echo $contact_number ?></p>
<p><strong>Location/Region:</strong> <?php echo $location_region ?></p>
<p><strong>Language skills:</strong> <?php foreach ($language_skills as $skill): ?>
	<?php echo $skill ?>, 
<?php endforeach ?></p>
<p>
	<table>
		<thead>
		<tr>
			<th>Membership body</th>
			<th>Reference number</th>
			<th>Expiration date</th>
			<th>Languages</th>
		</tr>
		</thead>
		<tbody>
			<?php foreach ($memberships as $body => $data): ?>
				<?php
					if(empty($data['reference_number']) && empty($data['expiration_date']) && empty($data['languages']))
						continue;
				?>
				<tr>
					<td><?php echo $body ?></td>
					<td><?php echo $data['reference_number'] ?></td>
					<td><?php echo $data['expiration_date'] ?></td>
					<td><?php echo $data['languages'] ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</p>
<p><strong>Hourly rate:</strong> <?php echo $hourly_rate ?></p>
<p><strong>Qualified/Trainee/CSW (level)?</strong> <?php echo $expert_level ?></p>
<p><strong>Specialisms</strong> <?php foreach ($specialisms as $specialism): ?>
	<?php echo $specialism ?>, 
<?php endforeach ?></p>
<p><strong>Additional Qualification/Accreditations/Comments:</strong><br> <?php echo nl2br($comments); ?></p>

<?php echo (string)View::make('layouts.email_newsletter_footer'); ?>