<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Dear <?php echo $person_name ?>,</p>
<p>Your Membership <strong><?php echo $membership_body ?> (expire: <?php echo $expiration_date ?></strong>) will expire in about a month!</p>
<p><a href="<?php echo URL::to('profile_step2') ?>">My language skills</a></p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>