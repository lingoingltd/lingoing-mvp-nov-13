<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Dear <?php echo $person_name ?>,</p>
<p>You just changed your email address on Lingoing.com. You have to confirm new email before using it.</p>
<p>Your new email address: <strong><?php echo $unconfirmed_email ?></strong></p>
<p><a href="<?php echo URL::to('confirm-new-email') ?>/<?php echo $new_email_confirmation ?>">Confirm new email address</a></p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>