<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Dear Lingoing's user,</p>
<p>To reset your password, complete this form: <?php echo URL::to('password-reset', array($token)); ?>.</p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>