<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Dear <?php echo $person_name ?>,</p>
<p>Our support team has just approved your Access to Work support package.</p>
<p><a href="<?php echo URL::to('atw-packages') ?>">My "Access to Work" packages</a></p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>