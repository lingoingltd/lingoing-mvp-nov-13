<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Invoice <strong><?php echo $invoice->number ?></strong> just receive an unknown status <strong><?php echo $status; ?></strong>.</p>
<p><a href="https://developer.gocardless.com/php/#bill-webhook">Possible GoCardless responses</a></p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>