<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Client <a href="<?php echo url('profile') ?>/<?php echo $client_id ?>"><strong><?php echo $client_name ?></strong></a> just posted AtW forms for the job <a href="<?php echo url('/jobs/view') ?>/<?php echo $job_id ?>"><strong><?php echo $job_name ?></strong></a>.</p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>