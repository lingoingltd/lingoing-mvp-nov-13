<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Payment for invoice <strong><?php echo $invoice->number ?></strong> is <strong>PENDING</strong> via GoCardless.</p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>