<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Client <a href="<?php echo url('profile') ?>/<?php echo $client_id ?>"><strong><?php echo $client_name ?></strong></a> responded:</p>
<p><a href="<?php echo url('profile') ?>/<?php echo $lsp_id ?>"><strong><?php echo $lsp_name ?></strong> did NOT attend a job <a href="<?php echo url('/jobs/view') ?>/<?php echo $job_id ?>"><strong><?php echo $job_name ?></strong></a>!</p>
<p>Client's message:</p>
<p><?php echo nl2br($text); ?></p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>