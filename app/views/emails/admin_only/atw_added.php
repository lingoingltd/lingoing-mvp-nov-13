<?php echo View::make('layouts.email_newsletter_header'); ?>

<p><?php echo $person_name ?> ADDED new Access to Work support package</p>
<p><a href="<?php echo URL::to('admin/users/atw-packages') ?>/<?php echo $person_id ?>">User's AtW packages</a></p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>