<?php echo View::make('layouts.email_newsletter_header'); ?>

<p><?php echo $person_name ?> just completed his/her profile.</p>
<p><a href="<?php echo URL::to('admin') ?>">Admin</a></p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>