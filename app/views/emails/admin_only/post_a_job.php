<?php echo View::make('layouts.email_newsletter_header'); ?>

<p>Hi <?php echo $admin_name ?>,</p>
<p><?php echo $person_name ?> just successfully posted a job to Lingoing.com.</p>
<p><a href="<?php echo URL::to('jobs/view') ?>/<?php echo $job_id ?>">Job “<?php echo $job_name ?>” details</a></p>
<p>Kind regards<br>Lingoing team</p>

<?php echo View::make('layouts.email_newsletter_footer'); ?>