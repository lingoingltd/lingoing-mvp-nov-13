$(document).ready(function(){
	$(".btn-agree").click(function(e){
		$.get('/agree-to-cookies', {}, function(response){
			if(response.status == 'OK')
				$(".cookie-notice").fadeOut();
		});
		
		e.preventDefault();
	});
	
	$("select").select2();
	
	$("#different_billing_address").change(function(){
		if($(this).is(':checked'))
			$(".disabledable").removeAttr('disabled');
		else
			$(".disabledable").attr('disabled', 'disabled');
	});
	
	$(".vat_registered").change(function(){
		if($(this).is(':checked'))
		{
			if($(this).val() == 'yes')
				$("#vat_number").removeAttr('disabled');
			else
				$("#vat_number").attr('disabled', 'disabled');
		}
	});
	
	var button = null;
	$("#insurance_documents").fileupload({
		dataType: 'json',
		url: '/profile_attachments',
		formData: {
			attachment_type: 'insurance'
		},
		start: function(e){
			$(button)
				.parent()
				.addClass('hidden')
				.next()
				.removeClass('hidden');
		},
		done: function(e, data){
			if(data.result.success == 'OK')
				$(button)
					.parent()
					.prev()
					.html(data.result.view);
				
			$(button)
				.parent()
				.removeClass('hidden')
				.next()
				.addClass('hidden');
				
			button = null;
		}
	});
	$(".upload-insurance-document").click(function(e){
		button = $(this);
		
		$("#insurance_documents").fileupload(
			'option',
			'url',
			'/profile_attachments/' + $(this).attr('data-id')
		).click();
		
		e.preventDefault();
	})
	$("#crb_documents").fileupload({
		dataType: 'json',
		url: '/profile_attachments',
		formData: {
			attachment_type: 'crb'
		},
		start: function(e){
			$(button)
				.parent()
				.addClass('hidden')
				.next()
				.removeClass('hidden');
		},
		done: function(e, data){
			if(data.result.success == 'OK')
				$(button)
					.parent()
					.prev()
					.html(data.result.view);
				
			$(button)
				.parent()
				.removeClass('hidden')
				.next()
				.addClass('hidden');
				
			button = null;
		}
	});
	$(".upload-crb-document").click(function(e){
		button = $(this);
		
		$("#crb_documents").click();
		
		e.preventDefault();
	});
	
	$("#profile-picture-upload").fileupload({
		dataType: 'json',
		url: '/set-profile-picture',
		done: function(e, data){
			if(data.result.success == 'OK')
			{
				$("img.profile-img").prop('src', data.result.url + "?" + new Date().getTime());
				$("#showProfilePicture").modal('show');
				$("#profile-type-select-area").prop('checked', true);
			}
			else
			{
				if(data.result.message != '')
					alert(data.result.message);
			}
		}
	});
	$("#showProfilePicture").on('shown.bs.modal', function(e){
		$(".set-profile-picture-thumbnail")
			.attr('data-position-x', 0)
			.attr('data-position-y', 0);
			
		$("img.profile-img").imgAreaSelect({
			x1: 0,
			y1: 0,
			x2: 150,
			y2: 150,
			show: true,
			resizable: false,
			persistent: true,
			onSelectEnd: function(img, selection) {
				$(".set-profile-picture-thumbnail")
					.attr('data-position-x', selection.x1)
					.attr('data-position-y', selection.y1);
			}
		});
	});
	$(".profile-type").change(function(){
		if($("#profile-type-select-area").is(':checked'))
		{
			$("img.profile-img").imgAreaSelect({
				show: true
			});
		}
		else
		{
			$("img.profile-img").imgAreaSelect({
				hide: true
			});
		}
	});
	$("#showProfilePicture").on('hide.bs.modal', function(e){
		$("img.profile-img").imgAreaSelect({
			hide: true
		});
	});
	$(".set-profile-picture-thumbnail").click(function(e){

		var data = null;
		if($("#profile-type-select-area").is(':checked'))
		{
			data = {
				position_x: $(this).attr('data-position-x'),
				position_y: $(this).attr('data-position-y'),
			};
		}
		else
		{
			data = {
				thumbnail: "yes"
			};
		}

		$.post($(this).attr('data-url'), data, function(response){
			if(response.success == 'OK')
			{
				$(".my-profile-picture img").prop('src', response.url + "?" + new Date().getTime());
				$("a.dropdown-toggle img").prop('src', response.url + "?" + new Date().getTime());
				
				$("a.delete-profile-picture").removeClass('hidden');
				
				$("#showProfilePicture").modal('hide');
			}
		});
		e.preventDefault();
	});
	
	$(".my-profile-picture img").click(function(e){
		$("#profile-picture-upload").click();
		e.preventDefault();
	});
	$("a.delete-profile-picture").click(function(e){
		$("#modalRemoveProfilePicture").modal('show');
		e.preventDefault();
	});
	$(".delete-profile-picture-confirmation").click(function(e){
		$("#modalRemoveProfilePicture").modal('hide');
		
		$.get($(this).attr('data-url'), {}, function(response){
			if(response.success == 'OK')
			{
				$(".my-profile-picture img").prop('src', response.url + "?" + new Date().getTime());
				$("a.dropdown-toggle img").prop('src', response.url + "?" + new Date().getTime());
				
				$("a.delete-profile-picture").addClass('hidden');
			}
		});
		e.preventDefault();
	});
	
	$(document.body).on('click', 'a.delete-attachment', function(e){
		var confirmation = $(this).attr('data-confirmation');
		if(confirmation != '')
		{
			if(!confirm(confirmation))
				return;
		}
		var type = $(this).attr('data-type');
		
		$.post('/' + $(this).attr('data-url'), {
			id: $(this).attr('data-id'),
			type: type
		}, function(response){
			if(response.success == 'OK')
				$("." + type + "_container").html(response.view);
		});
		
		e.preventDefault();
	});
	
	
	
	$(".delete-button").click(function(e){
		$(".job-title-dialog").html($(this).attr('data-title'));
		$(".cancel-the-job-confirmation").attr('data-id', $(this).attr('data-id'));
		
		$("#modalCancelJob").modal('show');
		
		e.preventDefault();
	});
	$(".cancel-the-job-confirmation").click(function(e){
		location.href = $(this).attr('data-url') + $(this).attr('data-id');
		
		e.preventDefault();
	});
	
	$(".project_budget").change(function(e){
		if($("#budget_type_fixed").is(':checked'))
		{
			$("#budget_type_fixed_container").removeClass('hidden');
			$("#budget_type_fixed_hourly_rate_container").addClass('hidden');
			$("#budget").focus();
		}
		if($("#budget_type_fixed_hourly_rate").is(':checked'))
		{
			$("#budget_type_fixed_container").addClass('hidden');
			$("#budget_type_fixed_hourly_rate_container").removeClass('hidden');
			$("#budget_hr").focus();
		}
		if($("#budget_type_open_to_quotes").is(':checked'))
		{
			$("#budget_type_fixed_container").addClass('hidden');
			$("#budget_type_fixed_hourly_rate_container").addClass('hidden');
		}
	});
	
	$("#other_arrangement").change(function(e){
		if($(this).is(':checked'))
		{
			$(".other_arrangement").removeClass('hidden');
			$("#other_arrangement_text").focus();
		}
		else
			$(".other_arrangement").addClass('hidden');
	});
	
	$(".no_of_professionals").change(function(e){
		var count = parseInt($(this).val());
		$(".requirements").each(function(index, value){
			if(parseInt($(value).attr('data-index')) <= count)
				$(value).removeClass('hidden');
			else
				$(value).addClass('hidden');
		});
	});
	$(".requirement").change(function(e){
		if($(this).is(':checked'))
			$(this).parent().parent().next().removeClass('hidden');
		else
			$(this).parent().parent().next().addClass('hidden');
	});
	$(".profile-languages").change(function(e){
		if($(this).is(':checked'))
		{
			$(this)
				.parent()
				.parent()
				.next().
				removeClass('hidden');
		}
		else
		{
			$(this)
				.parent()
				.parent()
				.next()
				.addClass('hidden');
		}
	});
	
	//find jobs
	$(".list-item-checkbox").change(function(){
		if($(this).is(':checked'))
			$(this).parent().next().removeClass('hidden');
		else
			$(this).parent().next().addClass('hidden');
	});
	
	//apply for the job
	$(".apply-for-the-job").click(function(e){
		$.post($(this).attr('data-url'), {
			budget: $("#budget").val()
		}, function(response){
			if(response.status == 'FAILED')
				document.location.href = response.redirect;
			else if(response.status == 'VALIDATION')
			{
				$('#applyForTheJob .input-group').addClass('has-error');
				$('#applyForTheJob .text-danger').html(response.error).removeClass('hidden');
			}
			else if(response.status == 'CLOSE')
			{
				$("#applyForTheJob").modal('hide');
				location.reload(true);
			}
			else
			{
				$("#applyForTheJob").modal('hide');
				$("#applyForTheJobSuccess").modal('show');
			}
		});
		
		e.preventDefault();
	});
	
	$(".button-ok").click(function(e){
		location.reload(true);
		e.preventDefault();
	});
	
	//send message
	$(".send-message").click(function(e){
		$.post($(this).attr('data-url'), {
			message: $("#message").val()
		}, function(response){
			if(response.status == 'FAILED')
				document.location.href = response.redirect;
			else if(response.status == 'VALIDATION')
			{
				$('#sendMessage textarea').addClass('has-error');
				$('#sendMessage .text-danger').html(response.error).removeClass('hidden');
			}
			else
			{
				$("#sendMessage").modal('hide');
				location.reload(true);
			}
		});
		
		e.preventDefault();
	});
	
	//hire lsp
	$(".hire-lsp").click(function(e){
		$(".lsp-name").html($(this).attr('data-name'));
		$(".confirm-lsp").attr('data-person-id', $(this).attr('data-person-id'));
		$('#modalHireLspConfirmation').modal('show');
		
		e.preventDefault();
	});
	$(".confirm-lsp").click(function(e){
		$.post($(this).attr('data-url'), {
			person_id: $(this).attr('data-person-id')
		}, function(response){
			if(response.status == 'FAILED')
				document.location.href = response.redirect;
			else
			{
				$("#modalHireLspConfirmation").modal('hide');
				location.reload(true);
			}
		});
		
		e.preventDefault();
	});
	
	//fire lsp
	$(".fire-lsp").click(function(e){
		$(".lsp-fire-name").html($(this).attr('data-name'));
		$(".confirm-fire-lsp").attr('data-position-id', $(this).attr('data-position-id'));
		$('#modalFireLspConfirmation').modal('show');
		
		e.preventDefault();
	});
	$(".confirm-fire-lsp").click(function(e){
		$(".password-fire-entry input").removeClass('error-form-control');
		$(".password-fire-entry div").html('').addClass('hidden');
		
		$.post($(this).attr('data-url'), {
			position_id: $(this).attr('data-position-id'),
			pass: $(".password-fire-entry input").val()
		}, function(response){
			if(response.status == 'VALIDATION')
			{
				$(".password-fire-entry input").addClass('error-form-control');
				$(".password-fire-entry div").html(response.validation).removeClass('hidden');
			}
			else if(response.status == 'FAILED')
				document.location.href = response.redirect;
			else
			{
				$("#modalHireLspConfirmation").modal('hide');
				location.reload(true);
			}
		});
	});
	
	//overtime request
	$(".request-overtime").click(function(e){
		$("#minutes-group")
			.removeClass('has-error')
			.children('.text-danger')
			.addClass('hidden')
			.html('');
			
		$("#hours-group")
			.removeClass('has-error')
			.children('.text-danger')
			.addClass('hidden')
			.html('');
			
		$.post($(this).attr('data-url'), {
			minutes: $("#minutes").val(),
			hours: $("#hours").val()
		}, function(response){
			if(response.status == 'FAILED')
				document.location.href = response.redirect;
			else if(response.status == 'VALIDATION')
			{
				$("#" + response.field + "-group")
					.addClass('has-error')
					.children('.text-danger')
					.removeClass('hidden')
					.html(response.error);
			}
			else
			{
				$("#requestOvertime").modal('hide');
				location.reload(true);
			}
		});
		
		e.preventDefault();
	});
	$(".reject-overtime").click(function(e){
		$.post($(this).attr('data-url'), {
			overtime_id: $(this).attr('data-overtime-id')
		}, function(response){
			if(response.status == 'FAILED')
				document.location.href = response.redirect;
			else
				location.reload(true);
		});
		
		e.preventDefault();
	});
	$(".approve-overtime").click(function(e){
		$.post($(this).attr('data-url'), {
			overtime_id: $(this).attr('data-overtime-id')
		}, function(response){
			if(response.status == 'FAILED')
				document.location.href = response.redirect;
			else
				location.reload(true);
		});
		
		e.preventDefault();
	});
	
	
	/* post a job - step 4 */
	$(".date-helper").datepicker({
		'dateFormat': 'dd/mm/yy'
	})
	$(".time-helper").timepicker();
	
	/* google maps */
	function googleMaps()
	{
		if($("#mapCanvas").length > 0)
		{
			var geocoder = new google.maps.Geocoder();
			var latLng = new google.maps.LatLng(51.5072, 0.1275);
			
			var mapCanvas = document.getElementById('mapCanvas');
			var mapOptions = {
				center: latLng,
				zoom: 11,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}
			var map = new google.maps.Map(mapCanvas, mapOptions);
			
			var address = $("#mapCanvas").attr('data-address');
			if(address != '')
			{
				geocoder.geocode({'address': address}, function(results, status) {
				    if (status == google.maps.GeocoderStatus.OK) {
				      map.setCenter(results[0].geometry.location);
				      var marker = new google.maps.Marker({
				          map: map,
				          position: results[0].geometry.location
				      });
				    } else {
				      console.log(status);
				    }
				});
			}
		}
	}
	google.maps.event.addDomListener(window, 'load', googleMaps);
	
	/* add-remove preffered-block list */
	$(".add-to-preferred-list").click(function(e){
		$.post('/add-to-preferred-list', {
			person_id: $(this).attr('data-id')
		}, function(response){
			
		});
	});
	$(".add-to-block-list").click(function(e){
		$.post('/add-to-block-list', {
			person_id: $(this).attr('data-id')
		}, function(response){
			
		});
	});
	
	$(".remove-from-preferred-list").click(function(e){
		$.post('/remove-from-preferred-list', {
			person_id: $(this).attr('data-id')
		}, function(response){
			
		});
	});
	$(".remove-from-block-list").click(function(e){
		$.post('/remove-from-block-list', {
			person_id: $(this).attr('data-id')
		}, function(response){
			
		});
	});
	
	//rating
	$(".rate-lsp").click(function(e){
		var name = $(this).attr('data-name');
		var data_position_id = $(this).attr('data-position-id');
		
		$(".lsp-name").html(name);
		$(".confirm-rate-lsp").attr('data-position-id', data_position_id);
		
		$("#modalRateLSP").modal('show');
	});
	$(".stars > .glyphicon-star").click(function(e){
		$(".glyphicon-star").removeClass('gold-star');
		
		var star = $(this).attr('data-star');
		for (var i=1; i <= star; i++){
			$("#star-" + i).addClass('gold-star');
		}
		$("#stars-value").val(star);
		
		e.preventDefault();
	});
	$(".confirm-rate-lsp").click(function(e){
		$.post($(this).attr('data-url'), {
			data_position_id: $(this).attr('data-position-id'),
			stars: $("#stars-value").val(),
			review: $("#review").val()
		}, function(response){
			if(response.success == 'OK')
				location.reload(true);
		});
		
		e.preventDefault();
	});
	
	$(".resign-button").click(function(e){
		$("#modalResign .text-danger").addClass('hidden');
		$("#resign-reason").removeClass('has-error');
		
		$.post($(this).attr('data-url'), {
			reason: $("#reason").val()
		}, function(response){
			if(response.success == 'OK')
			{
				$("#modalResign").modal('hide');
				location.reload(true);
			}
			else
			{
				$("#modalResign .text-danger").html(response.error).removeClass('hidden');
				$("#resign-reason").addClass('has-error');
			}
		});
		
		e.preventDefault();
	});
	$(".send-recommendation-button").click(function(e){
		$("#modalSendRecommendation .text-danger").addClass('hidden');
		$("#send-recommendation-email").removeClass('has-error');
		$("#send-recommendation-message").removeClass('has-error');
		
		$.post($(this).attr('data-url'), {
			email: $("#email").val(),
			message: $("#message").val()
		}, function(response){
			if(response.success == 'OK')
			{
				$("#modalSendRecommendation").modal('hide');
				location.reload(true);
			}
			else
			{
				$("#modalResign .text-danger").html(response.error).removeClass('hidden');
				$("#resign-reason").addClass('has-error');
			}
		});
		
		e.preventDefault();
	});
	
	$("#payment_method").change(function(e){
		if($(this).val() == "bacs")
		{
			$("#profile-paypal").addClass("hidden");
			$("#profile-bacs").removeClass("hidden");
		}
		else if($(this).val() == "paypal")
		{
			$("#profile-paypal").removeClass("hidden");
			$("#profile-bacs").addClass("hidden");
		}
		else
		{
			$("#profile-paypal").addClass("hidden");
			$("#profile-bacs").addClass("hidden");
		}
	});
	
	$('input[name="account_type"]').change(function(e){
		if($("#account_type_client").is(':checked'))
			$("#lsp_lingoing_address").addClass('hidden');
		else
			$("#lsp_lingoing_address").removeClass('hidden');
	});
	
	$("#type_of_job").change(function(){
		if($(this).val() == 'other')
			$("#type_other_container").removeClass('hidden');
		else
			$("#type_other_container").addClass('hidden');
	});
	
	$(".number_of_audience").change(function(){
		if($(this).val() == 'group')
			$(this).parent().parent().next().removeClass('hidden');
		else
			$(this).parent().parent().next().addClass('hidden');
	});
	$(".date_type").change(function(){
		if($(this).val() == 'one_day')
		{
			$(".weekly_bookings").addClass('hidden');
			$(".one_day_booking").removeClass('hidden');
			$(".btn-date-next").html('Next &gt;');
			$(".short-description").html('You\'ll see a preview of your job posting on the next page');
		}
		else
		{
			$(".one_day_booking").addClass('hidden');
			$(".weekly_bookings").removeClass('hidden');
			
			$(".btn-date-next").html('Review all dates &gt;');
			$(".short-description").html('You\'ll see all dates on the next page');
		}
	});

	$(document.body).on('click', 'a.add-another-date-button', function(e){
		$(".one_day_booking_item").each(function(index){
			if($(this).hasClass('hidden'))
			{
				$(this).removeClass('hidden');
				
				$.get('/jobs/partial-one-day-booking',{
					index: index + 1
				});
				return false;
			}
		});
		
		e.preventDefault();
	});
	
	$(document.body).on('click', 'a.remove-date', function(e){
		var button = $(this);
		$.post('/jobs/remove-booking-date', {
			id: $(this).attr('data-id')
		}, function(){
			button
				.parent()
				.parent()
				.next()
				.remove();
		
			button
				.parent()
				.parent()
				.remove();
		});
		
		e.preventDefault();
	});
	
	$(document.body).on('click', 'a.remove-one-day-booking', function(e){
		var button = $(this);
		$.post('/jobs/remove-one-day-booking', {
			index: $(this).attr('data-index')
		}, function(){
			button
				.parent()
				.parent()
				.remove();
		});
		
		e.preventDefault();
	});
	
	$(document.body).on('click', 'a.add-notes-attachments', function(e){
		var container = $(this)
							.parent()
							.parent()
							.next();
							
		if($(container).hasClass('hidden'))
		{
			$(container).removeClass('hidden');
			
			$(this).html('Hide notes or attachments');
		}
		else
		{
			$(container).addClass('hidden');
			
			$(this).html('Show notes or attachments');
		}
			
		e.preventDefault();
	});
	
	$(".upload-date-attachment").fileupload({
		dataType: 'json',
		url: $(this).attr('data-url'),
		start: function(e){
			$(this)
				.parent()
				.next()
				.removeClass('hidden');
		},
		done: function(e, data){
			if(data.result.success == 'OK')
			{
				$(this)
					.parent()
					.next()
					.addClass('hidden')
					.next()
					.html(data.result.html);
			}
			else
			{
				if(data.result.message != '')
					console.log(data.result.message);
			}
		}
	});
	$(document.body).on('click', 'a.remove-attachment', function(e){
		if(confirm('Do you really want to remove attachment “' + $(this).attr('data-name') + '”?'))
		{
			var button = $(this);

			$.post('/jobs/remove-attachment', {
				data_target: $(this).attr('data-target'),
				data_id: $(this).attr('data-id'),
				data_job_id: $(this).attr('data-job-id')
			}, function(response){
				if(response.success == 'OK')
					$(button)
						.parent()
						.remove();
			});
		}
	
		e.preventDefault();
	});
	
	$(".send-faq-question").click(function(e){
		var button = $(this);
		$.post('/send-faq-question', {
			email_address: $("#email_address").val(),
			your_question: $("#your_question").val()
		}, function(response){
			
			if(response.status == 'OK')
			{
				$(button)
					.parent()
					.hide()
					.next()
					.removeClass('hidden');
			}
			else
			{
				jQuery.each(response.errors, function(index, value){
					$("#" + value.field + "_container")
						.addClass('has-error')
						.children('.text-danger')
						.removeClass('hidden')
						.html(value.error[0]);
				});
			}
			
		});
		
		e.preventDefault();
	});
	
	$(".send-contact-us").click(function(e){
		var button = $(this);
		$.post('/send-contact-us-message', {
			name: $("#name").val(),
			email_address: $("#email_address").val(),
			message: $("#message").val()
		}, function(response){
			
			if(response.status == 'OK')
			{
				$(button)
					.parent()
					.hide()
					.next()
					.removeClass('hidden');
			}
			else
			{
				jQuery.each(response.errors, function(index, value){
					$("#" + value.field + "_container")
						.addClass('has-error')
						.children('.text-danger')
						.removeClass('hidden')
						.html(value.error[0]);
				});
			}
			
		});
		
		e.preventDefault();
	});
	
	$(".send-feedback").click(function(e){
		var button = $(this);
		$.post('/send-feedback', {
			name: $("#name").val(),
			email_address: $("#email_address").val(),
			feedback: $("#feedback").val()
		}, function(response){
			
			if(response.status == 'OK')
			{
				$(button)
					.parent()
					.hide()
					.next()
					.removeClass('hidden');
			}
			else
			{
				jQuery.each(response.errors, function(index, value){
					$("#" + value.field + "_container")
						.addClass('has-error')
						.children('.text-danger')
						.removeClass('hidden')
						.html(value.error[0]);
				});
			}
			
		});
		
		e.preventDefault();
	});
	
	$("#payment_method").change(function(){
		if($(this).val() == 'atw_customer')
			$(".atw_customer").removeClass('hidden');
		else
			$(".atw_customer").addClass('hidden');
	});
	
	$("#atw_support_type").change(function(){
		if($(this).val() == 'company')
			$(".company_data").removeClass('hidden');
		else
			$(".company_data").addClass('hidden');
	});
	
	$("#type_of_language_professional").click(function(){
		if($(this).val() == 'other')
			$(this).parent().next().removeClass('hidden');
		else
			$(this).parent().next().addClass('hidden');
	});
	
	$("#job_address_type_existing, #job_address_type_new").change(function(){
		if($(this).val() == 'existing')
		{
			$("#job_address_type_existing_container").removeClass('hidden');
			$("#job_address_type_new_container").addClass('hidden');
		}
		else
		{
			$("#job_address_type_existing_container").addClass('hidden');
			$("#job_address_type_new_container").removeClass('hidden');
		}
	});
	
	$(".atw_address_type").change(function(){
		if($(this).val() == 'existing')
		{
			$("#atw_address_existing_container").removeClass('hidden');
			$("#atw_address_new_container").addClass('hidden');
		}
		else
		{
			$("#atw_address_existing_container").addClass('hidden');
			$("#atw_address_new_container").removeClass('hidden');
		}
	});
	
	$(".unassign-lsp").click(function(e){
		if(confirm('Do you really want to unassign “' + $(this).attr('data-name') + '”?'))
		{
			$.post('/jobs/unassign-lsp', {
				id: $(this).attr('data-id')
			}, function(response){
				if(response.success == 'OK')
					location.reload(true);
			});			
		}
		
		e.preventDefault();
	});
	$(".assign_to").click(function(e){
		$.post('/jobs/assign-to', {
			id: $(this).attr('data-id'),
			worker_id: $(this).parent().parent().attr('data-worker-id'),
			job_id: $(this).parent().parent().attr('data-job-id')
		}, function(response){
			if(response.success == 'OK')
				location.reload(true);
		});
		
		e.preventDefault();
	});
	
	$(".atw-form-sent-user").click(function(e){
		var button = $(this);
		
		$.post('/jobs/atw-form-sent', {
			id: $(this).attr('data-id')
		}, function(response){
			if(response.success == 'OK')
			{
				$(button)
					.parent()
					.html(response.html);
					
				$(button)
					.hide();
			}
		});
		
		e.preventDefault();
	});
	
	$(".lsp-did-not-attend").click(function(e){
		$(".person-name-did-not-attend").html($(this).attr('data-name'));
		$(".send-did-not-attend").attr('data-position-id', $(this).attr('data-position-id'));
		$("#modalDidNotAttend").modal('show');
		
		e.preventDefault();
	});
	$(".send-did-not-attend").click(function(e){
		var button = $(this);
		
		if(!$(".did-not-respond-error").hasClass('hidden'))
			$(".did-not-respond-error").addClass('hidden');
			
		$(".dna-container").removeClass('has-error');
			
		$.post($(button).attr('data-url'), {
			position_id: $(button).attr('data-position-id'),
			message: $("#did-not-attend-message").val()
		}, function(response){
			if(response.success == 'OK')
			{
				$("#modalDidNotAttend").modal('hide');
				$("#modalDidNotAttendSuccess").modal('show');
			}
			else if(response.success == 'VALIDATOR')
			{
				$(".did-not-respond-error")
					.html(response.message)
					.removeClass('hidden');
					
				$(".dna-container").addClass('has-error');
			}
		});
	});
	$(".btn-close-dna").click(function(e){
		window.location.reload(true);
		e.preventDefault();
	});
});