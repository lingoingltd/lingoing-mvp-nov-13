$(document).ready(function(){
	$("select").select2();
	
	$("a.confirm-account").click(function(e){
		$(".user_name").html($(this).attr('data-person-name'));
		$("button.confirm-account").attr('data-person-id', $(this).attr('data-person-id'));
		$("#message").val('');
		
		$("#modalConfirmed").modal('show');
	
		e.preventDefault();
	});
	
	$("button.confirm-account").click(function(e){
		$("#modalConfirmed").modal('hide');
		
		$.post($(this).attr('data-url'), {
			account_id: $(this).attr('data-person-id'),
			message: $("#message").val()
		}, function(response){
			window.location.reload();
		});
		
		e.preventDefault();
	});
	
	
	$("a.send-message").click(function(e){
		$(".user_name").html($(this).attr('data-person-name'));
		$("button.send-message").attr('data-person-id', $(this).attr('data-person-id'));
		$("#message-text").val('');
		
		$("#modalMessage").modal('show');
	
		e.preventDefault();
	});
	$("button.send-message").click(function(e){
		$("#modalMessage").modal('hide');
		
		$.post($(this).attr('data-url'), {
			account_id: $(this).attr('data-person-id'),
			message: $("#message-text").val()
		});
		
		e.preventDefault();
	});
	
	$("a.mark-as-paid").click(function(e){
		if(!confirm("Do you really want to mark invoice " + $(this).attr("data-invoice-number") + " as Paid?"))
			return false;
			
		var $this = $(this);
		$.post('/admin/invoices/mark-as-paid', {
			invoice_id: $(this).attr('data-invoice-id'),
			field: $(this).attr('data-field')
		}, function(){
			$this.parent().prev().removeClass("active").removeClass("danger").removeClass("info").addClass("success").html('Paid');
			$this.hide();
		});
		
		e.preventDefault();
	});
	
	$("a.enable-sales-rep").click(function(e){
		var name = $(this).attr('data-person-name');
		var personId = $(this).attr('data-person-id');
		var nodeLink = $(this);
		
		if(confirm('Do you really want to set “' + name + '” as sales representative?'))
		{
			$.post('/admin/users/enable-sales-rep', {
				person_id: personId
			}, function(response){
				if(response.status == 'OK')
				{
					$(nodeLink)
						.parent()
						.prev()
						.html(response.html)
						.addClass('bg-success')
						.addClass('text-success');
						
					$(nodeLink).hide();
						
					alert('User successfully set as sales rep!');
				}
			});
		}
		
		e.preventDefault();
	});
	
	$("a.disable_enable_sales_rep").click(function(e){
		var name = $(this).attr('data-person-name');
		var personId = $(this).attr('data-person-id');
		var status = $(this).attr('data-status');
		var nodeLink = $(this);
		
		var message = '';
		if(status == 'N')
			message = 'Do you really want to disable “' + name + '” as sales rep?';
		else
			message = 'Do you really want to enable “' + name + '” as sales rep?';
			
		if(confirm(message))
		{
			$.post('/admin/users/change-sales-rep-status', {
				personId: personId,
				status: status
			}, function(response){
				if(response.status == 'OK')
				{
					if(status == 'Y')
					{
						$(nodeLink)
							.addClass('hidden')
							.prev()
							.removeClass('hidden')
							.parent()
							.prev()
							.html('Yes')
							.removeClass('bg-danger')
							.removeClass('text-danger')
							.addClass('bg-success')
							.addClass('text-success');
					}
					else
					{
						$(nodeLink)
							.addClass('hidden')
							.next()
							.removeClass('hidden')
							.parent()
							.prev()
							.html('No')
							.addClass('bg-danger')
							.addClass('text-danger')
							.removeClass('bg-success')
							.removeClass('text-success');
					}
				}
			});
		}
		
		e.preventDefault();
	});
	
	$(".unconfirm-job").click(function(e){
		if(!confirm('Do you really want to unconfirm job? Job will be hidden from main jobs list.'))
			return false;
			
		var button = $(this);
		
		$.post('/admin/jobs/change-confirmed', {
			id: $(this).attr('data-id'),
			status: '0'
		}, function(response){
			if(response.success == 'OK')
			{
				$(button)
					.parent()
					.prev()
					.removeClass('success')
					.removeClass('text-success')
					.html('Hidden');
					
				$(button)
					.addClass('hidden')
					.next()
					.removeClass('hidden');
			}
		});
	
		e.preventDefault();
	});
	$(".confirm-job").click(function(e){
		var button = $(this);
		
		$.post('/admin/jobs/change-confirmed', {
			id: $(this).attr('data-id'),
			status: '1'
		}, function(response){
			if(response.success == 'OK')
			{
				$(button)
					.parent()
					.prev()
					.addClass('success')
					.addClass('text-success')
					.html('Visible');
					
				$(button)
					.addClass('hidden')
					.prev()
					.removeClass('hidden');
			}
		});
	
		e.preventDefault();
		
		e.preventDefault();
	});
	
	$(".send-atw-form").click(function(e){
		var button = $(this);
		
		$.post('/admin/jobs/send-atw-form', {
			id: $(this).attr('data-id')
		}, function(response){
			if(response.success == 'OK')
			{
				$(button)
					.parent()
					.removeClass('danger')
					.removeClass('text-danger')
					.addClass('success')
					.addClass('text-success')
					.html(response.html);
			}
		});
		
		e.preventDefault();
	});
	
	$(".show-registered-users").click(function(e){
		var container = $(this).parent().parent().next();
		if($(container).hasClass('hidden'))
			$(container).removeClass('hidden');
		else
			$(container).addClass('hidden');
		
		e.preventDefault();
	});
});